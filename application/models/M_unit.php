<?php
class M_unit extends CI_Model
{
	public function get_all_unit2()
	{
		return $this->db->get('refunit')->result();
	}

	function get_all_unit3()
	{
		$hsl = $this->db->query("select * from refunit");
		return $hsl;
	}

	function get_all_unit()
	{
		return $this->db->get('refunit');
	}

	public function data()
	{
		$query = $this->db->query("SELECT KodeUnit as no_urut from refunit ORDER BY id DESC");
		$hasil = $query->row(0);
		return $hasil;
	}

	function simpan_unit($dataunit, $table)
	{
		$this->db->insert($table, $dataunit);
	}

	function update_edit_unit($where, $dataunit, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $dataunit);
	}

	public function hapus_unit($KodeUnit)
	{
		$this->db->where_in('KodeUnit', $KodeUnit);
		$this->db->delete('refunit');
	}

	function get_unit_byid($SOWKodeUnit)
	{
		$hsl = $this->db->query("select * from refunit where KodeUnit='$SOWKodeUnit'");
		return $hsl;
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refunit', $data);
	}

	public function get_where($where)
	{
		$this->db->select('*');
		$this->db->from('refunit');
		$this->db->where($where);
		return $this->db->get();
	}
}
