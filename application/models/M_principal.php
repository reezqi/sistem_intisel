<?php
class M_principal extends CI_Model
{

	function get_all_principal()
	{
		$this->db->order_by('kodePrincipal', 'ASC');
		return $this->db->get('refprincipal');
	}

	function simpan_principal($dataprincipal, $table)
	{
		$this->db->insert($table, $dataprincipal);
	}

	function update_edit_principal($where, $dataprincipal, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $dataprincipal);
	}

	public function hapus_principal($kodePrincipal)
	{
		$this->db->where_in('kodePrincipal', $kodePrincipal);
		$this->db->delete('refprincipal');
	}

	public function get_all_principal2()
	{
		return $this->db->get('refprincipal')->result();
	}

	function get_all_principal3()
	{
		$hsl = $this->db->query("select * from refprincipal");
		return $hsl;
	}

	function get_principal_by_kode($kodePrincipal)
	{
		$hsl = $this->db->query("SELECT * FROM refprincipal  where kodePrincipal='$kodePrincipal'");
		return $hsl;
	}

	function get_principal_byid($KodeCustomer)
	{
		$hsl = $this->db->query("select * from refprincipal where kodePrincipal='$KodeCustomer'");
		return $hsl;
	}

	function get_principal_byid1($SiteCustomerKode)
	{
		$hsl = $this->db->query("select * from refprincipal where kodePrincipal='$SiteCustomerKode'");
		return $hsl;
	}


	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refprincipal', $data);
	}

	public function get_select2($term)
	{
		if (!empty($term)) {
			$this->db->like('NamaPrincipal', $term);
		}
		return $this->db->get('refprincipal')->result();
	}
}
