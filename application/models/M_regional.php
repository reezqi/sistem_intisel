<?php
class M_regional extends CI_Model
{

	function get_all_regional()
	{
		return $this->db->get('refregional');
	}

	function ajax_get_all_regional()
	{
		$this->db->select('KODE, NAMA');
		return $this->db->get('refregional');
	}

	public function data()
	{
		$query = $this->db->query("SELECT max(KODE) as no_urut from refregional");
		$hasil = $query->row();
		return $hasil;
	}

	function simpan_regional($dataregional, $table)
	{
		$this->db->insert($table, $dataregional);
	}

	function update_edit_regional($where, $dataregional, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $dataregional);
	}

	public function hapus_regional($KODE)
	{
		$this->db->where_in('KODE', $KODE);
		$this->db->delete('refregional');
	}

	public function get_all_regional2()
	{
		return $this->db->get('refregional')->result();
	}

	public function get_where_regional2($array = NULL)
	{
		if (isset($array)) {
			$this->db->where($array);
		}
		return $this->db->get('refregional')->result();
	}

	function get_all_regional3()
	{
		$hsl = $this->db->query("select * from refregional");
		return $hsl;
	}

	function get_regional_byid($PdpRegionalKode)
	{
		$hsl = $this->db->query("select * from refregional where KODE='$PdpRegionalKode'");
		return $hsl;
	}

	public function hapus_regional2($KODEREG)
	{
		$this->db->where_in('KODE', $KODEREG);
		$this->db->delete('refregional');
	}

	function get_regional_by_kode($KODE)
	{
		$hsl = $this->db->query("SELECT * FROM refregional  where KODE='$KODE'");
		return $hsl;
	}

	function update_regional($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refregional', $data);
	}

	function get_provinsi()
	{
		$query = $this->db->get('refprovinsi');
		return $query;
	}

	function get_sub_provinsi($ProvinsiId)
	{
		$query = $this->db->get_where('refkabupaten', array('KabupatenProvinsiId' => $ProvinsiId));
		return $query;
	}

	function get_regional()
	{
		$this->db->select('KODE,NAMA,ProvinsiNama,KabupatenNama');
		$this->db->from('refregional');
		$this->db->join('refprovinsi', 'RegionalProvinsiId = ProvinsiId', 'left');
		$this->db->join('refkabupaten', 'RegionalKabupatenId = KabupatenId', 'left');
		$query = $this->db->get();
		return $query;
	}

	function get_regional_by_id($KODE)
	{
		$query = $this->db->get_where('refregional', array('KODE' =>  $KODE));
		return $query;
	}

	public function get_select2($term)
	{
		if (!empty($term)) {
			$this->db->like('NAMA', $term);
		}
		if ($this->session->userdata('is_admin') !== TRUE) {
			$this->db->where('KODE', $this->session->userdata('user_region'));
		}
		return $this->db->get('refregional')->result();
	}
}
