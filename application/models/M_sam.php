<?php
class M_sam extends CI_Model
{

	public function get_all_sam2()
	{
		return $this->db->get('mastersam')->result();
	}

	function get_sam_byid($samid)
	{
		$hsl = $this->db->query("select * from mastersam where KodeSAM='$samid'");
		return $hsl;
	}

	function get_all_sam3()
	{
		$hsl = $this->db->query("select * from mastersam");
		return $hsl;
	}

	function get_all_sam()
	{
		return $this->db->get('mastersam');
	}

	public function data()
	{
		$query = $this->db->query("SELECT KodeSAM as no_urut from mastersam ORDER BY KodeSAM DESC");
		$hasil = $query->row(0);
		return $hasil;
	}

	function simpan_sam($datasam, $table)
	{
		$this->db->insert($table, $datasam);
	}

	function update_edit_sam($where, $datasam, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datasam);
	}

	public function hapus_sam($KodeSAM)
	{
		$this->db->where_in('KodeSAM', $KodeSAM);
		$this->db->delete('mastersam');
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('mastersam', $data);
	}

	public function get_where($where)
	{
		$this->db->select('*');
		$this->db->from('mastersam');
		$this->db->where($where);
		return $this->db->get();
	}

	public function get_select2($term)
	{
		if (!empty($term)) {
			$this->db->like('NamaSAM', $term);
		}
		return $this->db->get('mastersam', 200)->result();
	}
}
