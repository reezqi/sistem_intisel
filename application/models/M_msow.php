<?php
class M_msow extends CI_Model
{

	function get_all_msow()
	{
		$this->db->order_by('No', 'DESC');
		return $this->db->get('refsow', 200);
	}

	function get_by_code($kode)
	{
		//grup berdasarkan KODE dan Regional

		$this->db->where('Code', $kode);
		$this->db->group_by('Code', 'SOWKodeRegional');
		return $this->db->get('refsow');
	}

	public function data()
	{
		$query = $this->db->query("SELECT max(Line) as no_urut from refsow");
		$hasil = $query->row();
		return $hasil;
	}

	function get_msow_by_kode($No)
	{
		$hsl = $this->db->query("SELECT * FROM refsow  where No='$No'");
		return $hsl;
	}

	function cari_msow_by_kode($No)
	{
		$hsl = $this->db->query("SELECT * FROM refsow  
									where 
									( Code like '%$No%' 
									or SOWDesc like '%$No%' )
									and ( SOWJenis like '%Internal%' ) GROUP BY Line");
		return $hsl;
	}

	function cari_msow_by_kode_region_customer($No, $Region, $Customer)
	{

		$data = $this->db->select('*')->from('refsow')
			->group_start()
			->like('Code', $No)
			->or_where('SOWDesc', $No)
			->group_end()
			->get();
		return $data;
	}

	function get_msow_by_line($No)
	{
		$hsl = $this->db->query("SELECT * FROM refsow  where refsow.Line='$No'");
		return $hsl;
	}

	function search_sow($KodeSOW)
	{
		$this->db->like('Code', $KodeSOW, 'both');
		$this->db->order_by('SOWDesc', 'ASC');
		$this->db->limit(10);
		return $this->db->get('refsow')->result();
	}

	public function cariPO()
	{
		$Customer = $this->input->GET('Customer', TRUE);
		$Regional = $this->input->GET('Regional', TRUE);
		$SOW = $this->input->GET('SOWDesc', TRUE);
		$Subcon = $this->input->GET('Subcon', TRUE);

		$data = $this->db->query("SELECT * from refsow 
								LEFT JOIN refregional on refsow.SOWKodeRegional=refregional.Kode 
								where SOWKodeCustomer like '%$Customer%' 
								and SOWKodeRegional like '%$Regional%' 
								and Code like '%$SOW%' 
								and SOWKodeSubcon like '%$Subcon%' LIMIT 200 ");

		return $data->result();
	}

	public function cariPO_()
	{
		$Customer = $this->input->GET('Customer', TRUE);
		$Regional = $this->input->GET('Regional', TRUE);
		$SOW = $this->input->GET('SOWDesc', TRUE);
		$Subcon = $this->input->GET('Subcon', TRUE);

		$data = $this->db->query("SELECT * from refsow 
								where SOWKodeCustomer like '%$Customer%' 
								and SOWKodeRegional like '%$Regional%' 
								and Code like '%$SOW%' 
								and SOWKodeSubcon like '%$Subcon%' GROUP BY Line LIMIT 200 ");

		return $data->result();
	}

	function get_msow_byid($PdpSOWKode)
	{
		$hsl = $this->db->query("select * from refsow where Code='$PdpSOWKode'");
		return $hsl;
	}

	function get_msow_byid1($SiteSOWKode)
	{
		$hsl = $this->db->query("select * from refsow where Code='$SiteSOWKode'");
		return $hsl;
	}

	function get_all_msow2()
	{
		$this->db->select('No, Code, SOWDesc');
		$this->db->from('refsow');
		$this->db->limit(100);
		$query = $this->db->get();
		return $query->result();
	}

	function get_msow_by_kode3($Code)
	{
		$hsl = $this->db->query("SELECT * FROM refsow where Code='$Code' ");
		return $hsl;
	}

	function get_msow_by_kode2($Code)
	{
		$hsl = $this->db->query("SELECT * FROM refsowdetail where Code='$Code'");
		return $hsl->result();
	}

	function get_all_msow3()
	{
		$hsl = $this->db->query("select * from refsow limit 200");
		return $hsl;
	}

	function simpan_msow($datamsow, $table)
	{
		$this->db->insert($table, $datamsow);
	}

	function update_edit_msow($where, $datamsow, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datamsow);
	}

	public function hapus_msow($No)
	{
		$this->db->where_in('No', $No);
		$this->db->delete('refsow');
	}

	public function hapus_line_msow($No)
	{
		$this->db->where_in('Line', $No);
		$this->db->delete('refsow');
	}

	function pencarian_d($KodeCust, $KodeRegional, $Code, $KodeSuppl)
	{
		$this->db->order_by('No', 'ASC');
		$this->db->where("SOWKodeCustomer", $KodeCust);
		$this->db->where("SOWKodeRegional", $KodeRegional);
		$this->db->where("Code", $Code);
		$this->db->where("SOWKodeSubcon", $KodeSuppl);
		return $this->db->get("refsow", 200);
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refsow', $data);
	}

	function cari_kode($keyword, $registered)
	{
		$not_in = '';

		$koma = explode(',', $registered);
		if (count($koma) > 1) {
			$not_in .= " AND `Code` NOT IN (";
			foreach ($koma as $k) {
				$not_in .= " '" . $k . "', ";
			}
			$not_in = rtrim(trim($not_in), ',');
			$not_in = $not_in . ")";
		}
		if (count($koma) == 1) {
			$not_in .= " AND `Code` != '" . $registered . "' ";
		}

		$sql = "
			SELECT 
			`Code`, `SOWDesc`, `HrgRegional`, `SOWJenis`, `SOWKodeRegional`, `SOWKodeCustomer`  
			FROM 
				`refsow` 
			WHERE 
					( `Code` LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
					OR `SOWDesc` LIKE '%" . $this->db->escape_like_str($keyword) . "%' )
					AND ( `SOWJenis` LIKE '%Internal%' )
				" . $not_in . "  LIMIT 10
		";

		return $this->db->query($sql);
	}

	function cari_kode_where($keyword, $registered, $where)
	{
		$not_in = '';

		$koma = explode(',', $registered);
		if (count($koma) > 1) {
			$not_in .= " AND `Code` NOT IN (";
			foreach ($koma as $k) {
				$not_in .= " '" . $k . "', ";
			}
			$not_in = rtrim(trim($not_in), ',');
			$not_in = $not_in . ")";
		}
		if (count($koma) == 1) {
			$not_in .= " AND `Code` != '" . $registered . "' ";
		}

		$sql = "
			SELECT 
				`Code`, `SOWDesc`, `HrgRegional`, `SOWJenis` 
			FROM 
				`refsow` 
			WHERE 
					( `Code` LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
					OR `SOWDesc` LIKE '%" . $this->db->escape_like_str($keyword) . "%' )
					AND ( `SOWKodeRegional` LIKE '%" . $where . "%' 
					AND `SOWJenis` LIKE '%Internal%' )
				" . $not_in . "  LIMIT 10
		
		";

		$this->db->select('Code, SOWDesc, HrgRegional');

		return $this->db->query($sql);
	}

	function cari_kode_eksternal($keyword, $registered)
	{
		$not_in = '';

		$koma = explode(',', $registered);
		if (count($koma) > 1) {
			$not_in .= " AND `Code` NOT IN (";
			foreach ($koma as $k) {
				$not_in .= " '" . $k . "', ";
			}
			$not_in = rtrim(trim($not_in), ',');
			$not_in = $not_in . ")";
		}
		if (count($koma) == 1) {
			$not_in .= " AND `Code` != '" . $registered . "' ";
		}

		$sql = "
			SELECT 
				`Code`, `SOWDesc`, `HrgRegional`, `SOWJenis`, `SOWKodeRegional`, `SOWKodeCustomer` 
			FROM 
				`refsow` 
			WHERE 
					( `Code` LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
					OR `SOWDesc` LIKE '%" . $this->db->escape_like_str($keyword) . "%' )
					AND ( `SOWJenis` LIKE '%External%' )
				" . $not_in . "  LIMIT 10
		";

		return $this->db->query($sql);
	}

	function cari_kode_eksternal_where($keyword, $registered, $where)
	{
		$not_in = '';

		$koma = explode(',', $registered);
		if (count($koma) > 1) {
			$not_in .= " AND `Code` NOT IN (";
			foreach ($koma as $k) {
				$not_in .= " '" . $k . "', ";
			}
			$not_in = rtrim(trim($not_in), ',');
			$not_in = $not_in . ")";
		}
		if (count($koma) == 1) {
			$not_in .= " AND `Code` != '" . $registered . "' ";
		}

		$sql = "
			SELECT 
				`Code`, `SOWDesc`, `HrgRegional`, `SOWJenis` 
			FROM 
				`refsow` 
			WHERE 
					( `Code` LIKE '%" . $this->db->escape_like_str($keyword) . "%' 
					OR `SOWDesc` LIKE '%" . $this->db->escape_like_str($keyword) . "%' )
					AND ( `SOWKodeRegional` LIKE '%" . $where . "%' 
					AND `SOWJenis` LIKE '%External%' )
				" . $not_in . "  LIMIT 10
		
		";

		return $this->db->query($sql);
	}

	function get_id($kode_barang)
	{
		return $this->db
			->select('Code, SOWDesc')
			->where('Code', $kode_barang)
			->limit(1)
			->get('refsow');
	}

	//data table server side
	var $table = 'refsow';
	var $column_order = array(null, 'Line');
	var $column_search = array('Code', 'SOWDesc');
	var $order = array('Line' => 'DESC');

	private function _get_datatables_query()
	{

		$this->db->from($this->table);

		$i = 0;

		foreach ($this->column_search as $item) // looping awal
		{
			if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
			{

				if ($i === 0) // looping awal
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['KodeRegional'])) {
			if (!empty($_POST['KodeRegional'])) {
				$this->db->group_start();
				$this->db->where('SOWKodeRegional', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer'])) {
			if (!empty($_POST['KodeCustomer'])) {
				$this->db->group_start();
				$this->db->where('SOWKodeCustomer', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}


		if (isset($_POST['KodeSOW'])) {
			if (!empty($_POST['KodeSOW'])) {
				$this->db->group_start();
				$this->db->where('Code', $_POST['KodeSOW']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSubcon'])) {
			if (!empty($_POST['KodeSubcon'])) {
				$this->db->group_start();
				$this->db->where('SOWKodeSubcon', $_POST['KodeSubcon']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['JenisSOW'])) {
			if (!empty($_POST['JenisSOW'])) {
				$this->db->group_start();
				$this->db->where('SOWJenis', $_POST['JenisSOW']);
				$this->db->group_end();
			}
		}


		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_line()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->group_by('Line');
		$query = $this->db->get();
		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
