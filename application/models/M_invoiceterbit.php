
<?php
class M_invoiceterbit extends CI_Model
{
	//data table server side
	var $table = 'trxinvoiceterbit';
    var $column_order = array(null, 'NoInvoice', 'TglInvoiceTerbit');
    var $column_search = array('NoPoInternal', 'NoInvoiceCustomer', 'TglIvoiceTerbit', 'KodeCustomer', 'KodeRegional', 'KodeSite');
	var $order = array('NoInvoice' => 'DESC');
	
	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
		}
		
		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['date_satu']) && isset($_POST['date_dua']))
		{
			if (!empty($_POST['date_satu']) && !empty($_POST['date_dua']))
			{
				$this->db->group_start();
					$this->db->where('TglInvoiceTerbit >=', $_POST['date_satu']);
					$this->db->where('TglInvoiceTerbit <=', $_POST['date_dua']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeRegional']))
		{
			if (!empty($_POST['KodeRegional']))
			{
				$this->db->group_start();
					$this->db->where('KodeRegional', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer']))
		{
			if (!empty($_POST['KodeCustomer']))
			{
				$this->db->group_start();
					$this->db->where('KodeCustomer', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSite']))
		{
			if (!empty($_POST['KodeSite']))
			{
				$this->db->group_start();
					$this->db->where('KodeSite', $_POST['KodeSite']);
				$this->db->group_end();
			}
		}

         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_invoice($table, $invo)
    {
        $this->db->where('NoPoCustomer', $table);
        $this->db->where('KodeSOW', $invo);
        $this->db->order_by('IdInvoiceTerbit', 'DESC');
        return $this->db->get('trxinvoiceterbit');
    }

    public function get_latest_invoice()
    {
        $this->db->order_by('NoInvoice', 'DESC');
        $this->db->limit(1);
        return $this->db->get('trxinvoiceterbit');
    }
}
