
<?php
class M_prregional extends CI_Model
{

	public function nourutpo()
	{
		$query = $this->db->query("SELECT NoPr as no_urut from trxprregionalh order by id DESC");
		$hasil = $query->row();
		return $hasil;
	}

	function get_prregional_by_kode($NoPr)
	{
		$hsl = $this->db->query("SELECT * FROM trxprregionalh where NoPr='$NoPr' ");
		return $hsl;
	}

	function get_prregional_by_kode2($NoPr)
	{
		$hsl = $this->db->query("SELECT * FROM trxprregionald where NoPr='$NoPr'");
		return $hsl->result();
	}

	public function tampilsemuadata()
	{
		$data = $this->db->query("SELECT * from trxpocush order by TglPo desc limit 100 ");
		return $data->result();
	}

	function simpan_prregional($datatrxpocush, $table)
	{
		$this->db->insert($table, $datatrxpocush);
	}

	function save_edit_prregional($where, $datatrxprregionalh, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datatrxprregionalh);
	}

	function save_edit_prregional_detail($where, $datatrxprregionalh, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datatrxprregionalh);
	}

	public function delete_prregional($NoPr)
	{
		$this->db->where_in('NoPr', $NoPr);
		$this->db->delete('trxprregionalh');
	}

	public function delete_prregional_detail($NoPr)
	{
		$this->db->where_in('NoPr', $NoPr);
		$this->db->delete('trxprregionald');
	}


	//baru
	function save_prregional($datatrxprregionalh, $table)
	{
		$this->db->insert($table, $datatrxprregionalh);
	}

	function save_prregional_detail($datatrxprregionald, $table)
	{
		$this->db->insert($table, $datatrxprregionald);
	}

	public function searching()
	{
		$KodeCustomer = $this->input->post('KodeCustomer', TRUE);
		$KodeSubcon = $this->input->post('KodeSubcon', TRUE);
		$KodeRegional = $this->input->post('KodeRegional', TRUE);
		$date_awal = $this->input->post('date_satu', TRUE);
		$date_akhir = $this->input->post('date_dua', TRUE);

		$data = $this->db->query("SELECT * from trxprregionalh where KodeSubcon like '%$KodeSubcon%' and KodeRegional like '%$KodeRegional%' and KodeCustomer like '%$KodeCustomer%' and TglPo between '$date_awal' and '$date_akhir'  LIMIT 200 ");
		return $data->result();
	}

	function get_prregional_by_kode3($NoPr)
	{
		$hsl = $this->db->query("SELECT * FROM trxprregionald where NoPr='$NoPr'");
		return $hsl->result();
	}

	public function get_all_prregional2()
	{
		return $this->db->get('trxprregionalh')->result();
	}
	public function get_where_prregional2($where)
	{
		$this->db->where($where);
		return $this->db->get('trxprregionalh')->result();
	}

	//data table server side
	var $table = 'trxprregionalh';
    var $column_order = array(null, 'TglPo');
    var $column_search = array('NoPr','TglPo', 'NamaCustomer', 'NamaRegional', 'NamaSite', 'KodeSite');
	var $order = array('id' => 'DESC');
	
	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
		}
		
		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['date_satu']) && isset($_POST['date_dua']))
		{
			if (!empty($_POST['date_satu']) && !empty($_POST['date_dua']))
			{
				$this->db->group_start();
					$this->db->where('TglPo >=', $_POST['date_satu']);
					$this->db->where('TglPo <=', $_POST['date_dua']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeRegional']))
		{
			if (!empty($_POST['KodeRegional']))
			{
				$this->db->group_start();
					$this->db->where('KodeRegional', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer']))
		{
			if (!empty($_POST['KodeCustomer']))
			{
				$this->db->group_start();
					$this->db->where('KodeCustomer', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}


		if (isset($_POST['KodeSubcon']))
		{
			if (!empty($_POST['KodeSubcon']))
			{
				$this->db->group_start();
					$this->db->where('KodeSubcon', $_POST['KodeSubcon']);
				$this->db->group_end();
			}
		}
		

        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
