<?php
class M_posubcon extends CI_Model
{
	public function nourutpo()
	{
		$query = $this->db->query("SELECT NoPo as no_urut from trxposeh order by id DESC");
		$hasil = $query->row();
		return $hasil;
	}

	public function nourutpoin()
	{
		$query = $this->db->query("SELECT NoPoIn as no_urut from trxposeh order by id DESC");
		$hasil = $query->row();
		return $hasil;
	}

	function save_posubcon($datatrxposubh, $table)
	{
		$this->db->insert($table, $datatrxposubh);
	}

	function save_posubcon_detail($datatrxposubd, $table)
	{
		$this->db->insert($table, $datatrxposubd);
	}

	function get_posubcon_by_kode($NoPo)
	{
		$hsl = $this->db->query("SELECT * FROM trxposeh where NoPo='$NoPo' ");
		return $hsl;
	}

	function get_pilih_site($NoPo)
	{
		$hsl = $this->db->query("SELECT * FROM trxposed where NoPo='$NoPo'");
		return $hsl->result();
	}

	function get_posubcon_by_kode2($NoPo)
	{
		$hsl = $this->db->query("SELECT * FROM trxposed where NoPo='$NoPo'");
		return $hsl->result();
	}

	public function delete_posubcon($NoPo)
	{
		$this->db->where_in('NoPo', $NoPo);
		$this->db->delete('trxposeh');
	}

	public function delete_posubcon_detail($NoPo)
	{
		$this->db->where_in('NoPo', $NoPo);
		$this->db->delete('trxposed');
	}

	public function delete($NoPo)
	{
		$this->db->where('NoPo', $NoPo);
		$this->db->delete('trxposed');
	}

	public function delete_posubcon_detail_multi($Lineno)
	{
		$this->db->where_in('Lineno', $Lineno);
		$this->db->delete('trxposed');
	}

	function save_edit_posubcon($where, $datatrxposeh, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datatrxposeh);
	}

	function save_edit_posubcon_detail($where, $datatrxposeh, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datatrxposeh);
	}

	public function searching()
	{
		$date_awal = $this->input->post('date_satu', TRUE);
		$date_akhir = $this->input->post('date_dua', TRUE);
		$KodeCustomer = $this->input->post('KodeCustomer', TRUE);
		$KodeRegional = $this->input->post('KodeRegional', TRUE);
		$KodeSubcon = $this->input->post('KodeSubcon', TRUE);
		$KodeSite = $this->input->post('KodeSite', TRUE);

		
		$data = $this->db->query("SELECT * from trxposeh 
								WHERE TipePO='3' and KodeRegional like '%$KodeRegional%'
								and KodeSubcon like '%$KodeSubcon%' 
								and KodeSite like '%$KodeSite%'
								and KodeCustomer like '%$KodeCustomer%'
								and TglPo between '$date_awal' and '$date_akhir' ORDER BY Id DESC LIMIT 200");

		return $data->result();
	}

	function get_posubcon_by_kode3($NoPo)
	{
		$hsl = $this->db->query("SELECT * FROM trxposed where NoPo='$NoPo'");
		return $hsl->result();
	}

	function get_po_customer()
	{
		$this->db->select('*');
		$this->db->from('trxpocush');
		$this->db->order_by('NoDokumen', 'ASC');
		return $this->db->get();
	}

	//data table server side
	var $table = 'trxposeh';
    var $column_order = array(null, 'TglPo');
    var $column_search = array('NoPo','PoDate', 'NamaCustomer', 'NamaRegional', 'NamaSite', 'KodeSite');
	var $order = array('TglPo' => 'DESC');
	
	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
		}
		
		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['date_satu']) && isset($_POST['date_dua']))
		{
			if (!empty($_POST['date_satu']) && !empty($_POST['date_dua']))
			{
				$this->db->group_start();
					$this->db->where('TglPo >=', $_POST['date_satu']);
					$this->db->where('TglPo <=', $_POST['date_dua']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeRegional']))
		{
			if (!empty($_POST['KodeRegional']))
			{
				$this->db->group_start();
					$this->db->where('KodeRegional', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer']))
		{
			if (!empty($_POST['KodeCustomer']))
			{
				$this->db->group_start();
					$this->db->where('KodeCustomer', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSite']))
		{
			if (!empty($_POST['KodeSite']))
			{
				$this->db->group_start();
					$this->db->where('KodeSite', $_POST['KodeSite']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSubcon']))
		{
			if (!empty($_POST['KodeSubcon']))
			{
				$this->db->group_start();
					$this->db->where('KodeSubcon', $_POST['KodeSubcon']);
				$this->db->group_end();
			}
		}

		$this->db->where('TipePO', 3);
		

        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
