<?php
class M_role extends CI_Model
{
    private $_table     = "refrole";
    private $_order     = "DESC";
    private $_order_by  = "id";

    public $Role;
    public $rMenu;
    public $rName;
    public $sStatus;

    public function get_roles_menus($rMenu = NULL)
    {
        if ($rMenu === NULL)
        {
            return NULL;
        }
        //convert menu string to array
        $rMenu = str_split($rMenu);

        //get menu
        for ($i=0; $i<sizeof($rMenu); $i++)
        {
            $rMenu[$i] = $this->db->get_where("refmenu", ['id' => $rMenu[$i]])->result();
        }
        return $rMenu;
    }

    public function get_menus()
    {        
        $reponse    = $this->db->get("refmenu");
        return $reponse;
    }

    public function get_where_menus($id)
    {        
        $this->db->where('Id', $id);
        $reponse    = $this->db->get("refmenu");
        return $reponse;
    }

    public function add_menu_to_role($role_id = NULL, $menu_id = NULL)
    {
        if (isset($role_id) && isset($menu_id))
        {
            //insert new role
            $this->db->insert('refmenuakses', ['role_id' => $role_id, 'menu_id' => $menu_id]);
        }
    }

    public function get_roles()
    {
        // set the order
		if (isset($this->_order_by) && isset($this->_order))
		{
			$this->db->order_by($this->_order_by, $this->_order);
        }
        
        $reponse    = $this->db->get($this->_table);
        return $reponse;
    }

    public function get_where_roles($array = NULL)
    {
        // set the order
		if (isset($this->_order_by) && isset($this->_order))
		{
			$this->db->order_by($this->_order_by, $this->_order);
        }

        if (isset($array))
        {
            $this->db->where($array);
        }
        
        $reponse    = $this->db->get($this->_table);
        return $reponse;
    }

    public function create_role()
    {
        $post = $this->input->post();

        $this->Role = $post['role'];
        $this->rName = $post['rname'];

        //if exsiting role
        $existing_role = $this->db->get_where($this->_table, ['Role' => $this->Role])->num_rows();
        if($existing_role !== 0)
        {
            return FALSE;
        }
        else
        {
            $this->db->insert($this->_table, $this);
            return $this->db->insert_id();
        }
    }

    public function update_role($id = NULL, $rMenu = NULL)
    {
        $post = $this->input->post();

        $this->Role     = $post['role'];
        $this->rName    = $post['rname'];
        $this->rMenu    = $rMenu;

        //if exsiting role
        $existing_role = $this->db->get_where($this->_table, ['Role' => $this->Role, 'id != ' =>  $id])->num_rows();
        if($existing_role !== 0)
        {
            return FALSE;
        }
        else
        {
            $this->db->where('id', $id);
            $this->db->update($this->_table, $this);
            return TRUE;
        }
    }

    public function delete_role($id = NULL)
    {
        if (isset($id))
        {
            $this->db->where('id', $id);
            $this->db->delete($this->_table);
        }
    }

    public function delete_menu_role($role_id = NULL)
    {
        if (isset($role_id))
        {
            $this->db->where('role_id', $role_id);
            $this->db->delete('refmenuakses');
        }
    }

    public function get_menus_submenus($menu_id = NULL)
    {
         // set the order
		if (isset($this->_order_by) && isset($this->_order))
		{
			$this->db->order_by($this->_order_by, $this->_order);
        }
        
        $reponse    = $this->db->get_where("refmenusub", ['menu_id' => $menu_id]);
        return $reponse;
    }

    public function get_submenus()
    {
         // set the order
		if (isset($this->_order_by) && isset($this->_order))
		{
			$this->db->order_by($this->_order_by, $this->_order);
        }
        
        $reponse    = $this->db->get("refmenusub");
        return $reponse;
    }
} 