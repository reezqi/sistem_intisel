<?php
class M_jenispekerjaan extends CI_Model
{

	function get_all_jenispekerjaan()
	{
		return $this->db->get('refjenispekerjaan');
	}

	function get_all_jenispekerjaan2()
	{
		return $this->db->get('refjenispekerjaan', 200)->result();
	}


	function simpan_jenispekerjaan($datajenispekerjaan, $table)
	{
		$this->db->insert($table, $datajenispekerjaan);
	}

	function update_edit_jenispekerjaan($where, $datajenispekerjaan, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datajenispekerjaan);
	}

	public function hapus_jenispekerjaan($id_jenis_pekerjaan)
	{
		$this->db->where_in('id_jenis_pekerjaan', $id_jenis_pekerjaan);
		$this->db->delete('refjenispekerjaan');
	}

	function get_jenispekerjaan_by_kode($id_jenis_pekerjaan)
	{
		$hsl = $this->db->query("SELECT * FROM refjenispekerjaan  where id_jenis_pekerjaan='$id_jenis_pekerjaan'");
		return $hsl;
	}

	function get_jenispekerjaan_byid($KodePekerjaan)
	{
		$hsl = $this->db->query("select * from refjenispekerjaan where id_jenis_pekerjaan='$KodePekerjaan'");
		return $hsl;
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refjenispekerjaan', $data);
	}

	function cari_pekerjaan_by_kode_customer($No, $Customer)
	{
		$data = $this->db->select('*')->from('refjenispekerjaan')
						->group_start()
							->like('JenisPekerjaan', $No)
						->group_end()
					->get();
		return $data;
	}
}
