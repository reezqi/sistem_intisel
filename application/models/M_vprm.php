<?php
class M_vprm extends CI_Model
{

 	public function get_all_vprm2()
	{
		return $this->db->get('refowner')->result();
	}

	function get_vprm_byid($vprmid)
	{
		$hsl = $this->db->query("select * from refowner where KodeOwner='$vprmid'");
		return $hsl;
	}

	function get_all_vprm3()
	{
		$hsl = $this->db->query("select * from refowner");
		return $hsl;
	} 

	function get_all_vprm()
	{
		return $this->db->get('refowner');
	}

	public function data()
	{
		$query = $this->db->query("SELECT KodeOwner as no_urut from refowner ORDER BY KodeOwner DESC");
		$hasil = $query->row(0);
		return $hasil;
	}

	function simpan_vprm($datavprm, $table)
	{
		$this->db->insert($table, $datavprm);
	}

	function update_edit_vprm($where, $datavprm, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datavprm);
	}

	public function hapus_vprm($KodeOwner)
	{
		$this->db->where_in('KodeOwner', $KodeOwner);
		$this->db->delete('refowner');
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refowner', $data);
	}

	public function get_where($where)
	{
		$this->db->select('*');
		$this->db->from('refowner');
		$this->db->where($where);
		return $this->db->get();
	}

}
