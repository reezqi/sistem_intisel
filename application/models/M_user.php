<?php
class M_user extends CI_Model
{
    private $_table     = "refuser";
    private $_fk_table  = "refregional";

    public $Id;
    public $Name;
    public $Password;
    public $rule_id;
    public $UserHirar;
    public $UserSAM;
    public $UserLevel;
    public $UserRegion;
    public $UserNational;
    public $PrintPengajuan;
    public $OldPwdDate;
    public $NewPwdDate;
    public $AllDept;
    public $IsAktive;
    public $Image;

    public function get_user()
    {
        $this->db->select($this->_table.'.*, refregional.NAMA AS namaregional');
        $this->db->join('refregional', 'refuser.UserRegion = refregional.kode');
        $this->db->order_by('refuser.Id', 'DESC');
        $this->db->from($this->_table);
        return $this->db->get();
    }

    public function create_user()
    {
        $post       = $this->input->post();

        $this->Name             = $post['name'];
        $this->Password         = $post['password'];
        $this->rule_id          = $post['role'];
        $this->UserRegion       = $post['regional'];
        $this->OldPwdDate       = time();
        $this->NewPwdDate       = time();
        $this->IsAktive          = 1;

        $this->db->insert($this->_table, $this);
        return $this->db->insert_id();
    }

    public function delete_user()
    {
        
    }
}