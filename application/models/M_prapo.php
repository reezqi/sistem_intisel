
<?php
class M_prapo extends CI_Model
{
	public function searching()
	{
		$KodeCustomer = $this->input->GET('KodeCustomer', TRUE);
		$KodeSubcon = $this->input->GET('KodeSubcon', TRUE);
		$KodeRegional = $this->input->GET('KodeRegional', TRUE);

		$data = $this->db->query("SELECT a.NoPr AS nopr, a.TglPo AS tglpr, a.KodeSubcon, a.NamaSubcon, a.KodeCustomer, a.NamaCustomer, a.KodeRegional, a.NamaRegional, a.KodeSite, a.NamaSite, GROUP_CONCAT(a.KodeSOW ORDER BY a.KodeSOW DESC SEPARATOR ',  ') AS kodesow,  GROUP_CONCAT(a.NamaSOW SEPARATOR ', ') AS namasow, GROUP_CONCAT(b.HrgRegional SEPARATOR ',  ') AS hrgregional, GROUP_CONCAT(a.Qty SEPARATOR ',  ') AS qty, SUM(b.HrgRegional) AS totalpo
		FROM trxprregionald a LEFT JOIN refsow b
		ON (a.KodeSOW=b.Code) AND (a.KodeRegional=b.SOWKodeRegional)
		WHERE KodeRegional like '%$KodeRegional%' and KodeSubcon like '%$KodeSubcon%' and KodeCustomer like '%$KodeCustomer%'
		GROUP BY nopr");
		return $data->result();
	}

	function save_prapo($datatrxprapo, $table)
	{
		$this->db->insert($table, $datatrxprapo);
	}
}
