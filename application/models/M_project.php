<?php
class M_project extends CI_Model{

	function get_all_project(){
		return $this->db->get('refproject');
	}

	function get_all_project3(){
		$hsl=$this->db->query("select * from refproject");
		return $hsl;
	}

	function simpan_project($dataproject,$table){
		$this->db->insert($table,$dataproject);
	}

	function update_project($where,$table){		
		return $this->db->get_where($table,$where);
	}

	public function hapus_project($KodeProject){
		$this->db->where_in('KodeProject', $KodeProject);
		$this->db->delete('refproject');
	}

	function get_project_byid($KodeProyek){
		$hsl=$this->db->query("select * from refproject where KodeProject='$KodeProyek'");
		return $hsl;
	}

	function update_edit_project($where,$dataproject,$table){		
		$this->db->where($where);
		$this->db->update($table,$dataproject);
	}

	public function get_all_project2(){
		return $this->db->get('refproject')->result();
	}

	function get_project_by_kode($KodeProject){
		$hsl=$this->db->query("SELECT * FROM refproject  where KodeProject='$KodeProject'");
		return $hsl;
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload
		
		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;
	  
		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
		  // Jika berhasil :
		  $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
		  return $return;
		}else{
		  // Jika gagal :
		  $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
		  return $return;
		}
	  }
	  
	  // Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	  public function insert_multiple($data){
		$this->db->insert_batch('refproject', $data);
	  }


}