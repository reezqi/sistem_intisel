<?php
class M_subcon extends CI_Model
{

	function get_all_subcon()
	{
		$this->db->order_by('KodeSuppl', 'ASC');
		return $this->db->get('refsubcon');
	}

	public function data()
	{
		$query = $this->db->query("SELECT max(KodeSuppl) as no_urut from refsubcon");
		$hasil = $query->row();
		return $hasil;
	}

	function get_bank()
	{
		$query = $this->db->get('refbank');
		return $query;
	}

	function simpan_subcon($datasubcon, $table)
	{
		$this->db->insert($table, $datasubcon);
	}

	function update_edit_subcon($where, $datasubcon, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datasubcon);
	}

	public function hapus_subcon($KodeSuppl)
	{
		$this->db->where_in('KodeSuppl', $KodeSuppl);
		$this->db->delete('refsubcon');
	}

	public function get_all_subcon2()
	{
		$this->db->select('*');
		$this->db->from('refsubcon');
		$query = $this->db->get();
		return $query->result();
	}

	function get_all_subcon3()
	{
		$hsl = $this->db->query("select * from refsubcon");
		return $hsl;
	}

	function get_subcon_by_kode($KodeSuppl)
	{
		$hsl = $this->db->query("SELECT * FROM refsubcon where KodeSuppl='$KodeSuppl'");
		return $hsl;
	}

	function get_subcon_byid($KodeSubcon)
	{
		$hsl = $this->db->query("SELECT * FROM refsubcon where KodeSuppl='$KodeSubcon'");
		return $hsl;
	}

	function get_subcon_by_id($KodeRegional)
	{
		$query = $this->db->get_where('refsubcon', array('KodeRegional' =>  $KodeRegional));
		return $query;
	}


	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refsubcon', $data);
	}

	function get_sub_subcon($KodeRegional)
	{
		$query = $this->db->get_where('refbank', array('KabupatenProvinsiId' => $KodeRegional));
		return $query;
	}
}
