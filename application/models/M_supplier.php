<?php
class M_supplier extends CI_Model{

	function get_all_supplier(){
		return $this->db->get('refsupplier');	
	}

	function simpan_supplier($datasupplier,$table){
		$this->db->insert($table,$datasupplier);
	}

	function update_supplier($where,$table){		
		return $this->db->get_where($table,$where);
	}

	function update_edit_supplier($where,$datasupplier,$table){		
		$this->db->where($where);
		$this->db->update($table,$datasupplier);
	}

}