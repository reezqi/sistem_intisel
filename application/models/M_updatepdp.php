<?php
class M_updatepdp extends CI_Model
{

	function get_all_updatepdp()
	{
		return $this->db->get('refmatrikpdp');
	}

	function get_updatepdp_by_kode($NoPo, $RegionCode, $SiteID, $SOW)
	{
		$hsl = $this->db->query("SELECT * FROM trxposeh join refregional on refregional.KODE=trxposeh.KodeRegional
		join refproject on refproject.KodeProject=trxposeh.KodeProyek
		join refprincipal on refprincipal.kodePrincipal=trxposeh.KodeCustomer
		join trxposed on trxposed.NoPo=trxposeh.NoPo
		join refsite on refsite.codeSite=trxposed.SiteID  where  RegionCode='$RegionCode' and SiteID='$SiteID' and SOW='$SOW'");
		return $hsl;
	}


	function get_updatepdp_by_kode2($NoPo)
	{
		$hsl = $this->db->query("SELECT JobCode, DocumentName, Progress FROM trxpdp  where  NoPo='$NoPo' ");
		return $hsl;
	}

	function get_updatepdp_by_kode1($NoPo)
	{
		$hsl = $this->db->query("SELECT * FROM trxposeh a left join trxposed b on b.NoPo=a.NoPo where  a.NoPo='$NoPo' ");
		return $hsl;
	}

	function simpan_detail()
	{
		$pekerjaan  = $this->input->post('item_pekerjaan', TRUE);
		$dokumen = $this->input->post('item_dokumen', TRUE);
		$presentasi  = $this->input->post('item_persentasi', TRUE);

		$statement = $this->db->prepare("INSERT INTO trxpdp (JobCode, DocumentName, Progress) VALUES ( ?, ?, ?)");
		for ($count = 0, $c = count($pekerjaan); $count < $c; $count++) {

			$statement->bind_param(
				'sss',
				$pekerjaan[$count],
				$dokumen[$count],
				$presentasi[$count]
			);
			return $statement->execute();
		}
	}

	function edit_data($where, $table)
	{
		return $this->db->get_where($table, $where);
	}

	public function cariPO()
	{
		$Regional = $this->input->GET('Regional', TRUE);
		$Site = $this->input->GET('Site', TRUE);
		$SOW = $this->input->GET('SOW', TRUE);
		$tanggal1 = $this->input->GET('tanggal1', TRUE);
		$tanggal2 = $this->input->GET('tanggal2', TRUE);

		$data = $this->db->query("SELECT a.NoPo, a.TglPo, a.KodeRegional, a.NamaRegional, a.KodeCustomer, a.NamaCustomer, 
										a.KodeSubcon, a.NamaSubcon, d.KodeSite, d.NamaSite, d.KodeSOW 
										from trxposeh a left join refregional b on b.KODE=a.KodeRegional
														left join trxposed d on d.NoPo=a.NoPo
														left join refsite e on e.codeSite=d.KodeSite
														where a.KodeRegional like '%$Regional%' and d.KodeSite like '%$Site%' and d.KodeSOW like '%$SOW%' limit 50");
		return $data->result();
	}

	public function cariPO_()
	{
		$regional 	= $this->input->post('regional', TRUE);
		$site 		= $this->input->post('site', TRUE);
		$sow 		= $this->input->post('SOW', TRUE);
		$tanggal1 	= $this->input->post('tanggal1', TRUE);
		$tanggal2 	= $this->input->post('tanggal2', TRUE);
		$customer 	= $this->input->post('kodecustomer', TRUE);

		$data = $this->db->query("SELECT a.NoPo, a.TglPo, a.KodeRegional, a.NamaRegional, a.KodeCustomer, a.NamaCustomer, 
										a.KodeSubcon, a.NamaSubcon, a.KodeSite, a.NamaSite
										from trxposeh a where a.KodeRegional like '%$regional%' and a.KodeSite like '%$site%' and a.KodeCustomer like '%$customer%' and TglPo between '$tanggal1' and '$tanggal2' limit 50");

		return $data->result();
	}

	function get_sub_pekerjaan($PekerjaanId)
	{
		$query = $this->db->get_where('refmatrikpdp', array('docCategory' => $PekerjaanId));
		return $query;
	}

	function get_matrik_sow($array)
	{
		$this->db->select('*');
		$this->db->from('trxposed');
		if (isset($array)) {
			$this->db->where($array);
		}
		return $this->db->get();
	}

	function get_matrik_pdp($array)
	{
		$this->db->select('idDocCat, PdpRegionalKode, docCategory, descDocCategory, bobot');
		$this->db->from('refmatrikpdp');
		if (isset($array)) {
			$this->db->where($array);
		}
		$this->db->order_by('bobot', 'ASC');
		$this->db->limit(1);
		return $this->db->get();
	}

	function insert_matrik_pdp($data)
	{
		$this->db->insert('trxpdp', $data);
	}

	function get_matrik_pdp_detail($array)
	{
		$this->db->select('*');
		$this->db->from('trxpdp');
		if (isset($array)) {
			$this->db->where($array);
		}
		return $this->db->get();
	}

	function get_matrik_pdp_detail_persentase($array)
	{
		$this->db->select('*');
		$this->db->from('trxpdp');
		if (isset($array)) {
			$this->db->where($array);
		}
		$this->db->order_by('Progress', 'DESC');
		$this->db->limit(1);
		return $this->db->get();
	}

	function delete_by_lineno($lineno)
	{
		$this->db->where('Lineno', $lineno);
		$this->db->delete('trxpdp');
	}


	function simpan_invoice_by_lineno($data)
	{
		$this->db->insert('trxinvoiceterbit', $data);
	}

	//data table server side
	var $table = 'trxposeh';
	var $column_order = array(null, 'TglPo');
	var $column_search = array('NoPo', 'PoDate', 'NamaCustomer', 'NamaRegional', 'NamaSite', 'KodeSite', 'TglPo');
	var $order = array('TglPo' => 'DESC');

	private function _get_datatables_query()
	{
		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column_search as $item) // looping awal
		{
			if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
			{

				if ($i === 0) // looping awal
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['date_satu']) && isset($_POST['date_dua'])) {
			if (!empty($_POST['date_satu']) && !empty($_POST['date_dua'])) {
				$this->db->group_start();
				$this->db->where('TglPo >=', $_POST['date_satu']);
				$this->db->where('TglPo <=', $_POST['date_dua']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeRegional'])) {
			if (!empty($_POST['KodeRegional'])) {
				$this->db->group_start();
				$this->db->where('KodeRegional', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer'])) {
			if (!empty($_POST['KodeCustomer'])) {
				$this->db->group_start();
				$this->db->where('KodeCustomer', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSite'])) {
			if (!empty($_POST['KodeSite'])) {
				$this->db->group_start();
				$this->db->where('KodeSite', $_POST['KodeSite']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1) 
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
