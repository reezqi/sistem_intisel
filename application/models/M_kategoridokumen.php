<?php
class M_kategoridokumen extends CI_Model
{

	function get_all_kategoridokumen()
	{
		return $this->db->get('refmatrikpdp', 100);
	}

	public function cariPO()
	{
		$Customer = $this->input->GET('Customer', TRUE);
		$Regional = $this->input->GET('Regional', TRUE);
		$SOW = $this->input->GET('SOW', TRUE);
		$Site = $this->input->GET('Site', TRUE);

		$data = $this->db->query("SELECT * from refmatrikpdp where PdpCustomerKode like '%$Customer%'
		and PdpRegionalKode like '%$Regional%'
		and PdpSOWKode like '%$SOW%'
		and PdpSiteKode like '%$Site%' GROUP BY line LIMIT 200");
		return $data->result();
	}

	public function cariPO_()
	{
		$Customer = $this->input->GET('Customer', TRUE);
		$Regional = $this->input->GET('Regional', TRUE);
		$SOW = $this->input->GET('SOW', TRUE);
		$Site = $this->input->GET('Site', TRUE);

		$data = $this->db->query("SELECT * from refmatrikpdp where PdpCustomerKode like '%$Customer%'
		and PdpRegionalKode like '%$Regional%'
		and PdpSOWKode like '%$SOW%'
		and PdpSiteKode like '%$Site%'  LIMIT 200");
		return $data->result();
	}

	function get_kategoridokumen_by_kode($idDocCat)
	{
		$hsl = $this->db->query("SELECT * FROM refmatrikpdp  where idDocCat='$idDocCat'");
		return $hsl;
	}

	function get_kategoridokumen_by_line($line)
	{
		$hsl = $this->db->query("SELECT * FROM refmatrikpdp  where line='$line'");
		return $hsl;
	}

	public function get_all_kategoridokumen2()
	{
		return $this->db->get('refmatrikpdp', 50)->result();
	}

	function simpan_kategoridokumen($datakategoridokumen, $table)
	{
		$this->db->insert($table, $datakategoridokumen);
	}

	public function carikategoridokumen()
	{
		$principalCode = $this->input->GET('principalCode', TRUE);
		$data = $this->db->query("SELECT * from refmatrikpdp where principalCode like '%$principalCode%' ORDER BY idDocCat DESC limit 100");
		return $data->result();
	}

	function update_edit_kategoridokumen($where, $datakategoridokumen, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datakategoridokumen);
	}


	public function hapus_kategoridokumen($idDocCat)
	{
		$this->db->where_in('idDocCat', $idDocCat);
		$this->db->delete('refmatrikpdp');
	}

	public function hapus_by_line($idDocCat)
	{
		$this->db->where_in('line', $idDocCat);
		$this->db->delete('refmatrikpdp');
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refmatrikpdp', $data);
	}

	public function get_latest_line()
	{
		$this->db->select('line');
		$this->db->from('refmatrikpdp');
		$this->db->order_by('idDocCat', 'DESC');
		return $this->db->get();
	}

	//data table server side
	var $table = 'refmatrikpdp';
    var $column_order = array(null, 'PdpSOWKode','PdpSOWNama');
    var $column_search = array('PdpSOWKode','PdpSOWNama');
	var $order = array('idDocCat' => 'DESC');
	
	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
		}
		
		if (isset($_POST['KodeRegional']))
		{
			if (!empty($_POST['KodeRegional']))
			{
				$this->db->group_start();
					$this->db->where('PdpRegionalKode', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer']))
		{
			if (!empty($_POST['KodeCustomer']))
			{
				$this->db->group_start();
					$this->db->where('PdpCustomerKode', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSite']))
		{
			if (!empty($_POST['KodeSite']))
			{
				$this->db->group_start();
					$this->db->where('PdpSiteKode', $_POST['KodeSite']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSOW']))
		{
			if (!empty($_POST['KodeSOW']))
			{
				$this->db->group_start();
					$this->db->where('PdpSOWKode', $_POST['KodeSOW']);
				$this->db->group_end();
			}
		}
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}
	
	function get_datatables_group_by_line()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->group_by('Line');
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
