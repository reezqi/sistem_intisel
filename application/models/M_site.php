<?php
class M_site extends CI_Model
{

	function get_all_site()
	{
		return $this->db->get('refsite');
	}

	function get_all_site3()
	{
		$hsl = $this->db->query("select * from refsite limit 200");
		return $hsl;
	}

	public function cariPO()
	{
		$Customer = $this->input->GET('Customer', TRUE);
		$Regional = $this->input->GET('Regional', TRUE);
		$SOW = $this->input->GET('SOW', TRUE);

		$data = $this->db->query("SELECT * from refsite where SiteCustomerKode like '%$Customer%'
		and SiteRegionalKode like '%$Regional%'
		and SiteSOWKode like '%$SOW%'
 LIMIT 200 ");
		return $data->result();
	}

	function search_site($KodeSite)
	{
		$this->db->like('codeSite', $KodeSite, 'both');
		$this->db->order_by('siteName', 'ASC');
		$this->db->limit(10);
		return $this->db->get('refsite')->result();
	}

	function search_site_where($KodeSite, $where)
	{
		$this->db->like('codeSite', $KodeSite, 'both');
		$this->db->where($where);
		$this->db->order_by('siteName', 'ASC');
		$this->db->limit(10);
		return $this->db->get('refsite')->result();
	}

	public function data()
	{
		$query = $this->db->query("SELECT max(noRef) as no_urut from refsite");
		$hasil = $query->row();
		return $hasil;
	}

	function simpan_site($datasite, $table)
	{
		$this->db->insert($table, $datasite);
	}

	function update_edit_site($where, $datasite, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datasite);
	}

	public function get_all_site2()
	{
		return $this->db->get('refsite', 200)->result();
	}

	function get_site_by_kode($idSite)
	{
		$hsl = $this->db->query("SELECT * FROM refsite  where idSite='$idSite'");
		return $hsl;
	}

	function get_site_byid($PdpSiteKode)
	{
		$hsl = $this->db->query("select * from refsite where codeSite='$PdpSiteKode'");
		return $hsl;
	}

	public function carisite()
	{
		$RegCode = $this->input->GET('RegCode', TRUE);
		$data = $this->db->query("SELECT * from refsite where RegCode like '%$RegCode%' order by idSite desc limit 100");
		return $data->result();
	}

	public function hapus_site($idSite)
	{
		$this->db->where_in('idSite', $idSite);
		$this->db->delete('refsite');
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refsite', $data);
	}

	function cari_site_by_kode_region_customer($No, $Region, $Customer)
	{
		$data = $this->db->select('*')->from('refsite')
			->group_start()
			->like('codeSite', $No)
			->or_where('siteName', $No)
			->group_end()
			->get();
		return $data;
	}

	//data table server side
	var $table = 'refsite';
	var $column_order = array(null, 'noRef');
	var $column_search = array('noRef', 'SiteRegionalKode', 'SiteRegionalNama', 'codeSite', 'siteName', 'ownerName', 'samnama', 'SiteSowKode', 'SiteSOWNama', 'SiteCustomerNama');
	var $order = array('idSite' => 'DESC');

	private function _get_datatables_query()
	{

		$this->db->from($this->table);

		$i = 0;

		foreach ($this->column_search as $item) // looping awal
		{
			if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
			{

				if ($i === 0) // looping awal
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['KodeRegional'])) {
			if (!empty($_POST['KodeRegional'])) {
				$this->db->group_start();
				$this->db->where('SiteRegionalKode', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer'])) {
			if (!empty($_POST['KodeCustomer'])) {
				$this->db->group_start();
				$this->db->where('SiteCustomerKode', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}


		if (isset($_POST['KodeSOW'])) {
			if (!empty($_POST['KodeSOW'])) {
				$this->db->group_start();
				$this->db->where('SiteSOWKode', $_POST['KodeSOW']);
				$this->db->group_end();
			}
		}


		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_select2($term)
	{
		if (!empty($term)) {
			$this->db->like('siteName', $term);
			$this->db->or_like('codeSite', $term);
		}
		return $this->db->get('refsite', 200)->result();
	}
}
