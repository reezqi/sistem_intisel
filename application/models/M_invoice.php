<?php
class M_invoice extends CI_Model {
    public function get()
    {   
        $KodeCustomer = $this->input->post('KodeCustomer', TRUE);
		$KodeRegional = $this->input->post('KodeRegional', TRUE);
		$date_awal = $this->input->post('date_satu', TRUE);
		$date_akhir = $this->input->post('date_dua', TRUE);
		$persentase = $this->input->post('persentase', TRUE);

		$data = $this->db->query("SELECT * from trxpdp 
                                where ProjectCode like '%$KodeCustomer%' 
                                and KodeRegional like '%$KodeRegional%'
                                and Progress like '%$persentase%'
                                and PoDate between '$date_awal' and '$date_akhir' GROUP BY NoPo LIMIT 200 ");

    
        return $data->result();
    }

    //data table server side
	var $table = 'trxpdp';
    var $column_order = array(null, 'PoDate');
    var $column_search = array('NoPp','PoDate', 'ProjectName', 'NamaRegional');
	var $order = array('PoDate' => 'DESC');
	
	private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
		}
		
		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['date_satu']) && isset($_POST['date_dua']))
		{
			if (!empty($_POST['date_satu']) && !empty($_POST['date_dua']))
			{
				$this->db->group_start();
					$this->db->where('PoDate >=', $_POST['date_satu']);
					$this->db->where('PoDate <=', $_POST['date_dua']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeRegional']))
		{
			if (!empty($_POST['KodeRegional']))
			{
				$this->db->group_start();
					$this->db->where('KodeRegional', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer']))
		{
			if (!empty($_POST['KodeCustomer']))
			{
				$this->db->group_start();
					$this->db->where('ProjectCode', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}


		if (isset($_POST['persen']))
		{
			if (!empty($_POST['persen']))
			{
				$this->db->group_start();
					$this->db->where('Progress', $_POST['persen']);
				$this->db->group_end();
			}
		}
		

        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $this->db->group_by('NoPo');
        $query = $this->db->get();
        return $query->result();
	}
	
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}