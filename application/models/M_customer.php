<?php
class M_customer extends CI_Model{

	function get_all_customer(){
		return $this->db->get('refcustomer');	
	}

	public function get_all_customer2(){
		return $this->db->get('refcustomer')->result();
	}

	function simpan_customer($datacustomer,$table){
		$this->db->insert($table,$datacustomer);
	}

	function update_customer($where,$table){		
		return $this->db->get_where($table,$where);
	}

	function update_edit_customer($where,$datacustomer,$table){		
		$this->db->where($where);
		$this->db->update($table,$datacustomer);
	}

	public function hapus_customer($KodeCust){
		$this->db->where_in('KodeCust', $KodeCust);
		$this->db->delete('refcustomer');
	}

}