<?php
class M_bank extends CI_Model
{

	function get_all_bank()
	{
		$hsl = $this->db->query("select * from masterbank order by NamaBank");
		return $hsl;
	}

	public function get_all_bank2()
	{
		return $this->db->get('masterbank')->result();
	}

	function get_bank_byid($KodeBank)
	{
		$hsl = $this->db->query("select * from masterbank where KodeBank='$KodeBank' order by NamaBank");
		return $hsl;
	}

	function get_bank_byid_subcon($SubconBankId)
	{
		$hsl = $this->db->query("select * from masterbank where KodeBank='$SubconBankId'");
		return $hsl;
	}

	function get_bank()
	{
		$query = $this->db->get('masterbank');
		return $query;
	}

	public function data()
	{
		$query = $this->db->query("SELECT max(KodeBank) as no_urut from masterbank");
		$hasil = $query->row();
		return $hasil;
	}

	function simpan_bank($databank, $table)
	{
		$this->db->insert($table, $databank);
	}

	function update_edit_bank($where, $databank, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $databank);
	}

	public function hapus_bank($KodeBank)
	{
		$this->db->where_in('KodeBank', $KodeBank);
		$this->db->delete('masterbank');
	}
}
