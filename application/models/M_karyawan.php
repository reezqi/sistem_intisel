<?php
class M_karyawan extends CI_Model
{

	function get_all_karyawan()
	{
		$hsl = $this->db->query("select * from masterkaryawan ");
		return $hsl;
	}

	function get_karyawan_by_kode($KodeAkun)
	{
		$hsl = $this->db->query("SELECT * FROM masterkaryawan  where KodeAkun='$KodeAkun'");
		return $hsl;
	}

	public function get_all_karyawan2()
	{
		return $this->db->get('masterkaryawan')->result();
	}

	function simpan_karyawan($datakaryawan, $table)
	{
		$this->db->insert($table, $datakaryawan);
	}

	function update_edit_karyawan($where, $datakaryawan, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datakaryawan);
	}

	public function hapus_karyawan($KodeAkun)
	{
		$this->db->where_in('KodeAkun', $KodeAkun);
		$this->db->delete('masterkaryawan');
	}

	function get_unit_byid($SOWKodeUnit)
	{
		$hsl = $this->db->query("select * from refunit where KodeUnit='$SOWKodeUnit'");
		return $hsl;
	}

	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename)
	{
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if ($this->upload->do_upload('file')) { // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		} else {
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	public function insert_multiple($data)
	{
		$this->db->insert_batch('refunit', $data);
	}
}
