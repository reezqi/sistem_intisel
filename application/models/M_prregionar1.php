<?php
class M_prregionar extends CI_Model
{
	public function searching()
	{
		$KodeCustomer = $this->input->GET('KodeCustomer', TRUE);
		$KodeRegional = $this->input->GET('KodeRegional', TRUE);
		$KodeSubcon = $this->input->GET('KodeSubcon', TRUE);

		$data = $this->db->query("SELECT * from trxprregionalh where KodeCustomer like '%$KodeCustomer%' and KodeRegional like '%$KodeRegional%' and KodeSubcon like '%$KodeSubcon%' ORDER BY NoPo DESC LIMIT 200 ");
		return $data->result();
	}

	function save_prregional($datatrxprregionalh, $table)
	{
		$this->db->insert($table, $datatrxprregionalh);
	}

	function save_prregional_detail($datatrxprregionald, $table)
	{
		$this->db->insert($table, $datatrxprregionald);
	}

	public function nourutpo()
	{
		$query = $this->db->query("SELECT max(NoPo) as no_urut from trxprregionalh");
		$hasil = $query->row();
		return $hasil;
	}
}
