
<?php
class M_pocustomer extends CI_Model
{

	public function nourutpo()
	{
		$query = $this->db->query("SELECT NoPoIn as no_urut from trxpocush order by NoDokumen DESC");
		$hasil = $query->row();
		return $hasil;
	}

	public function nourutdokumen()
	{
		$query = $this->db->query("SELECT NoDokumen as no_urut from trxpocush order by NoDokumen DESC");
		$hasil = $query->row();
		return $hasil;
	}

	public function tampilsemuadata()
	{
		$data = $this->db->query("SELECT * from trxpocush order by TglPo desc limit 100 ");
		return $data->result();
	}

	function get_pocustomer_by_kode($NoDokumen)
	{
		$hsl = $this->db->query("SELECT * FROM trxpocush where NoDokumen='$NoDokumen' ");
		return $hsl;
	}

	function get_pocustomer_by_kode2($Dokumen)
	{
		$hsl = $this->db->query("SELECT * FROM trxpocusd where NoDokumen='$Dokumen'");
		return $hsl->result();
	}

	function get_pocustomer_by_kode2_($Dokumen)
	{
		$hsl = $this->db->query("SELECT * FROM trxpocusd where NoDokumen='$Dokumen'");
		return $hsl;
	}


	function get_pocustomer_by_kode3($NoPoIn)
	{
		$hsl = $this->db->query("SELECT * FROM trxpocusd where NoPoIn='$NoPoIn'");
		return $hsl->result();
	}

	function get_all_pocustomer3($NoDokumen)
	{
		$hsl = $this->db->query("SELECT * FROM trxpocusd where NoDokumen='$NoDokumen'");
		return $hsl->result();
	}

	function simpan_pocustomer($datatrxpocush, $table)
	{
		$this->db->insert($table, $datatrxpocush);
	}

	function save_edit_pocustomer($where, $datatrxpcush, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datatrxpcush);
	}

	function save_edit_pocustomer_detail($where, $datatrxpcush, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $datatrxpcush);
	}

	public function delete_pocustomer($NoDokumen)
	{
		$this->db->where_in('NoDokumen', $NoDokumen);
		$this->db->delete('trxpocush');
	}

	public function delete_pocustomer_detail($NoDokumen)
	{
		$this->db->where_in('NoDokumen', $NoDokumen);
		$this->db->delete('trxpocusd');
	}

	public function delete_pocustomer_detail_multi($KodePoCusd)
	{
		$this->db->where_in('KodePoCusd', $KodePoCusd);
		$this->db->delete('trxpocusd');
	}

	function insert_master($NoDokumen, $NoPo, $TglPo)
	{
		$dt = array(
			'NoDokumen' => $NoDokumen,
			'NoPo' => $NoPo,
			'TglPo' => $TglPo
		);

		return $this->db->insert('trxpocush', $dt);
	}

	//baru
	function save_pocustomer($datatrxpocush, $table)
	{
		$this->db->insert($table, $datatrxpocush);
	}

	function save_pocustomer_detail($datatrxpocusd, $table)
	{
		$this->db->insert($table, $datatrxpocusd);
	}

	public function search_add_pocustomer()
	{
		$NoDokumen = $this->input->GET('NoDokumen', TRUE);
		$KodeCustomer = $this->input->GET('KodeCustomer', TRUE);

		$hsl = $this->db->query("SELECT * FROM trxpocush where NoDokumen = '$NoDokumen' and  KodeCustomer = '$KodeCustomer'");
		return $hsl->result();
	}

	public function cariPO()
	{
		$KodeCustomer = $this->input->post('KodeCustomer', TRUE);
		$KodeRegional = $this->input->post('KodeRegional', TRUE);
		$KodeSite = $this->input->post('KodeSite', TRUE);
		$NoPr = $this->input->post('NoPr', TRUE);
		$date_awal = $this->input->post('date_satu', TRUE);
		$date_akhir = $this->input->post('date_dua', TRUE);

		$data = $this->db->query("SELECT * from trxpocush where KodeCustomer like '%$KodeCustomer%' and KodeRegional like '%$KodeRegional%' and TglPo between '$date_awal' and '$date_akhir' LIMIT 200 ");
		return $data->result();
	}

	//data table server side
	var $table = 'trxpocush';
	var $column_order = array(null, 'NoDokumen', 'TglPo');
	var $column_search = array('NoPoIn', 'TglPo', 'NamaCustomer', 'NamaRegional', 'NamaSite', 'KodeSite');
	var $order = array('NoDokumen' => 'DESC');

	private function _get_datatables_query()
	{

		$this->db->from($this->table);

		$i = 0;

		foreach ($this->column_search as $item) // looping awal
		{
			if ($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
			{

				if ($i === 0) // looping awal
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		// if ($_POST['columns'][6]['search']['regex'] == true) 
		// {
		// 	$this->db->like('KodeRegional', $_POST['columns'][6]['search']['value']);
		// }

		if (isset($_POST['date_satu']) && isset($_POST['date_dua'])) {
			if (!empty($_POST['date_satu']) && !empty($_POST['date_dua'])) {
				$this->db->group_start();
				$this->db->where('TglPo >=', $_POST['date_satu']);
				$this->db->where('TglPo <=', $_POST['date_dua']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeRegional'])) {
			if (!empty($_POST['KodeRegional'])) {
				$this->db->group_start();
				$this->db->where('KodeRegional', $_POST['KodeRegional']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeCustomer'])) {
			if (!empty($_POST['KodeCustomer'])) {
				$this->db->group_start();
				$this->db->where('KodeCustomer', $_POST['KodeCustomer']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['KodeSite'])) {
			if (!empty($_POST['KodeSite'])) {
				$this->db->group_start();
				$this->db->where('KodeSite', $_POST['KodeSite']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['NoPr'])) {
			if (!empty($_POST['NoPr'])) {
				$this->db->group_start();
				$this->db->where('NoPr', $_POST['NoPr']);
				$this->db->group_end();
			}
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
