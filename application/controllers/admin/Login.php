<?php
class Login extends CI_Controller{
    function __construct(){
        parent:: __construct();
        $this->load->model('m_login');
        $this->load->model('m_pengguna');
		$this->load->library('upload');
    }
    function index(){
        $this->load->view('admin/v_login_admin');
    }

    private function _login(){
        $name=$this->input->post('name');
        $password=$this->input->post('password');

        $user = $this->db->get_where('refuser', ['Name' => $name])->row_array();
        
        if($user){
            if($user['IsAktive']==1){
                if($password == $user['Password']){
                   
                    if($user['rule_id'] == "ADMIN"){
                        $data =[
                            'user_id' => $user['Id'],
                            'name' => $user['Name'],
                            'rule_id' => $user['rule_id'],
                            'user_region' => $user['UserRegion'],
                            'is_admin'     => TRUE,
                        ];
                        $this->session->set_userdata($data);
                        redirect('admin/admin');
                    } else {
                        $data =[
                            'user_id' => $user['Id'],
                            'name' => $user['Name'],
                            'rule_id' => $user['rule_id'],
                            'user_region' => $user['UserRegion'],
                            'is_admin'     => FALSE,
                        ];
                        $this->session->set_userdata($data);
                        redirect('admin/user');
                    }
                }else{
                    $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"> Password Salah</div>');
                    redirect('administrator');
                }
            }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"> Akun Anda Aktik</div>');
            redirect('administrator');
            }
        }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger" role="alert"> Akun Anda Belum Diverifikasi Admin</div>');
            redirect('administrator');
        }
    }

    function auth(){
        $this->_login();
    }


    function auth1(){
        $username=strip_tags(str_replace("'", "", $this->input->post('username')));
        $password=strip_tags(str_replace("'", "", $this->input->post('password')));
        $u=$username;
        $p=$password;
        $cadmin=$this->m_login->cekadmin($u,$p);
        echo json_encode($cadmin);
        if($cadmin->num_rows() > 0){
         $this->session->set_userdata('masuk',true);
         $this->session->set_userdata('user',$u);
         $xcadmin=$cadmin->row_array();
         if($xcadmin['StsUser']=='1'){
         if($xcadmin['UserRole']=='SALES1'){
            $this->session->set_userdata('akses','1');
            $idadmin=$xcadmin['UserAkun'];
            $user_nama=$xcadmin['UserID'];
            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);
            redirect('admin/dashboard');
         }elseif($xcadmin['UserRole']=='APROVAL2'){
            $this->session->set_userdata('akses','2');
            $idadmin=$xcadmin['UserAkun'];
            $user_nama=$xcadmin['UserID'];
            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);
            redirect('admin/dashboard');
         }else{
            $this->session->set_userdata('akses','3');
            $idadmin=$xcadmin['UserAkun'];
            $user_nama=$xcadmin['UserID'];
            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);
            redirect('admin/dashboard');
         }
    }
    else{
        echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Akun Anda Belum Diverifikasi Admin</div>');
        redirect('administrator');
    }

       }else{
         echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Username Atau Password Salah</div>');
         redirect('administrator');
       }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('administrator');
    }
}
