<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_supplier');
		$this->load->helper('url');
	}

	function index()
	{
		$x['refsupplier']=$this->m_supplier->get_all_supplier()->result();
		$this->load->view('admin/v_supplier',$x);
	}

	function add_supplier(){
		$this->load->view('admin/v_add_supplier');
	}

	function simpan_supplier(){
		$TipeSuppl=strip_tags($this->input->post('TipeSuppl'));
		$KodeRegional=$this->input->post('KodeRegional');
		$KodeSuppl=$this->input->post('KodeSuppl');
		$NoAkun=$this->input->post('NoAkun');
		$Nama=$this->input->post('Nama');
		$Alamat=$this->input->post('Alamat');
		$Kota=$this->input->post('Kota');
		$Kodepos=$this->input->post('Kodepos');
		$Notelp=$this->input->post('Notelp');
		$Nohp=$this->input->post('Nohp');
		$PIC=$this->input->post('PIC');
		$Email=$this->input->post('Email');
		$Bank=$this->input->post('Bank');
		$Norek=$this->input->post('Norek');
		$NPWP=$this->input->post('NPWP');
		
		$datasupplier = array(
			'TipeSuppl' => $TipeSuppl,
			'KodeRegional'  => $KodeRegional,
			'KodeSuppl'  => $KodeSuppl,
			'NoAkun'  => $NoAkun,
			'Nama'  => $Nama,
			'Alamat'  => $Alamat,
			'Kota'  => $Kota,
			'Kodepos'  => $Kodepos,
			'Notelp'  => $Notelp,
			'Nohp'  => $Nohp,
			'PIC'  => $PIC,
			'Email'  => $Email,
			'Bank'  => $Bank,
			'Norek' => $Norek,
			'NPWP'  => $NPWP,
			
		);
		
		$this->m_supplier->simpan_supplier($datasupplier, 'refsupplier');
		echo $this->session->set_flashdata('msg','success');
		redirect('admin/supplier');
	}

	function update_supplier($KodeSuppl){
		$where = array('KodeSuppl' => $KodeSuppl);
		
		$data['refsupplier'] = $this->m_supplier->update_supplier($where,'refsupplier')->result();
		$this->load->view('admin/v_edit_supplier',$data);
	}

	function simpan_update_supplier(){
		$TipeSuppl1=$this->input->post('TipeSuppl');
		$KodeRegional1=$this->input->post('KodeRegional');
		$KodeSuppl1=$this->input->post('KodeSuppl');
		$NoAkun1=$this->input->post('NoAkun');
		$Nama1=$this->input->post('Nama');
		$Alamat1=$this->input->post('Alamat');
		$Kota1=$this->input->post('Kota');
		$Kodepos1=$this->input->post('Kodepos');
		$Notelp1=$this->input->post('Notelp');
		$Nohp1=$this->input->post('Nohp');
		$PIC1=$this->input->post('PIC');
		$Email1=$this->input->post('Email');
		$Bank1=$this->input->post('Bank');
		$Norek1=$this->input->post('Norek');
		$NPWP1=$this->input->post('NPWP');
		
		$datasupplier = array(
			'TipeSuppl' => $TipeSuppl1,
			'KodeRegional'  => $KodeRegional1,
			'KodeSuppl'  => $KodeSuppl1,
			'NoAkun'  => $NoAkun1,
			'Nama'  => $Nama1,
			'Alamat'  => $Alamat1,
			'Kota'  => $Kota1,
			'Kodepos'  => $Kodepos1,
			'Notelp'  => $Notelp1,
			'Nohp'  => $Nohp1,
			'PIC'  => $PIC1,
			'Email'  => $Email1,
			'Bank'  => $Bank1,
			'Norek' => $Norek1,
			'NPWP'  => $NPWP1
			
		);

		$where = array(
			'KodeSuppl' => $KodeSuppl1
		);
		
		$this->m_supplier->update_edit_supplier($where,$datasupplier,'refsupplier');
		redirect('admin/supplier');
	
	}
}