<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_customer');
	}

	function index()
	{
		$x['refcustomer']=$this->m_customer->get_all_customer()->result();
		$this->load->view('admin/v_customer',$x);
	}

	function add_customer(){
		$this->load->view('admin/v_add_customer');
	}

	function simpan_customer(){
		$KodeGroup=strip_tags($this->input->post('KodeGrup'));
		$KodeCust=$this->input->post('KodeCust');
		$Nama=$this->input->post('Nama');
		$Alamat=$this->input->post('Alamat');
		$Kota=$this->input->post('Kota');
		$Provinsi=$this->input->post('Provinsi');
		$KodePos=$this->input->post('KodePos');
		$NoTelp=$this->input->post('NoTelp');
		$PIC=$this->input->post('PIC');
		$NoHP=$this->input->post('NoHP');
		$Email=$this->input->post('Email');
		$Npwp=$this->input->post('Npwp');
		$alamatkirim=$this->input->post('alamatkirim');
		$tipeCust=$this->input->post('tipeCust');
		$Top=$this->input->post('Top');
		$StsAktiv=$this->input->post('StsAktiv');

		$datacustomer = array(
			'KodeGrup' => $KodeGrup,
			'KodeCust'  => $KodeCust,
			'Nama'  => $Nama,
			'Alamat'  => $Alamat,
			'Kota'  => $Kota,
			'Provinsi'  => $Provinsi,
			'KodePos'  => $KodePos,
			'NoTelp'  => $NoTelp,
			'PIC'  => $PIC,
			'NoHP'  => $NoHP,
			'Email'  => $Email,
			'Npwp'  => $Npwp,
			'alamatkirim'  => $alamatkirim,
			'tipeCust' => $tipeCust,
			'Top'  => $Top,
			'StsAktiv' => $StsAktiv
		);
		
		$this->m_customer->simpan_customer($datacustomer, 'refcustomer');
		echo $this->session->set_flashdata('msg','success');
		redirect('admin/customer');
	}

	function update_customer($KodeCust){
		$where = array('KodeCust' => $KodeCust);
		
		$data['refcustomer'] = $this->m_customer->update_customer($where,'refcustomer')->result();
		$this->load->view('admin/v_edit_customer',$data);
	}

	function simpan_update_customer(){
		$KodeGrup=$this->input->post('KodeGrup');
		$KodeCust=$this->input->post('KodeCust');
		$Nama=$this->input->post('Nama');
		$Alamat=$this->input->post('Alamat');
		$Kota=$this->input->post('Kota');
		$Provinsi=$this->input->post('Provinsi');
		$KodePos=$this->input->post('KodePos');
		$NoTelp=$this->input->post('NoTelp');
		$PIC=$this->input->post('PIC');
		$NoHP=$this->input->post('NoHP');
		$Email=$this->input->post('Email');
		$Npwp=$this->input->post('Npwp');
		$alamatkirim=$this->input->post('alamatkirim');
		$tipeCust=$this->input->post('tipeCust');
		$Top=$this->input->post('Top');
		$StsAktiv=$this->input->post('StsAktiv');
	 
		$datacustomer = array(
			'KodeGrup' => $KodeGrup,
			'KodeCust'  => $KodeCust,
			'Nama'  => $Nama,
			'Alamat'  => $Alamat,
			'Kota'  => $Kota,
			'Provinsi'  => $Provinsi,
			'KodePos'  => $KodePos,
			'NoTelp'  => $NoTelp,
			'PIC'  => $PIC,
			'NoHP'  => $NoHP,
			'Email'  => $Email,
			'Npwp'  => $Npwp,
			'alamatkirim'  => $alamatkirim,
			'tipeCust' => $tipeCust,
			'Top'  => $Top,
			'StsAktiv' => $StsAktiv
		);
	 
		$where = array(
			'KodeCust' => $KodeCust
		);
	 
		$this->m_customer->update_edit_customer($where,$datacustomer,'refcustomer');
		redirect('admin/customer');
	}

	function hapus_customer(){
		$KodeCust=strip_tags($this->input->post('KodeCust'));
		$this->m_customer->hapus_customer($KodeCust);

		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/customer');
	}

}