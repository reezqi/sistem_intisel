<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Updatepdp extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_updatepdp');
		$this->load->model('m_regional');
		$this->load->model('m_site');
		$this->load->model('m_project');
		$this->load->model('m_principal');
		$this->load->model('m_msow');
		$this->load->model('m_subcon');
		$this->load->model('m_jenispekerjaan');
		$this->load->model('m_invoiceterbit');
		$this->load->model('m_posubcon');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
	}

	function index()
	{
		if ($this->session->userdata('is_admin') === TRUE) {
			$data['refregional'] = $this->m_regional->get_all_regional2();
		} else {
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		$data['refprincipal'] = $this->m_principal->get_all_principal2();
		$data['refsite'] = $this->m_site->get_all_site2();
		$data['refmsow'] = $this->m_msow->get_all_msow2();

		$this->load->view('admin/transaksi/v_updatepdp', $data);
	}

	function get_add()
	{
		$NoPo = $this->uri->segment('4');

		$data['refpekerjaan'] = $this->m_jenispekerjaan->get_all_jenispekerjaan2();
		$data['data1'] = $this->m_updatepdp->get_updatepdp_by_kode1(base64_decode($NoPo));

		//Memilih Kode Site
		$data['detail_site'] = $this->m_posubcon->get_pilih_site(base64_decode($NoPo));

		$output = '<option value="">PILIH SITE</option>';
		foreach ($data['detail_site'] as $row) {
			$output .= '<option value="' . $row->KodeSite . ' ">  ' . $row->NamaSite . ' | ' . $row->KodeSite . ' </option>';
		}
		$data['detail_site'] = $output;

		//echo json_encode($data['data1']->result());

		$data['latest_invoice'] = $this->m_invoiceterbit->get_latest_invoice()->row(0);
		if (isset($data['latest_invoice'])) {
			$data['in'] = $data['latest_invoice']->NoInvoice;
		} else {
			$data['in'] = 0;
		}
		$this->load->view('admin/transaksi/v_add_update_pdp', $data);
	}

	function get_sub_pekerjaan()
	{
		$PekerjaanId = $this->input->post('id', TRUE);
		$data = $this->m_updatepdp->get_sub_pekerjaan($PekerjaanId)->result();
		echo json_encode($data);
	}


	function data_updatepdp()
	{
		$NoPo = $this->uri->segment('4');

		$data['Regional'] = $this->m_updatepdp->get_updatepdp_by_kode2(base64_decode($NoPo))->result();
		$this->load->view('admin/transaksi/v_data_updatepdp', $data);
	}


	public function hasil()
	{
		$data['refregional'] = $this->m_regional->get_all_regional2();
		$data['refproject'] = $this->m_project->get_all_project2();
		$data['refsite'] = $this->m_site->get_all_site2();
		$data['refmsow'] = $this->m_msow->get_all_msow2();

		$data['Regional'] = $this->m_updatepdp->cariPO();
		$this->load->view('admin/transaksi/v_updatepdp', $data);
	}

	function simpan_detail()
	{
		$connect = mysqli_connect("202.78.195.194", "usersakti", "u5ers4kti", "dbinvoice");

		$NoPo  = $_POST["NoPo"];
		$TglPo = $_POST["date"];
		$KodeProyek = $_POST["KodeProyek"];
		$NamaProyek = $_POST["NamaProyek"];
		$KodeRegional = $_POST["KodeRegional"];
		$NamaRegional = $_POST["NamaRegional"];
		$SiteID = $_POST["SiteID"];
		$SiteName = $_POST["SiteName"];
		$KodeSOW = $_POST["KodeSOW"];
		$SOW = $_POST["SOW"];
		$pekerjaan  = $_POST["item_pekerjaan"];
		$dokumen = $_POST["item_dokumen"];
		$presentasi  = $_POST["item_persentasi"];

		$statement = $connect->prepare("INSERT INTO trxpdp (NoPo, PoDate, ProjectCode, ProjectName, RegionalCode, RegionName, SiteID, SiteName, KodeSOW, SOW, JobCode, DocumentName, Progress) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		for ($count = 0, $c = count($pekerjaan); $count < $c; $count++) {
			$statement->bind_param(
				'sssssssssssss',
				$NoPo,
				$TglPo,
				$KodeProyek,
				$NamaProyek,
				$KodeRegional,
				$NamaRegional,
				$SiteID,
				$SiteName,
				$KodeSOW,
				$SOW,
				$pekerjaan[$count],
				$dokumen[$count],
				$presentasi[$count]
			);
			$statement->execute();
		}
		echo '<div class="alert alert-success">Item Details Saved</div>';
	}

	function fill_sub_category()
	{
		$connect = mysqli_connect("202.78.195.194", "usersakti", "u5ers4kti", "dbinvoice");

		function fill_select_box($connect, $category_id)
		{
			$query = "SELECT idDocCat, PdpRegionalKode, docCategory, descDocCategory, bobot FROM refmatrikpdp  WHERE idDocCat='216'  ";
			$result = $connect->query($query);

			$output = '';
			foreach ($result as $row) {
				$output .= '<option value="' . $row["descDocCategory"] . ' ">  ' . $row["descDocCategory"] . ' </option>';
			}
			return $output;
		}
		echo fill_select_box($connect, $_POST["descDocCategory"]);
	}

	function fill_sub_category1()
	{
		$connect = mysqli_connect("202.78.195.194", "usersakti", "u5ers4kti", "dbinvoice");

		function fill_select_box1($connect, $category_id)
		{
			$query = "SELECT idDocCat, PdpRegionalKode, docCategory, descDocCategory, bobot FROM refmatrikpdp  WHERE idDocCat='216'  ";
			$result = $connect->query($query);

			$output = '';
			foreach ($result as $row) {
				$output .= '<option value="' . $row["bobot"] . ' ">  ' . $row["bobot"] . ' </option>';
			}
			return $output;
		}
		echo fill_select_box1($connect, $_POST["descDocCategory"]);
	}

	// new rizqi version
	public function ajax_matrik_pdp()
	{
		if ($this->input->is_ajax_request()) {
			$pdp 		= $this->m_updatepdp;

			$NoPo 		= $this->input->post('NoPo');
			$KodeSOW 	= $this->input->post('SOW');
			$KodeSite 	= $this->input->post('Site');
			$regKode 	= $this->input->post('regKode');

			// get persentase terbaru
			$hasil_persentase = '';
			$persentase = $pdp->get_matrik_pdp_detail_persentase(['NoPo' => $NoPo, 'KodeSOW' => $KodeSOW])->row();
			if ($persentase === NULL) {
				$hasil_persentase = '0';
			} else {
				$hasil_persentase = $persentase->Progress;
			}


			$result 	= $pdp->get_matrik_pdp(['PdpRegionalKode' => $regKode, 'PdpSiteKode' => $KodeSite, 'PdpSOWKode' => $KodeSOW, 'bobot > ' => $hasil_persentase]);

			$output = '<option value="">PILIH PEKERJAAN</option>';
			if ($result->result() !== NULL) {
				foreach ($result->result() as $field) {
					$output .= '<option value="' . $field->idDocCat . ' ">  ' . $field->docCategory . ' </option>';
				}
			}

			echo json_encode($output);
		}
	}

	public function ajax_category_matrik_pdp()
	{
		if ($this->input->is_ajax_request()) {
			$pdp 		= $this->m_updatepdp;
			$idDocCat 	= $this->input->post('idDocCat');
			$result 	= $pdp->get_matrik_pdp(['idDocCat' => $idDocCat])->result();

			echo json_encode($result);
		}
	}

	public function ajax_pilih_site()
	{
		if ($this->input->is_ajax_request()) {
			$NoPo = $this->input->post('NoPo');
			$data['pilih_site'] = $this->m_posubcon->get_pilih_site($NoPo);

			$output = '<option value="">PILIH SITE</option>';
			foreach ($data['pilih_site'] as $row) {
				$output .= '<option value="' . $row->KodeSite . ' ">  ' . $row->NamaSite . ' | ' . $row->KodeSite . ' </option>';
			}

			echo json_encode($output);
		}
	}

	public function ajax_pilih_sow_no_site()
	{
		if ($this->input->is_ajax_request()) {
			$pdp 		= $this->m_updatepdp;

			$NoPo 		= $this->input->post('NoPo');
			$KodeSite 	= $this->input->post('Site');
			$regKode 	= $this->input->post('regKode');

			$result = $pdp->get_matrik_sow(['NoPo' => $NoPo, 'KodeRegional' => $regKode]);

			$output = '<option value="">PILIH SOW</option>';
			if ($result->result() !== NULL) {
				foreach ($result->result() as $row) {
					$output .= '<option value="' . $row->KodeSOW . ' ">  ' . $row->NamaSOW . ' | ' . $row->KodeSOW . ' </option>';
				}
			}

			echo json_encode($output);
		}
	}

	public function ajax_pilih_sow()
	{
		if ($this->input->is_ajax_request()) {
			$pdp 		= $this->m_updatepdp;

			$NoPo 		= $this->input->post('NoPo');
			$KodeSite 	= $this->input->post('Site');
			$regKode 	= $this->input->post('regKode');

			$result = $pdp->get_matrik_sow(['NoPo' => $NoPo, 'KodeSite' => $KodeSite, 'KodeRegional' => $regKode]);

			$output = '<option value="">PILIH SOW</option>';
			if ($result->result() !== NULL) {
				foreach ($result->result() as $row) {
					$output .= '<option value="' . $row->KodeSOW . ' ">  ' . $row->NamaSOW . ' | ' . $row->KodeSOW . ' </option>';
				}
			}

			echo json_encode($output);
		}
	}
	
	public function ajax_pilih_sow_detail_no_site()
	{
		if ($this->input->is_ajax_request()) {
			$pdp 		= $this->m_updatepdp;

			$NoPo 		= $this->input->post('NoPo');
			$KodeSite 	= $this->input->post('Site');
			$regKode 	= $this->input->post('regKode');

			$result = $pdp->get_matrik_sow(['NoPo' => $NoPo, 'KodeRegional' => $regKode]);

			$output = '<option value="">PILIH SOW</option>';
			if ($result->result() !== NULL) {
				foreach ($result->result() as $row) {
					$output .= '<option value="' . $row->KodeSOW . ' ">  ' . $row->NamaSOW . ' | ' . $row->KodeSOW . ' </option>';
				}
			}

			echo json_encode($output);
		}
	}

	public function ajax_pilih_sow_detail()
	{
		if ($this->input->is_ajax_request()) {
			$pdp 		= $this->m_updatepdp;

			$NoPo 		= $this->input->post('NoPo');
			$KodeSite 	= $this->input->post('Site');
			$regKode 	= $this->input->post('regKode');

			$result = $pdp->get_matrik_sow(['NoPo' => $NoPo, 'KodeSite' => $KodeSite, 'KodeRegional' => $regKode]);

			$output = '<option value="">PILIH SOW</option>';
			if ($result->result() !== NULL) {
				foreach ($result->result() as $row) {
					$output .= '<option value="' . $row->KodeSOW . ' ">  ' . $row->NamaSOW . ' | ' . $row->KodeSOW . ' </option>';
				}
			}

			echo json_encode($output);
		}
	}

	public function submit_matrik_pdp()
	{
		if ($this->input->is_ajax_request()) {
			if (isset($_POST['kode_barang'])) {
				$NoPo 			= $this->input->post('NoPo');
				$NoPoIn 		= $this->input->post('NoPoIn');
				$TglPo			= $this->input->post('date');
				$KodeProyek 	= $this->input->post('KodeCustomer');
				$NamaProyek 	= $this->input->post('NamaCustomer');
				$KodeRegional 	= $this->input->post('KodeRegional');
				$NamaRegional 	= $this->input->post('NamaRegional');
				$KodeSite 		= $this->input->post('KodeSite');
				$NamaSite 		= $this->input->post('NamaSite');
				//
				$KodeBarang  	= $this->input->post('kode_barang');
				$NamaBarang  	= $this->input->post('nama_barang');
				$Dokumen 		= $this->input->post('dokumen');
				$KodePekerjaan  = $this->input->post('kode_pekerjaan');
				$NamaPekerjaan  = $this->input->post('nama_pekerjaan');
				$Persentase  	= $this->input->post('persentase');

				foreach ($KodeBarang as $key => $k) {
					$data_matrik_pdp = array(
						'NoPo' => $NoPo,
						'NoPoIn' => $NoPoIn,
						'PoDate' => $TglPo,
						'ProjectCode' => $KodeProyek,
						'ProjectName' => $NamaProyek,
						'KodeRegional'	=> $KodeRegional,
						'NamaRegional'	=> $NamaRegional,
						'KodeSite'	=> $KodeSite,
						'NamaSite'	=> $NamaSite,
						'KodeSOW' 	=> $KodeBarang[$key],
						'NamaSow'	=> $NamaBarang[$key],
						'JobCode'	=> trim($NamaPekerjaan[$key]),
						'DocumentName'	=> $Dokumen[$key],
						'Progress'	=> $Persentase[$key],
					);

					$this->m_updatepdp->insert_matrik_pdp($data_matrik_pdp);
				}

				echo json_encode(array('status' => 1));
			} else {
				echo json_encode(array('status' => 2));
			}
		}
	}

	public function ajax_detail_matrik_pdp()
	{
		if ($this->input->is_ajax_request()) {
			$NoPo 		= $this->input->post('NoPo');
			$KodeSOW 	= $this->input->post('KodeSOW');

			$data['matrik_detail'] 		= $this->m_updatepdp->get_matrik_pdp_detail(['NoPo' => $NoPo, 'KodeSOW' => $KodeSOW])->result();
			$data['sow']				= $this->m_msow->get_by_code($KodeSOW)->result();
			$data['invoice']			= $this->m_invoiceterbit->get_invoice($NoPo, $KodeSOW)->row(0);

			echo json_encode($data);
		}
	}

	public function delete_matrik_pdp_by_leneno()
	{
		if ($_POST) {
			$lineno			= $this->input->post('lineno');
			$this->m_updatepdp->delete_by_lineno($lineno);
			echo json_encode(array('status' => 1));
		}
	}

	public function get_add_invoice()
	{
		if ($_POST) {
			$NoPoCustomer 			= $this->input->post('NoPo');
			$NoPoInternal 			= $this->input->post('NoPoIn');
			$TglInvoiceTerbit		= $this->input->post('date');
			$KodeCustomer 			= $this->input->post('KodeCustomer');
			$KodeRegional 			= $this->input->post('KodeRegional');
			$NoInvoice 				= $this->input->post('NoInvoice');
			$SOW 					= $this->input->post('SOW');
			$termin 				= $this->input->post('termin');

			$term_					= $this->m_msow->get_by_code($SOW)->result();

			//echo json_encode();

			// echo json_encode($data['sow']);
		
			foreach($_POST['status'] as $key => $value) {
				//echo json_encode($value);
				//echo json_encode('s');
				$persen =  $_POST['persentase'][$key];
				if ($value == 'true') {
					if ($persen >= $term_[0]->F8){
						$datainvoice = array(
							'NoInvoice'	=> $NoInvoice,
							'NoPoCustomer' => $NoPoCustomer,
							'NoPoInternal' => $NoPoInternal,
							'TglInvoiceTerbit' => date('d-m-Y'),
							'KodeCustomer' => $KodeCustomer,
							'KodeRegional'	=> $KodeRegional,
							'Terminke'	=> 3,
							'KodeSOW'	=> trim($SOW),
						);
					} else if($persen >= $term_[0]->F7) {
						$datainvoice = array(
							'NoInvoice'	=> $NoInvoice,
							'NoPoCustomer' => $NoPoCustomer,
							'NoPoInternal' => $NoPoInternal,
							'TglInvoiceTerbit' => date('d-m-Y'),
							'KodeCustomer' => $KodeCustomer,
							'KodeRegional'	=> $KodeRegional,
							'Terminke'	=> 2,
							'KodeSOW'	=> trim($SOW),
						);
					} else if ($persen >= $term_[0]->PemFull)
					{
						$datainvoice = array(
							'NoInvoice'	=> $NoInvoice,
							'NoPoCustomer' => $NoPoCustomer,
							'NoPoInternal' => $NoPoInternal,
							'TglInvoiceTerbit' => date('d-m-Y'),
							'KodeCustomer' => $KodeCustomer,
							'KodeRegional'	=> $KodeRegional,
							'Terminke'	=> 1,
							'KodeSOW'	=> trim($SOW),
						);
					}
				$this->m_updatepdp->simpan_invoice_by_lineno($datainvoice);

				}
			}

			echo json_encode(array('status' => 1));
		}
		// echo json_encode($_POST);
	}

	function ajax_table_updatepdp()
	{
		$list = $this->m_updatepdp->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			$this->db->select('Progress');
			$this->db->where('NoPo', $field->NoPo);
			$this->db->order_by('Progress', 'DESC');
			$this->db->limit(1);
			$hasil = $this->db->get('trxpdp')->row(0);
			//echo json_encode($hasil);
			if (empty($hasil))
			{
				$value = '0';
				$progres = '<div class="progress"><div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0</div></div>';
			} else {
				$progres = '<div class="progress"><div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: ' . $hasil->Progress . '%" aria-valuenow="' . $hasil->Progress . '" aria-valuemin="0" aria-valuemax="100">' . $hasil->Progress . '</div></div>';
				$value = $field->TotalBayar * $hasil->Progress / 100;
			}
			//break;
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->NoPo;
			$row[] = $field->TglPo;
			$row[] = $field->NamaSubcon;
			$row[] = $field->NamaCustomer;
			$row[] = $field->NamaRegional;
			$row[] = $field->KodeSite;
			$row[] = $field->TotalBayar;
			$row[] = $value;
			$row[] = $progres;
			$row[] = "
				<a class='badge' href='" . base_url() . "admin/updatepdp/get_add//" . base64_encode($field->NoPo) . "' style='background-color:blue;' title='Update Progres Pekerjaan'><span class='fa fa-edit'></span></a>
			";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_updatepdp->count_all(),
			"recordsFiltered" => $this->m_updatepdp->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}
}
