<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Site extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_site');
		$this->load->model('m_regional');
		$this->load->model('m_principal');
		$this->load->model('m_msow');
		$this->load->model('m_unit');
		$this->load->model('m_subcon');
		$this->load->model('m_owner');
		$this->load->model('m_sam');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refmsow'] = $this->m_msow->get_all_msow2();

		$x['refsite'] = $this->m_site->cariPO();
		$this->load->view('admin/v_site', $x);
	}

	public function hasil()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refmsow'] = $this->m_msow->get_all_msow2();

		$x['refsite'] = $this->m_site->cariPO();
		$this->load->view('admin/v_site', $x);
	}


	function get_autocomplete()
	{
		if (isset($_POST['term'])) {
			$result = "";
			$kode = $this->input->post('regKode');
			if ($this->session->userdata('is_admin') === TRUE) {
				if ($kode !== NULL) 
				{
					$result = $this->m_site->search_site_where($_POST['term'], ['SiteRegionalKode' => $this->input->post('regKode')]);
				}
			} else {
				$result = $this->m_site->search_site_where($_POST['term'], ['SiteRegionalKode' => $this->session->userdata('user_region')]);
			}
			if (count($result) > 0) {
				foreach ($result as $row)
					$arr_result[] = array(
						'label'			=> $row->codeSite,
						'description'	=> $row->siteName,
					);
				echo json_encode($arr_result);

			}

		}
		// echo json_encode($this->input->post('term'));
	}


	function add_site()
	{
		$x['refmsow'] = $this->m_msow->get_all_msow2();
		$x['refowner'] = $this->m_owner->get_all_owner2();
		$x['refsam'] = $this->m_sam->get_all_sam2();
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$x['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$x['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		//Membuat Kode Regional Otomatis
		$pegawai = $this->m_site->data();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$x['noRef'] = $noUrut + 1;
		//end

		$this->load->view('admin/v_add_site', $x);
	}

	function get_edit()
	{
		$idSite = $this->uri->segment(4);
		$data['data1'] = $this->m_site->get_site_by_kode($idSite);
		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$data['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}
		$data['refsow'] = $this->m_msow->get_all_msow3();
		$data['refsam'] = $this->m_sam->get_all_sam3();
		$data['refsubcon'] = $this->m_subcon->get_all_subcon3();
		$data['refunit'] = $this->m_unit->get_all_unit3();
		$data['refowner'] = $this->m_owner->get_all_owner3();

		$this->load->view('admin/v_edit_site', $data);
	}

	function simpan_site()
	{
		$idSite = strip_tags($this->input->post('idSite'));
		$noRef = strip_tags($this->input->post('noRef'));
		$codeSite = $this->input->post('codeSite');
		$siteName = $this->input->post('siteName');
		$codeSite1 = $this->input->post('codeSite1');
		$siteName1 = $this->input->post('siteName1');
		$codeSite2 = $this->input->post('codeSite2');
		$siteName2 = $this->input->post('siteName2');
		$codeSite3 = $this->input->post('codeSite3');
		$siteName3 = $this->input->post('siteName3');
		$shipmentNo = $this->input->post('shipmentNo');
		$POLine = $this->input->post('POLine');

		$ownerId = strip_tags($this->input->post('owner'));
		$data = $this->m_owner->get_owner_byid($ownerId);
		$q = $data->row_array();
		$ownerName = $q['NamaOwner'];

		$samid = strip_tags($this->input->post('sam'));
		$data = $this->m_sam->get_sam_byid($samid);
		$q = $data->row_array();
		$samnama = $q['NamaSAM'];

		$SiteRegionalKode = strip_tags($this->input->post('Regional'));
		$data = $this->m_regional->get_regional_byid($SiteRegionalKode);
		$q = $data->row_array();
		$SiteRegionalNama = $q['NAMA'];

		$SiteCustomerKode = strip_tags($this->input->post('Customer'));
		$data = $this->m_principal->get_principal_byid1($SiteCustomerKode);
		$q = $data->row_array();
		$SiteCustomerNama = $q['NamaPrincipal'];

		$SiteSOWKode = strip_tags($this->input->post('KodeSOW'));
		$SiteSOWNama = $this->input->post('NamaSOW');
		$noContract = $this->input->post('noContract');
		$thnContract_1 = $this->input->post('thnContract_1');
		$thnContract_2 = $this->input->post('thnContract_2');
		$thnContract_3 = $this->input->post('thnContract_3');
		$Koordinat = $this->input->post('Koordinat');
		$keterangan = $this->input->post('keterangan');

		$Tanggal = $this->input->post('datepicker');
		$userid = $this->session->userdata('user_id');
		$usernama = $this->session->userdata('name');

		$datasite = array(
			'created_at' => $Tanggal,
			'user_id' => $userid,
			'user_name' => $usernama,
			'idSite'  => $idSite,
			'noRef'  => $noRef,
			'codeSite'  => $codeSite,
			'siteName'  => $siteName,
			'codeSite1'  => $codeSite1,
			'siteName1' => $siteName1,
			'codeSite2' => $codeSite2,
			'siteName2' => $siteName2,
			'codeSite3' => $codeSite3,
			'siteName3'  => $siteName3,
			'shipmentNo'  => $shipmentNo,
			'POLine'  => $POLine,
			'ownerId'  => $ownerId,
			'ownerName'  => $ownerName,
			'samid'  => $samid,
			'samnama'  => $samnama,
			'SiteRegionalKode'  => $SiteRegionalKode,
			'SiteRegionalNama'  => $SiteRegionalNama,
			'SiteCustomerKode'  => $SiteCustomerKode,
			'SiteCustomerNama'  => $SiteCustomerNama,
			'SiteSOWKode'  => $SiteSOWKode,
			'SiteSOWNama' => $SiteSOWNama,
			'noContract'  => $noContract,
			'thnContract_1'  => $thnContract_1,
			'thnContract_2'  => $thnContract_2,
			'thnContract_3'  => $thnContract_3,
			'Koordinat'  => $Koordinat,
			'keterangan' => $keterangan
		);

		$this->m_site->simpan_site($datasite, 'refsite');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/site');
	}

	function simpan_update_site()
	{
		$idSite = strip_tags($this->input->post('idSite'));
		$noRef = strip_tags($this->input->post('noRef'));
		$codeSite = $this->input->post('codeSite');
		$siteName = $this->input->post('siteName');
		$codeSite1 = $this->input->post('codeSite1');
		$siteName1 = $this->input->post('siteName1');
		$codeSite2 = $this->input->post('codeSite2');
		$siteName2 = $this->input->post('siteName2');
		$codeSite3 = $this->input->post('codeSite3');
		$siteName3 = $this->input->post('siteName3');
		$shipmentNo = $this->input->post('shipmentNo');
		$POLine = $this->input->post('POLine');

		$ownerId = strip_tags($this->input->post('owner'));
		$data = $this->m_owner->get_owner_byid($ownerId);
		$q = $data->row_array();
		$ownerName = $q['NamaOwner'];

		$samid = strip_tags($this->input->post('sam'));
		$data = $this->m_sam->get_sam_byid($samid);
		$q = $data->row_array();
		$samnama = $q['NamaSAM'];

		$SiteRegionalKode = strip_tags($this->input->post('Regional'));
		$data = $this->m_regional->get_regional_byid($SiteRegionalKode);
		$q = $data->row_array();
		$SiteRegionalNama = $q['NAMA'];

		$SiteCustomerKode = strip_tags($this->input->post('Customer'));
		$data = $this->m_principal->get_principal_byid($SiteCustomerKode);
		$q = $data->row_array();
		$SiteCustomerNama = $q['NamaPrincipal'];

		$SiteSOWKode = strip_tags($this->input->post('KodeSOW'));
		$SiteSOWNama = strip_tags($this->input->post('NamaSOW'));

		$noContract = $this->input->post('noContract');
		$thnContract_1 = $this->input->post('thnContract_1');
		$thnContract_2 = $this->input->post('thnContract_2');
		$thnContract_3 = $this->input->post('thnContract_3');
		$Koordinat = $this->input->post('Koordinat');
		$keterangan = $this->input->post('keterangan');

		$Tanggal = $this->input->post('datepicker');
		$userid = $this->session->userdata('user_id');
		$usernama = $this->session->userdata('name');

		$datasite = array(
			'created_at' => $Tanggal,
			'user_id' => $userid,
			'user_name' => $usernama,
			'idSite'  => $idSite,
			'noRef'  => $noRef,
			'codeSite'  => $codeSite,
			'siteName'  => $siteName,
			'codeSite1'  => $codeSite1,
			'siteName1' => $siteName1,
			'codeSite2' => $codeSite2,
			'siteName2' => $siteName2,
			'codeSite3' => $codeSite3,
			'siteName3'  => $siteName3,
			'shipmentNo'  => $shipmentNo,
			'POLine'  => $POLine,
			'ownerId'  => $ownerId,
			'ownerName'  => $ownerName,
			'samid'  => $samid,
			'samnama'  => $samnama,
			'SiteRegionalKode'  => $SiteRegionalKode,
			'SiteRegionalNama'  => $SiteRegionalNama,
			'SiteCustomerKode'  => $SiteCustomerKode,
			'SiteCustomerNama'  => $SiteCustomerNama,
			'SiteSOWKode'  => $SiteSOWKode,
			'SiteSOWNama' => $SiteSOWNama,
			'noContract'  => $noContract,
			'thnContract_1'  => $thnContract_1,
			'thnContract_2'  => $thnContract_2,
			'thnContract_3'  => $thnContract_3,
			'Koordinat'  => $Koordinat,
			'keterangan' => $keterangan
		);

		$where = array(
			'noRef' => $noRef
		);

		$this->m_site->update_edit_site($where, $datasite, 'refsite');
		echo $this->session->set_flashdata('msg', 'info');
		redirect('admin/site');
	}

	function hapus_site()
	{
		$idSite = strip_tags($this->input->post('idSite'));
		$this->m_site->hapus_site($idSite);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/site');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_site->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_site_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'noRef' => $row['A'], // Insert data nis dari kolom A di excel
					'codeSite' => $row['B'], // Insert data nama dari kolom B di excel
					'siteName' => $row['C'],
					'codeSite1' => $row['D'],
					'siteName1' => $row['E'],
					'codeSite2' => $row['F'],
					'siteName2' => $row['G'],
					'codeSite3' => $row['H'],
					'siteName3' => $row['I'],
					'shipmentNo' => $row['J'],
					'POLine' => $row['K'],
					'ownerName' => $row['L'],
					'SiteRegionalKode' => $row['M'],
					'SiteRegionalNama' => $row['N'],
					'SiteCustomerKode' => $row['O'],
					'SiteCustomerNama' => $row['P'],

					'SiteSOWKode' => $row['Q'],
					'SiteSOWNama' => $row['R'],
					'noContract' => $row['S'],
					'thnContract_1' => $row['T'],
					'thnContract_2' => $row['U'],
					'thnContract_3' => $row['V'],
					'keterangan' => $row['W'],

				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_site->insert_multiple($data);

		redirect("admin/site"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER SITE"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:X1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Ref"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Kode Site Utama");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Nama Site Utama");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Kode Site (Projek)");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Nama Site (Projek) ");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Kode Site (Regional)");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "Nama Site (Regional)");
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Kode Site 3");
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "Nama Site 3");
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "No Shipment");
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "PO Line");
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "Nama Owner");
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "Kode Regional");
		$excel->setActiveSheetIndex(0)->setCellValue('N3', "Nama Regional");
		$excel->setActiveSheetIndex(0)->setCellValue('O3', "Kode Customer");
		$excel->setActiveSheetIndex(0)->setCellValue('P3', "Nama Customer");

		$excel->setActiveSheetIndex(0)->setCellValue('Q3', "Kode SOW");
		$excel->setActiveSheetIndex(0)->setCellValue('R3', "Nama SOW");
		$excel->setActiveSheetIndex(0)->setCellValue('S3', "No Kontrak");
		$excel->setActiveSheetIndex(0)->setCellValue('T3', "Tahun Kontrak 1");
		$excel->setActiveSheetIndex(0)->setCellValue('U3', "Tahun Kontrak 2");
		$excel->setActiveSheetIndex(0)->setCellValue('V3', "Tahun Kontrak 3");
		$excel->setActiveSheetIndex(0)->setCellValue('W3', "Komentar Masa Kontrak");


		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);

		$excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('S3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('T3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('U3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('V3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('W3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$site = $this->m_site->get_all_site2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($site as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->noRef);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->codeSite);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->siteName);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->codeSite1);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->siteName1);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->codeSite2);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->siteName2);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->codeSite3);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->siteName3);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->shipmentNo);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->POLine);
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->ownerName);
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->SiteRegionalKode);
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->SiteRegionalNama);
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->SiteCustomerKode);
			$excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->SiteCustomerNama);

			$excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $data->SiteSOWKode);
			$excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $data->SiteSOWNama);
			$excel->setActiveSheetIndex(0)->setCellValue('S' . $numrow, $data->noContract);
			$excel->setActiveSheetIndex(0)->setCellValue('T' . $numrow, $data->thnContract_1);
			$excel->setActiveSheetIndex(0)->setCellValue('U' . $numrow, $data->thnContract_2);
			$excel->setActiveSheetIndex(0)->setCellValue('V' . $numrow, $data->thnContract_3);
			$excel->setActiveSheetIndex(0)->setCellValue('W' . $numrow, $data->keterangan);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);

			$excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('R' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('S' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('T' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('U' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('V' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('W' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(14); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(22); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(70);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('R')->setWidth(35);
		$excel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('V')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('W')->setWidth(35);


		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Site");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Site.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	function ajax_site_region_customer()
	{
		if ($_POST)
		{
			$No 		= $_POST['term'];
			$Region 	= $_POST['region'];
			$Customer 	= $_POST['customer'];

			$hasil = $this->m_site->cari_site_by_kode_region_customer($No, $Region, $Customer);

			$data = array();
			foreach($hasil->result() as $field){

			   if (trim($field->SiteRegionalKode) == trim($Region) && trim($field->SiteCustomerKode) == trim($Customer) )
			   {
					$data[] = array("id"=>$field->codeSite, "text"=>$field->codeSite.' - '.$field->siteName, "slug"=>$field->siteName);
			   }
			}
			
			echo json_encode($data);
		}
	}

	function ajax_table_site()
    {
        $list = $this->m_site->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->noRef;
            $row[] = $field->codeSite;
            $row[] = $field->siteName;
            $row[] = $field->codeSite1;
            $row[] = $field->siteName1;
            $row[] = $field->codeSite2;
            $row[] = $field->siteName2;
            $row[] = $field->shipmentNo;
            $row[] = $field->POLine;
			$row[] = $field->ownerName;
			$row[] = $field->samnama;
            $row[] = $field->SiteRegionalNama;
            $row[] = $field->SiteCustomerNama;
            $row[] = $field->SiteSOWNama;
            $row[] = $field->noContract;
			$row[] = $field->thnContract_1 . ' - ' . $field->thnContract_2 . ' - ' . $field->thnContract_3;
			$row[] = $field->Koordinat;
            $row[] = $field->keterangan;
			$row[] = "
				<a class='badge' href='". base_url() . "admin/site/get_edit/" . $field->idSite ."' style='background-color:blue;' title='Edit PO'><span class='fa fa-edit'></span></a>
				<a class='badge' id='hapus' data-site='". $field->siteName ."' data-hapus='". $field->idSite ."' data-toggle='modal' style='background-color:red;' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_site->count_all(),
            "recordsFiltered" => $this->m_site->count_filtered(),
			"data" => $data,
        );
		echo json_encode($output);
	}

	public function select_site()
	{
		if ($_POST) {
			$term = $_POST['term'];
			$hasil = $this->m_site->get_select2($term);

			$data = array();
			foreach($hasil as $field){
				$data[] = array("id"=>$field->codeSite, "text"=>$field->codeSite.' - '.$field->siteName, "slug"=>$field->siteName);
			}
			echo json_encode($data);
		} else {
			$hasil = $this->m_site->get_select2('');

			$data = array();
			foreach($hasil as $field){
				$data[] = array("id"=>$field->codeSite, "text"=>$field->codeSite.' - '.$field->siteName, "slug"=>$field->siteName);
			}
			echo json_encode($data);
		}
	}
}
