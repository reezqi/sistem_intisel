<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategoridokumen extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_kategoridokumen');
		$this->load->helper('url');
		$this->load->model('m_regional');
		$this->load->model('m_principal');
		$this->load->model('m_msow');
		$this->load->model('m_site');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refmsow'] = $this->m_msow->get_all_msow2();
		$x['refsite'] = $this->m_site->get_all_site2();

		$this->load->view('admin/v_kategoridokumen', $x);
	}

	function add_kategoridokumen()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$x['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$x['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}
		$x['refmsow'] = $this->m_msow->get_all_msow2();
		$x['refsite'] = $this->m_site->get_all_site2();
		$line = $this->m_kategoridokumen->get_latest_line()->row(0);
		if (empty($line))
		{
			$x['line'] = 1;
		} else {
			$x['line'] = $line->line + 1;
		}

		//echo json_encode($line);
		$this->load->view('admin/v_add_kategoridokumen', $x);
	}

	public function hasil()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refmsow'] = $this->m_msow->get_all_msow2();
		$x['refsite'] = $this->m_site->get_all_site2();

		$x['Regional'] = $this->m_kategoridokumen->cariPO();
		$this->load->view('admin/v_kategoridokumen', $x);
	}

	function get_edit()
	{
		$line= $this->uri->segment(4);
		$data['data1'] = $this->m_kategoridokumen->get_kategoridokumen_by_line($line);
		$data['refprincipal'] = $this->m_principal->get_all_principal();
		$data['refmsow'] = $this->m_msow->get_all_msow();
		$data['refsite'] = $this->m_site->get_all_site();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$data['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		$this->load->view('admin/v_edit_kategoridokumen', $data);
	}

	function simpan_kategoridokumen()
	{
		$idDocCat = strip_tags($this->input->post('idDocCat'));

		$PdpCustomerKode = strip_tags($this->input->post('KodeCustomer'));
		$data = $this->m_principal->get_principal_byid($PdpCustomerKode);
		$q = $data->row_array();
		$PdpCustomerNama = $q['NamaPrincipal'];

		$PdpRegionalKode = strip_tags($this->input->post('KodeRegional'));
		$data = $this->m_regional->get_regional_byid($PdpRegionalKode);
		$q = $data->row_array();
		$PdpRegionalNama = $q['NAMA'];

		$PdpSOWKode = strip_tags($this->input->post('KodeSOW'));
		// $data = $this->m_msow->get_msow_byid($PdpSOWKode);
		// $q = $data->row_array();
		// $PdpSOWNama = $q['SOW'];
		$PdpSOWNama = $this->input->post('NamaSOW');


		$PdpSiteKode = strip_tags($this->input->post('KodeSite'));
		// $data = $this->m_site->get_site_byid($PdpSiteKode);
		// $q = $data->row_array();
		// $PdpSiteNama = $q['siteName'];
		$PdpSiteNama = $this->input->post('NamaSite');


		$docCategory = $_POST["item_category"];
		$descDocCategory = $_POST["item_sub_category"];
		$bobot = $_POST["item_persen"];
		$active = $this->input->post('active');
		$line = $this->input->post('line');

		$result = 0;
		foreach ($_POST['item_category'] as $key => $k) {
			if (!empty($k)) {
				$data = array(
					'line' => $line,
					'PdpCustomerKode' => $PdpCustomerKode, // Insert data nis dari kolom A di excel
					'PdpCustomerNama' => $PdpCustomerNama,
					'PdpSOWKode' => $PdpSOWKode,
					'PdpSOWNama' => $PdpSOWNama,
					'PdpSiteKode' => $PdpSiteKode,
					'PdpSiteNama' => $PdpSiteNama,
					'PdpRegionalKode' => $PdpRegionalKode,
					'PdpRegionalNama' => $PdpRegionalNama,
					'docCategory' => $_POST["item_category"][$key],
					'descDocCategory' => $_POST["item_sub_category"][$key],
					'bobot' => $_POST["item_persen"][$key],
					'active' => $active,
				);
				$this->m_kategoridokumen->simpan_kategoridokumen($data, 'refmatrikpdp');
				$result++;
			}
		}

		echo json_encode($result);

	}

	function simpan_update_kategoridokumen()
	{
		$idDocCat = strip_tags($this->input->post('idDocCat'));

		$PdpCustomerKode = strip_tags($this->input->post('KodeCustomer'));
		$data = $this->m_principal->get_principal_byid($PdpCustomerKode);
		$q = $data->row_array();
		$PdpCustomerNama = $q['NamaPrincipal'];

		$PdpRegionalKode = strip_tags($this->input->post('KodeRegional'));
		$data = $this->m_regional->get_regional_byid($PdpRegionalKode);
		$q = $data->row_array();
		$PdpRegionalNama = $q['NAMA'];

		$PdpSOWKode = strip_tags($this->input->post('KodeSOW'));
		// $data = $this->m_msow->get_msow_byid($PdpSOWKode);
		// $q = $data->row_array();
		// $PdpSOWNama = $q['SOW'];
		$PdpSOWNama = $this->input->post('NamaSOW');


		$PdpSiteKode = strip_tags($this->input->post('KodeSite'));
		// $data = $this->m_site->get_site_byid($PdpSiteKode);
		// $q = $data->row_array();
		// $PdpSiteNama = $q['siteName'];
		$PdpSiteNama = $this->input->post('NamaSite');


		$docCategory = $_POST["item_category"];
		$descDocCategory = $_POST["item_sub_category"];
		$bobot = $_POST["item_persen"];
		$active = $this->input->post('active');
		$line = $this->input->post('line');

		//delete by line
		$this->m_kategoridokumen->hapus_by_line($line);

		$result = 0;
		foreach ($_POST['item_category'] as $key => $k) {
			if (!empty($k)) {
				$data = array(
					'line' => $line,
					'PdpCustomerKode' => $PdpCustomerKode, // Insert data nis dari kolom A di excel
					'PdpCustomerNama' => $PdpCustomerNama,
					'PdpSOWKode' => $PdpSOWKode,
					'PdpSOWNama' => $PdpSOWNama,
					'PdpSiteKode' => $PdpSiteKode,
					'PdpSiteNama' => $PdpSiteNama,
					'PdpRegionalKode' => $PdpRegionalKode,
					'PdpRegionalNama' => $PdpRegionalNama,
					'docCategory' => $_POST["item_category"][$key],
					'descDocCategory' => $_POST["item_sub_category"][$key],
					'bobot' => $_POST["item_persen"][$key],
					'active' => $active,
				);
				$this->m_kategoridokumen->simpan_kategoridokumen($data, 'refmatrikpdp');
				$result++;
			}
		}

		echo json_encode($result);
	}

	public function pencariankategoridokumen()
	{
		$data2['principalCode'] = $this->m_kategoridokumen->carikategoridokumen();
		$this->load->view('admin/v_result_kategoridokumen', $data2);
	}

	function hapus_kategoridokumen()
	{
		$idDocCat = strip_tags($this->input->post('line'));
		$this->m_kategoridokumen->hapus_by_line($idDocCat);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/kategoridokumen');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_kategoridokumen->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_kategoridokumen_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'PdpCustomerKode' => $row['A'], // Insert data nis dari kolom A di excel
					'principalName' => $row['B'],
					'PdpSOWKode' => $row['C'],
					'PdpSOWNama' => $row['D'],
					'PdpSiteKode' => $row['E'],
					'PdpSiteNama' => $row['F'],
					'PdpRegionalKode' => $row['G'],
					'PdpRegionalNama' => $row['H'],
					'docCategory' => $row['I'],
					'descDocCategory' => $row['J'],
					'bobot' => $row['K'],
					'active' => $row['L'],
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_kategoridokumen->insert_multiple($data);

		redirect("admin/kategoridokumen"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER MATRIKS PDP"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:L1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Customer"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Customer");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Kode SOW");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Nama SOW");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Kode Site");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Nama Site");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "Kode Regional");
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Nama Regional");
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "Progres/Tahapan");
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "Dokumen");
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "Persen");
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "Status");

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$kategoridokumen = $this->m_kategoridokumen->get_all_kategoridokumen2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($kategoridokumen as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->PdpCustomerKode);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->principalName);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->PdpSOWKode);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->PdpSOWNama);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->PdpSiteKode);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->PdpSiteNama);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->PdpRegionalKode);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->PdpRegionalNama);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->docCategory);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->descDocCategory);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->bobot);
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->active);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(50);
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Matrik PDP");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Matrik PDP.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	function ajax_table_matrik()
    {
        $list_semua = $this->m_kategoridokumen->get_datatables();
        $list = $this->m_kategoridokumen->get_datatables_group_by_line();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

			//cek aktif
			$status = "<span class='badge bg-green'>Aktif</span>";
			if ($field->active != 'Y') { $status = "<span class='badge bg-green'>Tidak Aktif</span>"; }
			//end cek aktif

			// add proses
			$proses = '';
			foreach($list_semua as $x)
			{
				if ($field->line === $x->line) 
				{
					$proses .= $x->docCategory . "<hr />";
				}
			}
			//

			// add dokumen
			$dokumen = '';
			foreach($list_semua as $x)
			{
				if ($field->line === $x->line) 
				{
					$dokumen .= $x->descDocCategory . "<hr />";
				}
			}
			//

			// add dokumen
			$bobot = '';
			foreach($list_semua as $x)
			{
				if ($field->line === $x->line) 
				{
					$bobot .= $x->bobot . "<hr />";
				}
			}
			//

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->PdpCustomerNama;
            $row[] = $field->PdpSOWNama;
            $row[] = $field->PdpSiteNama;
            $row[] = $field->PdpRegionalNama;
            $row[] = $proses;
            $row[] = $dokumen;
            $row[] = $bobot;
            $row[] = $status;
			$row[] = "
				<a style='padding: 6px 6px;' href='" .  base_url() . "admin/kategoridokumen/get_edit/" . $field->line . "'>
					<span class='fa fa-pencil'></span>
				</a>

				<a style='padding: 6px 6px;' id='hapus' data-line='". $field->line ."' data-target='#ModalHapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_kategoridokumen->count_all(),
            "recordsFiltered" => $this->m_kategoridokumen->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
