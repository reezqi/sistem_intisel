<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Principal extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_principal');
		$this->load->model('m_provinsi');
		$this->load->model('m_kabupaten');
		$this->load->model('m_regional');
	}

	function index()
	{
		$x['provinsi'] = $this->m_regional->get_provinsi()->result();
		$x['refprincipal'] = $this->m_principal->get_all_principal()->result();
		$this->load->view('admin/v_principal', $x);
	}

	function get_edit()
	{
		$kodePrincipal = $this->uri->segment(4);
		$data['data1'] = $this->m_principal->get_principal_by_kode($kodePrincipal);
		$data['provinsi'] = $this->m_provinsi->get_all_provinsi();
		$data['kabupaten'] = $this->m_kabupaten->get_all_kabupaten();

		$this->load->view('admin/v_edit_principal', $data);
	}

	function simpan_principal()
	{
		$kodePrincipal = strip_tags($this->input->post('kodePrincipal'));
		$NamaPrincipal = $this->input->post('NamaPrincipal');
		$Alamat = $this->input->post('Alamat');

		$PrincipalKabupatenId = strip_tags($this->input->post('kabupaten'));
		$data = $this->m_kabupaten->get_kabupaten_byid($PrincipalKabupatenId);
		$q = $data->row_array();
		$PrincipalKabupatenNama = $q['KabupatenNama'];

		$PrincipalProvinsiId = strip_tags($this->input->post('provinsi'));
		$data = $this->m_provinsi->get_provinsi_byid($PrincipalProvinsiId);
		$q = $data->row_array();
		$PrincipalProvinsiNama = $q['ProvinsiNama'];

		$KodePos = $this->input->post('KodePos');
		$NoTelp = $this->input->post('NoTelp');
		$NoHp = $this->input->post('NoHp');
		$Email = $this->input->post('Email');
		$NPWP = $this->input->post('NPWP');
		$Website = $this->input->post('Website');
		$ContactPerson = $this->input->post('ContactPerson');

		$dataprincipal = array(
			'kodePrincipal' => $kodePrincipal,
			'NamaPrincipal' => $NamaPrincipal,
			'Alamat' => $Alamat,
			'PrincipalKabupatenId' => $PrincipalKabupatenId,
			'PrincipalKabupatenNama' => $PrincipalKabupatenNama,
			'PrincipalProvinsiId' => $PrincipalProvinsiId,
			'PrincipalProvinsiNama' => $PrincipalProvinsiNama,
			'KodePos' => $KodePos,
			'NoTelp' => $NoTelp,
			'NoHp' => $NoHp,
			'Email' => $Email,
			'NPWP' => $NPWP,
			'Website' => $Website,
			'ContactPerson' => $ContactPerson
		);

		$this->m_principal->simpan_principal($dataprincipal, 'refprincipal');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/principal');
	}

	function simpan_update_principal()
	{
		$kodePrincipal = strip_tags($this->input->post('kodePrincipal'));
		$NamaPrincipal = $this->input->post('NamaPrincipal');
		$Alamat = $this->input->post('Alamat');

		$PrincipalKabupatenId = strip_tags($this->input->post('kabupaten'));
		$data = $this->m_kabupaten->get_kabupaten_byid($PrincipalKabupatenId);
		$q = $data->row_array();
		$PrincipalKabupatenNama = $q['KabupatenNama'];

		$PrincipalProvinsiId = strip_tags($this->input->post('provinsi'));
		$data = $this->m_provinsi->get_provinsi_byid($PrincipalProvinsiId);
		$q = $data->row_array();
		$PrincipalProvinsiNama = $q['ProvinsiNama'];

		$KodePos = $this->input->post('KodePos');
		$NoTelp = $this->input->post('NoTelp');
		$NoHp = $this->input->post('NoHp');
		$Email = $this->input->post('Email');
		$NPWP = $this->input->post('NPWP');
		$Website = $this->input->post('Website');
		$ContactPerson = $this->input->post('ContactPerson');

		$dataprincipal = array(
			'kodePrincipal' => $kodePrincipal,
			'NamaPrincipal' => $NamaPrincipal,
			'Alamat' => $Alamat,
			'PrincipalKabupatenId' => $PrincipalKabupatenId,
			'PrincipalKabupatenNama' => $PrincipalKabupatenNama,
			'PrincipalProvinsiId' => $PrincipalProvinsiId,
			'PrincipalProvinsiNama' => $PrincipalProvinsiNama,
			'KodePos' => $KodePos,
			'NoTelp' => $NoTelp,
			'NoHp' => $NoHp,
			'Email' => $Email,
			'NPWP' => $NPWP,
			'Website' => $Website,
			'ContactPerson' => $ContactPerson
		);

		$where = array(
			'kodePrincipal' => $kodePrincipal
		);

		$this->m_principal->update_edit_principal($where, $dataprincipal, 'refprincipal');
		redirect('admin/principal');
	}

	function hapus_principal()
	{
		$kodePrincipal = strip_tags($this->input->post('kodePrincipal'));
		$this->m_principal->hapus_principal($kodePrincipal);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/principal');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_principal->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_principal_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'kodePrincipal' => $row['A'], // Insert data nis dari kolom A di excel
					'NamaPrincipal' => $row['B'],
					'Alamat' => $row['C'],
					'PrincipalKabupatenId' => $row['D'],
					'PrincipalKabupatenNama' => $row['E'],
					'PrincipalProvinsiId' => $row['F'],
					'PrincipalProvinsiNama' => $row['G'],
					'KodePos' => $row['H'],
					'NoTelp' => $row['I'],
					'NoHp' => $row['J'],
					'Email' => $row['K'],
					'NPWP' => $row['L'],
					'Website' => $row['M'],

				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_principal->insert_multiple($data);

		redirect("admin/principal"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER CUSTOMER"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:M1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Principal"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Principal");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Alamat");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Kode Kabupaten");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Nama Kabupaten");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Kode Provinsi");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "Nama Provinsi");
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Kode Pos");
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "No Telepon");
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "No HP");
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "Email");
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "NPWP");
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "Website");


		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$principal = $this->m_principal->get_all_principal2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($principal as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->kodePrincipal);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->NamaPrincipal);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->Alamat);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->PrincipalKabupatenId);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->PrincipalKabupatenNama);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->PrincipalProvinsiId);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->PrincipalProvinsiNama);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->KodePos);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->NoTelp);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->NoHp);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->Email);
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->NPWP);
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->Website);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('M')->setWidth(30);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Customer");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Customer.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	public function select_principal()
	{
		if ($_POST) {
			$term = $_POST['term'];
			$hasil = $this->m_principal->get_select2($term);

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->kodePrincipal, "text" => $field->kodePrincipal . ' - ' . $field->NamaPrincipal, "slug" => $field->NamaPrincipal);
			}
			echo json_encode($data);
		} else {
			$hasil = $this->m_principal->get_select2('');

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->kodePrincipal, "text" => $field->kodePrincipal . ' - ' . $field->NamaPrincipal, "slug" => $field->NamaPrincipal);
			}
			echo json_encode($data);
		}
	}
}
