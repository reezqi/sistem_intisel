<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Project extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_project');
		$this->load->model('m_regional');
		$this->load->model('m_provinsi');
		$this->load->model('m_kabupaten');
		$this->load->model('m_principal');
		$this->load->helper('url');
	}

	function index()
	{
		$x['refproject'] = $this->m_project->get_all_project()->result();
		$this->load->view('admin/v_project', $x);
	}

	function add_project()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['provinsi'] = $this->m_regional->get_provinsi()->result();
		$this->load->view('admin/v_add_project', $x);
	}

	function get_edit()
	{
		$KodeProject = $this->uri->segment(4);
		$data['data1'] = $this->m_project->get_project_by_kode($KodeProject);
		$data['provinsi'] = $this->m_provinsi->get_all_provinsi();
		$data['kabupaten'] = $this->m_kabupaten->get_all_kabupaten();
		$data['refprincipal'] = $this->m_principal->get_all_principal();

		$this->load->view('admin/v_edit_project', $data);
	}

	function simpan_project()
	{
		$ProjectKodeCustomer = strip_tags($this->input->post('KodeCustomer'));
		$data = $this->m_principal->get_principal_byid($ProjectKodeCustomer);
		$q = $data->row_array();
		$ProjectNamaCustomer = $q['NamaPrincipal'];

		$ProjectName = $this->input->post('ProjectName');
		$Alamat1 = $this->input->post('Alamat');

		$ProjekKabupatenId = strip_tags($this->input->post('kabupaten'));
		$data = $this->m_kabupaten->get_kabupaten_byid($ProjekKabupatenId);
		$q = $data->row_array();
		$ProjekKabupatenNama = $q['KabupatenNama'];

		$ProjekProvinsiId = strip_tags($this->input->post('provinsi'));
		$data = $this->m_provinsi->get_provinsi_byid($ProjekProvinsiId);
		$q = $data->row_array();
		$ProjekProvinsiNama = $q['ProvinsiNama'];

		$KodePos1 = $this->input->post('KodePos');
		$NoTelp1 = $this->input->post('NoTelp');
		$PIC1 = $this->input->post('PIC');
		$NoHP1 = $this->input->post('NoHP');
		$Email1 = $this->input->post('Email');
		$Npwp1 = $this->input->post('Npwp');
		$alamatkirim1 = $this->input->post('alamatkirim');
		$tipeCust1 = $this->input->post('tipeCust');
		$Top1 = $this->input->post('Top');
		$StsAktiv1 = $this->input->post('StsAktiv');

		$dataproject = array(
			'ProjectKodeCustomer'  => $ProjectKodeCustomer,
			'ProjectNamaCustomer'  => $ProjectNamaCustomer,
			'ProjectNama'  => $ProjectName,
			'Alamat'  => $Alamat1,
			'ProjekKabupatenId' => $ProjekKabupatenId,
			'ProjekKabupatenNama' => $ProjekKabupatenNama,
			'ProjekProvinsiId' => $ProjekProvinsiId,
			'ProjekProvinsiNama' => $ProjekProvinsiNama,
			'KodePos'  => $KodePos1,
			'NoTelp'  => $NoTelp1,
			'PIC'  => $PIC1,
			'NoHP'  => $NoHP1,
			'Email'  => $Email1,
			'Npwp'  => $Npwp1,
			'alamatkirim'  => $alamatkirim1,
			'tipeCust' => $tipeCust1,
			'Top'  => $Top1,
			'StsAktiv' => $StsAktiv1
		);

		$this->m_project->simpan_project($dataproject, 'refproject');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/project');
	}

	function update_project($KodeCust)
	{
		$where = array('KodeCust' => $KodeCust);

		$data['refproject'] = $this->m_project->update_project($where, 'refproject')->result();
		$this->load->view('admin/v_edit_project', $data);
	}

	function hapus_project()
	{
		$KodeProject = strip_tags($this->input->post('KodeProject'));
		$this->m_project->hapus_project($KodeProject);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/project');
	}

	function simpan_update_project()
	{
		$KodeProject = strip_tags($this->input->post('KodeProject'));
		$ProjectKodeCustomer = strip_tags($this->input->post('KodeCustomer'));
		$data = $this->m_principal->get_principal_byid($ProjectKodeCustomer);
		$q = $data->row_array();
		$ProjectNamaCustomer = $q['NamaPrincipal'];

		$Nama1 = $this->input->post('Nama');
		$Alamat1 = $this->input->post('Alamat');

		$ProjekKabupatenId = strip_tags($this->input->post('kabupaten'));
		$data = $this->m_kabupaten->get_kabupaten_byid($ProjekKabupatenId);
		$q = $data->row_array();
		$ProjekKabupatenNama = $q['KabupatenNama'];

		$ProjekProvinsiId = strip_tags($this->input->post('provinsi'));
		$data = $this->m_provinsi->get_provinsi_byid($ProjekProvinsiId);
		$q = $data->row_array();
		$ProjekProvinsiNama = $q['ProvinsiNama'];

		$KodePos1 = $this->input->post('KodePos');
		$NoTelp1 = $this->input->post('NoTelp');
		$PIC1 = $this->input->post('PIC');
		$NoHP1 = $this->input->post('NoHP');
		$Email1 = $this->input->post('Email');
		$Npwp1 = $this->input->post('Npwp');
		$alamatkirim1 = $this->input->post('alamatkirim');
		$tipeCust1 = $this->input->post('tipeCust');
		$Top1 = $this->input->post('Top');
		$StsAktiv1 = $this->input->post('StsAktiv');

		$dataproject = array(
			'ProjectKodeCustomer'  => $ProjectKodeCustomer,
			'ProjectNamaCustomer'  => $ProjectNamaCustomer,
			'ProjectNama'  => $Nama1,
			'Alamat'  => $Alamat1,
			'ProjekKabupatenId' => $ProjekKabupatenId,
			'ProjekKabupatenNama' => $ProjekKabupatenNama,
			'ProjekProvinsiId' => $ProjekProvinsiId,
			'ProjekProvinsiNama' => $ProjekProvinsiNama,
			'KodePos'  => $KodePos1,
			'NoTelp'  => $NoTelp1,
			'PIC'  => $PIC1,
			'NoHP'  => $NoHP1,
			'Email'  => $Email1,
			'Npwp'  => $Npwp1,
			'alamatkirim'  => $alamatkirim1,
			'tipeCust' => $tipeCust1,
			'Top'  => $Top1,
			'StsAktiv' => $StsAktiv1
		);

		$where = array(
			'KodeProject' => $KodeProject
		);

		$this->m_project->update_edit_project($where, $dataproject, 'refproject');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/project');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_project->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_project_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'ProjectKodeCustomer' => $row['A'], // Insert data nis dari kolom A di excel
					'ProjectNamaCustomer' => $row['B'], // Insert data nama dari kolom B di excel
					'ProjectNama' => $row['C'],
					'Alamat' => $row['D'],
					'ProjekKabupatenId' => $row['E'],
					'ProjekKabupatenNama' => $row['F'],
					'ProjekProvinsiId' => $row['G'],
					'ProjekProvinsiNama' => $row['H'],
					'KodePos' => $row['I'],
					'NoTelp' => $row['J'],
					'NoHP' => $row['K'],
					'PIC' => $row['L'],
					'Email' => $row['M'],
					'Npwp' => $row['N'],
					'StsAktiv' => $row['O'],

				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_project->insert_multiple($data);

		redirect("admin/project"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER PROJECT"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:O1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Customer"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Customer"); // Set kolom B3 dengan tulisan "NIS"
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Nama Project");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Alamat");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Kode Kota");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Kota");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "Kode Provinsi");
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Provinsi");
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "Kode Pos");
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "No Telepon");
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "No HP");
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "PIC");
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "Email");
		$excel->setActiveSheetIndex(0)->setCellValue('N3', "NPWP");
		$excel->setActiveSheetIndex(0)->setCellValue('O3', "Status Aktiv");

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$project = $this->m_project->get_all_project2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($project as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->ProjectKodeCustomer);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->ProjectNamaCustomer);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->ProjectNama);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->Alamat);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->ProjekKabupatenId);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->ProjekKabupatenNama);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->ProjekProvinsiId);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->ProjekProvinsiNama);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->KodePos);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->NoTelp);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->NoHP);
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->PIC);
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->Email);
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->Npwp);
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->StsAktiv);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('O')->setWidth(15);


		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Project");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Project.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}
}
