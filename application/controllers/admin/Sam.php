<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sam extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_sam');
		$this->load->helper('url');
	}

	function index()
	{
		$x['mastersam'] = $this->m_sam->get_all_sam()->result();

		//Membuat Kode Regional Otomatis
		$pegawai = $this->m_sam->data();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$x['KodeSAM'] = $noUrut + 1;
		//end
		//echo json_encode($noUrut);
		$this->load->view('admin/v_sam', $x);
	}

	function simpan_sam()
	{
		$KodeSAM = strip_tags($this->input->post('KodeSAM'));
		$NamaSAM = $this->input->post('NamaSAM');
		$Tanggal = $this->input->post('datepicker');

		$datasam = array(
			'KodeSAM' => $KodeSAM,
			'NamaSAM' => $NamaSAM,
			'created_at' => $Tanggal,
			'user_id' => $this->session->userdata('user_id'),
			'user_name' => $this->session->userdata('name'),
		);

		$this->m_sam->simpan_sam($datasam, 'mastersam');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/sam');
	}

	function update_sam()
	{

		$KodeSAM = $this->input->post('KodeSAM');
		$NamaSAM = $this->input->post('NamaSAM');
		$Tanggal = $this->input->post('datepicker');

		$datasam = array(
			'KodeSAM' => $KodeSAM,
			'NamaSAM' => $NamaSAM,
			'created_at' => $Tanggal,
			'user_id' => $this->session->userdata('user_id'),
			'user_name' => $this->session->userdata('name'),
		);

		$where = array(
			'KodeSAM' => $KodeSAM
		);

		$this->m_sam->update_edit_sam($where, $datasam, 'mastersam');
		redirect('admin/sam');
	}

	function ajax_get_sam()
	{
		if ($_POST) {
			$KodeSAM 	= $this->input->post('KodeSAM');
			$hasil 	    = $this->m_sam->get_where(['KodeSAM' => $KodeSAM]);

			echo json_encode($hasil->row(0));
		}
	}

	function hapus_sam()
	{
		$KodeSAM = strip_tags($this->input->post('KODE'));
		$this->m_sam->hapus_sam($KodeSAM);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/sam');
	}

	public function select_sam()
	{
		if ($_POST) {
			$term = $_POST['term'];
			$hasil = $this->m_sam->get_select2($term);

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->KodeSAM, "text" => $field->KodeSAM . ' - ' . $field->NamaSAM, "slug" => $field->NamaSAM);
			}
			echo json_encode($data);
		} else {
			$hasil = $this->m_sam->get_select2('');

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->KodeSAM, "text" => $field->KodeSAM . ' - ' . $field->NamaSAM, "slug" => $field->NamaSAM);
			}
			echo json_encode($data);
		}
	}
}
