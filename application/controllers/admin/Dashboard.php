<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_pengunjung');
	}
	function index(){
		if($this->session->userdata('akses')=='1'){
			$this->load->view('admin/v_index_admin');
		}elseif($this->session->userdata('akses')=='2'){
			$this->load->view('admin/v_index_admin');
		}elseif($this->session->userdata('akses')=='3'){
			$this->load->view('admin/v_index_admin');
		}else{
			redirect('administrator');
		}
	
	}
	
}