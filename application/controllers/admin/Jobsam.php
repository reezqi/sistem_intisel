<?php
class Jobsam extends CI_Controller{

    function __construct()
	{
		parent::__construct();
		$this->load->model('m_prregional');
		$this->load->model('m_jobsam');
		$this->load->model('m_regional');
		$this->load->model('m_site');
		$this->load->model('m_principal');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
    }
    
	function index(){
        $data['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$data['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}
        $data['refsite'] = $this->m_site->get_all_site2();
        
		$this->load->view('admin/transaksi/v_jobsam', $data);
    }
    
    public function add_jobsam()
    {
        $this->load->view('admin/transaksi/v_add_jobsam');
	}

	public function view_jobsam_edit($id)
	{
		$data['sam'] = $this->m_jobsam->get($id)->result();
        $this->load->view('admin/transaksi/v_edit_jobsam', $data);
	}

	public function ajax_jobsam_edit()
	{
		if ($_POST)
		{
			$id = $_POST['no'];
			$hasil = $this->m_jobsam->get($id);
			echo json_encode($hasil->result());
		}
	}
	
	public function submit_jobsam()
	{
		if ($_POST)
		{
			if (!empty($_POST['KodeRegional']))
			{
				foreach ($_POST['KodeRegional'] as $key=>$k) {
					$KodeRegional 	= $_POST['KodeRegional'][$key];
					$NamaRegional 	= $_POST['NamaRegional'][$key];
					$KodeCustomer 	= $_POST['KodeCustomer'][$key];
					$NamaCustomer 	= $_POST['NamaCustomer'][$key];
					$KodeSite	 	= $_POST['KodeSite'][$key];
					$NamaSite 		= $_POST['NamaSite'][$key];
					$KodeSAM 		= $_POST['KodeSAM'][$key];
					$NamaSAM 		= $_POST['NamaSAM'][$key];
					$KodeVPRM 		= $_POST['KodeVPRM'][$key];
					$NamaVPRM 		= $_POST['NamaVPRM'][$key];

					$data = array(
						'KodeRegional' 	=> $KodeRegional,
						'NamaRegional'	=> $NamaRegional,
						'KodeCustomer'	=> $KodeCustomer,
						'NamaCustomer'	=> $NamaCustomer,
						'KodeSite'		=> $KodeSite,
						'NamaSite'		=> $NamaSite,
						'KodeSAM'		=> $KodeSAM,
						'NamaSAM'		=> $NamaSAM,
						'KodeVPRM'		=> $KodeVPRM,
						'NamaVPRM'		=> $NamaVPRM,
					);
					
					$this->m_jobsam->save($data);
				}

				echo json_encode(['status' => 1]);
			}
		}
	}

	public function update_jobsam()
	{
		if ($_POST)
		{
			//echo json_encode($_POST);
			if (!empty($_POST['KodeRegional']))
			{
				foreach ($_POST['KodeRegional'] as $key=>$k) {
					$KodeRegional 	= $_POST['KodeRegional'][$key];
					$NamaRegional 	= $_POST['NamaRegional'][$key];
					$KodeCustomer 	= $_POST['KodeCustomer'][$key];
					$NamaCustomer 	= $_POST['NamaCustomer'][$key];
					$KodeSite	 	= $_POST['KodeSite'][$key];
					$NamaSite 		= $_POST['NamaSite'][$key];
					$KodeSAM 		= $_POST['KodeSAM'][$key];
					$NamaSAM 		= $_POST['NamaSAM'][$key];
					$KodeVPRM 		= $_POST['KodeVPRM'][$key];
					$NamaVPRM 		= $_POST['NamaVPRM'][$key];
					$idjob 			= $_POST['idjob'][$key];

					$data = array(
						'IdJob'			=> $idjob,
						'KodeRegional' 	=> $KodeRegional,
						'NamaRegional'	=> $NamaRegional,
						'KodeCustomer'	=> $KodeCustomer,
						'NamaCustomer'	=> $NamaCustomer,
						'KodeSite'		=> $KodeSite,
						'NamaSite'		=> $NamaSite,
						'KodeSAM'		=> $KodeSAM,
						'NamaSAM'		=> $NamaSAM,
						'KodeVPRM'		=> $KodeVPRM,
						'NamaVPRM'		=> $NamaVPRM,
					);
					
					if ($idjob == '')
					{
						$this->m_jobsam->save($data);
					} 
					else 
					{
						$this->m_jobsam->update($data, $idjob);
					}
				}

				echo json_encode(['status' => 1]);
			}
		}
	}
	
	public function delete_sam()
	{
		$id = $this->input->post('id');
		$this->m_jobsam->delete($id);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/jobsam');
	}

	public function ajax_delete_sam()
	{
		$id = $this->input->post('id');
		$this->m_jobsam->delete($id);

		echo json_encode(['status' => 1]);
	}

	function ajax_table_jobsam()
    {
        $list = $this->m_jobsam->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
			$row[] = $field->NamaSAM;
            $row[] = $field->NamaRegional;
			$row[] = $field->NamaCustomer;
            $row[] = $field->NamaSite;
            $row[] = $field->NamaVPRM;
			$row[] = "
				<a class='badge' href='". base_url() . "admin/jobsam/view_jobsam_edit/" . $field->IdJob ."' style='background-color:blue;' title='Edit PO'><span class='fa fa-edit'></span></a>
				<a class='badge' id='hapus' data-sam='". $field->NamaSAM ."' data-hapus='". $field->IdJob ."' data-toggle='modal' style='background-color:red;' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_jobsam->count_all(),
            "recordsFiltered" => $this->m_jobsam->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
	}
	
}