<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Regional extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_regional');
		$this->load->model('m_provinsi');
		$this->load->model('m_kabupaten');
		$this->load->model('m_perusahaan');
		$this->load->library('session');
		$this->load->helper('url');
	}

	function index()
	{
		$x['perusahaan'] = $this->m_perusahaan->get_all_perusahaan2();
		$x['provinsi'] = $this->m_regional->get_provinsi()->result();

		//Membuat Kode Regional Otomatis
		$pegawai = $this->m_regional->data();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$x['KODE'] = $noUrut + 1;
		//end

		$x['refregional'] = $this->m_regional->get_all_regional()->result();
		$this->load->view('admin/v_regional', $x);
	}

	function get_edit()
	{
		$KODE = $this->uri->segment(4);
		$data['data1'] = $this->m_regional->get_regional_by_kode($KODE);
		$data['provinsi'] = $this->m_provinsi->get_all_provinsi();
		$data['kabupaten'] = $this->m_kabupaten->get_all_kabupaten();
		$data['perusahaan'] = $this->m_perusahaan->get_all_perusahaan();

		$this->load->view('admin/v_edit_regional', $data);
	}

	function update_regional($KODE)
	{
		$where = array('KODE' => $KODE);
		$data['refregional'] = $this->m_regional->update_regional($where, 'refregional')->result();
		$this->load->view('admin/v_edit_regional', $data);
	}

	function get_data_edit()
	{
		$KODE = $this->input->post('KODE', TRUE);
		$data = $this->m_regional->get_regional_by_id($KODE)->result();
		echo json_encode($data);
	}

	function get_sub_provinsi()
	{
		$ProvinsiId = $this->input->post('id', TRUE);
		$data = $this->m_regional->get_sub_provinsi($ProvinsiId)->result();
		echo json_encode($data);
	}

	function simpan_regional()
	{
		$KODE = strip_tags($this->input->post('KODE'));
		$NAMA = $this->input->post('NAMA');
		$Alamat = $this->input->post('Alamat');

		$RegionalKabupatenId = $this->input->post('kabupaten');
		$data = $this->m_kabupaten->get_kabupaten_byid($RegionalKabupatenId);
		$q = $data->row_array();
		$RegionalKabupatenNama = $q['KabupatenNama'];

		$RegionalProvinsiId = $this->input->post('provinsi');
		$data = $this->m_provinsi->get_provinsi_byid($RegionalProvinsiId);
		$q = $data->row_array();
		$RegionalProvinsiNama = $q['ProvinsiNama'];

		$KodePos = $this->input->post('KodePos');
		$NoTelp = $this->input->post('NoTelp');
		$NoHp = $this->input->post('NoHp');
		$Email = $this->input->post('Email');
		$NoFax = $this->input->post('NoFax');
		$RegionalLeader = $this->input->post('RegionalLeader');
		$NoHpRL = $this->input->post('NoHpRL');
		$EmailRL = $this->input->post('EmailRL');
		$SAM = $this->input->post('SAM');
		$VPRM = $this->input->post('VPRM');

		$RegionalKodePerusahaan = $this->input->post('perusahaan');
		$data = $this->m_perusahaan->get_perusahaan_byid($RegionalKodePerusahaan);
		$q = $data->row_array();
		$RegionalNamaPerusahaan = $q['NamaPerusahaan'];

		$dataregional = array(
			'KODE' => $KODE,
			'NAMA' => $NAMA,
			'Alamat' => $Alamat,
			'RegionalKabupatenId' => $RegionalKabupatenId,
			'RegionalKabupatenNama' => $RegionalKabupatenNama,
			'RegionalProvinsiId' => $RegionalProvinsiId,
			'RegionalProvinsiNama' => $RegionalProvinsiNama,
			'KodePos' => $KodePos,
			'NoTelp' => $NoTelp,
			'NoHp' => $NoHp,
			'Email' => $Email,
			'NoFax' => $NoFax,
			'RegionalLeader' => $RegionalLeader,
			'NoHpRL' => $NoHpRL,
			'EmailRL' => $EmailRL,
			'SAM' => $SAM,
			'VPRM' => $VPRM,
			'RegionalKodePerusahaan' => $RegionalKodePerusahaan,
			'RegionalNamaPerusahaan' => $RegionalNamaPerusahaan
		);

		$this->m_regional->simpan_regional($dataregional, 'refregional');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/regional');
	}

	function simpan_update_regional()
	{
		$KODE = strip_tags($this->input->post('KODE'));
		$NAMA = $this->input->post('NAMA');
		$Alamat = $this->input->post('Alamat');

		$RegionalKabupatenId = strip_tags($this->input->post('kabupaten'));
		$data = $this->m_kabupaten->get_kabupaten_byid($RegionalKabupatenId);
		$q = $data->row_array();
		$RegionalKabupatenNama = $q['KabupatenNama'];

		$RegionalProvinsiId = strip_tags($this->input->post('provinsi'));
		$data = $this->m_provinsi->get_provinsi_byid($RegionalProvinsiId);
		$q = $data->row_array();
		$RegionalProvinsiNama = $q['ProvinsiNama'];

		$KodePos = $this->input->post('KodePos');
		$NoTelp = $this->input->post('NoTelp');
		$NoHp = $this->input->post('NoHp');
		$Email = $this->input->post('Email');
		$NoFax = $this->input->post('NoFax');
		$RegionalLeader = $this->input->post('RegionalLeader');
		$NoHpRL = $this->input->post('NoHpRL');
		$EmailRL = $this->input->post('EmailRL');
		$SAM = $this->input->post('SAM');
		$VPRM = $this->input->post('VPRM');

		$RegionalKodePerusahaan = strip_tags($this->input->post('perusahaan'));
		$data = $this->m_perusahaan->get_perusahaan_byid($RegionalKodePerusahaan);
		$q = $data->row_array();
		$RegionalNamaPerusahaan = $q['NamaPerusahaan'];

		$dataregional = array(
			'KODE' => $KODE,
			'NAMA' => $NAMA,
			'Alamat' => $Alamat,
			'RegionalKabupatenId' => $RegionalKabupatenId,
			'RegionalKabupatenNama' => $RegionalKabupatenNama,
			'RegionalProvinsiId' => $RegionalProvinsiId,
			'RegionalProvinsiNama' => $RegionalProvinsiNama,
			'KodePos' => $KodePos,
			'NoTelp' => $NoTelp,
			'NoHp' => $NoHp,
			'Email' => $Email,
			'NoFax' => $NoFax,
			'RegionalLeader' => $RegionalLeader,
			'NoHpRL' => $NoHpRL,
			'EmailRL' => $EmailRL,
			'SAM' => $SAM,
			'VPRM' => $VPRM,
			'RegionalKodePerusahaan' => $RegionalKodePerusahaan,
			'RegionalNamaPerusahaan' => $RegionalNamaPerusahaan
		);

		$where = array(
			'KODE' => $KODE
		);

		$this->m_regional->update_edit_regional($where, $dataregional, 'refregional');
		echo $this->session->set_flashdata('msg', 'info');
		redirect('admin/regional');
	}

	function hapus_regional()
	{
		$KODE = strip_tags($this->input->post('KODE'));
		$this->m_regional->hapus_regional($KODE);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/regional');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_regional->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_regional_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'KODE' => $row['A'], // Insert data nis dari kolom A di excel
					'NAMA' => $row['B'],
					'Alamat' => $row['C'],
					'RegionalKabupatenId' => $row['D'],
					'RegionalKabupatenNama' => $row['E'],
					'RegionalProvinsiId' => $row['F'],
					'RegionalProvinsiNama' => $row['G'],
					'KodePos' => $row['H'],
					'NoTelp' => $row['I'],
					'NoHp' => $row['J'],
					'Email' => $row['K'],
					'RegionalLeader' => $row['L'],
					'NoHpRL' => $row['M'],
					'EmailRL' => $row['N'],
					'RegionalKodePerusahaan' => $row['O'],
					'RegionalNamaPerusahaan' => $row['P'],
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_regional->insert_multiple($data);

		redirect("admin/regional"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER REGIONAL"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:P1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Regional"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Regional");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Alamat");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Kode Kabupaten");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Nama Kabupaten");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Kode Provinsi");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "Nama Provinsi");
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Kode Pos");
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "No Telepon");
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "No HP");
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "Email");
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "Regional Leader");
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "No HP Leader");
		$excel->setActiveSheetIndex(0)->setCellValue('N3', "Email RL");
		$excel->setActiveSheetIndex(0)->setCellValue('O3', "Kode Perusahaan");
		$excel->setActiveSheetIndex(0)->setCellValue('P3', "Nama Perusahaan");

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$regional = $this->m_regional->get_all_regional2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($regional as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->KODE);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->NAMA);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->Alamat);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->RegionalKabupatenId);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->RegionalKabupatenNama);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->RegionalProvinsiId);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->RegionalProvinsiNama);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->KodePos);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->NoTelp);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->NoHp);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->Email);
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->RegionalLeader);
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->NoHpRL);
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->EmailRL);
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->RegionalKodePerusahaan);
			$excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->RegionalNamaPerusahaan);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('O')->setWidth(17);
		$excel->getActiveSheet()->getColumnDimension('P')->setWidth(18);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Regional");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Regional.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	public function ajax_regional()
	{
		$data = $this->m_regional->ajax_get_all_regional();
		echo json_encode($data->result_array());
	}

	public function select_regional()
	{
		if ($_POST) {
			$term = $_POST['term'];
			$hasil = $this->m_regional->get_select2($term);

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->KODE, "text" => $field->KODE . ' - ' . $field->NAMA, "slug" => $field->NAMA);
			}
			echo json_encode($data);
		} else {
			$hasil = $this->m_regional->get_select2('');

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->KODE, "text" => $field->KODE . ' - ' . $field->NAMA, "slug" => $field->NAMA);
			}
			echo json_encode($data);
		}
	}
}
