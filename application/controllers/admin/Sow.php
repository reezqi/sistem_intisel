<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sow extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper("url");
		$this->load->model('m_sow');
	}

	function index()
	{
		$x['data']=$this->m_sow->get_all_sow();
		$this->load->view('admin/v_sow',$x);
	}

	public function cari() 
	{
		$this->load->view('search');
	}
 
	public function hasil()
	{
		$data2['cari'] = $this->m_sow->cariPO();
		$data2['KodeCust'] = $this->m_sow->cariPO();
		$this->load->view('admin/v_result', $data2);
	}

	

}