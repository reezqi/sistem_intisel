<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vprm extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_vprm');
		$this->load->model('m_owner');
		$this->load->helper('url');
	}

	function index()
	{
		$x['mastervprm'] = $this->m_vprm->get_all_vprm()->result();

		//Membuat Kode Regional Otomatis
		$pegawai = $this->m_vprm->data();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$x['KodeOwner'] = $noUrut + 1;
		//end
		//echo json_encode($noUrut);
		$this->load->view('admin/v_vprm', $x);
	}

	function simpan_vprm()
	{
		$KodeOwner = strip_tags($this->input->post('KodeOwner'));
		$NamaOwner = $this->input->post('NamaOwner');
		$Tanggal = $this->input->post('datepicker');

		$datavprm = array(
			'KodeOwner' => $KodeOwner,
			'NamaOwner' => $NamaOwner,
			'created_at' => $Tanggal,
			'user_id' => $this->session->userdata('user_id'),
			'user_name' => $this->session->userdata('name'),
		);

		$this->m_vprm->simpan_vprm($datavprm, 'refowner');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/vprm');
	}

	function update_vprm()
	{

		$KodeOwner = strip_tags($this->input->post('KodeOwner'));
		$NamaOwner = $this->input->post('NamaOwner');
		$Tanggal = $this->input->post('datepicker');

		$datavprm = array(
			'KodeOwner' => $KodeOwner,
			'NamaOwner' => $NamaOwner,
			'created_at' => $Tanggal,
			'user_id' => $this->session->userdata('user_id'),
			'user_name' => $this->session->userdata('name'),
		);

		$where = array(
			'KodeOwner' => $KodeOwner
		);

		$this->m_vprm->update_edit_vprm($where, $datavprm, 'refowner');
		redirect('admin/vprm');
	}

	function ajax_get_vprm()
	{
		if ($_POST) {
			$KodeOwner 	= $this->input->post('KodeOwner');
			$hasil 	    = $this->m_vprm->get_where(['KodeOwner' => $KodeOwner]);

			echo json_encode($hasil->row(0));
		}
	}

	function hapus_vprm()
	{
		$KodeOwner = strip_tags($this->input->post('KODE'));
		$this->m_vprm->hapus_vprm($KodeOwner);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/vprm');
	}

	public function select_vprm()
	{
		if ($_POST) {
			$term = $_POST['term'];
			$hasil = $this->m_owner->get_select2($term);

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->KodeOwner, "text" => $field->KodeOwner . ' - ' . $field->NamaOwner, "slug" => $field->NamaOwner);
			}
			echo json_encode($data);
		} else {
			$hasil = $this->m_owner->get_select2('');

			$data = array();
			foreach ($hasil as $field) {
				$data[] = array("id" => $field->KodeOwner, "text" => $field->KodeOwner . ' - ' . $field->NamaOwner, "slug" => $field->NamaOwner);
			}
			echo json_encode($data);
		}
	}
}
