<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shoplistdetail extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_shoplistdetail');
	}

	function index()
	{
		$x['data']=$this->m_shoplistdetail->get_all_shoplistdetail();
		$this->load->view('admin/v_shoplist_detail',$x);
	}

}