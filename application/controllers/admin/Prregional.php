<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prregional extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_prregional');
		$this->load->model('m_posubcon');
		$this->load->model('m_pocustomer');
		$this->load->model('m_regional');
		$this->load->model('m_top');
		$this->load->model('m_site');
		$this->load->model('m_principal');
		$this->load->model('m_msow');
		$this->load->model('m_subcon');
		$this->load->model('m_jenispekerjaan');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$x['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$x['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		$x['refsubcon'] = $this->m_subcon->get_all_subcon2();

		$this->load->view('admin/transaksi/v_prregional', $x);
	}

	function add_prregional()
	{
		$data['refsubcon'] = $this->m_subcon->get_all_subcon2();
		$data['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$data['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		$pegawai = $this->m_prregional->nourutpo();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$noUrut = (int) substr($noUrut, 25, 30);
		$data['NoUrutPo'] =  sprintf("%05s", $noUrut + 1);


		$pegawai1 = $this->m_pocustomer->nourutpo();
		if (empty($pegawai1) || is_null($pegawai1)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai1->no_urut;
		}

		$noUrut = (int) substr($noUrut, 35, 35);
		$data['NoUrutPoIn'] =  sprintf("%05s", $noUrut + 1);


		$this->load->view('admin/transaksi/v_add_prregional', $data);
	}

	function view_prregional_detail()
	{
		$NoPr = $this->uri->segment('4');

		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		$data['refregional'] = $this->m_regional->get_all_regional3();
		$data['refsubcon'] = $this->m_subcon->get_all_subcon3();

		$data['data1'] = $this->m_prregional->get_prregional_by_kode(base64_decode($NoPr));
		$data['reftxtposed'] = $this->m_prregional->get_prregional_by_kode2(base64_decode($NoPr));
		$this->load->view('admin/transaksi/v_view_prregional_detail', $data);
	}

	function delete_prregional()
	{
		$NoPr = strip_tags($this->input->post('NoPr'));

		$this->m_prregional->delete_prregional($NoPr);
		$this->m_prregional->delete_prregional_detail($NoPr);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/prregional');
	}

	public function save_prregional()
	{
		if ($_POST) {
			if (!empty($_POST['kode_barang'])) {

				$NoPr     = $this->input->post('NoPr');
				$KodeCustomer     = $this->input->post('KodeCustomer');
				$data = $this->m_principal->get_principal_byid($KodeCustomer);
				$q = $data->row_array();
				$NamaCustomer = $q['NamaPrincipal'];
				$KodeSubcon     = $this->input->post('KodeSubcon');
				$data = $this->m_subcon->get_subcon_byid($KodeSubcon);
				$q = $data->row_array();
				$NamaSubcon = $q['Nama'];
				$KodeRegional     = $this->input->post('KodeRegional');
				$data = $this->m_regional->get_regional_byid($KodeRegional);
				$q = $data->row_array();
				$NamaRegional = $q['NAMA'];
				$datepicker     = $this->input->post('datepicker');
				$datepicker1     = $this->input->post('datepicker1');
				$AtasPermin     = $this->input->post('AtasPermin');
				$KodeSite     = $this->input->post('KodeSite');
				$NamaSite     = $this->input->post('NamaSite');
				$Keterangan    = $this->input->post('Keterangan');
				$NoPoIn1    = $this->input->post('NoPoIn1');
				$grand_total    = $this->input->post('grand_total');
				$Sysdate = date('Y-m-d');

				$datatrxprregionalh = array(
					'TipePo' => 2,
					'NoPr' => $NoPr,
					'NoPoIn' => $NoPoIn1,
					'TglPo' => $datepicker,
					'TglPermin' => $datepicker1,
					'AtasPermin' => $AtasPermin,
					'KodeSubcon' => $KodeSubcon,
					'NamaSubcon' => $NamaSubcon,
					'KodeCustomer' => $KodeCustomer,
					'NamaCustomer' => $NamaCustomer,
					'KodeRegional' => $KodeRegional,
					'NamaRegional' => $NamaRegional,
					'KodeSite' => $KodeSite,
					'NamaSite' => $NamaSite,
					'Keterangan' => $Keterangan,
					'TotalBayar' => $grand_total,
					'Sysdate' => $Sysdate,
				);

				$this->m_prregional->save_prregional($datatrxprregionalh, 'trxprregionalh');

				$inserted    = 0;
				$no_array    = 0;

				foreach ($_POST['kode_barang'] as $k) {

					if (!empty($k)) {
						$kode_barang 	= $_POST['kode_barang'][$no_array];
						$nama_barang 	= $_POST['nama_barang'][$no_array];
						$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];
						$harga_satuan 	= $_POST['harga_satuan'][$no_array];
						$sub_total 		= $_POST['sub_total'][$no_array];

						$datatrxprregionald = array(
							'TipePo' => 2,
							'NoPr' => $NoPr,
							'NoPoIn' => $NoPoIn1,
							'TglPo' => $datepicker,
							'KodeSubcon' => $KodeSubcon,
							'NamaSubcon' => $NamaSubcon,
							'KodeCustomer' => $KodeCustomer,
							'NamaCustomer' => $NamaCustomer,
							'KodeRegional' => $KodeRegional,
							'NamaRegional' => $NamaRegional,
							'KodeSite' => $KodeSite,
							'NamaSite' => $NamaSite,
							'Sysdate' => $Sysdate,
							'KodeSOW' => $kode_barang,
							'NamaSOW' => $nama_barang,
							'Qty' => $jumlah_beli,
							'UnitPrice' => $harga_satuan,
							'TotalBayar' => $sub_total,
						);

						$this->m_prregional->save_prregional_detail($datatrxprregionald, 'trxprregionald');

						$inserted++;
					}
					$no_array++;
				}

				if ($inserted > 0) {
					echo json_encode(array('status' => 1));
				} else {
					$this->query_error();
				}
			} else {
				$this->load->view('admin/posubcon');
			}
		}
	}

	public function cetakpopdf()
	{
		$NoPr = $this->uri->segment(4);

		$a['datacetakpopdf']	= $this->db->query("SELECT * FROM trxprregionald WHERE NoPr = '$NoPr'")->row();
		$a['reftxtprregionald'] = $this->m_prregional->get_prregional_by_kode3(base64_decode($NoPr));
		$this->load->view('admin/transaksi/v_print_prregional_pdf', $a);
	}

	function view_prregional_edit()
	{
		$NoPr = $this->uri->segment('4');

		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		$data['refregional'] = $this->m_regional->get_all_regional3();
		$data['refsubcon'] = $this->m_subcon->get_all_subcon3();

		$data['data1'] = $this->m_prregional->get_prregional_by_kode(base64_decode($NoPr));
		$data['reftxtposed'] = $this->m_prregional->get_prregional_by_kode2(base64_decode($NoPr));
		$this->load->view('admin/transaksi/v_view_prregional_edit', $data);
	}

	public function ajax_prregional_edit() 
	{
		if($this->input->is_ajax_request())
		{
			$NoPr = $this->input->post('no');
			
			$data['data'] = $this->m_prregional->get_prregional_by_kode(base64_decode($NoPr))->result();
			$data['reftxtposed'] = $this->m_prregional->get_prregional_by_kode2(base64_decode($NoPr));
			echo json_encode($data);
		}
	}

	public function ajax_prregional_edit_no_encode() 
	{
		if($this->input->is_ajax_request())
		{
			$NoPr = $this->input->post('no');
			
			$data['data'] = $this->m_prregional->get_prregional_by_kode($NoPr)->result();
			$data['reftxtposed'] = $this->m_prregional->get_prregional_by_kode2($NoPr);
			echo json_encode($data);
		}
	}

	public function submit_prregional_edit()
	{
		if ($_POST) {
			if (!empty($_POST['kode_barang'])) {

				$NoLama     = $this->input->post('NoLama');
				$NoPr     = $this->input->post('NoPr');
				$KodeCustomer     = $this->input->post('KodeCustomer');
				$data = $this->m_principal->get_principal_byid($KodeCustomer);
				$q = $data->row_array();
				$NamaCustomer = $q['NamaPrincipal'];
				$KodeSubcon     = $this->input->post('KodeSubcon');
				$data = $this->m_subcon->get_subcon_byid($KodeSubcon);
				$q = $data->row_array();
				$NamaSubcon = $q['Nama'];
				$KodeRegional     = $this->input->post('KodeRegional');
				$data = $this->m_regional->get_regional_byid($KodeRegional);
				$q = $data->row_array();
				$NamaRegional = $q['NAMA'];
				$datepicker     = $this->input->post('datepicker');
				$datepicker1     = $this->input->post('datepicker1');
				$AtasPermin     = $this->input->post('AtasPermin');
				$KodeSite     = $this->input->post('KodeSite');
				$NamaSite     = $this->input->post('NamaSite');
				$Keterangan    = $this->input->post('Keterangan');
				$NoPoIn1    = $this->input->post('NoPoIn1');
				$grand_total    = $this->input->post('grand_total');
				$Sysdate = date('Y-m-d');

				$datatrxprregionalh = array(
					'TipePo' => 2,
					'NoPr' => $NoPr,
					'NoPoIn' => $NoPoIn1,
					'TglPo' => $datepicker,
					'TglPermin' => $datepicker1,
					'AtasPermin' => $AtasPermin,
					'KodeSubcon' => $KodeSubcon,
					'NamaSubcon' => $NamaSubcon,
					'KodeCustomer' => $KodeCustomer,
					'NamaCustomer' => $NamaCustomer,
					'KodeRegional' => $KodeRegional,
					'NamaRegional' => $NamaRegional,
					'KodeSite' => $KodeSite,
					'NamaSite' => $NamaSite,
					'Keterangan' => $Keterangan,
					'TotalBayar' => $grand_total,
					'Sysdate' => $Sysdate,
				);
				
				$this->m_prregional->save_edit_prregional(['NoPr' => $NoLama], $datatrxprregionalh, 'trxprregionalh');
				$this->m_prregional->delete_prregional_detail($NoLama);
				$inserted    = 0;
				$no_array    = 0;

				foreach ($_POST['kode_barang'] as $k) {

					if (!empty($k)) {
						$kode_barang 	= $_POST['kode_barang'][$no_array];
						$nama_barang 	= $_POST['nama_barang'][$no_array];
						$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];
						$harga_satuan 	= $_POST['harga_satuan'][$no_array];
						$sub_total 		= $_POST['sub_total'][$no_array];

						$datatrxprregionald = array(
							'TipePo' => 2,
							'NoPr' => $NoPr,
							'NoPoIn' => $NoPoIn1,
							'TglPo' => $datepicker,
							'KodeSubcon' => $KodeSubcon,
							'NamaSubcon' => $NamaSubcon,
							'KodeCustomer' => $KodeCustomer,
							'NamaCustomer' => $NamaCustomer,
							'KodeRegional' => $KodeRegional,
							'NamaRegional' => $NamaRegional,
							'KodeSite' => $KodeSite,
							'NamaSite' => $NamaSite,
							'Sysdate' => $Sysdate,
							'KodeSOW' => $kode_barang,
							'NamaSOW' => $nama_barang,
							'Qty' => $jumlah_beli,
							'UnitPrice' => $harga_satuan,
							'TotalBayar' => $sub_total,
						);

						$this->m_prregional->save_prregional_detail($datatrxprregionald, 'trxprregionald');

						$inserted++;
					}
					$no_array++;
				}

				if ($inserted > 0) {
					echo json_encode(array('status' => 1));
				} else {
					$this->query_error();
				}
			} else {
				$this->load->view('admin/posubcon');
			}
		}
	}

	function ajax_table_prregional()
    {
        $list = $this->m_prregional->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->NoPr;
            $row[] = $field->TglPo;
            $row[] = $field->NamaSubcon;
            $row[] = $field->NamaCustomer;
            $row[] = $field->NamaRegional;
			$row[] = "
				<a class='badge' href='". base_url() . "admin/prregional/view_prregional_edit/" . base64_encode($field->NoPr) ."' style='background-color:blue;' title='Edit PO'><span class='fa fa-edit'></span></a>
				<a class='badge' id='hapus' data-customer='". $field->NamaCustomer ."' data-hapus='". $field->NoPr ."' data-toggle='modal' style='background-color:red;' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_prregional->count_all(),
            "recordsFiltered" => $this->m_prregional->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
	}

}
