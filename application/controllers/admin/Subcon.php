<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Subcon extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_subcon');
		$this->load->model('m_bank');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('m_provinsi');
		$this->load->model('m_kabupaten');
		$this->load->model('m_regional');
	}

	function index()
	{
		$x['provinsi'] = $this->m_regional->get_provinsi()->result();
		$x['refsubcon'] = $this->m_subcon->get_all_subcon()->result();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['bank'] = $this->m_bank->get_bank()->result();

		$pegawai = $this->m_subcon->data();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$noUrut = (int) substr($noUrut, 3, 3);
		$x['KodeSuppl'] = '00.' . sprintf("%02s", $noUrut + 1);

		$this->load->view('admin/v_subcon', $x);
	}

	function simpan_subcon()
	{
		$KodeRegional = strip_tags($this->input->post('Regional'));
		$SubconJenis = $this->input->post('JenisSubcon');
		$KodeSuppl = $this->input->post('KodeSuppl');
		$Nama = $this->input->post('Nama');
		$Alamat = $this->input->post('Alamat');

		$SubconKabupatenId = strip_tags($this->input->post('kabupaten'));
		$data = $this->m_kabupaten->get_kabupaten_byid_subcon($SubconKabupatenId);
		$q = $data->row_array();
		$SubconKabupatenNama = $q['KabupatenNama'];

		$SubconProvinsiId = strip_tags($this->input->post('provinsi'));
		$data = $this->m_provinsi->get_provinsi_byid_subcon($SubconProvinsiId);
		$q = $data->row_array();
		$SubconProvinsiNama = $q['ProvinsiNama'];

		$Kodepos = $this->input->post('Kodepos');
		$Notelp = $this->input->post('Notelp');
		$Nohp = $this->input->post('Nohp');
		$PIC = $this->input->post('PIC');
		$Email = $this->input->post('Email');

		$SubconBankId = strip_tags($this->input->post('bank'));
		$data = $this->m_bank->get_bank_byid_subcon($SubconBankId);
		$q = $data->row_array();
		$SubconBankNama = $q['NamaBank'];

		$Norek = $this->input->post('Norek');
		$NPWP = $this->input->post('NPWP');
		$SubconLeader = $this->input->post('SubconLeader');

		$AreaKerja1 = $this->input->post('AreaKerja1');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja1);
		$q = $data->row_array();
		$AreaKerja1Nama = $q['ProvinsiNama'];

		$AreaKerja2 = $this->input->post('AreaKerja2');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja2);
		$q = $data->row_array();
		$AreaKerja2Nama = $q['ProvinsiNama'];

		$AreaKerja3 = $this->input->post('AreaKerja3');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja3);
		$q = $data->row_array();
		$AreaKerja3Nama = $q['ProvinsiNama'];

		$AreaKerja4 = $this->input->post('AreaKerja4');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja3);
		$q = $data->row_array();
		$AreaKerja4Nama = $q['ProvinsiNama'];

		$AreaKerja5 = $this->input->post('AreaKerja5');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja5);
		$q = $data->row_array();
		$AreaKerja5Nama = $q['ProvinsiNama'];

		$datasubcon = array(
			'KodeRegional' => $KodeRegional,
			'SubconJenis' => $SubconJenis,
			'KodeSuppl' => $KodeSuppl,
			'Nama' => $Nama,
			'Alamat' => $Alamat,
			'SubconKabupatenId' => $SubconKabupatenId,
			'SubconKabupatenNama' => $SubconKabupatenNama,
			'SubconProvinsiId' => $SubconProvinsiId,
			'SubconProvinsiNama' => $SubconProvinsiNama,
			'Kodepos' => $Kodepos,
			'Notelp' => $Notelp,
			'Nohp' => $Nohp,
			'PIC' => $PIC,
			'Email' => $Email,
			'SubconBankId' => $SubconBankId,
			'SubconBankNama'  => $SubconBankNama,
			'Norek' => $Norek,
			'NPWP' => $NPWP,
			'SubconLeader' => $SubconLeader,
			'AreaKerja1' => $AreaKerja1,
			'AreaKerja1Nama' => $AreaKerja1Nama,
			'AreaKerja2' => $AreaKerja2,
			'AreaKerja2Nama' => $AreaKerja2Nama,
			'AreaKerja3' => $AreaKerja3,
			'AreaKerja3Nama' => $AreaKerja3Nama,
			'AreaKerja4' => $AreaKerja4,
			'AreaKerja4Nama' => $AreaKerja4Nama,
			'AreaKerja5' => $AreaKerja5,
			'AreaKerja5Nama' => $AreaKerja5Nama
		);

		$this->m_subcon->simpan_subcon($datasubcon, 'refsubcon');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/subcon');
	}

	function simpan_update_subcon()
	{
		$KodeRegional = strip_tags($this->input->post('regional'));
		$SubconJenis = $this->input->post('JenisSubcon');
		$KodeSuppl = $this->input->post('KodeSuppl');
		$Nama = $this->input->post('Nama');
		$Alamat = $this->input->post('Alamat');

		$SubconKabupatenId = strip_tags($this->input->post('kabupaten'));
		$data = $this->m_kabupaten->get_kabupaten_byid_subcon($SubconKabupatenId);
		$q = $data->row_array();
		$SubconKabupatenNama = $q['KabupatenNama'];

		$SubconProvinsiId = strip_tags($this->input->post('provinsi'));
		$data = $this->m_provinsi->get_provinsi_byid_subcon($SubconProvinsiId);
		$q = $data->row_array();
		$SubconProvinsiNama = $q['ProvinsiNama'];

		$Kodepos = $this->input->post('Kodepos');
		$Notelp = $this->input->post('Notelp');
		$Nohp = $this->input->post('Nohp');
		$PIC = $this->input->post('PIC');
		$Email = $this->input->post('Email');

		$SubconBankId = strip_tags($this->input->post('bank'));
		$data = $this->m_bank->get_bank_byid_subcon($SubconBankId);
		$q = $data->row_array();
		$SubconBankNama = $q['NamaBank'];

		$Norek = $this->input->post('Norek');
		$NPWP = $this->input->post('NPWP');
		$SubconLeader = $this->input->post('SubconLeader');

		$AreaKerja1 = $this->input->post('AreaKerja1');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja1);
		$q = $data->row_array();
		$AreaKerja1Nama = $q['ProvinsiNama'];

		$AreaKerja2 = $this->input->post('AreaKerja2');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja2);
		$q = $data->row_array();
		$AreaKerja2Nama = $q['ProvinsiNama'];

		$AreaKerja3 = $this->input->post('AreaKerja3');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja3);
		$q = $data->row_array();
		$AreaKerja3Nama = $q['ProvinsiNama'];

		$AreaKerja4 = $this->input->post('AreaKerja4');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja3);
		$q = $data->row_array();
		$AreaKerja4Nama = $q['ProvinsiNama'];

		$AreaKerja5 = $this->input->post('AreaKerja5');
		$data = $this->m_provinsi->get_provinsi_byid_subcon($AreaKerja5);
		$q = $data->row_array();
		$AreaKerja5Nama = $q['ProvinsiNama'];

		$datasubcon = array(
			'KodeRegional' => $KodeRegional,
			'SubconJenis' => $SubconJenis,
			'KodeSuppl' => $KodeSuppl,
			'Nama' => $Nama,
			'Alamat' => $Alamat,
			'SubconKabupatenId' => $SubconKabupatenId,
			'SubconKabupatenNama' => $SubconKabupatenNama,
			'SubconProvinsiId' => $SubconProvinsiId,
			'SubconProvinsiNama' => $SubconProvinsiNama,
			'Kodepos' => $Kodepos,
			'Notelp' => $Notelp,
			'Nohp' => $Nohp,
			'PIC' => $PIC,
			'Email' => $Email,
			'SubconBankId' => $SubconBankId,
			'SubconBankNama'  => $SubconBankNama,
			'Norek' => $Norek,
			'NPWP' => $NPWP,
			'SubconLeader' => $SubconLeader,
			'AreaKerja1' => $AreaKerja1,
			'AreaKerja1Nama' => $AreaKerja1Nama,
			'AreaKerja2' => $AreaKerja2,
			'AreaKerja2Nama' => $AreaKerja2Nama,
			'AreaKerja3' => $AreaKerja3,
			'AreaKerja3Nama' => $AreaKerja3Nama,
			'AreaKerja4' => $AreaKerja4,
			'AreaKerja4Nama' => $AreaKerja4Nama,
			'AreaKerja5' => $AreaKerja5,
			'AreaKerja5Nama' => $AreaKerja5Nama
		);

		$where = array(
			'KodeSuppl' => $KodeSuppl
		);

		$this->m_subcon->update_edit_subcon($where, $datasubcon, 'refsubcon');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/subcon');
	}

	function hapus_subcon()
	{
		$KodeSuppl = strip_tags($this->input->post('KodeSuppl'));
		$this->m_subcon->hapus_subcon($KodeSuppl);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/subcon');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_subcon->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_subcon_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'KodeRegional' => $row['A'], // Insert data nis dari kolom A di excel
					'SubconJenis' => $row['B'], // Insert data nama dari kolom B di excel
					'KodeSuppl' => $row['C'],
					'Nama' => $row['D'],
					'Alamat' => $row['E'],
					'SubconKabupatenId' => $row['F'],
					'SubconKabupatenNama' => $row['G'],
					'SubconProvinsiId' => $row['H'],
					'SubconProvinsiNama' => $row['I'],
					'Kodepos' => $row['J'],
					'Notelp' => $row['K'],
					'Nohp' => $row['L'],
					'PIC' => $row['M'],
					'Email' => $row['N'],
					'SubconBankId' => $row['O'],
					'SubconBankNama' => $row['P'],
					'Norek' => $row['Q'],
					'NPWP' => $row['R'],
					'SubconLeader' => $row['R'],
					'AreaKerja1' => $row['T'],
					'AreaKerja2' => $row['U'],
					'AreaKerja3' => $row['V'],
					'AreaKerja4' => $row['W'],
					'AreaKerja5' => $row['X']
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_subcon->insert_multiple($data);

		redirect("admin/subcon"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER SUBCON"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:X1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Regional"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Jenis Subcon");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Kode Subcon");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Nama");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Alamat");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Kode Kota");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "Kota");
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Kode Provinsi");
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "Provinsi ");
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "Kode Pos");
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "No Telpon");
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "No HP");
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "PIC");
		$excel->setActiveSheetIndex(0)->setCellValue('N3', "Email");
		$excel->setActiveSheetIndex(0)->setCellValue('O3', "Kode Bank");
		$excel->setActiveSheetIndex(0)->setCellValue('P3', "Bank");
		$excel->setActiveSheetIndex(0)->setCellValue('Q3', "No Rekening");
		$excel->setActiveSheetIndex(0)->setCellValue('R3', "NPWP");
		$excel->setActiveSheetIndex(0)->setCellValue('S3', "Subcon Leader");
		$excel->setActiveSheetIndex(0)->setCellValue('T3', "Kode Area Kerja 1");
		$excel->setActiveSheetIndex(0)->setCellValue('U3', "Kode Area Kerja 2");
		$excel->setActiveSheetIndex(0)->setCellValue('V3', "Kode Area Kerja 3");
		$excel->setActiveSheetIndex(0)->setCellValue('W3', "Kode Area Kerja 4");
		$excel->setActiveSheetIndex(0)->setCellValue('X3', "Kode Area Kerja 5");

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('S3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('T3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('U3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('V3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('W3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('X3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$subcon = $this->m_subcon->get_all_subcon2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($subcon as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->KodeRegional);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->SubconJenis);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->KodeSuppl);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->Nama);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->Alamat);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->SubconKabupatenId);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->SubconKabupatenNama);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->SubconProvinsiId);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->SubconProvinsiNama);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->Kodepos);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->Notelp);
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->Nohp);
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->PIC);
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->Email);
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->SubconBankId);
			$excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->SubconBankNama);
			$excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $data->Norek);
			$excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $data->NPWP);
			$excel->setActiveSheetIndex(0)->setCellValue('S' . $numrow, $data->SubconLeader);
			$excel->setActiveSheetIndex(0)->setCellValue('T' . $numrow, $data->AreaKerja1);
			$excel->setActiveSheetIndex(0)->setCellValue('U' . $numrow, $data->AreaKerja2);
			$excel->setActiveSheetIndex(0)->setCellValue('V' . $numrow, $data->AreaKerja3);
			$excel->setActiveSheetIndex(0)->setCellValue('W' . $numrow, $data->AreaKerja4);
			$excel->setActiveSheetIndex(0)->setCellValue('X' . $numrow, $data->AreaKerja5);

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('R' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('S' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('T' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('U' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('V' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('W' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('X' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(60);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('N')->setWidth(35);
		$excel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('S')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Subcon");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Subcon.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	function get_edit()
	{
		$KodeSuppl = $this->uri->segment(4);
		$data['data1'] = $this->m_subcon->get_subcon_by_kode($KodeSuppl);
		$data['provinsi'] = $this->m_provinsi->get_all_provinsi();
		$data['kabupaten'] = $this->m_kabupaten->get_all_kabupaten();
		$data['bank'] = $this->m_bank->get_all_bank();
		$data['regional'] = $this->m_regional->get_all_regional();

		$this->load->view('admin/v_edit_subcon', $data);
	}

	function get_data_edit()
	{
		$KodeRegional = $this->input->post('KodeRegional', TRUE);
		$data = $this->m_regional->get_subcon_by_id($KodeRegional)->result();
		echo json_encode($data);
	}
}
