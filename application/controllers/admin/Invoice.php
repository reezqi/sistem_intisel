<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invoice extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_principal');
        $this->load->model('m_regional');
        $this->load->model('m_invoice');
    }

    public function index()
    {
        $data['refprincipal'] = $this->m_principal->get_all_principal2();
        if ($this->session->userdata('is_admin') === TRUE) {
            $data['refregional'] = $this->m_regional->get_all_regional2();
        } else {
            $data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
        }

        $this->load->view('admin/transaksi/v_invoice', $data);
    }

    function ajax_table_invoice()
    {
        $list = $this->m_invoice->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $this->db->select('Progress');
            $this->db->from('trxpdp');
            $this->db->where('NoPo', $field->NoPo);
            $this->db->order_by('Progress', 'DESC');
            $this->db->limit(1);
            $hasil = $this->db->get()->row(0);

            $progres = '<div class="progress"><div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: ' . $hasil->Progress . '%" aria-valuenow="' . $hasil->Progress . '" aria-valuemin="0" aria-valuemax="100">' . $hasil->Progress . '</div></div>';


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->NoPo;
            $row[] = $field->NoPoIn;
            $row[] = $field->PoDate;
            $row[] = $field->ProjectName;
            $row[] = $field->NamaRegional;
            $row[] = $field->NamaSite;
            $row[] = $field->Lineno;
            $row[] = $progres;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_invoice->count_all(),
            "recordsFiltered" => $this->m_invoice->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
