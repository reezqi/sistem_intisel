<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profit extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_profit');
		$this->load->helper('url');
	}

	function index()
	{
		$x['reprofit']=$this->m_profit->get_all_profit()->result();
		$this->load->view('admin/v_profit',$x);
	}

	function simpan_profit(){
		$kodereg=strip_tags($this->input->post('kodereg'));
		$namaregional=$this->input->post('namaregional');
		$profit=$this->input->post('profit');
		$sysdate=$this->input->post('sysdate');
		$userid=$this->input->post('userid');
		$compname=$this->input->post('compname');

		$dataprofit = array(
			'kodereg' => $kodereg,
			'namaregional' => $namaregional,
			'profit' => $profit,
			'sysdate' => $sysdate,
			'userid' => $userid,
			'compname' => $compname
		);

		$this->m_profit->simpan_profit($dataprofit, 'reprofit');
		echo $this->session->set_flashdata('msg','success');
		redirect('admin/profit');
	}

	function simpan_update_profit(){
		$kodereg=$this->input->post('kodereg');
		$namaregional=$this->input->post('namaregional');
		$profit=$this->input->post('profit');
		$sysdate=$this->input->post('sysdate');
		$userid=$this->input->post('userid');
		$compname=$this->input->post('compname');

		$dataprofit = array(
			'kodereg' => $kodereg,
			'namaregional' => $namaregional,
			'profit' => $profit,
			'sysdate' => $sysdate,
			'userid' => $userid,
			'compname' => $compname
		);

		$where = array (
			'kodereg' => $kodereg
		);

		$this->m_profit->update_edit_profit($where,$dataprofit, 'reprofit');
		echo $this->session->set_flashdata('msg','success');
		redirect('admin/profit');
	}

	function hapus_profit(){
		$kodereg=strip_tags($this->input->post('kodereg'));
		$this->m_profit->hapus_profit($kodereg);

		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/profit');
	}

}