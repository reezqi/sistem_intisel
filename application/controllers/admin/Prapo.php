<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Prapo extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_prapo');
		$this->load->model('m_prregional');
		$this->load->model('m_posubcon');
		$this->load->model('m_pocustomer');
		$this->load->model('m_regional');
		$this->load->model('m_top');
		$this->load->model('m_site');
		$this->load->model('m_principal');
		$this->load->model('m_msow');
		$this->load->model('m_subcon');
		$this->load->model('m_jenispekerjaan');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refsubcon'] = $this->m_subcon->get_all_subcon2();

		$x['prapo'] = $this->m_prapo->searching();
		$this->load->view('admin/transaksi/v_prapo', $x);
	}

	public function save_prapo()
	{
		if ($_POST) {
			if (!empty($_POST['nopr'])) {

				$Sysdate = date('Y-m-d');

				$inserted    = 0;
				$no_array    = 0;
				foreach ($_POST['nopr'] as $k) {
					if (!empty($k)) {
						$nopr     = $_POST['nopr'][$no_array];
						$NoPo     = $_POST['NoPo'][$no_array];
						$NoDokumen     = $_POST['NoDokumen'][$no_array];
						$TglPr     = $_POST['TglPr'][$no_array];
						$KodeSubcon     = $_POST['KodeSubcon'][$no_array];
						$NamaSubcon     = $_POST['NamaSubcon'][$no_array];
						$KodeCustomer     = $_POST['KodeCustomer'][$no_array];
						$NamaCustomer     = $_POST['NamaCustomer'][$no_array];
						$KodeRegional     = $_POST['KodeRegional'][$no_array];
						$NamaRegional     = $_POST['NamaRegional'][$no_array];
						$KodeSite     = $_POST['KodeSite'][$no_array];
						$NamaSite     = $_POST['NamaSite'][$no_array];
						$KodeSOW     = $_POST['KodeSOW'][$no_array];

						$datatrxprapo = array(
							'TipePo' => 2,
							'NoPr' => $nopr,
							'NoPo' => $NoPo,
							'NoDokumen' => $NoDokumen,
							'TglPo' => $TglPr,
							'KodeSubcon' => $KodeSubcon,
							'NamaSubcon' => $NamaSubcon,
							'KodeCustomer' => $KodeCustomer,
							'NamaCustomer' => $NamaCustomer,
							'KodeRegional' => $KodeRegional,
							'NamaRegional' => $NamaRegional,
							'KodeSite' => $KodeSite,
							'NamaSite' => $NamaSite,
							'KodeSOW' => $KodeSOW,

							'Sysdate' => $Sysdate,

						);

						$this->m_prapo->save_prapo($datatrxprapo, 'trxpocush');
						$inserted++;
					}
					$no_array++;
				}
				if ($inserted > 0) {
					echo $this->session->set_flashdata('msg', 'success');
					redirect('admin/prapo');
				} else {
					$this->query_error();
				}
			} else {
				echo $this->session->set_flashdata('msg', 'peringatan');
				redirect('admin/prapo');
			}
		}
	}
}
