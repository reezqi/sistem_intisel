<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posubcon extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_prregional');
		$this->load->model('m_posubcon');
		$this->load->model('m_pocustomer');
		$this->load->model('m_regional');
		$this->load->model('m_top');
		$this->load->model('m_site');
		$this->load->model('m_principal');
		$this->load->model('m_msow');
		$this->load->model('m_subcon');
		$this->load->model('m_jenispekerjaan');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$x['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$x['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}
		$x['refsubcon'] = $this->m_subcon->get_all_subcon2();
		$x['refsite'] = $this->m_site->get_all_site2();
		
		$this->load->view('admin/transaksi/v_posubcon', $x);

	}

	function add_posubcon()
	{
		$data['refsubcon'] = $this->m_subcon->get_all_subcon2();
		$data['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$data['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}
		$data['reftop'] = $this->m_top->get_all_top2();

		$pegawai = $this->m_posubcon->nourutpo();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$noUrut = (int) substr($noUrut, 26, 30);
		$data['NoUrutPo'] =  sprintf("%05s", $noUrut + 1);

		$data['poin'] = $this->m_posubcon->get_po_customer();
		$this->load->view('admin/transaksi/v_add_posubcon', $data);
	}

	function view_posubcon_detail()
	{
		$NoPo = $this->uri->segment('4');

		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		$data['refregional'] = $this->m_regional->get_all_regional3();
		$data['refsubcon'] = $this->m_subcon->get_all_subcon3();
		$data['reftop'] = $this->m_top->get_all_top3();

		$data['data1'] = $this->m_posubcon->get_posubcon_by_kode(base64_decode($NoPo));
		$data['reftxtposed'] = $this->m_posubcon->get_posubcon_by_kode2(base64_decode($NoPo));
		$this->load->view('admin/transaksi/v_view_posubcon_detail', $data);
	}

	function delete_posubcon()
	{
		$NoPo = strip_tags($this->input->post('NoPo'));

		$this->m_posubcon->delete_posubcon($NoPo);
		$this->m_posubcon->delete_posubcon_detail($NoPo);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/posubcon');
	}

	function delete_posubcon_detail()
	{
		$Lineno = $_POST['Lineno'];
		$NoPo = strip_tags($this->input->post('NoPo'));

		$this->m_posubcon->delete_posubcon_detail_multi($Lineno);
		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/posubcon/view_posubcon_detail/' . $NoPo);
	}

	public function save_posubcon()
	{
		if ($_POST) {
			if (!empty($_POST['kode_barang'])) {

				$NoPo 		= $this->input->post('NoPo');
				$NoPoIn 	= $this->input->post('NoPoIn');
				$datepicker 	= $this->input->post('datepicker');
				$datepicker2 	= $this->input->post('datepicker2');

				$KodeSubcon 	= $this->input->post('KodeSubcon');
				if (!empty($KodeSubcon))
				{
					$data = $this->m_subcon->get_subcon_byid($KodeSubcon);
					$q = $data->row_array();
					$NamaSubcon = $q['Nama'];
				} else {
					$NamaSubcon = '';
				}
			

				$TotalTermin 	= $this->input->post('TotalTermin');
				
				$KodeCustomer 	= $this->input->post('KodeCustomer');
				if (!empty($KodeCustomer)){
					$data = $this->m_principal->get_principal_byid($KodeCustomer);
					$q = $data->row_array();
					$NamaCustomer = $q['NamaPrincipal'];
				} else {
					$NamaCustomer = '';
				}

				$KodeRegional 	= $this->input->post('KodeRegional');
				$data = $this->m_regional->get_regional_byid($KodeRegional);
				$q = $data->row_array();
				$NamaRegional = $q['NAMA'];
				$KodeSite 	= $this->input->post('KodeSite');
				$NamaSite 	= $this->input->post('NamaSite');
				$grand_total	= $this->input->post('grand_total');
				$Sysdate = date('Y-m-d');

				$datatrxposubh = array(
					'TipePo' => 3,
					'NoPo' => $NoPo,
					'NoPoIn' => $NoPoIn,
					'TglPo' => $datepicker,
					'TglAkhirPo' => $datepicker2,
					'KodeSubcon' => $KodeSubcon,
					'NamaSubcon' => $NamaSubcon,
					'KodeCustomer' => $KodeCustomer,
					'NamaCustomer' => $NamaCustomer,
					'KodeRegional' => $KodeRegional,
					'NamaRegional' => $NamaRegional,
					'KodeSite' => $KodeSite,
					'NamaSite' => $NamaSite,
					'TotalTermin' => $TotalTermin,
					'TotalBayar' => $grand_total,
					'Sysdate' => $Sysdate,

				);

				$this->m_posubcon->save_posubcon($datatrxposubh, 'trxposeh');

				$inserted	= 0;

				$no_array	= 0;
				foreach ($_POST['kode_barang'] as $key=>$k) {
					if (!empty($k)) {

							$ppn = "";
							$pph = "";
							if (isset($_POST['jumlah_beli_ppn' . $key])) {
								$ppn = 1;
							} else {
								$ppn = 0;
							}
			
							if (isset($_POST['jumlah_beli_pph' . $key])) {
								$pph = 1;
							} else {
								$pph = 0;
							}

						$kode_barang 	= $_POST['kode_barang'][$no_array];
						$nama_barang1 	= $_POST['nama_barang1'][$no_array];
						$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];
						$harga_satuan 	= $_POST['harga_satuan'][$no_array];
						$sub_total 		= $_POST['sub_total'][$no_array];

						$datatrxposubd = array(
							'TipePo' => 3,
							'NoPo' => $NoPo,
							'NoPoIn' => $NoPoIn,
							'TglPo' => $datepicker,
							'KodeSubcon' => $KodeSubcon,
							'NamaSubcon' => $NamaSubcon,
							'KodeCustomer' => $KodeCustomer,
							'NamaCustomer' => $NamaCustomer,
							'KodeRegional' => $KodeRegional,
							'NamaRegional' => $NamaRegional,
							'KodeSite' => $KodeSite,
							'NamaSite' => $NamaSite,
							'Sysdate' => $Sysdate,

							'KodeSOW' => $kode_barang,
							'NamaSOW' => $nama_barang1,
							'Qty' => $jumlah_beli,
							'UnitPrice' => $harga_satuan,
							'TotalBayar' => $sub_total,
							'pph'	=> $pph,
							'ppn'	=> $ppn,

						);

						$this->m_posubcon->save_posubcon_detail($datatrxposubd, 'trxposed');
						$inserted++;
					}
					$no_array++;
				}

				if ($inserted > 0) {
					echo json_encode(array('status' => 1));
				} 
			} else {
				$this->load->view('admin/posubcon');
			}
		}
	}

	function save_edit_posubcon()
	{
		$NoPo = strip_tags($this->input->post('NoPO'));
		$TglPo 	= $this->input->post('TglPo');
		$KodeSubcon 	= $this->input->post('KodeSubcon');
		$data = $this->m_subcon->get_subcon_byid($KodeSubcon);
		$q = $data->row_array();
		$NamaSubcon = $q['Nama'];

		$KodeCustomer 	= $this->input->post('KodeCustomer');
		$data = $this->m_principal->get_principal_byid($KodeCustomer);
		$q = $data->row_array();
		$NamaCustomer = $q['NamaPrincipal'];

		$KodeRegional 	= $this->input->post('KodeRegional');
		$data = $this->m_regional->get_regional_byid($KodeRegional);
		$q = $data->row_array();
		$NamaRegional = $q['NAMA'];

		$datatrxposeh = array(
			'NoPo' => $NoPo,
			'TglPo' => $TglPo,
			'KodeCustomer' => $KodeCustomer,
			'NamaCustomer' => $NamaCustomer,
			'KodeSubcon' => $KodeSubcon,
			'NamaSubcon' => $NamaSubcon,
			'KodeRegional' => $KodeRegional,
			'NamaRegional' => $NamaRegional
		);

		$where = array(
			'NoPo' => $NoPo
		);

		$this->m_posubcon->save_edit_posubcon($where, $datatrxposeh, 'trxposeh');
		$this->m_posubcon->save_edit_posubcon_detail($where, $datatrxposeh, 'trxposed');

		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/posubcon');
	}

	public function cetakpopdf()
	{
		$NoPo = $this->uri->segment(4);

		$a['datacetakpopdf']	= $this->db->query("SELECT * FROM trxposed WHERE NoPo = '$NoPo'")->row();
		$a['reftxtposed'] = $this->m_posubcon->get_posubcon_by_kode3(base64_decode($NoPo));
		$this->load->view('admin/transaksi/v_print_posubcon_pdf', $a);
	}

	function view_posubcon_edit()
	{
		$NoPo = $this->uri->segment('4');

		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		if ($this->session->userdata('is_admin') === TRUE)
		{
			$data['refregional'] = $this->m_regional->get_all_regional2();
		}
		else
		{
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}
		$data['refsubcon'] = $this->m_subcon->get_all_subcon3();
		$data['reftop'] = $this->m_top->get_all_top3();

		$data['poin'] = $this->m_posubcon->get_po_customer();

		$data['data1'] = $this->m_posubcon->get_posubcon_by_kode(base64_decode($NoPo));
		$data['reftxtposed'] = $this->m_posubcon->get_posubcon_by_kode2(base64_decode($NoPo));
		$this->load->view('admin/transaksi/v_view_posubcon_edit', $data);
	}

	function ajax_posubcon_edit()
	{
		if($this->input->is_ajax_request())
		{
			$NoPo = $this->input->post('no');

			$data['data1'] = $this->m_posubcon->get_posubcon_by_kode(base64_decode($NoPo));
			$data['reftxtposed'] = $this->m_posubcon->get_posubcon_by_kode2(base64_decode($NoPo));
			echo json_encode($data);
		}
	}

	public function submit_posubcon_edit()
	{
		if ($_POST) {
			if (!empty($_POST['kode_barang'])) {
				$NoPoLama 	= $this->input->post('NoPoLama');
				$NoPo 	= $this->input->post('NoPo');
				$NoPoIn 	= $this->input->post('NoPoIn');
				$datepicker 	= $this->input->post('datepicker');
				$datepicker2 	= $this->input->post('datepicker2');

				$KodeSubcon 	= $this->input->post('KodeSubcon');
				if (!empty($KodeSubcon))
				{
					$data = $this->m_subcon->get_subcon_byid($KodeSubcon);
					$q = $data->row_array();
					$NamaSubcon = $q['Nama'];
				} else {
					$NamaSubcon = '';
				}

				$TotalTermin 	= $this->input->post('TotalTermin');
				$KodeCustomer 	= $this->input->post('KodeCustomer');
				if (!empty($KodeCustomer)){
					$data = $this->m_principal->get_principal_byid($KodeCustomer);
					$q = $data->row_array();
					$NamaCustomer = $q['NamaPrincipal'];
				} else {
					$NamaCustomer = '';
				}

				$KodeRegional 	= $this->input->post('KodeRegional');
				$data = $this->m_regional->get_regional_byid($KodeRegional);
				$q = $data->row_array();
				$NamaRegional = $q['NAMA'];
				$KodeSite 	= $this->input->post('KodeSite');
				$NamaSite 	= $this->input->post('NamaSite');
				$grand_total	= $this->input->post('grand_total');
				$Sysdate = date('Y-m-d');

				$datatrxposubh = array(
					'TipePo' => 3,
					'NoPo' => $NoPo,
					'NoPoIn' => $NoPoIn,
					'TglPo' => $datepicker,
					'TglAkhirPo' => $datepicker2,
					'KodeSubcon' => $KodeSubcon,
					'NamaSubcon' => $NamaSubcon,
					'KodeCustomer' => $KodeCustomer,
					'NamaCustomer' => $NamaCustomer,
					'KodeRegional' => $KodeRegional,
					'NamaRegional' => $NamaRegional,
					'KodeSite' => $KodeSite,
					'NamaSite' => $NamaSite,
					'TotalTermin' => $TotalTermin,
					'TotalBayar' => $grand_total,
					'Sysdate' => $Sysdate,

				);

				$this->m_posubcon->save_edit_posubcon(['NoPo' => $NoPoLama] , $datatrxposubh, 'trxposeh');
				$this->m_posubcon->delete($NoPoLama);

				$inserted	= 0;

				$no_array	= 0;
				foreach ($_POST['kode_barang'] as $key=>$k) {
					if (!empty($k)) {

							$ppn = "";
							$pph = "";
							if (isset($_POST['jumlah_beli_ppn' . $key])) {
								$ppn = 1;
							} else {
								$ppn = 0;
							}
			
							if (isset($_POST['jumlah_beli_pph' . $key])) {
								$pph = 1;
							} else {
								$pph = 0;
							}

						$kode_barang 	= $_POST['kode_barang'][$no_array];
						$nama_barang1 	= $_POST['nama_barang1'][$no_array];
						$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];
						$harga_satuan 	= $_POST['harga_satuan'][$no_array];
						$sub_total 		= $_POST['sub_total'][$no_array];

						$datatrxposubd = array(
							'TipePo' => 3,
							'NoPo' => $NoPo,
							'NoPoIn' => $NoPoIn,
							'TglPo' => $datepicker,
							'KodeSubcon' => $KodeSubcon,
							'NamaSubcon' => $NamaSubcon,
							'KodeCustomer' => $KodeCustomer,
							'NamaCustomer' => $NamaCustomer,
							'KodeRegional' => $KodeRegional,
							'NamaRegional' => $NamaRegional,
							'KodeSite' => $KodeSite,
							'NamaSite' => $NamaSite,
							'Sysdate' => $Sysdate,

							'KodeSOW' => $kode_barang,
							'NamaSOW' => $nama_barang1,
							'Qty' => $jumlah_beli,
							'UnitPrice' => $harga_satuan,
							'TotalBayar' => $sub_total,
							'pph'	=> $pph,
							'ppn'	=> $ppn,

						);

						$this->m_posubcon->save_posubcon_detail($datatrxposubd, 'trxposed');
						$inserted++;
					}
					$no_array++;
				}

				if ($inserted > 0) {
					echo json_encode(array('status' => 1));
				} 
			} else {
				$this->load->view('admin/posubcon');
			}
		}
	}

	function ajax_table_posubcon()
    {
        $list = $this->m_posubcon->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->NoPo;
            $row[] = $field->TglPo;
            $row[] = $field->NamaSubcon;
            $row[] = $field->NamaCustomer;
            $row[] = $field->NamaRegional;
            $row[] = $field->TotalTermin;
            $row[] = $field->KodeSite;
            $row[] = $field->NamaSite;
            $row[] = $field->TotalBayar;
			$row[] = "
				<a class='badge' href='". base_url() . "admin/posubcon/view_posubcon_edit/" . base64_encode($field->NoPo) ."' style='background-color:blue;' title='Edit PO'><span class='fa fa-edit'></span></a>
				<a class='badge' id='hapus' data-po='". $field->NoPo ."' data-hapus='". $field->NamaCustomer ."' data-toggle='modal' style='background-color:red;' title='Hapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_posubcon->count_all(),
            "recordsFiltered" => $this->m_posubcon->count_filtered(),
			"data" => $data,
        );
        //output dalam format JSON
		echo json_encode($output);
	}
}
