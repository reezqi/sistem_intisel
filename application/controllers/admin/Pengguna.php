<?php
class Pengguna extends CI_Controller{

	function __construct() {
		parent::__construct();
		$this->load->library(['form_validation']);
		$this->load->model('M_role');
		$this->load->model('M_user');
		$this->load->model('M_regional');
	}
	

	public function index(){
		$m_user = $this->M_user;
		$this->data['user'] 	= $m_user->get_user();
		$this->_render_page('admin/akses/v_user', $this->data);
	}

	public function create_user(){
		$m_user 	= $this->M_user;
		$m_reg 		= $this->M_regional;
		$m_role 	= $this->M_role;
		
		// get addional data
		$regional  	= $m_reg->get_all_regional();
		$role 		= $m_role->get_roles();

		//validasi
		$this->form_validation->set_rules('name', ' Nama ', 'trim|required|callback_name_user_exist',
				array('name_user_exist' => '{field} tidak boleh sama'));
		$this->form_validation->set_rules('regional', ' Regional ', 'trim|required');
		$this->form_validation->set_rules('password', 'Kata Sandi', 'required|min_length[8]|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 'Kata Sandi', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$m_user->create_user();
			redirect('admin/pengguna');
		}

		$this->data['regional'] = $regional;
		$this->data['role'] 	= $role;

		$this->data['name'] = [
			'name' => 'name',
			'id' => 'name',
			'type' => 'text',
			'class' => 'form-control',
			'placeholder' => 'isi username',
			'value' => $this->form_validation->set_value('name'),
		];

		$this->data['password'] = [
			'name' => 'password',
			'id' => 'password',
			'type' => 'password',
			'class' => 'form-control',
			'placeholder' => 'kata sandi',
			'value' => $this->form_validation->set_value('password'),
		];

		$this->data['password_confirm'] = [
			'name' => 'password_confirm',
			'id' => 'password_confirm',
			'type' => 'password',
			'class' => 'form-control',
			'placeholder' => 'Konfirmasi kata sandi',
			'value' => $this->form_validation->set_value('password_confirm'),
		];

		$this->_render_page('admin/akses/v_create_user', $this->data);
	}

	public function role()
	{
		$m_role = $this->M_role;
		// cek admin atau bukan
		if (1 == 2)
		{
			redirect('auth', 'refresh');
		}

		// $m_role->_order_by 	= 'id';
		// $m_role->_order 	= 'DSC';
		$this->data['roles']	= $m_role->get_roles()->result();

		
		foreach ($this->data['roles'] as $k => $role)
		{
			$this->data['roles'][$k]->menus = $m_role->get_roles_menus($role->rMenu);
		}
		//echo json_encode($this->data['roles']);
		$this->_render_page('admin/akses/v_role', $this->data);
	}

	public function create_role()
	{
		$role = $this->M_role;
		// cek admin atau bukan
		if (1 == 2)
		{
			redirect('auth', 'refresh');
		}

		$this->form_validation->set_rules('role', 'Role', 'trim|required');
		$this->form_validation->set_rules('rname', 'Role', 'trim|required');
		//$this->form_validation->set_rules('rstatus', 'Role', 'trim|required');

		if ($this->form_validation->run() === TRUE) 
		{
			
			$new_role_id = $role->create_role();
			if ($new_role_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', validation_errors());
				redirect("admin/pengguna/role", 'refresh');
			}
			else
            {
				$this->session->set_flashdata('message', validation_errors());
            }		
		}

		$this->data['message'] = (validation_errors() ? validation_errors() :  $this->session->flashdata('message'));

		$this->data['role'] = [
			'name'  => 'role',
			'id'    => 'role',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder' => 'isi role',
			'value' => $this->form_validation->set_value('role'),
		];

		$this->data['rname'] = [
			'name'  => 'rname',
			'id'    => 'rname',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder' => 'isi nama atau deskripsi role',
			'value' => $this->form_validation->set_value('rname'),
		];
	
		$this->_render_page('admin/akses/v_create_role', $this->data);
	}

	public function edit_role($id)
	{
		$m_role = $this->M_role;
		// cek admin atau bukan
		if (1 == 2)
		{
			redirect('auth', 'refresh');
		}

		$role 			= $m_role->get_where_roles(['id' => $id]);
		$menus 			= $m_role->get_menus();
		$currentMenu	= $m_role->get_roles_menus($role->row(0)->rMenu);

		$this->form_validation->set_rules('role', 'Role', 'trim|required');
		$this->form_validation->set_rules('rname', 'Role', 'trim|required');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				//update menu if user is admin
				if (1 == 1)
				{
					//update data with menu
					$rMenu = implode("", $this->input->post('menus'));
					if ($m_role->update_role($id, $rMenu) === TRUE)
					{
						$menusData = $this->input->post('menus');
						if (isset($menusData) && !empty($menusData))
						{
							$m_role->delete_menu_role($role->row(0)->Role);
							for($i=0; $i<sizeof($menusData); $i++) 
							{
								$m_role->add_menu_to_role($role->row(0)->Role, $menusData[$i]);
							}
						}

						$this->session->set_flashdata('message', 'Berhasil');
						redirect('admin/pengguna/role');
					}
					else
					{
						echo "salah";
					}
				}
				else
				{
					//update data without menu
				}
			}
		}
		$this->data['currentMenu'] = $currentMenu;
		$this->data['menus'] = $menus;
		$this->data['id'] 	= $id;
		$this->data['role'] = [
			'name'  => 'role',
			'id'    => 'role',
			'type'  => 'text',
			'class' => 'form-control',
            'placeholder' => 'isi role',
            'readonly'=>'true',
			'value' => $this->form_validation->set_value('role', $role->row(0)->Role),
		];

		$this->data['rname'] = [
			'name'  => 'rname',
			'id'    => 'rname',
			'type'  => 'text',
			'class' => 'form-control',
			'placeholder' => 'isi nama atau deskripsi role',
			'value' => $this->form_validation->set_value('rname', $role->row(0)->rName),
		];
		
		//echo json_encode($this->data['menus']);
		$this->_render_page('admin/akses/v_edit_role', $this->data);

	}

	public function delete_role($id = NULL)
	{
		$m_role = $this->M_role;

		if (isset($id))
		{
			//delte menu akses
			$role 			= $m_role->get_where_roles(['id' => $id]);
			$m_role->delete_menu_role($role->row(0)->Role);

			//delete role
			$m_role->delete_role($id);

			redirect('admin/pengguna/role');
		}
	}

	public function menus()
	{
		$m_role = $this->M_role;
		// cek admin atau bukan
		if (1 == 2)
		{
			redirect('auth', 'refresh');
		}

		$this->data['menus']	= $m_role->get_menus()->result();
		
		foreach ($this->data['menus'] as $k => $menu)
		{
			$this->data['menus'][$k]->submenus = $m_role->get_menus_submenus($menu->Id)->result();
		}
		//echo json_encode($this->data['menus']);
		$this->_render_page('admin/akses/v_menu', $this->data);
	}

	public function edit_menu($id)
	{
		$m_role = $this->M_role;
		// cek admin atau bukan
		if (1 == 2)
		{
			redirect('auth', 'refresh');
		}

		$currentMenus 		= $m_role->get_where_menus($id);
		$subMenu 			= $m_role->get_submenus();
		$currentSubMenu		= $m_role->get_menus_submenus($id);


		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				//update menu if user is admin
				if (1 == 1)
				{
					
				}
				else
				{
					//update data without menu
				}
			}
		}

		$this->data['subMenus']	= $m_role->get_menus()->result();
		
		foreach ($this->data['subMenus'] as $k => $menu)
		{
			$this->data['subMenus'][$k]->submenus = $m_role->get_menus_submenus($menu->Id)->result();
		}
		
		$this->data['currentMenus'] 		= $currentMenus->row();
		$this->data['currentSubMenu'] 	= $currentSubMenu->result();
		
		//echo json_encode($this->data['subMenus']);
		$this->_render_page('admin/akses/v_edit_menu', $this->data);

	}

	public function name_user_exist($name)
	{
		$nama = $this->db->get_where('refuser', ['Name' => $name])->row();
		if (!empty($nama)) { return FALSE; } else { return TRUE;}
	}

	public function _render_page($view, $data = NULL, $returnhtml = FALSE)
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

	
		if ($returnhtml)
		{
			return $view_html;
		}
	}
}