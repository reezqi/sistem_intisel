<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pocustomer extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_prregional');
		$this->load->model('m_pocustomer');
		$this->load->model('m_posubcon');
		$this->load->model('m_regional');
		$this->load->model('m_site');
		$this->load->model('m_project');
		$this->load->model('m_principal');
		$this->load->model('m_msow');
		$this->load->model('m_subcon');
		$this->load->model('m_jenispekerjaan');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		if ($this->session->userdata('is_admin') === TRUE) {
			$x['refregional'] = $this->m_regional->get_all_regional2();
		} else {
			$x['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		$x['refsite'] = $this->m_site->get_all_site2();
		$x['refpr'] = $this->m_prregional->get_all_prregional2();

		$this->load->view('admin/transaksi/v_pocustomer', $x);
	}

	public function search_add_pocustomer()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();

		$data['pocustomer'] = $this->m_pocustomer->search_add_pocustomer();
		$this->load->view('admin/transaksi/v_add_pocustomer', $data);
	}


	function add_pocustomer()
	{
		$data['refsubcon'] = $this->m_subcon->get_all_subcon2();
		$data['refprincipal'] = $this->m_principal->get_all_principal2();
		$data['refproject'] = $this->m_project->get_all_project2();
		if ($this->session->userdata('is_admin') === TRUE) {
			$data['refpr'] = $this->m_prregional->get_all_prregional2();
			$data['refregional'] = $this->m_regional->get_all_regional2();
		} else {
			$data['refpr'] = $this->m_prregional->get_where_prregional2(['KodeRegional' => $this->session->userdata('user_region')]);
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}
		$data['refpekerjaan'] = $this->m_jenispekerjaan->get_all_jenispekerjaan2();

		$pegawai = $this->m_pocustomer->nourutpo();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}


		$noUrut = (int) substr($noUrut, 26, 30);

		$data['NoUrutPo'] =  sprintf("%05s", $noUrut + 1);
		//echo json_encode($data['NoUrutPo']);
		//Membuat Kode Regional Otomatis
		$pegawai1 = $this->m_pocustomer->nourutdokumen();
		if (empty($pegawai1) || is_null($pegawai1)) {
			$NoUrutDokumen = 0;
		} else {
			$NoUrutDokumen = $pegawai1->no_urut;
		}

		$data['NoDokumen'] = (int) $NoUrutDokumen + 1;
		//end
		//echo json_encode($data['NoUrutPo']);
		$data['pocustomer'] = $this->m_pocustomer->search_add_pocustomer();
		$this->load->view('admin/transaksi/v_add_pocustomer', $data);
	}

	function add_pocustomer_detail()
	{
		$data['refregional'] = $this->m_regional->get_all_regional2();

		$NoDokumen = $this->uri->segment('4');
		$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode($NoDokumen);
		$this->load->view('admin/transaksi/v_add_pocustomer_detail', $data);
	}

	function add_pocustomer_detail_site()
	{
		$data['refregional'] = $this->m_regional->get_all_regional2();

		$NoDokumen = $this->uri->segment('4');
		$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode($NoDokumen);
		$this->load->view('admin/transaksi/v_add_pocustomer_detail_site', $data);
	}

	function add_pocustomer_detail_sow()
	{
		$data['refregional'] = $this->m_regional->get_all_regional2();

		$NoDokumen = $this->uri->segment('4');
		$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode($NoDokumen);
		$this->load->view('admin/transaksi/v_add_pocustomer_detail_sow', $data);
	}

	function add_pocustomer_detail_sowsite()
	{
		$data['refregional'] = $this->m_regional->get_all_regional2();

		$NoDokumen = $this->uri->segment('4');
		$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode($NoDokumen);
		$this->load->view('admin/transaksi/v_add_pocustomer_detail_sowsite', $data);
	}

	function view_pocustomer_detail()
	{
		$NoDokumen = $this->uri->segment('4');

		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		$data['refregional'] = $this->m_regional->get_all_regional3();

		$pegawai = $this->m_pocustomer->nourutpo();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$noUrut = (int) substr($noUrut, 17, 17);
		$data['NoPoIn'] = 'PO/INTISEL/2020/' . sprintf("%05s", $noUrut + 1);

		$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode(base64_decode($NoDokumen));
		$data['reftxtpocusd'] = $this->m_pocustomer->get_pocustomer_by_kode2(base64_decode($NoDokumen));
		$this->load->view('admin/transaksi/v_view_pocustomer_detail', $data);
	}

	function edit_pocustomer()
	{
		$NoDokumen = $this->uri->segment('4');

		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		$data['refregional'] = $this->m_regional->get_all_regional3();

		$pegawai = $this->m_pocustomer->nourutpo();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$noUrut = (int) substr($noUrut, 35, 35);
		$data['NoUrutPo'] =  sprintf("%05s", $noUrut + 1);

		//Membuat Kode Regional Otomatis
		$pegawai1 = $this->m_pocustomer->nourutdokumen();
		if (empty($pegawai1) || is_null($pegawai1)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai1->no_urut;
		}

		$data['NoDokumen'] = $noUrut + 1;
		//end

		$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode(base64_decode($NoDokumen));
		$this->load->view('admin/transaksi/v_edit_pocustomer', $data);
	}

	public function ajax_detail_transaksi()
	{
		if ($this->input->is_ajax_request()) {
			$NoDokumen = $this->uri->segment('4');

			$dt['master'] = $this->m_pocustomer->get_pocustomer_by_kode(base64_decode($NoDokumen));
			$dt['detail'] = $this->m_pocustomer->get_pocustomer_by_kode(base64_decode($NoDokumen));

			echo json_encode($dt);
		}
	}

	function save_edit_pocustomer()
	{
		$NoPoIn = strip_tags($this->input->post('NoPOIn'));
		$NoPo = strip_tags($this->input->post('NoPO'));

		$KodeCustomer = strip_tags($this->input->post('KodeCustomer'));
		$data = $this->m_principal->get_principal_byid($KodeCustomer);
		$q = $data->row_array();
		$NamaCustomer = $q['NamaPrincipal'];

		$NoDokumen = $this->input->post('NoDokumen');
		$TglPo = $this->input->post('TglPo');

		$SOWKodeRegional = strip_tags($this->input->post('KodeRegional'));
		$data = $this->m_regional->get_regional_byid($SOWKodeRegional);
		$q = $data->row_array();
		$SOWNamaRegional = $q['NAMA'];

		$datatrxpcush = array(
			'NoPoIn' => $NoPoIn,
			'NoPo' => $NoPo,
			'KodeCustomer' => $KodeCustomer,
			'NamaCustomer' => $NamaCustomer,
			'NoDokumen' => $NoDokumen,
			'TglPo' => $TglPo,
			'KodeRegional' => $SOWKodeRegional,
			'NamaRegional' => $SOWNamaRegional
		);

		$where = array(
			'NoDokumen' => $NoDokumen
		);

		$this->m_pocustomer->save_edit_pocustomer($where, $datatrxpcush, 'trxpocush');
		$this->m_pocustomer->save_edit_pocustomer_detail($where, $datatrxpcush, 'trxpocusd');

		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/pocustomer');
	}

	function delete_pocustomer()
	{
		$NoDokumen = strip_tags($this->input->post('NoDokumen'));

		$this->m_pocustomer->delete_pocustomer($NoDokumen);

		$this->m_pocustomer->delete_pocustomer_detail($NoDokumen);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/pocustomer');
	}

	function delete_pocustomer_detail()
	{
		$KodePoCusd = $_POST['KodePoCusd'];
		$NoDokumen = strip_tags($this->input->post('NoDokumen'));

		$this->m_pocustomer->delete_pocustomer_detail_multi($KodePoCusd);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/pocustomer/view_pocustomer_detail/' . $NoDokumen);
	}

	public function ajax_kode()
	{
		if ($this->input->is_ajax_request()) {
			$keyword 	= $this->input->post('keyword');
			$registered	= $this->input->post('registered');
			$regional 	= $this->input->post('regional');
			$customer 	= $this->input->post('customer');

			$this->load->model('m_msow');

			// $barang = "";
			// if ($this->session->userdata('is_admin') == TRUE) {
			// } else {
			// 	$barang = $this->m_msow->cari_kode_where($keyword, $registered, $this->session->userdata('user_region'));
			// }
			$barang = $this->m_msow->cari_kode($keyword, $registered);

			if ($barang->num_rows() > 0) {
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar-autocomplete'>";
				foreach ($barang->result() as $b) {
					if (trim($b->SOWKodeRegional) === trim($regional) && trim($b->SOWKodeCustomer) === trim($customer)) {
						$json['datanya'] .= "
							<li>
								<b>Kode</b> :
								<span id='kodenya'>" . $b->Code . "</span> <br />
								<span id='barangnya'>" . $b->SOWDesc . "</span>
								<span id='harganya' style='display:none;'>" . $b->HrgRegional . "</span>
							</li>
					";
					}
				}
				$json['datanya'] .= "</ul>";
			} else {
				$json['status'] 	= 0;
			}

			echo json_encode($json);
		}
	}

	public function ajax_kode_ekternal()
	{
		if ($this->input->is_ajax_request()) {
			$keyword 	= $this->input->post('keyword');
			$registered	= $this->input->post('registered');
			$regional 	= $this->input->post('regional');
			$customer 	= $this->input->post('customer');
			$this->load->model('m_msow');

			// $barang = "";
			// if ($this->session->userdata('is_admin') == TRUE) {
			// 	$barang = $this->m_msow->cari_kode_eksternal($keyword, $registered);
			// } else {
			// 	$barang = $this->m_msow->cari_kode_eksternal_where($keyword, $registered, $this->session->userdata('user_region'));
			// }
			$barang = $this->m_msow->cari_kode_eksternal($keyword, $registered);

			if ($barang->num_rows() > 0) {
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar-autocomplete'>";
				foreach ($barang->result() as $b) {
					if (strtolower($b->SOWJenis) === 'external' && trim($b->SOWKodeRegional) === trim($regional) && trim($b->SOWKodeCustomer) === trim($customer)) {
						$json['datanya'] .= "
							<li>
								<b>Kode</b> :
								<span id='kodenya'>" . $b->Code . "</span> <br />
								<span id='barangnya'>" . $b->SOWDesc . "</span>
								<span id='harganya' style='display:none;'>" . $b->HrgRegional . "</span>
							</li>
						";
					}
				}
				$json['datanya'] .= "</ul>";
			} else {
				$json['status'] 	= 0;
			}

			echo json_encode($json);
		}
	}

	public function save_pocustomer()
	{
		if ($_POST) {
			if (!empty($_POST['kode_barang'])) {

				//echo json_encode($_POST);

				$NoDokumen 	= $this->input->post('NoDokumen');
				$NoPoIn 	= $this->input->post('NoPoIn');
				$NoPr 		= $this->input->post('NoPr');
				$NoPo 	= $this->input->post('NoPo');
				$datepicker3     = $this->input->post('datepicker3');
				$datepicker2     = $this->input->post('datepicker2');
				$KodeCustomer 	= $this->input->post('KodeCustomer');
				$data = $this->m_principal->get_principal_byid($KodeCustomer);
				$q = $data->row_array();
				$NamaCustomer = $q['NamaPrincipal'];

				$KodeRegional 	= $this->input->post('KodeRegional');
				$data = $this->m_regional->get_regional_byid($KodeRegional);
				$q = $data->row_array();
				$NamaRegional = $q['NAMA'];
				$KodeSite 	= $this->input->post('KodeSite');
				$NamaSite 	= $this->input->post('NamaSite');
				$grand_total	= $this->input->post('grand_total');
				$Sysdate = date('Y-m-d');

				$datatrxpocush = array(
					'TipePo' => 1,
					'NoDokumen' => $NoDokumen,
					'NoPoIn' => $NoPoIn,
					'NoPr' => $NoPr,
					'NoPo' => $NoPo,
					'TglPo' => $datepicker3,
					'TglAkhirPo' => $datepicker2,
					'KodeCustomer' => $KodeCustomer,
					'NamaCustomer' => $NamaCustomer,
					'KodeRegional' => $KodeRegional,
					'NamaRegional' => $NamaRegional,
					'KodeSite' => $KodeSite,
					'NamaSite' => $NamaSite,
					'TotalPo' => $grand_total,
					'Sysdate' => $Sysdate,
				);

				$this->m_pocustomer->save_pocustomer($datatrxpocush, 'trxpocush');

				$inserted	= 0;
				$no_array	= 0;
				foreach ($_POST['kode_barang'] as $key => $k) {
					if (!empty($k)) {
						$ppn = "";
						$pph = "";
						if (isset($_POST['jumlah_beli_ppn' . $key])) {
							$ppn = 1;
						} else {
							$ppn = 0;
						}

						if (isset($_POST['jumlah_beli_pph' . $key])) {
							$pph = 1;
						} else {
							$pph = 0;
						}

						$kode_barang 	= $_POST['kode_barang'][$no_array];
						$nama_barang 	= $_POST['nama_barang'][$no_array];
						$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];
						$harga_satuan 	= $_POST['harga_satuan'][$no_array];
						$sub_total 		= $_POST['sub_total'][$no_array];

						$datatrxpocusd = array(
							'TipePo' => 1,
							'NoDokumen' => $NoDokumen,
							'NoPoIn' => $NoPoIn,
							'NoPo' => $NoPo,
							'TglPo' => $datepicker3,
							'KodeCustomer' => $KodeCustomer,
							'NamaCustomer' => $NamaCustomer,
							'KodeRegional' => $KodeRegional,
							'NamaRegional' => $NamaRegional,
							'KodeSite' => $KodeSite,
							'NamaSite' => $NamaSite,
							'Sysdate' => $Sysdate,

							'KodeSOW' => $kode_barang,
							'NamaSOW' => $nama_barang,
							'Qty' => $jumlah_beli,
							'pph'	=> $pph,
							'ppn'	=> $ppn,
							'HrgRegional' => $harga_satuan,
							'JumlahHrgRegional' => $sub_total
						);


						$this->m_pocustomer->save_pocustomer_detail($datatrxpocusd, 'trxpocusd');

						$inserted++;
					}
					$no_array++;
				}

				if ($inserted > 0) {
					echo json_encode(array('status' => 1));
				}
			} else {
				$this->load->view('admin/pocustomer');
			}
		}
	}

	public function cetakpopdf()
	{
		$NoPoIn = $this->uri->segment(4);

		$a['datacetakpopdf']	= $this->db->query("SELECT * FROM trxpocusd WHERE NoPoIn = '$NoPoIn'")->row();
		$a['reftxtpocusd'] = $this->m_pocustomer->get_pocustomer_by_kode3(base64_decode($NoPoIn));
		$this->load->view('admin/transaksi/v_print_pocustomer_pdf', $a);
	}

	public function cetakpoexcel()
	{
		$NoDokumen = $this->uri->segment(4);


		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER UNIT"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Unit"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Unit");


		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$reftxtpocusd = $this->m_pocustomer->get_all_pocustomer3(base64_decode($NoDokumen));
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($reftxtpocusd as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->NoDokumen);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->NoPoIn);


			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Unit");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Unit.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	public function view_pocustomer_edit()
	{
		$NoDokumen = $this->uri->segment('4');

		$data['refprincipal'] = $this->m_principal->get_all_principal3();

		if ($this->session->userdata('is_admin') === TRUE) {
			$data['refpr'] = $this->m_prregional->get_all_prregional2();
			$data['refregional'] = $this->m_regional->get_all_regional2();
		} else {
			$data['refpr'] = $this->m_prregional->get_where_prregional2(['KodeRegional' => $this->session->userdata('user_region')]);
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		$pegawai = $this->m_pocustomer->nourutpo();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$noUrut = (int) substr($noUrut, 17, 17);
		$data['NoPoIn'] = 'PO/INTISEL/2020/' . sprintf("%05s", $noUrut + 1);

		$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode(base64_decode($NoDokumen));
		$data['reftxtpocusd'] = $this->m_pocustomer->get_pocustomer_by_kode2(base64_decode($NoDokumen));
		$this->load->view('admin/transaksi/v_view_pocustomer_edit', $data);
		//echo json_encode($data['reftxtpocusd']);
	}

	public function ajax_pocustomer_edit()
	{
		if ($this->input->is_ajax_request()) {
			$NoDokumen = $this->input->post('no');

			$data['no'] = $this->input->post('no');
			$data['refprincipal'] = $this->m_principal->get_all_principal3();
			$data['refregional'] = $this->m_regional->get_all_regional3();

			$pegawai = $this->m_pocustomer->nourutpo();
			if (empty($pegawai) || is_null($pegawai)) {
				$noUrut = 0;
			} else {
				$noUrut = $pegawai->no_urut;
			}

			$noUrut = (int) substr($noUrut, 17, 17);
			$data['NoPoIn'] = 'PO/INTISEL/2020/' . sprintf("%05s", $noUrut + 1);

			$data['data1'] = $this->m_pocustomer->get_pocustomer_by_kode(base64_decode($NoDokumen))->result();
			$data['reftxtpocusd'] = $this->m_pocustomer->get_pocustomer_by_kode2_(base64_decode($NoDokumen))->result_array();
			echo json_encode($data);
		}
	}

	public function submit_pocustomer_edit()
	{
		if ($_POST) {
			if (!empty($_POST['kode_barang'])) {
				//echo json_encode($_POST);
				$NoDokumen 	= $this->input->post('NoDokumen');
				$NoPoIn 	= $this->input->post('NoPoIn');
				$NoPo 		= $this->input->post('NoPo');
				$NoPr 		= $this->input->post('NoPr');
				$datepicker3     = $this->input->post('datepicker3');
				$datepicker2     = $this->input->post('datepicker2');
				$KodeCustomer 	= $this->input->post('KodeCustomer');

				$data = $this->m_principal->get_principal_byid($KodeCustomer);
				$q = $data->row_array();
				$NamaCustomer = $q['NamaPrincipal'];

				$KodeRegional 	= $this->input->post('KodeRegional');
				$data = $this->m_regional->get_regional_byid($KodeRegional);
				$q = $data->row_array();
				$NamaRegional = $q['NAMA'];
				$KodeSite 	= $this->input->post('KodeSite');
				$NamaSite 	= $this->input->post('NamaSite');
				$grand_total	= $this->input->post('grand_total');
				$Sysdate = date('Y-m-d');

				$inserted	= 0;

				$no_array	= 0;

				//update head data
				$datatrxpocush = array(
					'TipePo' => 1,
					'NoDokumen' => $NoDokumen,
					'NoPoIn' => $NoPoIn,
					'NoPo' => $NoPo,
					'NoPr' => $NoPr,
					'TglPo' => $datepicker3,
					'TglAkhirPo' => $datepicker2,
					'KodeCustomer' => $KodeCustomer,
					'NamaCustomer' => $NamaCustomer,
					'KodeRegional' => $KodeRegional,
					'NamaRegional' => $NamaRegional,
					'KodeSite' => $KodeSite,
					'NamaSite' => $NamaSite,
					'TotalPo' => $grand_total,
					'Sysdate' => $Sysdate,
				);
				$this->m_pocustomer->save_edit_pocustomer(['NoDokumen' => $NoDokumen], $datatrxpocush, 'trxpocush');

				// hapus dulu detail yang dulu
				$this->m_pocustomer->delete_pocustomer_detail($NoDokumen);

				//input yang detail baru
				foreach ($_POST['kode_barang'] as $key => $k) {
					if (!empty($k)) {

						$ppn = "";
						$pph = "";
						if (isset($_POST['jumlah_beli_ppn' . $key])) {
							$ppn = 1;
						} else {
							$ppn = 0;
						}

						if (isset($_POST['jumlah_beli_pph' . $key])) {
							$pph = 1;
						} else {
							$pph = 0;
						}

						$kode_barang 	= $_POST['kode_barang'][$no_array];
						$nama_barang 	= $_POST['nama_barang'][$no_array];
						$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];
						$harga_satuan 	= $_POST['harga_satuan'][$no_array];
						$sub_total 		= $_POST['sub_total'][$no_array];

						$datatrxpocusd = array(
							'TipePo' => 1,
							'NoDokumen' => $NoDokumen,
							'NoPoIn' => $NoPoIn,
							'NoPo' => $NoPo,
							'TglPo' => $datepicker3,
							'KodeCustomer' => $KodeCustomer,
							'NamaCustomer' => $NamaCustomer,
							'KodeRegional' => $KodeRegional,
							'NamaRegional' => $NamaRegional,
							'KodeSite' => $KodeSite,
							'NamaSite' => $NamaSite,
							'Sysdate' => $Sysdate,
							'pph'	=> $pph,
							'ppn'	=> $ppn,
							'KodeSOW' => $kode_barang,
							'NamaSOW' => $nama_barang,
							'Qty' => $jumlah_beli,
							'HrgRegional' => $harga_satuan,
							'JumlahHrgRegional' => $sub_total
						);
						$this->m_pocustomer->save_pocustomer_detail($datatrxpocusd, 'trxpocusd');
						$inserted++;
					}
					$no_array++;
				}

				if ($inserted > 0) {
					echo json_encode(array('status' => 1));
				}
			} else {
				echo json_encode(array('status' => 2));
			}
		}
	}

	function ajax_table_pocustomer()
	{
		$list = $this->m_pocustomer->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->NoDokumen;
			$row[] = $field->TglPo;
			$row[] = $field->NoPoIn;
			$row[] = $field->NoPo;
			$row[] = $field->NamaCustomer;
			$row[] = $field->NamaRegional;
			$row[] = $field->KodeSite;
			$row[] = $field->TotalPo;
			$row[] = "
				<a class='badge' href='" . base_url() . "admin/pocustomer/view_pocustomer_edit/" . base64_encode($field->NoDokumen) . "' style='background-color:blue;' title='Edit PO'><span class='fa fa-edit'></span></a>
				<a class='badge' id='hapus' data-po='" . $field->NoPoIn . "' data-hapus='" . $field->NoDokumen . "' data-toggle='modal' style='background-color:red;' title='Hapus'><span class='fa fa-trash'></span></a>
			";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_pocustomer->count_all(),
			"recordsFiltered" => $this->m_pocustomer->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}
}
