<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenispekerjaan extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_jenispekerjaan');
		$this->load->helper('url');
		$this->load->model('m_principal');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refjenispekerjaan'] = $this->m_jenispekerjaan->get_all_jenispekerjaan()->result();
		$this->load->view('admin/v_jenis_pekerjaan', $x);
	}

	function get_edit()
	{
		$id_jenis_pekerjaan = $this->uri->segment(4);
		$data['data1'] = $this->m_jenispekerjaan->get_jenispekerjaan_by_kode($id_jenis_pekerjaan);
		$data['refprincipal'] = $this->m_principal->get_all_principal();

		$this->load->view('admin/v_edit_jenispekerjaan', $data);
	}

	function simpan_jenispekerjaan()
	{
		$id_jenis_pekerjaan = strip_tags($this->input->post('id_jenis_pekerjaan'));

		$KodeCustomer = strip_tags($this->input->post('KodeCustomer'));
		$data = $this->m_principal->get_principal_byid($KodeCustomer);
		$q = $data->row_array();
		$Customer = $q['NamaPrincipal'];

		$JenisPekerjaan = $this->input->post('JenisPekerjaan');
		$Dokumen = $this->input->post('Dokumen');
		$Keterangan = $this->input->post('Keterangan');

		$Tanggal = $this->input->post('datepicker');
		$userid = $this->session->userdata('user_id');
		$usernama = $this->session->userdata('name');

		$datajenispekerjaan = array(
			'id_jenis_pekerjaan' => $id_jenis_pekerjaan,
			'KodePrincipal'  => $KodeCustomer,
			'Customer'  => $Customer,
			'JenisPekerjaan' => $JenisPekerjaan,
			'Dokumen' => $Dokumen,
			'Keterangan' => $Keterangan,
			'created_at' => $Tanggal,
			'user_id' 	=> $userid,
			'user_name' => $usernama,
			'PekerjaanCustomerKode' => $KodeCustomer,
			'PekerjaanCustomerNama' => $Customer,
		);

		$this->m_jenispekerjaan->simpan_jenispekerjaan($datajenispekerjaan, 'refjenispekerjaan');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/jenispekerjaan');
	}

	function simpan_update_jenispekerjaan()
	{
		$id_jenis_pekerjaan = $this->input->post('id_jenis_pekerjaan');

		$PekerjaanCustomerKode = strip_tags($this->input->post('KodeCustomer'));
		$data = $this->m_principal->get_principal_byid($PekerjaanCustomerKode);
		$q = $data->row_array();
		$PekerjaanCustomerNama = $q['NamaPrincipal'];

		$JenisPekerjaan = $this->input->post('JenisPekerjaan');
		$Dokumen = $this->input->post('Dokumen');
		$Keterangan = $this->input->post('Keterangan');

		$Tanggal = $this->input->post('datepicker');
		$userid = $this->session->userdata('user_id');
		$usernama = $this->session->userdata('name');

		$datajenispekerjaan = array(
			'id_jenis_pekerjaan' => $id_jenis_pekerjaan,
			'PekerjaanCustomerKode'  => $PekerjaanCustomerKode,
			'PekerjaanCustomerNama'  => $PekerjaanCustomerNama,
			'JenisPekerjaan' => $JenisPekerjaan,
			'Dokumen' => $Dokumen,
			'Keterangan' => $Keterangan,
			'created_at' => $Tanggal,
			'user_id' 	=> $userid,
			'user_name' => $usernama,
			'PekerjaanCustomerKode' => $PekerjaanCustomerKode,
			'PekerjaanCustomerNama' => $PekerjaanCustomerNama,
		);

		$where = array(
			'id_jenis_pekerjaan' => $id_jenis_pekerjaan
		);

		$this->m_jenispekerjaan->update_edit_jenispekerjaan($where, $datajenispekerjaan, 'refjenispekerjaan');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/jenispekerjaan');
	}

	function hapus_jenispekerjaan()
	{
		$id_jenis_pekerjaan = strip_tags($this->input->post('id_jenis_pekerjaan'));
		$this->m_jenispekerjaan->hapus_jenispekerjaan($id_jenis_pekerjaan);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/jenispekerjaan');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_jenispekerjaan->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_jenispekerjaan_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'id_jenis_pekerjaan' => $row['A'], // Insert data nis dari kolom A di excel
					'JenisPekerjaan' => $row['B'],
					'KodePrincipal' => $row['C'],
					'Customer' => $row['D'],
					'Dokumen' => $row['E'],
					'Keterangan' => $row['F'],

				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_jenispekerjaan->insert_multiple($data);

		redirect("admin/jenispekerjaan"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER CUSTOMER"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode Jenis Pekerjaan"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama Jenis Pekerjaan");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Kode Customer");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Nama Customer");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Dokumen");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Keterangan");


		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);


		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$jenispekerjaan = $this->m_jenispekerjaan->get_all_jenispekerjaan2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($jenispekerjaan as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->id_jenis_pekerjaan);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->JenisPekerjaan);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->PekerjaanCustomerKode);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->PekerjaanCustomerNama);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->Dokumen);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->Keterangan);


			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);


			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(20); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(60);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Jenis Pekerjaan");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Jenis Pekerjaan.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	function ajax_pekerjaan_customer()
	{
		if ($_POST)
		{
			$No 		= $_POST['term'];
			$Customer 	= $_POST['customer'];

			$hasil = $this->m_jenispekerjaan->cari_pekerjaan_by_kode_customer($No, $Customer);

			$data = array();
			foreach($hasil->result() as $field){
				if (trim($field->PekerjaanCustomerKode) == trim($Customer) )
				{
					$data[] = array("id"=>$field->JenisPekerjaan, "text"=>$field->JenisPekerjaan, "slug"=>$field->Dokumen);
				}
			}
			
			echo json_encode($data);
		}
	}
}
