<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msow extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_msow');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('m_regional');
		$this->load->model('m_perusahaan');
		$this->load->model('m_subcon');
		$this->load->model('m_principal');
		$this->load->model('m_unit');
	}

	function index()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refsubcon'] = $this->m_subcon->get_all_subcon2();
		$x['refmsow'] = $this->m_msow->get_all_msow2();

		$x['Regional'] = $this->m_msow->cariPO_();
		$x['detail'] = $this->m_msow->cariPO();

		$this->load->view('admin/v_msow', $x);
	}

	function get_autocomplete()
	{
		if (isset($_GET['term'])) {
			$result = $this->m_msow->search_sow($_GET['term']);
			if (count($result) > 0) {
				foreach ($result as $row)
					$arr_result[] = array(
						'label'			=> $row->Code,
						'description'	=> $row->SOWDesc,
					);
				echo json_encode($arr_result);
			}
		}
	}

	function add_msow()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refperusahaan'] = $this->m_perusahaan->get_all_perusahaan2();
		$x['refsubcon'] = $this->m_subcon->get_all_subcon2();
		$x['refunit'] = $this->m_unit->get_all_unit2();
		$x['refmsow'] = $this->m_msow->get_all_msow2();

		//Membuat Kode Regional Otomatis
		$pegawai = $this->m_msow->data();
		if (empty($pegawai) || is_null($pegawai)) {
			$noUrut = 0;
		} else {
			$noUrut = $pegawai->no_urut;
		}

		$x['Line'] = $noUrut + 1;
		//end

		$this->load->view('admin/v_add_msow', $x);
	}

	function ajax_sow()
	{
		if ($_POST)
		{
			$No = $_POST['term'];
			$hasil = $this->m_msow->cari_msow_by_kode($No);

			$data = array();
			foreach($hasil->result() as $field){
			   $data[] = array("id"=>$field->Line, "text"=>$field->Code.' - '.$field->SOWDesc);
			}
			
			echo json_encode($data);
		}
	}

	function ajax_sow_region_customer()
	{
		if ($_POST)
		{
			$No 		= $_POST['term'];
			$Region 	= $_POST['region'];
			$Customer 	= $_POST['customer'];

			$hasil = $this->m_msow->cari_msow_by_kode_region_customer($No, $Region, $Customer);

			$data = array();
			foreach($hasil->result() as $field){
				if ($field->SOWJenis == 'Internal' && trim($field->SOWKodeRegional) == trim($Region) && trim($field->SOWKodeCustomer) == trim($Customer) )
				{
					$data[] = array("id"=>$field->Code, "text"=>$field->Code.' - '.$field->SOWDesc, "slug"=>$field->SOWDesc);
				}
			}
			
			echo json_encode($data);
		}
		//echo json_encode($_POST);
	}

	function ajax_sow_select()
	{
		if ($_POST)
		{
			$No = $_POST['line'];
			$hasil = $this->m_msow->get_msow_by_line($No);

			echo json_encode($hasil->result());
		}
	
	}

	public function hasil()
	{
		$x['refprincipal'] = $this->m_principal->get_all_principal2();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['refsubcon'] = $this->m_subcon->get_all_subcon2();
		$x['refmsow'] = $this->m_msow->get_all_msow2();

		$x['Regional'] = $this->m_msow->cariPO();
		$this->load->view('admin/v_msow', $x);
	}


	function get_edit()
	{
		$No = $this->uri->segment(4);
		$data['data1'] = $this->m_msow->get_msow_by_kode($No);
		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		$data['refregional'] = $this->m_regional->get_all_regional3();
		$data['refperusahaan'] = $this->m_perusahaan->get_all_perusahaan3();
		$data['refsubcon'] = $this->m_subcon->get_all_subcon3();
		$data['refunit'] = $this->m_unit->get_all_unit3();

		$this->load->view('admin/v_edit_msow', $data);
	}

	function get_detail_msow()
	{
		$No = $this->uri->segment(4);
		$data['data1'] = $this->m_msow->get_msow_by_kode($No);
		$data['refregional'] = $this->m_regional->get_all_regional3();

		$this->load->view('admin/v_detail_msow', $data);
	}

	function simpan_msow()
	{

		$connect = mysqli_connect("202.78.195.194", "usersakti", "u5ers4kti", "dbinvoice");

		$No = strip_tags($this->input->post('No'));
		$Line = $this->input->post('Line');
		$ListJob = $this->input->post('ListJob');
		$JenisSOW = $this->input->post('JenisSOW');
		$Code = $this->input->post('Code');
		$SOWDesc = $this->input->post('SOWDesc');

		$SOWKodeUnit = strip_tags($this->input->post('Unit'));
		$data = $this->m_unit->get_unit_byid($SOWKodeUnit);
		$q = $data->row_array();
		$SOWNamaUnit = $q['NamaUnit'];

		$PemFull = $this->input->post('PemFull');
		$F7 = $this->input->post('F7');
		$F8 = $this->input->post('F8');
		$Pem1 = $this->input->post('Pem1');
		$Pem2 = $this->input->post('Pem2');
		$Pem3 = $this->input->post('Pem3');

		$SOWKodeCustomer = strip_tags($this->input->post('Customer'));
		$data = $this->m_principal->get_principal_byid($SOWKodeCustomer);
		$q = $data->row_array();
		$SOWNamaCustomer = $q['NamaPrincipal'];

		$SOWKodeRegional = $_POST["item_category"];

		$HrgRegional = $_POST["item_sub_category"];

		$SOWKodePerusahaan = strip_tags($this->input->post('Perusahaan'));
		$data = $this->m_perusahaan->get_perusahaan_byid($SOWKodePerusahaan);
		$q = $data->row_array();
		$SOWNamaPerusahaan = $q['NamaPerusahaan'];

		$SOWKodeSubcon = strip_tags($this->input->post('Subcon'));
		$data = $this->m_subcon->get_subcon_byid($SOWKodeSubcon);
		$q = $data->row_array();
		$SOWNamaSubcon = $q['Nama'];

		$NoKontrak = $this->input->post('NoKontrak');
		$ThnKontrak1 = $this->input->post('ThnKontrak1');
		$ThnKontrak2 = $this->input->post('ThnKontrak2');
		$ThnKontrak3 = $this->input->post('ThnKontrak3');
		$Komentar = $this->input->post('Keterangan');
		$Tanggal = $this->input->post('datepicker');
		$userid = $this->session->userdata('user_id');
		$usernama = $this->session->userdata('name');

		foreach ($_POST['item_category'] as $key => $k) {
			if (!empty($k)) {

				$KodeRegional = $_POST['item_category'][$key];
				$data = $this->m_regional->get_regional_byid(trim($KodeRegional));
				$q = $data->row_array();
				$SOWNamaRegional = $q['NAMA'];

				$HrgRegional = $_POST['item_sub_category'][$key];
				//update
				$datamsow = array(
					'Line' => $Line,
					'ListJob' => $ListJob,
					'SOWJenis' => $JenisSOW,
					'Code' => $Code,
					'SOWDesc' => $SOWDesc,
					'SOWKodeUnit' => $SOWKodeUnit,
					'SOWNamaUnit' => $SOWNamaUnit,
					'PemFull' => $PemFull,
					'F7' => $F7,
					'F8' => $F8,
					'Pem1' => $Pem1,
					'Pem2' => $Pem2,
					'Pem3' => $Pem3,
					'SOWKodeCustomer' => $SOWKodeCustomer,
					'SOWNamaCustomer' => $SOWNamaCustomer,
					'SOWKodePerusahaan' => $SOWKodePerusahaan,
					'SOWNamaPerusahaan' => $SOWNamaPerusahaan,
					'SOWKodeSubcon' => $SOWKodeSubcon,
					'SOWNamaSubcon' => $SOWNamaSubcon,
					'NoKontrak' => $NoKontrak,
					'ThnKontrak1' => $ThnKontrak1,
					'ThnKontrak2' => $ThnKontrak2,
					'ThnKontrak3' => $ThnKontrak3,
					'Komentar' => $Keterangan,

					'SOWKodeRegional' => $KodeRegional,
					'SOWNamaRegional' => $SOWNamaRegional,
					'HrgRegional' => $HrgRegional,
					'created_at' => $Tanggal,
					'user_id' => $userid,
					'user_name' => $usernama,
				);

				$this->m_msow->simpan_msow($datamsow, 'refsow');
			}
		}
	}

	function simpan_update_msow()
	{
		$Line = $this->input->post('Line');
		$ListJob = $this->input->post('ListJob');
		$No = strip_tags($this->input->post('No'));
		$JenisSOW = $this->input->post('JenisSOW');
		$Code = $this->input->post('Code');
		$SOWDesc = $this->input->post('SOWDesc');

		$SOWKodeUnit = strip_tags($this->input->post('Unit'));
		$data = $this->m_unit->get_unit_byid($SOWKodeUnit);
		$q = $data->row_array();
		$SOWNamaUnit = $q['NamaUnit'];

		$PemFull = $this->input->post('PemFull');
		$F7 = $this->input->post('F7');
		$F8 = $this->input->post('F8');
		$Pem1 = $this->input->post('Pem1');
		$Pem2 = $this->input->post('Pem2');
		$Pem3 = $this->input->post('Pem3');

		$SOWKodeCustomer = strip_tags($this->input->post('Customer'));
		$data = $this->m_principal->get_principal_byid($SOWKodeCustomer);
		$q = $data->row_array();
		$SOWNamaCustomer = $q['NamaPrincipal'];

		$SOWKodeRegional = strip_tags($this->input->post('Regional'));
		$data = $this->m_regional->get_regional_byid($SOWKodeRegional);
		$q = $data->row_array();
		$SOWNamaRegional = $q['NAMA'];

		$HrgRegional = $this->input->post('HrgRegional');

		$SOWKodePerusahaan = strip_tags($this->input->post('Perusahaan'));
		$data = $this->m_perusahaan->get_perusahaan_byid($SOWKodePerusahaan);
		$q = $data->row_array();
		$SOWNamaPerusahaan = $q['NamaPerusahaan'];

		$SOWKodeSubcon = strip_tags($this->input->post('Subcon'));
		$data = $this->m_subcon->get_subcon_byid($SOWKodeSubcon);
		$q = $data->row_array();
		$SOWNamaSubcon = $q['Nama'];

		$NoKontrak = $this->input->post('NoKontrak');
		$ThnKontrak1 = $this->input->post('ThnKontrak1');
		$ThnKontrak2 = $this->input->post('ThnKontrak2');
		$ThnKontrak3 = $this->input->post('ThnKontrak3');
		$Keterangan = $this->input->post('Keterangan');

		$datamsow = array(
			'No' => $No,
			'Line' => $Line,
			'ListJob' => $ListJob,
			'SOWJenis' => $JenisSOW,
			'Code' => $Code,
			'SOWDesc' => $SOWDesc,
			'SOWKodeUnit' => $SOWKodeUnit,
			'SOWNamaUnit' => $SOWNamaUnit,
			'PemFull' => $PemFull,
			'F7' => $F7,
			'F8' => $F8,
			'Pem1' => $Pem1,
			'Pem2' => $Pem2,
			'Pem3' => $Pem3,
			'SOWKodeCustomer' => $SOWKodeCustomer,
			'SOWNamaCustomer' => $SOWNamaCustomer,
			'SOWKodeRegional' => $SOWKodeRegional,
			'SOWNamaRegional' => $SOWNamaRegional,
			'HrgRegional' => $HrgRegional,
			'SOWKodePerusahaan' => $SOWKodePerusahaan,
			'SOWNamaPerusahaan' => $SOWNamaPerusahaan,
			'SOWKodeSubcon' => $SOWKodeSubcon,
			'SOWNamaSubcon' => $SOWNamaSubcon,
			'NoKontrak' => $NoKontrak,
			'ThnKontrak1' => $ThnKontrak1,
			'ThnKontrak2' => $ThnKontrak2,
			'ThnKontrak3' => $ThnKontrak3,
			'Komentar' => $Keterangan
		);

		$where = array(
			'No' => $No
		);

		$this->m_msow->update_edit_msow($where, $datamsow, 'refsow');
		echo $this->session->set_flashdata('msg', 'info');
		redirect('admin/msow');
	}

	function hapus_msow()
	{
		$No = strip_tags($this->input->post('No'));
		$this->m_msow->hapus_msow($No);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/msow');
	}

	function hapus_line_msow()
	{
		$No = strip_tags($this->input->post('No'));
		$this->m_msow->hapus_line_msow($No);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/msow');
	}

	function simpan_msow_detail()
	{
		$connect = mysqli_connect("202.78.195.194", "usersakti", "u5ers4kti", "dbinvoice");

		$Code  = $_POST["Code"];

		$item_category  = $_POST["item_category"];
		$item_sub_category  = $_POST["item_sub_category"];

		$statement = $connect->prepare("INSERT INTO refsowdetail ( Code, KodeRegional, HargaRegional) VALUES ( ?, ?, ?)");
		for ($count = 0, $c = count($item_category); $count < $c; $count++) {
			$statement->bind_param(
				'sss',
				$Code,

				$item_category[$count],
				$item_sub_category[$count]
			);
			$statement->execute();
		}
	}

	function get_detail_msow_data()
	{
		$Code = $this->uri->segment('4');

		$data['data1'] = $this->m_msow->get_msow_by_kode3(base64_decode($Code));
		$data['refsowdetail'] = $this->m_msow->get_msow_by_kode2(base64_decode($Code));
		$this->load->view('admin/v_detail_msow_data', $data);
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_msow->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_msow_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'Code' => $row['A'], // Insert data nis dari kolom A di excel
					'SOWDesc' => $row['B'],
					'SOWKodeUnit' => $row['C'],
					'SOWNamaUnit' => $row['D'],
					'PemFull' => $row['E'],
					'F7' => $row['F'],
					'F8' => $row['G'],
					'Pem1' => $row['H'],
					'Pem2' => $row['I'],
					'Pem3' => $row['J'],
					'SOWKodeCustomer' => $row['K'],
					'SOWNamaCustomer' => $row['L'],
					'SOWKodeSubcon' => $row['M'],
					'SOWNamaSubcon' => $row['N'],
					'SOWKodeRegional' => $row['O'],
					'SOWNamaRegional' => $row['P'],
					'SOWKodePerusahaan' => $row['Q'],
					'SOWNamaPerusahaan' => $row['R'],
					'HrgRegional' => $row['S'],
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_msow->insert_multiple($data);

		redirect("admin/msow"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER SOW"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:S1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Kode SOW"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Nama SOW");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Kode Unit");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Nama Unit");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', " Persentase 1 (%)");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', " Persentase 2 (%)");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', " Persentase 3 (%)");
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "Pembayaran 1 (Rp)");
		$excel->setActiveSheetIndex(0)->setCellValue('I3', "Pembayaran 2 (Rp)");
		$excel->setActiveSheetIndex(0)->setCellValue('J3', "Pembayaran 3 (Rp)");
		$excel->setActiveSheetIndex(0)->setCellValue('K3', "Kode Customer");
		$excel->setActiveSheetIndex(0)->setCellValue('L3', "Nama Customer");
		$excel->setActiveSheetIndex(0)->setCellValue('M3', "Kode Subcon");
		$excel->setActiveSheetIndex(0)->setCellValue('N3', "Nama Subcon");
		$excel->setActiveSheetIndex(0)->setCellValue('O3', "Kode Regional");
		$excel->setActiveSheetIndex(0)->setCellValue('P3', "Nama Regional");
		$excel->setActiveSheetIndex(0)->setCellValue('Q3', "Kode Perusahaan");
		$excel->setActiveSheetIndex(0)->setCellValue('R3', "Nama Perusahaan");
		$excel->setActiveSheetIndex(0)->setCellValue('S3', "Harga");


		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('S3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$msow = $this->m_msow->get_all_msow2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($msow as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->Code);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->SOWDesc);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->SOWKodeUnit);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->SOWNamaUnit);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->PemFull);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->F7);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->F8);
			$excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->Pem1);
			$excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->Pem2);
			$excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->Pem3);
			$excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->SOWKodeCustomer);
			$excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->SOWNamaCustomer);
			$excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->SOWKodeSubcon);
			$excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->SOWNamaSubcon);
			$excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->SOWKodeRegional);
			$excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->SOWNamaRegional);
			$excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $data->SOWKodePerusahaan);
			$excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $data->SOWNamaPerusahaan);
			$excel->setActiveSheetIndex(0)->setCellValue('S' . $numrow, $data->HrgRegional);


			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('R' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('S' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(20); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(16);
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('M')->setWidth(16);
		$excel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('O')->setWidth(16);
		$excel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('Q')->setWidth(16);
		$excel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
		$excel->getActiveSheet()->getColumnDimension('S')->setWidth(30);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master SOW");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master SOW.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}

	function get_view_edit()
	{
		$No = $this->uri->segment(4);
		$data['data1'] = $this->m_msow->get_msow_by_line($No);
		$data['refprincipal'] = $this->m_principal->get_all_principal3();
		if ($this->session->userdata('is_admin') === TRUE) {
			$data['refregional'] = $this->m_regional->get_all_regional2();
		} else {
			$data['refregional'] = $this->m_regional->get_where_regional2(['KODE' => $this->session->userdata('user_region')]);
		}

		$data['refperusahaan'] = $this->m_perusahaan->get_all_perusahaan3();
		$data['refsubcon'] = $this->m_subcon->get_all_subcon3();
		$data['refunit'] = $this->m_unit->get_all_unit3();

		$this->load->view('admin/v_view_edit_msow', $data);
	}

	function submit_edit_msow()
	{
		if ($_POST) {

			$Line = $this->input->post('Line');
			$ListJob = $this->input->post('ListJob');
			$No = strip_tags($this->input->post('No'));
			$JenisSOW = $this->input->post('JenisSOW');
			$Code = $this->input->post('Code');
			$SOWDesc = $this->input->post('SOWDesc');

			$SOWKodeUnit = strip_tags($this->input->post('Unit'));
			$data = $this->m_unit->get_unit_byid($SOWKodeUnit);
			$q = $data->row_array();
			$SOWNamaUnit = $q['NamaUnit'];

			$PemFull = $this->input->post('PemFull');
			$F7 = $this->input->post('F7');
			$F8 = $this->input->post('F8');
			$Pem1 = $this->input->post('Pem1');
			$Pem2 = $this->input->post('Pem2');
			$Pem3 = $this->input->post('Pem3');

			$SOWKodeCustomer = strip_tags($this->input->post('Customer'));
			$data = $this->m_principal->get_principal_byid($SOWKodeCustomer);
			$q = $data->row_array();
			$SOWNamaCustomer = $q['NamaPrincipal'];

			$SOWKodePerusahaan = strip_tags($this->input->post('Perusahaan'));
			$data = $this->m_perusahaan->get_perusahaan_byid($SOWKodePerusahaan);
			$q = $data->row_array();
			$SOWNamaPerusahaan = $q['NamaPerusahaan'];

			$SOWKodeSubcon = strip_tags($this->input->post('Subcon'));
			$data = $this->m_subcon->get_subcon_byid($SOWKodeSubcon);
			$q = $data->row_array();
			$SOWNamaSubcon = $q['Nama'];

			$NoKontrak = $this->input->post('NoKontrak');
			$ThnKontrak1 = $this->input->post('ThnKontrak1');
			$ThnKontrak2 = $this->input->post('ThnKontrak2');
			$ThnKontrak3 = $this->input->post('ThnKontrak3');
			$Keterangan = $this->input->post('Keterangan');

			if ($_POST['item_category'] != NULL) {
				$this->m_msow->hapus_line_msow($this->input->post('LineLama'));
			}
			foreach ($_POST['item_category'] as $key => $k) {
				if (!empty($k)) {

					$KodeRegional = $_POST['item_category'][$key];
					$data = $this->m_regional->get_regional_byid(trim($KodeRegional));
					$q = $data->row_array();
					$SOWNamaRegional = $q['NAMA'];

					$HrgRegional = $_POST['item_sub_category'][$key];
					//update
					$datamsow = array(
						'Line' => $Line,
						'ListJob' => $ListJob,
						'SOWJenis' => $JenisSOW,
						'Code' => $Code,
						'SOWDesc' => $SOWDesc,
						'SOWKodeUnit' => $SOWKodeUnit,
						'SOWNamaUnit' => $SOWNamaUnit,
						'PemFull' => $PemFull,
						'F7' => $F7,
						'F8' => $F8,
						'Pem1' => $Pem1,
						'Pem2' => $Pem2,
						'Pem3' => $Pem3,
						'SOWKodeCustomer' => $SOWKodeCustomer,
						'SOWNamaCustomer' => $SOWNamaCustomer,
						'SOWKodePerusahaan' => $SOWKodePerusahaan,
						'SOWNamaPerusahaan' => $SOWNamaPerusahaan,
						'SOWKodeSubcon' => $SOWKodeSubcon,
						'SOWNamaSubcon' => $SOWNamaSubcon,
						'NoKontrak' => $NoKontrak,
						'ThnKontrak1' => $ThnKontrak1,
						'ThnKontrak2' => $ThnKontrak2,
						'ThnKontrak3' => $ThnKontrak3,
						'Komentar' => $Keterangan,

						'SOWKodeRegional' => $KodeRegional,
						'SOWNamaRegional' => $SOWNamaRegional,
						'HrgRegional' => $HrgRegional,
					);

					$this->m_msow->simpan_msow($datamsow, 'refsow');
				}
			}
		}
	}

	function ajax_table_sow()
    {
        $list_semua = $this->m_msow->get_datatables();
        $list = $this->m_msow->get_datatables_line();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {


			// add dokumen
			$regional = '';
			foreach($list_semua as $x)
			{
				if ($field->Line === $x->Line) 
				{
					$regional .= $x->SOWNamaRegional . "<hr />";
				}
			}
			//

			// add dokumen
			$harga = '';
			foreach($list_semua as $x)
			{
				if ($field->Line === $x->Line) 
				{
					$harga .= $x->HrgRegional . "<hr />";
				}
			}
	
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->Line;
            $row[] = $field->SOWJenis;
            $row[] = $field->Code;
            $row[] = $field->SOWDesc;
            $row[] = $field->SOWKodeUnit;
            $row[] = $field->PemFull;
            $row[] = $field->F7;
            $row[] = $field->F8;
            $row[] = $field->Pem1;
            $row[] = $field->Pem2;
			$row[] = $field->Pem3;
            $row[] = $field->SOWNamaCustomer;
            $row[] = $field->SOWNamaSubcon;
            $row[] = $field->SOWNamaPerusahaan;
            $row[] = $field->NoKontrak;
            $row[] = $field->ThnKontrak1 . ' - ' . $field->ThnKontrak2 . ' - ' . $field->ThnKontrak3;
            $row[] = $field->Komentar;
			
            $row[] = $regional;
            $row[] = $harga;
			$row[] = "
				<a style='padding: 6px 6px;' href='" .  base_url() . "admin/msow/get_view_edit/" . $field->Line . "'>
					<span class='fa fa-pencil'></span>
				</a>

				<a style='padding: 6px 6px;' id='hapus' data-sow='". $field->SOWDesc ."' data-line='". $field->Line ."' data-target='#ModalHapus'><span class='fa fa-trash'></span></a>
			";
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_msow->count_all(),
            "recordsFiltered" => $this->m_msow->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
}
