<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{
	private $filename = "import_data"; // Kita tentukan nama filenya

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_karyawan');
		$this->load->model('m_regional');
		$this->load->model('m_bank');
		$this->load->helper('url');
	}

	function index()
	{
		$x['refkaryawan'] = $this->m_karyawan->get_all_karyawan()->result();
		$x['refregional'] = $this->m_regional->get_all_regional2();
		$x['bank'] = $this->m_bank->get_all_bank()->result();

		$this->load->view('admin/v_karyawan', $x);
	}

	function get_edit()
	{
		$KodeAkun = $this->uri->segment(4);
		$data['data1'] = $this->m_karyawan->get_karyawan_by_kode($KodeAkun);
		$data['refregional'] = $this->m_regional->get_all_regional();
		$data['refbank'] = $this->m_bank->get_all_bank();

		$this->load->view('admin/v_edit_karyawan', $data);
	}

	function simpan_karyawan()
	{
		$KodeAkun = strip_tags($this->input->post('KodeAkun'));
		$Nama = $this->input->post('NamaKaryawan');
		$Alamat = $this->input->post('Alamat');
		$KodeReg = $this->input->post('Regional');

		$KodeBank = strip_tags($this->input->post('Bank'));
		$data = $this->m_bank->get_bank_byid($KodeBank);
		$q = $data->row_array();
		$NamaBank = $q['NamaBank'];

		$NoRek = $this->input->post('NoRek');
		$StsKaryawan = $this->input->post('StsKaryawan');

		$datakaryawan = array(
			'KodeAkun' => $KodeAkun,
			'Nama' => $Nama,
			'Alamat' => $Alamat,
			'KodeReg' => $KodeReg,
			'KodeBank' => $KodeBank,
			'Bank' => $NamaBank,
			'NoRek' => $NoRek,
			'StsKaryawan' => $StsKaryawan
		);

		$this->m_karyawan->simpan_karyawan($datakaryawan, 'masterkaryawan');
		echo $this->session->set_flashdata('msg', 'success');
		redirect('admin/karyawan');
	}

	function simpan_update_karyawan()
	{
		$KodeAkun = strip_tags($this->input->post('KodeAkun'));
		$Nama = $this->input->post('NamaKaryawan');
		$Alamat = $this->input->post('Alamat');
		$KodeReg = $this->input->post('Regional');

		$KodeBank = strip_tags($this->input->post('Bank'));
		$data = $this->m_bank->get_bank_byid($KodeBank);
		$q = $data->row_array();
		$NamaBank = $q['NamaBank'];

		$NoRek = $this->input->post('NoRek');
		$StsKaryawan = $this->input->post('StsKaryawan');

		$datakaryawan = array(
			'KodeAkun' => $KodeAkun,
			'Nama' => $Nama,
			'Alamat' => $Alamat,
			'KodeReg' => $KodeReg,
			'KodeBank' => $KodeBank,
			'Bank' => $NamaBank,
			'NoRek' => $NoRek,
			'StsKaryawan' => $StsKaryawan
		);

		$where = array(
			'KodeAkun' => $KodeAkun
		);

		$this->m_karyawan->update_edit_karyawan($where, $datakaryawan, 'masterkaryawan');
		redirect('admin/karyawan');
	}

	function hapus_karyawan()
	{
		$KodeAkun = strip_tags($this->input->post('KodeAkun'));
		$this->m_karyawan->hapus_karyawan($KodeAkun);

		echo $this->session->set_flashdata('msg', 'success-hapus');
		redirect('admin/karyawan');
	}

	public function form()
	{
		$data = array(); // Buat variabel $data sebagai array

		if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->m_unit->upload_file($this->filename);

			if ($upload['result'] == "success") { // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet;
			} else { // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}

		$this->load->view('admin/v_unit_form', $data);
	}

	public function import()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . '.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'KodeUnit' => $row['A'], // Insert data nis dari kolom A di excel
					'NamaUnit' => $row['B'],

				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}
		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->m_unit->insert_multiple($data);

		redirect("admin/unit"); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}

	public function export()
	{
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		// Settingan awal fil excel
		$excel->getProperties()->setCreator('My Notes Code')
			->setLastModifiedBy('My Notes Code')
			->setTitle("Data Siswa")
			->setSubject("Siswa")
			->setDescription("Laporan Semua Data Siswa")
			->setKeywords("Data Siswa");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		$excel->setActiveSheetIndex(0)->setCellValue('A1', "MASTER KARYAWAN"); // Set kolom A1 dengan tulisan "DATA SISWA"
		$excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Regional"); // Set kolom A3 dengan tulisan "NO"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "Kode Akun");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Nama Karyawan");
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Alamat");
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Bank");
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "No Rekening");
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "Status");


		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);

		// Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
		$karyawan = $this->m_karyawan->get_all_karyawan2();
		//$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach ($karyawan as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $data->KodeReg);
			$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->KodeAkun);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->Nama);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->Alamat);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->Bank);
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->NoRek);
			$excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->StsKaryawan);


			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);

			//$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Master Karyawan");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Master Karyawan.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}
}
