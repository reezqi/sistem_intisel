<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sowdetail extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_sowdetail');
	}

	function index()
	{
		$x['data']=$this->m_sowdetail->get_all_sowdetail();
		$this->load->view('admin/v_sow_detail',$x);
	}

}