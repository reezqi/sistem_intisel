<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shoplist extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_shoplist');
	}

	function index()
	{
		$x['data']=$this->m_shoplist->get_all_shoplist();
		$this->load->view('admin/v_shoplist',$x);
	}

}