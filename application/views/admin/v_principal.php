<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Customer
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li class="active"> Customer</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Tambah Customer</a>
                                    <a class="btn btn-primary btn-sm" href="<?php echo base_url("admin/principal/form"); ?>"><span class="fa fa-user-plus"></span> Import Data</a>
                                    <a class="btn btn-info btn-sm" href="<?php echo base_url("admin/principal/export"); ?>"><span class="fa fa-file-excel-o"> </span> Export Data</a>
                                </div>

                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                                            <thead>
                                                <tr>
                                                    <th class="bg-primary">No</th>
                                                    <th class="bg-primary">Kode Customer</th>
                                                    <th class="bg-primary">Nama Customer</th>
                                                    <th class="bg-primary">Alamat</th>
                                                    <th class="bg-primary">Kota</th>
                                                    <th class="bg-primary">Provinsi</th>
                                                    <th class="bg-primary">Kode Pos</th>
                                                    <th class="bg-primary">No Telpon</th>
                                                    <th class="bg-primary">No HP</th>
                                                    <th class="bg-primary">Email</th>
                                                    <th class="bg-primary">NPWP</th>
                                                    <th class="bg-primary">Web</th>
                                                    <th class="bg-primary">Contact Person</th>
                                                    <th class="bg-primary" style="text-align:right;">Operasi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $no = 0;
                                                foreach ($refprincipal as $i) {
                                                    $no++; ?>
                                                    <tr>
                                                        <td><?php echo $no; ?></td>
                                                        <td><?php echo $i->kodePrincipal ?></td>
                                                        <td><?php echo $i->NamaPrincipal ?></td>
                                                        <td><?php echo $i->Alamat ?></td>
                                                        <td><?php echo $i->PrincipalKabupatenNama ?></td>
                                                        <td><?php echo $i->PrincipalProvinsiNama ?></td>
                                                        <td><?php echo $i->KodePos ?></td>
                                                        <td><?php echo $i->NoTelp ?></td>
                                                        <td><?php echo $i->NoHp ?></td>
                                                        <td><?php echo $i->Email ?></td>
                                                        <td><?php echo $i->NPWP ?></td>
                                                        <td><?php echo $i->Website ?></td>
                                                        <td><?php echo $i->ContactPerson ?></td>

                                                        <td style="text-align:right;">
                                                            <a style="padding: 6px 6px;" href="<?php echo base_url() . 'admin/principal/get_edit/' . $i->kodePrincipal; ?>"><span class="fa fa-pencil"></span></a>

                                                            <a style="padding: 6px 6px;" data-toggle="modal" data-target="#ModalHapus<?php echo $i->kodePrincipal; ?>"><span class="fa fa-trash"></span></a>
                                                        </td>

                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

                    </div>
                    <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>


        <!--Modal Add Regional-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#337AB7">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel" style="color:white">Tambah Customer</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url() . 'admin/principal/simpan_principal' ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Customer</label>
                                <div class="col-sm-7">
                                    <input type="text" name="kodePrincipal" class="form-control" id="inputUserName" placeholder="Kode Customer" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Customer</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NamaPrincipal" class="form-control" id="inputUserName" placeholder="Nama Customer" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Alamat</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="Alamat" class="form-control" id="inputUserName" placeholder="Alamat" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Provinsi</label>
                                <div class="col-sm-7">
                                    <select name="provinsi" class="form-control" id="provinsi">
                                        <option value="">No Selected</option>
                                        <?php foreach ($provinsi as $row) : ?>
                                            <option value="<?php echo $row->ProvinsiId; ?>"><?php echo $row->ProvinsiId; ?> - <?php echo $row->ProvinsiNama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kota</label>
                                <div class="col-sm-7">
                                    <select name="kabupaten" id="kabupaten" class="kabupaten form-control">
                                        <option>No Selected</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Pos</label>
                                <div class="col-sm-4">
                                    <input type="text" name="KodePos" class="form-control" id="inputUserName" placeholder="Kode Pos" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Telpon</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoTelp" class="form-control" id="inputUserName" placeholder="No Telpon" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No HP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoHp" class="form-control" id="inputUserName" placeholder="No HP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Email</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Email" class="form-control" id="inputUserName" placeholder="Email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> NPWP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NPWP" class="form-control" id="inputUserName" placeholder="NPWP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Web</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Website" class="form-control" id="inputUserName" placeholder="Web" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Contact Person</label>
                                <div class="col-sm-7">
                                    <input type="text" name="ContactPerson" class="form-control" id="inputUserName" placeholder="Contact Person" required>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                            <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--Modal Add Regional-->

        <!--Modal Edit Principal-->
        <?php foreach ($refprincipal as $i) { ?>

            <div class="modal fade" id="ModalEdit<?php echo $i->kodePrincipal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#337AB7">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                            <h4 class="modal-title" id="myModalLabel" style="color:white">Edit Customer</h4>
                        </div>

                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/principal/update_principal' ?>" method="post" enctype="multipart/form-data">
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Nama Customer</label>
                                    <div class="col-sm-7">
                                        <input type="hidden" name="kodePrincipal" value="<?php echo $i->kodePrincipal; ?>">
                                        <input type="text" name="NamaPrincipal" class="form-control" value="<?php echo $i->NamaPrincipal; ?>" id="inputUserName" placeholder="Nama Unit" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Alamat Regional</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="Alamat" class="form-control" value="<?php echo $i->Alamat; ?>" id="inputUserName" placeholder="Alamat Regional" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Kota</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="Kota" class="form-control" value="<?php echo $i->Kota; ?>" id="inputUserName" placeholder="Kota" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Provinsi</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="Provinsi" class="form-control" value="<?php echo $i->Provinsi; ?>" id="inputUserName" placeholder="Provinsi" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Kode Pos</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="KodePos" class="form-control" value="<?php echo $i->KodePos; ?>" id="inputUserName" placeholder="Kode Pos" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">No Telepon</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="NoTelp" class="form-control" value="<?php echo $i->NoTelp; ?>" id="inputUserName" placeholder="No Telepon" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">No HP</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="NoHp" class="form-control" value="<?php echo $i->NoHp; ?>" id="inputUserName" placeholder="No HP" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="Email" class="form-control" value="<?php echo $i->Email; ?>" id="inputUserName" placeholder="Email" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">NPWP</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="NPWP" class="form-control" value="<?php echo $i->NPWP; ?>" id="inputUserName" placeholder="NPWP" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Web</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="Website" class="form-control" value="<?php echo $i->Website; ?>" id="inputUserName" placeholder="Web" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputUserName" class="col-sm-4 control-label">Contact Person</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="Contact Person" class="form-control" value="<?php echo $i->ContactPerson; ?>" id="inputUserName" placeholder="Contact Person" required>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Perbarui</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!--Modal Edit Unit-->

        <?php foreach ($refprincipal as $i) { ?>
            <!--Modal Hapus Unit-->
            <div class="modal fade" id="ModalHapus<?php echo $i->kodePrincipal; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color:#337AB7">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                            <h4 class="modal-title" id="myModalLabel" style="color:white">Hapus Customer</h4>
                        </div>
                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/principal/hapus_principal' ?>" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                <input type="hidden" name="kodePrincipal" value="<?php echo $i->kodePrincipal; ?>" />
                                <p>Apakah Anda yakin menghapus Customer <b><?php echo $i->NamaPrincipal; ?></b> ?</p>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>



        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
        <!-- page script -->

        <script type="text/javascript">
            $(document).ready(function() {

                $('#provinsi').change(function() {
                    var id = $(this).val();
                    $.ajax({
                        url: "<?php echo site_url('admin/regional/get_sub_provinsi'); ?>",
                        method: "POST",
                        data: {
                            id: id
                        },
                        async: true,
                        dataType: 'json',
                        success: function(data) {

                            var html = '';
                            var i;
                            for (i = 0; i < data.length; i++) {
                                html += '<option value=' + data[i].KabupatenId + '>' + data[i].KabupatenNama + '</option>';
                            }
                            $('#kabupaten').html(html);

                        }
                    });
                    return false;
                });

            });
        </script>

        <script>
            $(function() {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });

                $('#datepicker').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $('#datepicker2').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $('.datepicker3').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $('.datepicker4').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $(".timepicker").timepicker({
                    showInputs: true
                });

            });
        </script>
        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Principal Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Principal berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Principal Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>
</body>

</html>