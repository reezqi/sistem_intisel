<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Matriks PDP
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li class="active"> Matriks PDP</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">

                        <!-- Horizontal Form -->
                        <div class="box box-info">
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form class="form-horizontal" id="formcari"  action="POST">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-3 control-label"> Customer </label>
                                        <div class="col-sm-3">
                                            <select class="form-control input-sm" name="KodeCustomer" id="KodeCustomer" style="width: 100%;">
                                                <option value="">No Selected</option>
                                                <?php foreach ($refprincipal as $i) { // Lakukan looping pada variabel siswa dari controller
                                                    echo "<option value='" . $i->kodePrincipal . "'>" . $i->kodePrincipal . " - " . $i->NamaPrincipal . "</option>";
                                                } ?>
                                            </select>
                                        </div>

                                        <label for="inputUserName" class="col-sm-1 control-label"> SOW </label>
                                        <div class="col-sm-3">
                                            <select class="form-control input-sm" name="KodeSOW" id="KodeSOW" style="width: 100%;">
                                                <option value="">No Selected</option>
                                                <?php foreach ($refmsow as $i) { // Lakukan looping pada variabel siswa dari controller
                                                    echo "<option value='" . $i->Code . "'>" . $i->Code . " - " . $i->SOW . "</option>";
                                                } ?>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-sm" id="simpan"><span class="fa  fa-search"></span> Submit</button>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-3 control-label"> Regional </label>
                                        <div class="col-sm-3">
                                            <select class="form-control input-sm" name="KodeRegional" id="KodeRegional" style="width: 100%;">
                                                <option value="">No Selected</option>
                                                <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                                                    echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                                                } ?>
                                            </select>
                                        </div>

                                        <label for="inputUserName" class="col-sm-1 control-label"> Site </label>
                                        <div class="col-sm-3">
                                            <select class="form-control input-sm" name="KodeSite" id="KodeSite" style="width: 100%;">
                                                <option value="">No Selected</option>
                                                <?php foreach ($refsite as $i) { // Lakukan looping pada variabel siswa dari controller
                                                    echo "<option value='" . $i->codeSite . "'>" . $i->codeSite . " - " . $i->siteName . "</option>";
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->

                        <div class="box">
                            <div class="box-header">
                                <!-- / <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span>Tambah Matriks PDP</a> -->
                                <a class="btn btn-success btn-sm" href="<?php echo base_url("admin/kategoridokumen/add_kategoridokumen"); ?>"><span class="fa fa-user-plus"></span>Tambah Matriks PDP</a>
                                <a class="btn btn-primary btn-sm" href="<?php echo base_url("admin/kategoridokumen/form"); ?>"><span class="fa fa-user-plus"></span> Import Data</a>
                                <a class="btn btn-info btn-sm" href="<?php echo base_url("admin/kategoridokumen/export"); ?>"><span class="fa fa-file-excel-o"> </span> Export Data</a>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-striped" style="font-size:12px;">
                                        <thead>
                                            <tr>
                                                <th class="bg-primary">No</th>
                                                <th class="bg-primary">Customer</th>
                                                <th class="bg-primary">SOW</th>
                                                <th class="bg-primary">Site</th>
                                                <th class="bg-primary">Regional</th>
                                                <th class="bg-primary">Progres/Tahapan</th>
                                                <th class="bg-primary">Dokumen</th>
                                                <th class="bg-primary">%</th>
                                                <th class="bg-primary">Status</th>
                                                <th class="bg-primary" style="text-align:right;">Operasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                         
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php $this->load->view('admin/v_footer'); ?>


        <!--Modal Hapus Pengguna-->
        <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#337AB7">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel" style="color:white">Hapus Matriks PDP</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url() . 'admin/kategoridokumen/hapus_kategoridokumen' ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <input type="hidden" name="line" id="line"/>
                            <p>Apakah Anda yakin menghapus Matriks PDP </p>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                            <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

    <script type="text/javascript">
        var table;
        $(document).ready(function() {

             //datatables
            table = $('#example1').DataTable({ 
                "processing": true, 
                "serverSide": true, 
                "order": [], 
                
                "ajax": {
                    "url": "<?php echo site_url('admin/kategoridokumen/ajax_table_matrik')?>",
                    "type": "POST",
                    "dataType": "JSON",
                    "data": {
                      "KodeRegional"    : $('#KodeRegional').val(),
                      "KodeCustomer"    : $('#KodeCustomer').val(),
                      "KodeSite"        : $('#KodeSite').val(),
                      "KodeSOW"         : $('#KodeSOW').val(),
                    }
                },

                
                "columnDefs": [
                    { 
                        "targets": [ 0 ], 
                        "orderable": false, 
                    },
                ],

            });

            $('#example1').on('click', '#hapus', function(e) {
                var line = $(this).attr('data-line')
                $('#line').val(line)
                $('#ModalHapus').modal('show');
                
            })

            $("#formcari").submit(function(e){
            //
            e.preventDefault()
            table = $('#example1').DataTable({ 
                "bDestroy": true,
                "processing": true, 
                "serverSide": true, 
                "order": [], 
                
                "ajax": {
                    "url": "<?php echo site_url('admin/kategoridokumen/ajax_table_matrik')?>",
                    "type": "POST",
                    "dataType": "JSON",
                    "data": {
                      "KodeRegional"    : $('#KodeRegional').val(),
                      "KodeCustomer"    : $('#KodeCustomer').val(),
                      "KodeSite"        : $('#KodeSite').val(),
                      "KodeSOW"         : $('#KodeSOW').val(),
                    }
                },

                
                "columnDefs": [
                    { 
                        "targets": [ 0 ], 
                        "orderable": false, 
                    },
                ],
            });
            // 
        });
           

        });

    </script>

    <script>
        $(function() {
            
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $('#datepicker2').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $('.datepicker3').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $('.datepicker4').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $(".timepicker").timepicker({
                showInputs: true
            });

        });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
        <script type="text/javascript">
            $.toast({
                heading: 'Error',
                text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                showHideTransition: 'slide',
                icon: 'error',
                autohide: true,
                position: 'bottom-right',
                bgColor: '#FF4859'
            });
        </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
        <script type="text/javascript">
            $.toast({
                heading: 'Success',
                text: "Matriks PDP Berhasil disimpan ke database.",
                showHideTransition: 'slide',
                icon: 'success',
                autohide: true,
                position: 'bottom-right',
                bgColor: '#7EC857'
            });
        </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
        <script type="text/javascript">
            $.toast({
                heading: 'Info',
                text: "Matriks PDP berhasil di update",
                showHideTransition: 'slide',
                icon: 'info',
                autohide: true,
                position: 'bottom-right',
                bgColor: '#00C9E6'
            });
        </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
        <script type="text/javascript">
            $.toast({
                heading: 'Success',
                text: "Matriks PDP Berhasil dihapus.",
                showHideTransition: 'slide',
                icon: 'success',
                autohide: true,
                position: 'bottom-right',
                bgColor: '#7EC857'
            });
        </script>
    <?php else : ?>

    <?php endif; ?>


</body>

</html>