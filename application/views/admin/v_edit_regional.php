<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit Regional
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Edit Regional</a></li>
                    <li class="active">Edit Regional</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="modal-dialog" role="document">
                        <!--Modal Edit Regional-->
                        <?php
                        $b = $data1->row_array();
                        ?>

                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/regional/simpan_update_regional' ?>" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Regional</label>
                                <div class="col-sm-7">
                                    <input type="hidden" name="KODE" value="<?php echo $b['KODE']; ?>">
                                    <input type="text" name="NAMA" class="form-control" value="<?php echo $b['NAMA']; ?>" id="inputUserName" placeholder="Nama Regional" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Alamat </label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="Alamat" placeholder="Alamat " required><?php echo $b['Alamat']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Provinsi</label>
                                <div class="col-sm-7">
                                    <select name="provinsi" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['RegionalProvinsiId'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kota/Kabupaten</label>
                                <div class="col-sm-7">
                                    <select name="kabupaten" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($kabupaten->result_array() as $i) {
                                            $KabupatenId = $i['KabupatenId'];
                                            $KabupatenNama = $i['KabupatenNama'];
                                            if ($b['RegionalKabupatenId'] == $KabupatenId)
                                                echo "<option value='$KabupatenId' selected>$KabupatenId - $KabupatenNama</option>";
                                            else
                                                echo "<option value='$KabupatenId'>$KabupatenId - $KabupatenNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Pos</label>
                                <div class="col-sm-4">
                                    <input type="text" name="KodePos" class="form-control" value="<?php echo $b['KodePos']; ?>" id="inputUserName" placeholder="Kode Pos" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Telepon</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoTelp" class="form-control" value="<?php echo $b['NoTelp']; ?>" id="inputUserName" placeholder="No Telepon" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No HP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoHp" class="form-control" value="<?php echo $b['NoHp']; ?>" id="inputUserName" placeholder="No HP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Email" class="form-control" value="<?php echo $b['Email']; ?>" id="inputUserName" placeholder="Email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Fax</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoFax" class="form-control" value="<?php echo $b['NoFax']; ?>" id="inputUserName" placeholder="No Fax" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Regional Leader</label>
                                <div class="col-sm-7">
                                    <input type="text" name="RegionalLeader" class="form-control" value="<?php echo $b['RegionalLeader']; ?>" id="inputUserName" placeholder="Regional Leader" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No HP RL</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoHpRL" class="form-control" value="<?php echo $b['NoHpRL']; ?>" id="inputUserName" placeholder="No HP RL" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Email RL</label>
                                <div class="col-sm-7">
                                    <input type="text" name="EmailRL" class="form-control" value="<?php echo $b['EmailRL']; ?>" id="inputUserName" placeholder="Email RL" required>
                                </div>
                            </div>

                           <!--  <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">SAM</label>
                                <div class="col-sm-7">
                                    <input type="text" name="SAM" class="form-control" value="<?php echo $b['SAM']; ?>" id="inputUserName" placeholder="SAM" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">VPRM</label>
                                <div class="col-sm-7">
                                    <input type="text" name="VPRM" class="form-control" value="<?php echo $b['VPRM']; ?>" id="inputUserName" placeholder="VPRM" required>
                                </div>
                            </div> -->

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Perusahaan</label>
                                <div class="col-sm-7">
                                    <select name="perusahaan" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($perusahaan->result_array() as $i) {
                                            $KodePerusahaan = $i['KodePerusahaan'];
                                            $NamaPerusahaan = $i['NamaPerusahaan'];
                                            if ($b['RegionalKodePerusahaan'] == $KodePerusahaan)
                                                echo "<option value='$KodePerusahaan' selected>$KodePerusahaan - $NamaPerusahaan</option>";
                                            else
                                                echo "<option value='$KodePerusahaan'>$KodePerusahaan - $NamaPerusahaan</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" onclick="window.history.back();" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                        </form>
                    </div>
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

        <!-- FastClick -->
        <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
        <!-- DataTables -->


        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Agenda berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>



</body>

</html>