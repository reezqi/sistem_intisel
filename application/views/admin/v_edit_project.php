<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Edit Project
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#">Edit Project</a></li>
          <li class="active">Edit Project</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="modal-dialog" role="document">

            <?php $b = $data1->row_array(); ?>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/project/simpan_update_project' ?>" method="post" enctype="multipart/form-data">

              <div class="form-group">
                <input type="hidden" name="KodeProject" value="<?php echo $b['KodeProject']; ?>">
                <label for="inputUserName" class="col-sm-4 control-label">Kode Customer</label>
                <div class="col-sm-7">
                  <select name="KodeCustomer" class="form-control" required>
                    <option value="">No Selected</option>
                    <?php
                    foreach ($refprincipal->result_array() as $i) {
                      $kodePrincipal = $i['kodePrincipal'];
                      $NamaPrincipal = $i['NamaPrincipal'];
                      if ($b['ProjectKodeCustomer'] == $kodePrincipal)
                        echo "<option value='$kodePrincipal' selected>$kodePrincipal - $NamaPrincipal</option>";
                      else
                        echo "<option value='$kodePrincipal'>$kodePrincipal - $NamaPrincipal</option>";
                    } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Nama Project</label>
                <div class="col-sm-7">
                  <input type="text" name="Nama" class="form-control" id="inputUserName" value="<?php echo $b['Nama']; ?>" placeholder="Nama Project " required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Alamat </label>
                <div class="col-sm-7">
                  <textarea class="form-control" rows="3" name="Alamat" placeholder="Alamat " placeholder="Alamat" required><?php echo $b['Alamat']; ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Provinsi</label>
                <div class="col-sm-7">
                  <select name="provinsi" class="form-control" required>
                    <option value="">No Selected</option>
                    <?php
                    foreach ($provinsi->result_array() as $i) {
                      $ProvinsiId = $i['ProvinsiId'];
                      $ProvinsiNama = $i['ProvinsiNama'];
                      if ($b['ProjekProvinsiId'] == $ProvinsiId)
                        echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                      else
                        echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                    } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Kota</label>
                <div class="col-sm-7">
                  <select name="kabupaten" class="form-control" required>
                    <option value="">No Selected</option>
                    <?php
                    foreach ($kabupaten->result_array() as $i) {
                      $KabupatenId = $i['KabupatenId'];
                      $KabupatenNama = $i['KabupatenNama'];
                      if ($b['ProjekKabupatenId'] == $KabupatenId)
                        echo "<option value='$KabupatenId' selected>$KabupatenId - $KabupatenNama</option>";
                      else
                        echo "<option value='$KabupatenId'>$KabupatenId - $KabupatenNama</option>";
                    } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Kode Pos</label>
                <div class="col-sm-4">
                  <input type="text" name="KodePos" class="form-control" value="<?php echo $b['KodePos']; ?>" id="inputUserName" placeholder="Kode Pos" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">No Telepon</label>
                <div class="col-sm-7">
                  <input type="text" name="NoTelp" class="form-control" value="<?php echo $b['NoTelp']; ?>" id="inputUserName" placeholder="No Telepon" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">PIC</label>
                <div class="col-sm-7">
                  <input type="text" name="PIC" class="form-control" id="inputUserName" value="<?php echo $b['PIC']; ?>" placeholder="PIC" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">No HP</label>
                <div class="col-sm-7">
                  <input type="text" name="NoHP" class="form-control" value="<?php echo $b['NoHP']; ?>" id="inputUserName" placeholder="No HP" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Email</label>
                <div class="col-sm-7">
                  <input type="text" name="Email" class="form-control" value="<?php echo $b['Email']; ?>" id="inputUserName" placeholder="Email" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">NPWP</label>
                <div class="col-sm-7">
                  <input type="text" name="Npwp" class="form-control" id="inputUserName" value="<?php echo $b['Npwp']; ?>" placeholder="NPWP" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Status </label>
                <div class="col-sm-4">
                  <input type="text" name="StsAktiv" class="form-control" id="inputUserName" value="<?php echo $b['StsAktiv']; ?>" placeholder="Status" required>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
              </div>
            </form>
          </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->


    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker2').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Agenda Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "Agenda berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Agenda Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>


</body>

</html>