<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>

  <script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>
  <script>
    $(document).ready(function() {
      // Sembunyikan alert validasi kosong
      $("#kosong").hide();
    });
  </script>

  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Customer
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
          <li class="active"> Customer</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">

              <div class="box">
                <div class="box-header">
                  <a class="btn btn-primary btn-sm" href="<?php echo base_url("excel/format_customer.xlsx"); ?>"><span class="fa fa-user-plus"></span> Download File</a>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="<?php echo base_url("admin/principal/form"); ?>" enctype="multipart/form-data">
                    <!--
    -- Buat sebuah input type file
    -- class pull-left berfungsi agar file input berada di sebelah kiri
    -->
                    <input type="file" name="file">

                    <!--
    -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
    -->
                    <br>
                    <input class="btn btn-warning btn-sm" type="submit" name="preview" value="Preview Dokumen">
                  </form>

                  <br>
                  <?php
                  if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
                    if (isset($upload_error)) { // Jika proses upload gagal
                      echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
                      die; // stop skrip
                    }

                    // Buat sebuah tag form untuk proses import data ke database
                    echo "<form method='post' action='" . base_url("admin/principal/import") . "'>";

                    // Buat sebuah div untuk alert validasi kosong
                    echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";

                    echo " <div class='table-responsive'>
                    <table id='example1' class='table table-sm table-striped table-hover' style='font-size:12px;'>
    <tr>
      <th colspan='13'>Preview Data</th>
    </tr>
    <tr>
      <th class='bg-primary'> Kode Customer </th>
      <th class='bg-primary'> Nama Customer </th>
      <th class='bg-primary'> Alamat </th>
      <th class='bg-primary'> Kode Kabupaten </th>
      <th class='bg-primary'> Nama Kabupaten </th>
      <th class='bg-primary'> Kode Provinsi </th>
      <th class='bg-primary'> Nama Provinsi </th>
      <th class='bg-primary'> Kode Pos </th>
      <th class='bg-primary'> No Telepon </th>
      <th class='bg-primary'> No HP </th>
      <th class='bg-primary'> Email </th>
      <th class='bg-primary'> NPWP </th>
      <th class='bg-primary'> Website </th>
    </tr>";

                    $numrow = 1;
                    $kosong = 0;

                    // Lakukan perulangan dari data yang ada di excel
                    // $sheet adalah variabel yang dikirim dari controller
                    foreach ($sheet as $row) {
                      // Ambil data pada excel sesuai Kolom
                      $kodePrincipal = $row['A']; // Ambil data NIS
                      $NamaPrincipal = $row['B'];
                      $Alamat = $row['C'];
                      $PrincipalKabupatenId = $row['D'];
                      $PrincipalKabupatenNama = $row['E'];
                      $PrincipalProvinsiId = $row['F'];
                      $PrincipalProvinsiNama = $row['G'];
                      $KodePos = $row['H'];
                      $NoTelp = $row['I'];
                      $NoHp = $row['J'];
                      $Email = $row['K'];
                      $NPWP = $row['L'];
                      $Website = $row['M'];

                      // Cek jika semua data tidak diisi
                      if ($kodePrincipal == "" && $NamaPrincipal == "" && $Alamat == "" && $PrincipalKabupatenId == "" && $PrincipalKabupatenNama == "" && $PrincipalProvinsiId == "" && $PrincipalProvinsiNama == "" && $KodePos == "" && $NoTelp == "" && $NoHp == "" && $Email == "" && $NPWP == "" && $Website == "")
                        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                      // Cek $numrow apakah lebih dari 1
                      // Artinya karena baris pertama adalah nama-nama kolom
                      // Jadi dilewat saja, tidak usah diimport
                      if ($numrow > 1) {
                        // Validasi apakah semua data telah diisi
                        $kodePrincipal_td = (!empty($kodePrincipal)) ? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                        $NamaPrincipal_td = (!empty($NamaPrincipal)) ? "" : " style='background: #E07171;'";
                        $Alamat_td = (!empty($Alamat)) ? "" : " style='background: #E07171;'";
                        $PrincipalKabupatenId_td = (!empty($PrincipalKabupatenId)) ? "" : " style='background: #E07171;'";
                        $PrincipalKabupatenNama_td = (!empty($PrincipalKabupatenNama)) ? "" : " style='background: #E07171;'";
                        $PrincipalProvinsiId_td = (!empty($PrincipalProvinsiId)) ? "" : " style='background: #E07171;'";
                        $PrincipalProvinsiNama_td = (!empty($PrincipalProvinsiNama)) ? "" : " style='background: #E07171;'";
                        $KodePos_td = (!empty($KodePos)) ? "" : " style='background: #E07171;'";
                        $NoTelp_td = (!empty($NoTelp)) ? "" : " style='background: #E07171;'";
                        $NoHp_td = (!empty($NoHp)) ? "" : " style='background: #E07171;'";
                        $Email_td = (!empty($Email)) ? "" : " style='background: #E07171;'";
                        $NPWP_td = (!empty($NPWP)) ? "" : " style='background: #E07171;'";
                        $Website_td = (!empty($Website)) ? "" : " style='background: #E07171;'";


                        // Jika salah satu data ada yang kosong
                        if ($kodePrincipal == "" or $NamaPrincipal == "" or $Alamat == "" or $PrincipalKabupatenId == "" or $PrincipalKabupatenNama == "" or $PrincipalProvinsiId == "" or $PrincipalProvinsiNama == "" or $KodePos == "" or $NoTelp == "" or $NoHp == "" or $Email == "" or $NPWP == "" or $Website == "") {
                          $kosong++; // Tambah 1 variabel $kosong
                        }

                        echo "<tr>";
                        echo "<td" . $kodePrincipal_td . ">" . $kodePrincipal . "</td>";
                        echo "<td" . $NamaPrincipal_td . ">" . $NamaPrincipal . "</td>";
                        echo "<td" . $Alamat_td . ">" . $Alamat . "</td>";
                        echo "<td" . $PrincipalKabupatenId_td . ">" . $PrincipalKabupatenId . "</td>";
                        echo "<td" . $PrincipalKabupatenNama_td . ">" . $PrincipalKabupatenNama . "</td>";
                        echo "<td" . $PrincipalProvinsiId_td . ">" . $PrincipalProvinsiId . "</td>";
                        echo "<td" . $PrincipalProvinsiNama_td . ">" . $PrincipalProvinsiNama . "</td>";
                        echo "<td" . $KodePos_td . ">" . $KodePos . "</td>";
                        echo "<td" . $NoTelp_td . ">" . $NoTelp . "</td>";
                        echo "<td" . $NoHp_td . ">" . $NoHp . "</td>";
                        echo "<td" . $Email_td . ">" . $Email . "</td>";
                        echo "<td" . $NPWP_td . ">" . $NPWP . "</td>";
                        echo "<td" . $Website_td . ">" . $Website . "</td>";
                        echo "</tr>";
                      }

                      $numrow++; // Tambah 1 setiap kali looping
                    }

                    echo "</table>  </div>";

                    // Cek apakah variabel kosong lebih dari 0
                    // Jika lebih dari 0, berarti ada data yang masih kosong
                    if ($kosong > 0) {
                  ?>
                      <script>
                        $(document).ready(function() {
                          // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                          $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                          $("#kosong").show(); // Munculkan alert validasi kosong
                        });
                      </script>
                  <?php
                    } else { // Jika semua data sudah diisi
                      echo "<hr>";

                      // Buat sebuah tombol untuk mengimport data ke database
                      echo "<button type='submit' class='btn btn-primary btn-sm' name='import'><span class='fa fa-user-plus'></span> Import File</button>";
                      echo "<a href='" . base_url("admin/principal") . "'style='margin:10px;' class='btn btn-warning btn-sm'><span class='fa  fa-close '></span> Batal</a>";
                    }

                    echo "</form>";
                  }
                  ?>


                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>



    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

</body>

</html>