<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Subcon
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li class="active"> Subcon</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">

                        <div class="box">
                            <div class="box-header">
                                <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Tambah Subcon</a>
                                <a class="btn btn-primary btn-sm" href="<?php echo base_url("admin/subcon/form"); ?>"><span class="fa fa-user-plus"></span> Import Data</a>
                                <a class="btn btn-info btn-sm" href="<?php echo base_url("admin/subcon/export"); ?>"><span class="fa fa-file-excel-o"> </span> Export Data</a>
                            </div>

                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                                        <thead>
                                            <tr>
                                                <th class="bg-primary">No</th>
                                                <th class="bg-primary">Kode Regional</th>
                                                <th class="bg-primary">Jenis Subcon</th>
                                                <th class="bg-primary">Kode Subcon</th>
                                                <th class="bg-primary">Nama Subcon</th>
                                                <th class="bg-primary">Alamat</th>
                                                <th class="bg-primary">Kota</th>
                                                <th class="bg-primary">Provinsi</th>
                                                <th class="bg-primary">Kode Pos</th>
                                                <th class="bg-primary">No Telpon</th>
                                                <th class="bg-primary">No HP</th>
                                                <th class="bg-primary">PIC</th>
                                                <th class="bg-primary">Email</th>
                                                <th class="bg-primary">Bank</th>
                                                <th class="bg-primary">Norek</th>
                                                <th class="bg-primary">NPWP</th>
                                                <th class="bg-primary">Subcon Leader</th>
                                                <th class="bg-primary">Area 1</th>
                                                <th class="bg-primary">Area 2</th>
                                                <th class="bg-primary">Area 3</th>
                                                <th class="bg-primary">Area 4</th>
                                                <th class="bg-primary">Area 5</th>
                                                <th class="bg-primary" style="text-align:right;">Operasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $no = 0;
                                            foreach ($refsubcon as $i) {
                                                $no++; ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $i->KodeRegional ?></td>
                                                    <td><?php echo $i->SubconJenis ?></td>
                                                    <td><?php echo $i->KodeSuppl ?></td>
                                                    <td><?php echo $i->Nama ?></td>
                                                    <td><?php echo $i->Alamat ?></td>
                                                    <td><?php echo $i->SubconKabupatenNama ?></td>
                                                    <td><?php echo $i->SubconProvinsiNama ?></td>
                                                    <td><?php echo $i->Kodepos ?></td>
                                                    <td><?php echo $i->Notelp ?></td>
                                                    <td><?php echo $i->Nohp ?></td>
                                                    <td><?php echo $i->PIC ?></td>
                                                    <td><?php echo $i->Email ?></td>
                                                    <td><?php echo $i->SubconBankNama ?></td>
                                                    <td><?php echo $i->Norek ?></td>
                                                    <td><?php echo $i->NPWP ?></td>
                                                    <td><?php echo $i->SubconLeader ?></td>
                                                    <td><?php echo $i->AreaKerja1Nama ?></td>
                                                    <td><?php echo $i->AreaKerja2Nama ?></td>
                                                    <td><?php echo $i->AreaKerja3Nama ?></td>
                                                    <td><?php echo $i->AreaKerja4Nama ?></td>
                                                    <td><?php echo $i->AreaKerja5Nama ?></td>

                                                    <td style="text-align:right;">
                                                        <a style="padding: 6px 6px;" href="<?php echo base_url() . 'admin/subcon/get_edit/' . $i->KodeSuppl; ?>"><span class="fa fa-pencil"></span></a>
                                                        <a style="padding: 6px 6px;" data-toggle="modal" data-target="#ModalHapus<?php echo $i->KodeSuppl; ?>"><span class="fa fa-trash"></span></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->

                    </div>
                    <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

        <!--Modal Add Profit-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#337AB7">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel" style="color:white">Tambah Subcon</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url() . 'admin/subcon/simpan_subcon' ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Regional </label>
                                <div class="col-sm-7">
                                    <select class="form-control select2" name="Regional" style="width: 100%;" required>
                                        <option value="">No Selected</option>
                                        <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                                            echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                                        } ?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Jenis Subcon</label>
                                <div class="col-sm-7">
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadio1" value="Internal" name="JenisSubcon" checked>
                                        <label for="inlineRadio1"> Internal </label>
                                    </div>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadio1" value="External" name="JenisSubcon">
                                        <label for="inlineRadio2"> External </label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Subcon </label>
                                <div class="col-sm-3">
                                    <input type="text" name="KodeSuppl" class="form-control" id="KodeSuppl" value="<?php echo $KodeSuppl; ?>" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Subcon</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Nama" class="form-control" id="inputUserName" placeholder="Nama" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Alamat</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="Alamat" class="form-control" id="inputUserName" placeholder="Alamat" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Provinsi</label>
                                <div class="col-sm-7">
                                    <select name="provinsi" class="form-control" id="provinsi">
                                        <option value="">No Selected</option>
                                        <?php foreach ($provinsi as $row) : ?>
                                            <option value="<?php echo $row->ProvinsiId; ?>"><?php echo $row->ProvinsiId; ?> - <?php echo $row->ProvinsiNama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kota</label>
                                <div class="col-sm-7">
                                    <select name="kabupaten" id="kabupaten" class="kabupaten form-control">
                                        <option>No Selected</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Pos</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Kodepos" class="form-control" id="inputUserName" placeholder="Kode Pos" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Telepon </label>
                                <div class="col-sm-7">
                                    <input type="text" name="Notelp" class="form-control" id="inputUserName" placeholder="No Telepon" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> No HP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Nohp" class="form-control" id="inputUserName" placeholder="No HP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">PIC</label>
                                <div class="col-sm-7">
                                    <input type="text" name="PIC" class="form-control" id="inputUserName" placeholder="PIC" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Email" class="form-control" id="inputUserName" placeholder="Email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Bank</label>
                                <div class="col-sm-7">
                                    <select name="bank" class="form-control" id="bank">
                                        <option value="">No Selected</option>
                                        <?php foreach ($bank as $row) : ?>
                                            <option value="<?php echo $row->KodeBank; ?>"><?php echo $row->KodeBank; ?> - <?php echo $row->NamaBank; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Rekening</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Norek" class="form-control" id="inputUserName" placeholder="No Rekening" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">NPWP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NPWP" class="form-control" id="inputUserName" placeholder="NPWP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Subcon Leader</label>
                                <div class="col-sm-7">
                                    <input type="text" name="SubconLeader" class="form-control" id="inputUserName" placeholder="Subcon Leader" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Area Kerja 1</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja1" class="form-control" id="AreaKerja1">
                                        <option value="">No Selected</option>
                                        <?php foreach ($provinsi as $row) : ?>
                                            <option value="<?php echo $row->ProvinsiId; ?>"><?php echo $row->ProvinsiId; ?> - <?php echo $row->ProvinsiNama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Area Kerja 2</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja2" class="form-control" id="AreaKerja2">
                                        <option value="">No Selected</option>
                                        <?php foreach ($provinsi as $row) : ?>
                                            <option value="<?php echo $row->ProvinsiId; ?>"><?php echo $row->ProvinsiId; ?> - <?php echo $row->ProvinsiNama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Area Kerja 3</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja3" class="form-control" id="AreaKerja3">
                                        <option value="">No Selected</option>
                                        <?php foreach ($provinsi as $row) : ?>
                                            <option value="<?php echo $row->ProvinsiId; ?>"><?php echo $row->ProvinsiId; ?> - <?php echo $row->ProvinsiNama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Area Kerja 4</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja4" class="form-control" id="AreaKerja4">
                                        <option value="">No Selected</option>
                                        <?php foreach ($provinsi as $row) : ?>
                                            <option value="<?php echo $row->ProvinsiId; ?>"><?php echo $row->ProvinsiId; ?> - <?php echo $row->ProvinsiNama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Area Kerja 5</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja5" class="form-control" id="AreaKerja5">
                                        <option value="">No Selected</option>
                                        <?php foreach ($provinsi as $row) : ?>
                                            <option value="<?php echo $row->ProvinsiId; ?>"><?php echo $row->ProvinsiId; ?> - <?php echo $row->ProvinsiNama; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                            <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--Modal Add profit-->



        <?php foreach ($refsubcon as $i) { ?>
            <!--Modal Hapus Pengguna-->
            <div class="modal fade" id="ModalHapus<?php echo $i->KodeSuppl; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                            <h4 class="modal-title" id="myModalLabel">Hapus Subcon</h4>
                        </div>
                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/subcon/hapus_subcon' ?>" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                <input type="hidden" name="KodeSuppl" value="<?php echo $i->KodeSuppl; ?>" />
                                <p>Apakah Anda yakin menghapus Subcon <b><?php echo $i->Nama; ?></b> ?</p>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>


        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
        <!-- page script -->

        <script type="text/javascript">
            $(document).ready(function() {

                $('#provinsi').change(function() {
                    var id = $(this).val();
                    $.ajax({
                        url: "<?php echo site_url('admin/regional/get_sub_provinsi'); ?>",
                        method: "POST",
                        data: {
                            id: id
                        },
                        async: true,
                        dataType: 'json',
                        success: function(data) {

                            var html = '';
                            var i;
                            for (i = 0; i < data.length; i++) {
                                html += '<option value=' + data[i].KabupatenId + '>' + data[i].KabupatenNama + '</option>';
                            }
                            $('#kabupaten').html(html);

                        }
                    });
                    return false;
                });

            });
        </script>

        <script>
            $(function() {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });

                $('#datepicker').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $('#datepicker2').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $('.datepicker3').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $('.datepicker4').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                });
                $(".timepicker").timepicker({
                    showInputs: true
                });

            });
        </script>
        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    autohide: true,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Subcon Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    autohide: true,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Subcon berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    autohide: true,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Subcon Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    autohide: true,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>
</body>

</html>