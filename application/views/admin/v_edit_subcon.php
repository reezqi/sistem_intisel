<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit Subcon
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Edit Subcon</a></li>
                    <li class="active">Edit Subcon</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="modal-dialog" role="document">
                        <!--Modal Edit Regional-->
                        <?php $b = $data1->row_array(); ?>
                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/subcon/simpan_update_subcon' ?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="KodeSuppl" class="form-control" value="<?php echo $b['KodeSuppl']; ?>" id="inputUserName" placeholder="Nama Supplier" required>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Regional</label>
                                <div class="col-sm-7">
                                    <select name="regional" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($regional->result_array() as $i) {
                                            $KODE = $i['KODE'];
                                            $NAMA = $i['NAMA'];
                                            if ($b['KodeRegional'] == $KODE)
                                                echo "<option value='$KODE' selected>$KODE - $NAMA</option>";
                                            else
                                                echo "<option value='$KODE'>$KODE - $NAMA</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Jenis Subcon</label>
                                <div class="col-sm-7">
                                    <?php if ($b['SubconJenis'] == 'SI') : ?>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="Internal" name="JenisSubcon" checked>
                                            <label for="inlineRadio1">  Internal </label>
                                        </div>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="External" name="JenisSubcon">
                                            <label for="inlineRadio2">  External </label>
                                        </div>
                                    <?php else : ?>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="Internal" name="JenisSubcon">
                                            <label for="inlineRadio1">  Internal </label>
                                        </div>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="External" name="JenisSubcon" checked>
                                            <label for="inlineRadio2">  External </label>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Subcon</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Nama" class="form-control" value="<?php echo

                                                                                                    $b['Nama']; ?>" id="inputUserName" placeholder="Nama Subcon" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Alamat </label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="Alamat" placeholder="Alamat " required><?php echo $b['Alamat']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Provinsi</label>
                                <div class="col-sm-7">
                                    <select name="provinsi" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['SubconProvinsiId'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kota</label>
                                <div class="col-sm-7">
                                    <select name="kabupaten" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($kabupaten->result_array() as $i) {
                                            $KabupatenId = $i['KabupatenId'];
                                            $KabupatenNama = $i['KabupatenNama'];
                                            if ($b['SubconKabupatenId'] == $KabupatenId)
                                                echo "<option value='$KabupatenId' selected>$KabupatenId - $KabupatenNama</option>";
                                            else
                                                echo "<option value='$KabupatenId'>$KabupatenId-  $KabupatenNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Kode Pos</label>
                                <div class="col-sm-4">
                                    <input type="text" name="Kodepos" class="form-control" value="<?php echo $b['Kodepos']; ?>" id="inputUserName" placeholder=" Kode Pos" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No telepon</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Notelp" class="form-control" value="<?php echo $b['Notelp']; ?>" id="inputUserName" placeholder=" No Telepon" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No HP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Nohp" class="form-control" value="<?php echo $b['Nohp']; ?>" id="inputUserName" placeholder=" No HP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> PIC</label>
                                <div class="col-sm-7">
                                    <input type="text" name="PIC" class="form-control" value="<?php echo $b['PIC']; ?>" id="inputUserName" placeholder=" PIC" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Email</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Email" class="form-control" value="<?php echo $b['Email']; ?>" id="inputUserName" placeholder=" Email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Bank</label>
                                <div class="col-sm-7">
                                    <select name="bank" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($bank->result_array() as $i) {
                                            $KodeBank = $i['KodeBank'];
                                            $NamaBank = $i['NamaBank'];
                                            if ($b['SubconBankId'] == $KodeBank)
                                                echo "<option value='$KodeBank' selected>$KodeBank - $NamaBank</option>";
                                            else
                                                echo "<option value='$KodeBank'>$KodeBank - $NamaBank</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> No Rekening</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Norek" class="form-control" value="<?php echo $b['Norek']; ?>" id="inputUserName" placeholder=" No Rek" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> NPWP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NPWP" class="form-control" value="<?php echo $b['NPWP']; ?>" id="inputUserName" placeholder=" NPWP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Subcon Leader</label>
                                <div class="col-sm-7">
                                    <input type="text" name="SubconLeader" class="form-control" value="<?php echo $b['SubconLeader']; ?>" id="inputUserName" placeholder=" Subcon Leader" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Area Kerja 1</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja1" class="form-control">
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['AreaKerja1'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'> $ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Area Kerja 2</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja2" class="form-control">
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['AreaKerja2'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Area Kerja 3</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja3" class="form-control">
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['AreaKerja3'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Area Kerja 4</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja4" class="form-control">
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['AreaKerja4'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Area Kerja 5</label>
                                <div class="col-sm-7">
                                    <select name="AreaKerja5" class="form-control">
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['AreaKerja5'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="button" onclick="window.history.back();" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                        </form>
                    </div>
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

        <!-- FastClick -->
        <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery-3.3.1.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                //call function get data edit
                get_data_edit();
                $('.Bank').change(function() {
                    var id = $(this).val();
                    var KabupatenId = "<?php echo $Kabupaten_Id; ?>";
                    $.ajax({
                        url: "<?php echo site_url('admin/subcon/get_sub_bank'); ?>",
                        method: "POST",
                        data: {
                            id: id
                        },
                        async: true,
                        dataType: 'json',
                        success: function(data) {

                            $('select[name="kabupaten"]').empty();

                            $.each(data, function(key, value) {
                                if (KabupatenId == value.KabupatenId) {
                                    $('select[name="kabupaten"]').append('<option value="' + value.KabupatenId + '" selected>' + value.KabupatenNama + '</option>').trigger('change');
                                } else {
                                    $('select[name="kabupaten"]').append('<option value="' + value.KabupatenId + '">' + value.KabupatenNama + '</option>');
                                }
                            });

                        }
                    });
                    return false;
                });

                //load data for edit
                function get_data_edit() {
                    var KodeRegional = $('[name="KodeRegional"]').val();
                    $.ajax({
                        url: "<?php echo site_url('admin/subcon/get_data_edit'); ?>",
                        method: "POST",
                        data: {
                            KodeRegional: KodeRegional
                        },
                        async: true,
                        dataType: 'json',
                        success: function(data) {
                            $.each(data, function(i, item) {
                                $('[name="Nama"]').val(data[i].Nama);
                                $('[name="Bank"]').val(data[i].KodeBank).trigger('change');

                            });
                        }
                    });
                }

            });
        </script>

        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Agenda berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>



</body>

</html>