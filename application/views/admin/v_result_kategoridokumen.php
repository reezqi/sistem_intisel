<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo-intisel2.jpg'?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/AdminLTE.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.css'?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datepicker/datepicker3.css'?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/skins/_all-skins.min.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>
  

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
  ?>


tr:nth-child(even){background-color: #f2f2f2}
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Matriks PDP
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"> Matriks PDP</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

          <div class="box">
            <div class="box-header">
            <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Tambah Matriks PDP</a>

             <!-- <a class="btn btn-primary btn-flat" href="<?php echo base_url("admin/regional/form"); ?>" ><span class="fa fa-user-plus"></span> Tambah Matriks PDP Excel</a>
              <a class="btn btn-info btn-flat" href="<?php echo base_url("admin/regional/export"); ?>"><span class="fa fa-file-excel-o"> </span>  Export Matriks PDP</a> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
                    <th>ID Dok Cat</th>
                    <th>Principal Code</th>
                    <th>Principal Name</th>
                    <th>ID Principal</th>
                    <th>ID SOW</th>
                    <th>ID Site</th>
                    <th>Doc Category</th>
                    <th>Des Doc Category</th>
                    <th>Bobot</th>
                    <th>Active</th>
                    <th>Mod Time</th>
                    <th style="text-align:right;">Operasi</th>
                </tr>
                </thead>
                <tbody>
				 <?php

      if(count($principalCode)>0){
			  foreach ($principalCode as $data) {
          ?>
      
                      <tr>
                      <td> <?php echo $data->idDocCat ?></td>
                       <td><?php echo $data->principalCode?></td>
                       <td><?php echo $data->principalName?></td>
                       <td><?php echo $data->idPrincipal?></td>
                       <td><?php echo $data->idSOW?></td>
                       <td><?php echo $data->idSite?></td>
                       <td><?php echo $data->docCategory?></td>
                       <td><?php echo $data->descDocCategory?></td>
                       <td><?php echo $data->bobot?></td>
                       <td><?php echo $data->active?></td>
                       <td><?php echo $data->modtime?></td>
                       <td style="text-align:center;">
                        <a style="padding: 6px 6px;" data-toggle="modal" data-target="#ModalEdit<?php echo $data->idDocCat;?>"><span class="fa fa-pencil"></span></a>
                        <a style="padding: 6px 6px;" data-toggle="modal" data-target="#ModalHapus<?php echo $data->idDocCat;?>"><span class="fa fa-trash"></span></a>
                  </td>

              </tr>
      <?php }} ?>
                </tbody>
              </table>
            </div>
          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
  ?>
  

  <!--Modal Add Profit-->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Tambah Matriks PDP</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/kategoridokumen/simpan_kategoridokumen'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">ID Doc Cat</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="idDocCat" class="form-control" id="inputUserName" placeholder="ID Doc Cat" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Kode Principal </label>
                                        <div class="col-sm-7">
                                            <input type="text" name="principalCode" class="form-control" id="inputUserName" placeholder="Kode Principal" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Nama Principal</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="principalName" class="form-control" id="inputUserName" placeholder="Nama Principal" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label"> ID Principal</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="idPrincipal" class="form-control" id="inputUserName" placeholder=" ID Principal" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">ID SOW</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="idSOW" class="form-control" id="inputUserName" placeholder="ID SOW" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">ID Site</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="idSite" class="form-control" id="inputUserName" placeholder="ID Site" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Doc Kategori </label>
                                        <div class="col-sm-7">
                                            <input type="text" name="docCategory" class="form-control" id="inputUserName" placeholder="Doc Kategori" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label"> Des Doc Kategori</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="descDocCategory" class="form-control" id="inputUserName" placeholder=" Des Doc Kategori" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Bobot</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="bobot" class="form-control" id="inputUserName" placeholder="Bobot" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Active</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="active" class="form-control" id="inputUserName" placeholder="Active" required>
                                        </div>
                                    </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
<!--Modal Add profit-->

<!--Modal Edit Regional-->
<?php foreach ($principalCode as $i) { ?>
        <div class="modal fade" id="ModalEdit<?php echo $i->idDocCat;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Matriks PDP</h4>
                    </div>

                    <form class="form-horizontal" action="<?php echo base_url().'admin/kategoridokumen/update_kategoridokumen'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">

                                  <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Kode Principal</label>
                                        <div class="col-sm-7">
                                        <input type="hidden" name="idDocCat" value="<?php echo $i->idDocCat;?>">
                                            <input type="text" name="principalCode" class="form-control" value="<?php echo $i->principalCode;?>" id="inputUserName" placeholder="Kode Principal" required>
                                        </div>
                                    </div>

                                  <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Nama Principal</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="principalName" class="form-control" value="<?php echo $i->principalName;?>" id="inputUserName" placeholder="Nama Principal" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label"> ID Principal</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="idPrincipal" class="form-control" value="<?php echo $i->idPrincipal;?>" id="inputUserName" placeholder="ID Principal" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label"> ID SOW</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="idSOW" class="form-control" value="<?php echo $i->idSOW;?>" id="inputUserName" placeholder="ID SOW" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">  ID Site</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="idSite" class="form-control" value="<?php echo $i->idSite;?>" id="inputUserName" placeholder=" ID Site" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Doc Kategori</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="docCategory" class="form-control" value="<?php echo $i->docCategory;?>" id="inputUserName" placeholder=" Doc Kategori" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label">Des Doc Kategori</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="descDocCategory" class="form-control" value="<?php echo $i->descDocCategory;?>" id="inputUserName" placeholder=" Des Doc Kategori" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label"> Bobot</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="bobot" class="form-control" value="<?php echo $i->bobot;?>" id="inputUserName" placeholder=" Bobot" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputUserName" class="col-sm-4 control-label"> Active</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="active" class="form-control" value="<?php echo $i->active;?>" id="inputUserName" placeholder=" Active" required>
                                        </div>
                                    </div>
                                    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
<?php } ?>
<!--Modal Edit Regional-->

<?php foreach ($principalCode as $i) { ?>
<!--Modal Hapus Pengguna-->
<div class="modal fade" id="ModalHapus<?php echo $i->idDocCat;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Matriks PDP</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/kategoridokumen/hapus_kategoridokumen'?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
							<input type="hidden" name="idDocCat" value="<?php echo $i->idDocCat;?>"/>
                            <p>Apakah Anda yakin menghapus Matriks PDP <b><?php echo $i->idDocCat;?></b> ?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
  <?php } ?>


<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datepicker/bootstrap-datepicker.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.js'?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker3').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker4').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $(".timepicker").timepicker({
      showInputs: true
    });

  });
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Matriks PDP Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Matriks PDP berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Matriks PDP Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>

    
</body>
</html>
