<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo-intisel2.jpg'?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/AdminLTE.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.css'?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datepicker/datepicker3.css'?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/skins/_all-skins.min.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
  ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         PO SOW DETAIL
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">PO SOW DETAIL</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

          <div class="box">
            <div class="box-header">
              <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Tambah PO SOW DETAIL</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
                    <th>Kode Regional</th>
                    <th>No SOW</th>
                    <th>SOW</th>
                    <th>Nama SOW</th>
                    <th>Satuan</th>
                    <th>ID_Principal</th>
                    <th>KodeCust</th>
                    <th>NamaCust</th>
                    <th>Term_1</th>
                    <th>Term_2</th>
                    <th>Term_3</th>
                    <th>Term_4</th>
                    <th>NoKontrak</th>
                    <th>TahunKontrak</th>
                    <th>Keterangan</th>
                    <th>Kode Regx</th>
                    <th>SOW Price</th>
                    <th>Type</th>
                    <th style="text-align:right;">Operasi</th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($data->result_array() as $i) :
                       $KodeRegional=$i['KodeRegional'];
                       $NoSow=$i['NoSow'];
                       $SOW=$i['SOW'];
                       $NamaSow=$i['NamaSow'];
                       $Satuan=$i['Satuan'];
                       $ID_Principal=$i['ID_Principal'];
                       $KodeCust=$i['KodeCust'];
                       $NamaCust=$i['NamaCust'];
                       $Term_1=$i['Term_1'];
                       $Term_2=$i['Term_2'];
                       $Term_3=$i['Term_3'];
                       $Term_4=$i['Term_4'];
                       $NoKontrak=$i['NoKontrak'];
                       $TahunKontrak=$i['TahunKontrak'];
                       $Keterangan=$i['Keterangan'];
                       $KodeRegx=$i['KodeRegx'];
                       $SowPrice=$i['SowPrice'];
                       $Type=$i['Type'];
                    ?>
                <tr>
                  <td>
                    <?php echo $KodeRegional;?> 
                  </td>
                  <td>
                  <?php echo $NoSow;?> 
                 </td>
                 <td>
                  <?php echo $NoSow;?> 
                 </td>
                 <td>
                  <?php echo $SOW;?> 
                 </td>
                 <td>
                  <?php echo $NamaSow;?> 
                 </td>
                 <td>
                  <?php echo $Satuan;?> 
                 </td>
                 <td>
                  <?php echo $ID_Principal;?> 
                 </td>
                 <td>
                  <?php echo $KodeCust;?> 
                 </td>
                 <td>
                  <?php echo $NamaCust;?> 
                 </td>
                 <td>
                  <?php echo $Term_1;?> 
                 </td>
                 <td>
                  <?php echo $Term_2;?> 
                 </td>
                 <td>
                  <?php echo $Term_3;?> 
                 </td>
                 <td>
                  <?php echo $Term_4;?> 
                 </td>
                 <td>
                  <?php echo $NoKontrak;?> 
                 </td>
                 <td>
                  <?php echo $TahunKontrak;?> 
                 </td>
                 <td>
                  <?php echo $Keterangan;?> 
                 </td>
                 <td>
                  <?php echo $KodeRegx;?> 
                 </td>
                 <td>
                  <?php echo $SowPrice;?> 
                 </td>
                 <td>
                  <?php echo $Type;?> 
                 </td>
                 <td style="text-align:right;">
                        <a class="btn" data-toggle="modal" data-target="#ModalEdit<?php echo $KodeRegional;?>"><span class="fa fa-pencil"></span></a>
                        <a class="btn" data-toggle="modal" data-target="#ModalHapus<?php echo $KodeRegional;?>"><span class="fa fa-trash"></span></a>
                  </td>
                </tr>
				<?php endforeach;?>
                </tbody>
              </table>
            </div></div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
  ?>
  





<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datepicker/bootstrap-datepicker.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.js'?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker3').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker4').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $(".timepicker").timepicker({
      showInputs: true
    });

  });
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Agenda berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
</body>
</html>
