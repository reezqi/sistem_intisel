<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Karyawan
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li class="active"> Karyawan</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">

              <div class="box">
                <div class="box-header">
                  <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Tambah Karyawan</a>
                  <!--<a class="btn btn-primary btn-sm" href="<?php echo base_url("admin/karyawan/form"); ?>"><span class="fa fa-user-plus"></span> Import Data</a>-->
                  <a class="btn btn-info btn-sm" href="<?php echo base_url("admin/karyawan/export"); ?>"><span class="fa fa-file-excel-o"> </span> Export Data</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                    <thead>
                      <tr>
                        <th class="bg-primary">No</th>
                        <th class="bg-primary">Regional</th>
                        <th class="bg-primary">Kode Akun</th>
                        <th class="bg-primary">Nama Karyawan</th>
                        <th class="bg-primary">Alamat</th>
                        <th class="bg-primary">Bank</th>
                        <th class="bg-primary">No Rekening </th>
                        <th class="bg-primary">Status</th>
                        <th class="bg-primary" style="text-align:right;">Operasi</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php
                      $no = 0;
                      foreach ($refkaryawan as $i) {
                        $no++;
                      ?>

                        <tr>
                          <td> <?php echo $no; ?></td>
                          <td> <?php echo $i->KodeReg ?></td>
                          <td> <?php echo $i->KodeAkun ?></td>
                          <td> <?php echo $i->Nama ?></td>
                          <td> <?php echo $i->Alamat ?></td>
                          <td> <?php echo $i->Bank ?></td>
                          <td> <?php echo $i->NoRek ?></td>
                          <td><?php if ($i->StsKaryawan == '1') : ?>
                              <span class="badge bg-green">Aktif</span>
                            <?php else : ?>
                              <span class="badge bg-red">Tidak Aktif</span>
                            <?php endif; ?></td>

                          <td style="text-align:right;">
                            <a style="padding: 6px 6px;" href="<?php echo base_url() . 'admin/karyawan/get_edit/' . $i->KodeAkun; ?>"><span class="fa fa-pencil"></span></a>
                            <!--<a style="padding: 6px 6px;" data-toggle="modal" data-target="#ModalHapus<?php echo $i->KodeAkun; ?>"><span class="fa fa-trash"></span></a>-->
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>


    <!--Modal Add Unit-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#337AB7">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel" style="color:white">Tambah Karyawan</h4>
          </div>
          <form class="form-horizontal" action="<?php echo base_url() . 'admin/karyawan/simpan_karyawan' ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Kode Akun</label>
                <div class="col-sm-3">
                  <input type="text" name="KodeAkun" class="form-control" id="inputUserName" placeholder="Kode Akun" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Nama Karyawan</label>
                <div class="col-sm-7">
                  <input type="text" name="NamaKaryawan" class="form-control" id="inputUserName" placeholder="Nama Karyawan" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Alamat</label>
                <div class="col-sm-7">
                  <textarea class="form-control" rows="3" name="Alamat" class="form-control" id="inputUserName" placeholder="Alamat" required></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label"> Regional </label>
                <div class="col-sm-7">
                  <select class="form-control select2" name="Regional" style="width: 100%;" required>
                    <option value="">No Selected</option>
                    <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                      echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                    } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Bank</label>
                <div class="col-sm-7">
                  <select class="form-control select2" name="Bank" style="width: 100%;" required>
                    <option value="">No Selected</option>
                    <?php foreach ($bank as $i) { // Lakukan looping pada variabel siswa dari controller
                      echo "<option value='" . $i->KodeBank . "'>" . $i->KodeBank . " - " . $i->NamaBank . "</option>";
                    } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">No Rekening</label>
                <div class="col-sm-7">
                  <input type="text" name="NoRek" class="form-control" id="inputUserName" placeholder="No Rekening" required>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Status</label>
                <div class="col-sm-5">
                  <select class="form-control" name="StsKaryawan" required>
                    <option value="1">Aktif</option>
                    <option value="2">Tidak Aktif </option>
                  </select>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Add Regional-->

    <?php foreach ($refkaryawan as $i) { ?>
      <!--Modal Hapus Unit-->
      <div class="modal fade" id="ModalHapus<?php echo $i->KodeAkun; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#337AB7">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Hapus Karyawan</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/karyawan/hapus_karyawan' ?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="KodeAkun" value="<?php echo $i->KodeAkun; ?>" />
                <p>Apakah Anda yakin menghapus Akun Karyawan <b><?php echo $i->Nama; ?></b> ?</p>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    <?php } ?>


    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->
    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Karyawan Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "Karyawan berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Karyawan Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>
</body>

</html>