<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>

  <script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>
  <script>
    $(document).ready(function() {
      // Sembunyikan alert validasi kosong
      $("#kosong").hide();
    });
  </script>

  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Regional
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Beranada</a></li>
          <li class="active">Regional</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">

              <div class="box">
                <div class="box-header">
                  <a class="btn btn-primary btn-sm" href="<?php echo base_url("excel/format_regional.xlsx"); ?>"><span class="fa fa-user-plus"></span> Download File</a>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="<?php echo base_url("admin/regional/form"); ?>" enctype="multipart/form-data">
                    <!-- 
    -- Buat sebuah input type file
    -- class pull-left berfungsi agar file input berada di sebelah kiri
    -->
                    <input type="file" name="file">

                    <!--
    -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
    -->
                    <br>
                    <input class="btn btn-warning btn-sm" type="submit" name="preview" value="Preview Dokumen">
                  </form>

                  <br>
                  <?php
                  if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
                    if (isset($upload_error)) { // Jika proses upload gagal
                      echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
                      die; // stop skrip
                    }


                    // Buat sebuah tag form untuk proses import data ke database
                    echo "<form method='post' action='" . base_url("admin/regional/import") . "'>";

                    // Buat sebuah div untuk alert validasi kosong
                    echo "<div style='color: red;' id='kosong'>
                               Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
                          </div>";

                    echo "
                    <div class='table-responsive'>
                    <table id='example1' class='table table-sm table-striped table-hover' style='font-size:12px;'>
    <tr>
      <th colspan='16'><center><b>Preview Data</b></center></th>
    </tr>
    <tr>
      <th class='bg-primary'><Center> Kode Regional</center></th>
      <th class='bg-primary'><Center> Nama Regional</center></th>
      <th class='bg-primary'><Center> Alamat</center></th>
      <th class='bg-primary'><Center> Kode Kabupaten</center></th>
      <th class='bg-primary'><Center> Nama Kabupaten</center></th>
      <th class='bg-primary'><Center> Kode Provinsi</center></th>
      <th class='bg-primary'><Center> Nama Provinsi</center></th>
      <th class='bg-primary'><Center> Kode Pos</center></th>
      <th class='bg-primary'><Center> No Telepon</center></th>
      <th class='bg-primary'><Center> No HP</center></th>
      <th class='bg-primary'><Center> Email</center></th>
      <th class='bg-primary'><Center> Regional Leader</center></th>
      <th class='bg-primary'><Center> No HP Leader</center></th>
      <th class='bg-primary'><Center> Email RL</center></th>
      <th class='bg-primary'><Center> Kode Perusahaan</center></th>
      <th class='bg-primary'><Center> Nama Perusahaan</center></th>
    </tr>";

                    $numrow = 1;
                    $kosong = 0;

                    // Lakukan perulangan dari data yang ada di excel
                    // $sheet adalah variabel yang dikirim dari controller
                    foreach ($sheet as $row) {
                      // Ambil data pada excel sesuai Kolom
                      $KODE = $row['A']; // Ambil data NIS
                      $NAMA = $row['B']; // Ambil data nama
                      $Alamat = $row['C'];
                      $RegionalKabupatenId = $row['D'];
                      $RegionalKabupatenNama = $row['E'];
                      $RegionalProvinsiId = $row['F'];
                      $RegionalProvinsiNama = $row['G'];
                      $KodePos = $row['H'];
                      $NoTelp = $row['I'];
                      $NoHp = $row['J'];
                      $Email = $row['K'];
                      $RegionalLeader = $row['L'];
                      $NoHpRL = $row['M'];
                      $EmailRL = $row['N'];
                      $RegionalKodePerusahaan = $row['O'];
                      $RegionalNamaPerusahaan = $row['P'];

                      // Cek jika semua data tidak diisi
                      if ($KODE == "" && $NAMA == "" && $Alamat == "" && $RegionalKabupatenId == "" && $RegionalKabupatenNama == "" && $RegionalProvinsiId == "" && $RegionalProvinsiNama == "" && $KodePos == "" && $NoTelp == "" && $NoHp == ""  && $Email == "" && $RegionalLeader == "" && $NoHpRL == "" && $EmailRL == "" && $RegionalKodePerusahaan == "" && $RegionalNamaPerusahaan == "")
                        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                      // Cek $numrow apakah lebih dari 1
                      // Artinya karena baris pertama adalah nama-nama kolom
                      // Jadi dilewat saja, tidak usah diimport
                      if ($numrow > 1) {
                        // Validasi apakah semua data telah diisi
                        $KODE_td = (!empty($KODE)) ? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                        $NAMA_td = (!empty($NAMA)) ? "" : " style='background: #E07171;'";
                        $Alamat_td = (!empty($Alamat)) ? "" : " style='background: #E07171;'";
                        $RegionalKabupatenId_td = (!empty($RegionalKabupatenId)) ? "" : " style='background: #E07171;'";
                        $RegionalKabupatenNama_td = (!empty($RegionalKabupatenNama)) ? "" : " style='background: #E07171;'";
                        $RegionalProvinsiId_td = (!empty($RegionalProvinsiId)) ? "" : " style='background: #E07171;'";
                        $RegionalProvinsiNama_td = (!empty($RegionalProvinsiNama)) ? "" : " style='background: #E07171;'";
                        $KodePos_td = (!empty($KodePos)) ? "" : " style='background: #E07171;'";
                        $NoTelp_td = (!empty($NoTelp)) ? "" : " style='background: #E07171;'";
                        $NoHp_td = (!empty($NoHp)) ? "" : " style='background: #E07171;'";
                        $Email_td = (!empty($Email)) ? "" : " style='background: #E07171;'";
                        $RegionalLeader_td = (!empty($RegionalLeader)) ? "" : " style='background: #E07171;'";
                        $NoHpRL_td = (!empty($NoHpRL)) ? "" : " style='background: #E07171;'";
                        $EmailRL_td = (!empty($EmailRL)) ? "" : " style='background: #E07171;'";
                        $RegionalKodePerusahaan_td = (!empty($RegionalKodePerusahaan)) ? "" : " style='background: #E07171;'";
                        $RegionalNamaPerusahaan_td = (!empty($RegionalNamaPerusahaan)) ? "" : " style='background: #E07171;'";


                        // Jika salah satu data ada yang kosong
                        if ($KODE == "" or $NAMA == "" or $Alamat == "" or $RegionalKabupatenId == "" or $RegionalKabupatenNama == "" or $RegionalProvinsiId == "" or $RegionalProvinsiNama == "" or $KodePos == "" or $NoTelp == "" or $NoHp == ""  or $Email == "" or $RegionalLeader == "" or $NoHpRL == "" or $EmailRL == "" or $RegionalKodePerusahaan == "" or $RegionalNamaPerusahaan == "") {
                          $kosong++; // Tambah 1 variabel $kosong
                        }

                        echo "<tr>";
                        echo "<td" . $KODE_td . ">" . $KODE . "</td>";
                        echo "<td" . $NAMA_td . ">" . $NAMA . "</td>";
                        echo "<td" . $Alamat_td . ">" . $Alamat . "</td>";
                        echo "<td" . $RegionalKabupatenId_td . ">" . $RegionalKabupatenId . "</td>";
                        echo "<td" . $RegionalKabupatenNama_td . ">" . $RegionalKabupatenNama . "</td>";
                        echo "<td" . $RegionalProvinsiId_td . ">" . $RegionalProvinsiId . "</td>";
                        echo "<td" . $RegionalProvinsiNama_td . ">" . $RegionalProvinsiNama . "</td>";
                        echo "<td" . $KodePos_td . ">" . $KodePos . "</td>";
                        echo "<td" . $NoTelp_td . ">" . $NoTelp . "</td>";
                        echo "<td" . $NoHp_td . ">" . $NoHp . "</td>";
                        echo "<td" . $Email_td . ">" . $Email . "</td>";
                        echo "<td" . $RegionalLeader_td . ">" . $RegionalLeader . "</td>";
                        echo "<td" . $NoHpRL_td . ">" . $NoHpRL . "</td>";
                        echo "<td" . $EmailRL_td . ">" . $EmailRL . "</td>";
                        echo "<td" . $RegionalKodePerusahaan_td . ">" . $RegionalKodePerusahaan . "</td>";
                        echo "<td" . $RegionalNamaPerusahaan_td . ">" . $RegionalNamaPerusahaan . "</td>";
                        echo "</tr>";
                      }

                      $numrow++; // Tambah 1 setiap kali looping
                    }

                    echo "</table> </div>";

                    // Cek apakah variabel kosong lebih dari 0
                    // Jika lebih dari 0, berarti ada data yang masih kosong
                    if ($kosong > 0) {
                  ?>
                      <script>
                        $(document).ready(function() {
                          // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                          $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                          $("#kosong").show(); // Munculkan alert validasi kosong
                        });
                      </script>
                  <?php
                    } else { // Jika semua data sudah diisi
                      echo "<hr>";
                      // Buat sebuah tombol untuk mengimport data ke database
                      echo "<button type='submit' class='btn btn-primary btn-sm' name='import'><span class='fa fa-user-plus'></span> Import File</button>";
                      echo "<a href='" . base_url("admin/regional") . "'  style='margin:10px;' class='btn btn-warning btn-sm'><span class='fa  fa-close '></span> Batal</a>";
                    }

                    echo "</form>";
                  }
                  ?>

                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

</body>

</html>