<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>

  <script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>
  <script>
    $(document).ready(function() {
      // Sembunyikan alert validasi kosong
      $("#kosong").hide();
    });
  </script>

  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Project
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
          <li class="active"> Project</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">

              <div class="box">
                <div class="box-header">
                  <a class="btn btn-primary btn-sm" href="<?php echo base_url("excel/format_project1.xlsx"); ?>"><span class="fa fa-user-plus"></span> Download Format</a>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="<?php echo base_url("admin/project/form"); ?>" enctype="multipart/form-data">
                    <!-- 
    -- Buat sebuah input type file
    -- class pull-left berfungsi agar file input berada di sebelah kiri
    -->
                    <input type="file" name="file">

                    <!--
    -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
    -->
                    <br>
                    <input class="btn btn-warning btn-sm" type="submit" name="preview" value="Preview Dokumen">
                  </form>

                  <br>
                  <?php
                  if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form 
                    if (isset($upload_error)) { // Jika proses upload gagal
                      echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
                      die; // stop skrip
                    }


                    // Buat sebuah tag form untuk proses import data ke database
                    echo "<form method='post' action='" . base_url("admin/project/import") . "'>";

                    // Buat sebuah div untuk alert validasi kosong
                    echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";

                    echo "<div class='table-responsive'>
                    <table id='example1' class='table table-sm table-striped table-hover' style='font-size:12px;'>
    <tr>
    <th colspan='15'><center><b>Preview Data</b></center></th>
    </tr>
    <tr>
    <th class='bg-primary'>Kode Customer</th>
    <th class='bg-primary'>Nama Customer</th>
    <th class='bg-primary'>Nama Project</th>
    <th class='bg-primary'>Alamat</th>
    <th class='bg-primary'>Kode Kota</th>
    <th class='bg-primary'>Kota</th>
    <th class='bg-primary'>Kode Provinsi</th>
    <th class='bg-primary'>Provinsi</th>
    <th class='bg-primary'>Kode Pos</th>
    <th class='bg-primary'>No Telepon</th>
    <th class='bg-primary'>No HP</th>
    <th class='bg-primary'>PIC</th>
    <th class='bg-primary'>Email</th>
    <th class='bg-primary'>NPWP</th>
    <th class='bg-primary'>Status Aktiv</th>
    </tr>";

                    $numrow = 1;
                    $kosong = 0;

                    // Lakukan perulangan dari data yang ada di excel
                    // $sheet adalah variabel yang dikirim dari controller
                    foreach ($sheet as $row) {
                      // Ambil data pada excel sesuai Kolom
                      $ProjectKodeCustomer = $row['A']; // Ambil data NIS
                      $ProjectNamaCustomer = $row['B']; // Ambil data nama
                      $ProjectNama = $row['C'];
                      $Alamat = $row['D'];
                      $ProjekKabupatenKode = $row['E'];
                      $ProjekKabupatenNama = $row['F'];
                      $ProjekProvinsiKode = $row['G'];
                      $ProjekProvinsiNama = $row['H'];
                      $KodePos = $row['I'];
                      $NoTelp = $row['J'];
                      $NoHP = $row['K'];
                      $PIC = $row['L'];
                      $Email = $row['M'];
                      $Npwp = $row['N'];
                      $StsAktiv = $row['O'];

                      // Cek jika semua data tidak diisi
                      if (
                        $ProjectKodeCustomer == ""
                        && $ProjectNamaCustomer == ""
                        && $ProjectNama == ""
                        && $Alamat == ""
                        && $ProjekKabupatenKode == ""
                        && $ProjekKabupatenNama == ""
                        && $ProjekProvinsiKode == ""
                        && $ProjekProvinsiNama == ""
                        && $KodePos == ""
                        && $NoTelp == ""
                        && $NoHP == ""
                        && $PIC == ""
                        && $Email == ""
                        && $Npwp == ""
                        && $StsAktiv == ""
                      )
                        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                      // Cek $numrow apakah lebih dari 1
                      // Artinya karena baris pertama adalah nama-nama kolom
                      // Jadi dilewat saja, tidak usah diimport
                      if ($numrow > 1) {
                        // Validasi apakah semua data telah diisi
                        $ProjectKodeCustomer_td = (!empty($ProjectKodeCustomer)) ? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                        $ProjectNamaCustomer_td = (!empty($ProjectNamaCustomer)) ? "" : " style='background: #E07171;'";
                        $ProjectNama_td = (!empty($ProjectNama)) ? "" : " style='background: #E07171;'";
                        $Alamat_td = (!empty($Alamat)) ? "" : " style='background: #E07171;'";
                        $ProjekKabupatenKode_td = (!empty($ProjekKabupatenKode)) ? "" : " style='background: #E07171;'";
                        $ProjekKabupatenNama_td = (!empty($ProjekKabupatenNama)) ? "" : " style='background: #E07171;'";
                        $ProjekProvinsiKode_td = (!empty($ProjekProvinsiKode)) ? "" : " style='background: #E07171;'";
                        $ProjekProvinsiNama_td = (!empty($ProjekProvinsiNama)) ? "" : " style='background: #E07171;'";
                        $KodePos_td = (!empty($KodePos)) ? "" : " style='background: #E07171;'";
                        $NoTelp_td = (!empty($NoTelp)) ? "" : " style='background: #E07171;'";
                        $NoHP_td = (!empty($NoHP)) ? "" : " style='background: #E07171;'";
                        $PIC_td = (!empty($PIC)) ? "" : " style='background: #E07171;'";
                        $Email_td = (!empty($Email)) ? "" : " style='background: #E07171;'";
                        $Npwp_td = (!empty($Npwp)) ? "" : " style='background: #E07171;'";
                        $StsAktiv_td = (!empty($StsAktiv)) ? "" : " style='background: #E07171;'";

                        // Jika salah satu data ada yang kosong
                        if (
                          $ProjectKodeCustomer == ""
                          or $ProjectNamaCustomer == ""
                          or $ProjectNama == ""
                          or $Alamat == ""
                          or $ProjekKabupatenKode == ""
                          or $ProjekKabupatenNama == ""
                          or $ProjekProvinsiKode == ""
                          or $ProjekProvinsiNama == ""
                          or $KodePos == ""
                          or $NoTelp == ""
                          or $NoHP == ""
                          or $PIC == ""
                          or $Email == ""
                          or $Npwp == ""
                          or $StsAktiv == ""
                        ) {

                          $kosong++; // Tambah 1 variabel $kosong
                        }

                        echo "<tr>";
                        echo "<td" . $ProjectKodeCustomer_td . ">" . $ProjectKodeCustomer . "</td>";
                        echo "<td" . $ProjectNamaCustomer_td . ">" . $ProjectNamaCustomer . "</td>";
                        echo "<td" . $ProjectNama_td . ">" . $ProjectNama . "</td>";
                        echo "<td" . $Alamat_td . ">" . $Alamat . "</td>";
                        echo "<td" . $ProjekKabupatenKode_td . ">" . $ProjekKabupatenKode . "</td>";
                        echo "<td" . $ProjekKabupatenNama_td . ">" . $ProjekKabupatenNama . "</td>";
                        echo "<td" . $ProjekProvinsiKode_td . ">" . $ProjekProvinsiKode . "</td>";
                        echo "<td" . $ProjekProvinsiNama_td . ">" . $ProjekProvinsiNama . "</td>";
                        echo "<td" . $KodePos_td . ">" . $KodePos . "</td>";
                        echo "<td" . $NoTelp_td . ">" . $NoTelp . "</td>";
                        echo "<td" . $NoHP_td . ">" . $NoHP . "</td>";
                        echo "<td" . $PIC_td . ">" . $PIC . "</td>";
                        echo "<td" . $Email_td . ">" . $Email . "</td>";
                        echo "<td" . $Npwp_td . ">" . $Npwp . "</td>";
                        echo "<td" . $StsAktiv_td . ">" . $StsAktiv . "</td>";
                        echo "</tr>";
                      }

                      $numrow++; // Tambah 1 setiap kali looping
                    }

                    echo "</table> </div>";

                    // Cek apakah variabel kosong lebih dari 0
                    // Jika lebih dari 0, berarti ada data yang masih kosong
                    if ($kosong > 0) {
                      ?>
                      <script>
                        $(document).ready(function() {
                          // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                          $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                          $("#kosong").show(); // Munculkan alert validasi kosong
                        });
                      </script>
                  <?php
                    } else { // Jika semua data sudah diisi
                      echo "<hr>";

                      // Buat sebuah tombol untuk mengimport data ke database
                      echo "<button type='submit' class='btn btn-primary btn-sm' name='import'><span class='fa fa-user-plus'></span> Import File</button>";
                      echo "<a href='" . base_url("admin/project") . "'style='margin:10px;' class='btn btn-warning btn-sm'><span class='fa  fa-close '></span> Batal</a>";
                    }

                    echo "</form>";
                  }
                  ?>



                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>



    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker2').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Principal Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "Principal berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Principal Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>
</body>

</html>