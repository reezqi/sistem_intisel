<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
</head>


<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit Site
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Edit Site</a></li>
                    <li class="active">Edit Site</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="modal-dialog" role="document">
                        <!--Modal Edit Regional-->
                        <?php
                        $b = $data1->row_array();
                        ?>

                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/site/simpan_update_site' ?>" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Ref </label>
                                <div class="col-sm-5">
                                    <input type="hidden" name="idSite" value="<?php echo $b['idSite']; ?>">
                                    <input class="form-control" value="<?php echo $b['noRef']; ?>" rows="3" name="noRef" placeholder="No Ref " readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site Utama (Base on PO Customer)</label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['codeSite']; ?>" name="codeSite" placeholder="Kode Site Utama (Base on PO Customer)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site Utama (Base on PO Customer) </label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['siteName']; ?>" name="siteName" placeholder="Nama Site Utama (Base on PO Customer)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site (Projek) </label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['codeSite1']; ?>" name="codeSite1" placeholder="Kode Site (Projek)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site (Projek)</label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['siteName1']; ?>" name="siteName1" placeholder="Nama Site (Projek)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site (Regional)</label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['codeSite2']; ?>" name="codeSite2" placeholder="Kode Site (Regional) " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site (Regional)</label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['siteName2']; ?>" name="siteName2" placeholder="Nama Site (Regional) " required>
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site 3</label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['codeSite3']; ?>" name="codeSite3" placeholder="Kode Site 3 " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site 3</label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['siteName3']; ?>" name="siteName3" placeholder="Nama Site 3 " required>
                                </div>
                            </div>-->

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">DUID/ No Shipment </label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['shipmentNo']; ?>" name="shipmentNo" placeholder="No Shipment " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">PO Line </label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['POLine']; ?>" name="POLine" placeholder="PO Line " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">VPRM</label>
                                <div class="col-sm-5">
                                    <select name="owner" class="js-example-basic-single form-control js-example-basic-single" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refowner->result_array() as $i) {
                                            $KodeOwner = $i['KodeOwner'];
                                            $NamaOwner = $i['NamaOwner'];
                                            if ($b['ownerId'] == $KodeOwner)
                                                echo "<option value='$KodeOwner' selected>$KodeOwner - $NamaOwner</option>";
                                            else
                                                echo "<option value='$KodeOwner'>$KodeOwner - $NamaOwner</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> SAM</label>
                                <div class="col-sm-5">
                                    <select name="sam" class="js-example-basic-single form-control js-example-basic-single" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refsam->result_array() as $i) {
                                            $KodeSAM = $i['KodeSAM'];
                                            $NamaSAM = $i['NamaSAM'];
                                            if ($b['samid'] == $KodeSAM)
                                                echo "<option value='$KodeSAM' selected>$KodeSAM - $NamaSAM</option>";
                                            else
                                                echo "<option value='$KodeSAM'>$KodeSAM - $NamaSAM</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Regional</label>
                                <div class="col-sm-7">
                                    <select name="Regional" id="Regional" class="js-example-basic-single form-control js-example-basic-single" required>
                                    <?php if ($this->session->userdata('is_admin') === TRUE) {
                                        ?>
                                            <option value="">No Selected</option>
                                        <?php } ?>
                                        <?php
                                        foreach ($refregional as $i) {
                                            $KODE = $i->KODE;
                                            $NAMA = $i->NAMA;
                                            if ($b['SiteRegionalKode'] == $KODE)
                                                echo "<option value='$KODE' selected>$KODE - $NAMA</option>";
                                            else
                                                echo "<option value='$KODE'>$KODE - $NAMA</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Customer</label>
                                <div class="col-sm-7">
                                    <select name="Customer" id="Customer" class="form-control js-example-basic-single" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refprincipal->result_array() as $i) {
                                            $kodePrincipal = $i['kodePrincipal'];
                                            $NamaPrincipal = $i['NamaPrincipal'];
                                            if ($b['SiteCustomerKode'] == $kodePrincipal)
                                                echo "<option value='$kodePrincipal' selected>$kodePrincipal - $NamaPrincipal</option>";
                                            else
                                                echo "<option value='$kodePrincipal'>$kodePrincipal - $NamaPrincipal</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Kode SOW </label>
                                <div class="col-sm-7">
                                    <select class="form-control js-example-basic-single" name="KodeSOW" id="KodeSOW" style="width: 100%;" required>
                                       <option value="<?php echo $b['SiteSOWKode']; ?>"><?php echo $b['SiteSOWKode']; ?></option>
                                    </select>
                                    <!-- <input type="text" name="KodeSOW" value="<?php echo $b['SiteSOWKode']; ?>" class="form-control" id="KodeSOW" placeholder="Kode SOW"> -->
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Nama SOW </label>
                                <div class="col-sm-7">
                                    <textarea name="NamaSOW" id="NamaSOW" class="form-control" placeholder="Nama SOW" readonly><?php echo $b['SiteSOWNama']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Kontrak </label>
                                <div class="col-sm-5">
                                    <input class="form-control" rows="3" value="<?php echo $b['noContract']; ?>" name="noContract" placeholder="No Kontrak " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Tahun Kontrak </label>
                                <div class="col-sm-2">
                                    [1] <input class="form-control" rows="3" value="<?php echo $b['thnContract_1']; ?>" name="thnContract_1" placeholder="Tahun 1 " required>
                                </div>
                                <div class="col-sm-2">
                                    [2] <input class="form-control" rows="3" value="<?php echo $b['thnContract_2']; ?>" name="thnContract_2" placeholder="Tahun 2 " required>
                                </div>
                                <div class="col-sm-2">
                                    [3] <input class="form-control" rows="3" value="<?php echo $b['thnContract_3']; ?>" name="thnContract_3" placeholder="Tahun 3 " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Koordinat</label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['Koordinat']; ?>" name="Koordinat" class="form-control" id="inputUserName" placeholder="Koordinat" required> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Komentar Masa Kontrak</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="keterangan" class="form-control" id="inputUserName" placeholder="Komentar Masa Kontrak" required> <?php echo $b['keterangan']; ?></textarea>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Tanggal</label>
                                <div class="col-sm-7">
                                    <input class="form-control input-sm" value="<?php echo $b['created_at'] ?>" type="text" id="datepicker" name="datepicker" placeholder="Tanggal PO ">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" onclick="window.history.back();" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                        </form>
                    </div>
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>


            <!-- jQuery 2.2.3 -->
            <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <!-- page script -->

        <script type="text/javascript">
            $(document).ready(function() {  
                $('.js-example-basic-single').select2();


                // $('#KodeSOW').autocomplete({
                //     source: "<?php echo site_url('admin/msow/get_autocomplete'); ?>",

                //     select: function(event, ui) {
                //         $('[name="KodeSOW"]').val(ui.item.label);
                //         $('[name="NamaSOW"]').val(ui.item.description);
                //     }
                // });

                $('#KodeSOW').select2({
                    minimumInputLength: 1,
                    placeholder: "PILIH KODE SOW",
                    ajax: {
                        url: "<?php echo site_url('admin/msow/ajax_sow_region_customer')?>",
                        dataType: 'json',
                        type: "POST",
                        delay: 250,
                        data: function (params) {
                            return {
                                term: params.term,
                                region: $('#Regional').val(),
                                customer: $('#Customer').val()
                            };
                        },
                        processResults: function (data) {
                            return {
                                results : data
                            }
                        }
                    }
                })

                $('#KodeSOW').on('select2:select', function (e) {
                    var data = e.params.data;
                    $('#NamaSOW').text(data.slug);
                })

            });
        </script>

<script>
            $(function() {

                $('#datepicker').datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });

            });
        </script>
        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Agenda berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>



</body>

</html>