<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <?php
      $rule_id = $this->session->userdata('rule_id');
      $queryMenu = "SELECT refmenu.Id, Menu  
                            FROM refmenu JOIN refmenuakses
                              ON refmenu.Id = refmenuakses.menu_id
                           WHERE refmenuakses.role_id = '$rule_id'
                        ORDER BY refmenuakses.menu_id ASC
                        ";
      $menu = $this->db->query($queryMenu)->result_array();
      //echo json_encode($menu);
      ?>


      <?php foreach ($menu as $m) : ?>
        <li class="header"> <?= $m['Menu']; ?></li>

        <?php
        $menuId = $m['Id'];
        $querySubMenu = "SELECT * 
                          FROM refmenusub JOIN refmenu
                          ON refmenusub.menu_id = refmenu.Id
                          WHERE refmenusub.menu_id = '$menuId'
                          AND refmenusub.is_active = '1' ";
        $subMenu = $this->db->query($querySubMenu)->result_array();
        ?>

        <?php foreach ($subMenu as $sm) : ?>
          <li>
            <a href="<?php echo base_url($sm['url']); ?>">
              <i class="<?= $sm['icon']; ?>"></i>
              <span><?= $sm['title']; ?></span>

              <span class="pull-right-container">
                <small class="label pull-right"></small>
              </span>
            </a>
          </li>
        <?php endforeach; ?>

      <?php endforeach; ?>
        
     
          <!-- <li>
            <a href="<?php echo base_url('admin/pengguna/menus'); ?>">
           
              <span>Menu</span>

              <span class="pull-right-container">
                <small class="label pull-right"></small>
              </span>
            </a>
          </li> -->



      <?php if ($this->session->userdata('akses') == '1') : ?>

        <li class="active treeview">
          <a href="<?php echo base_url() . 'admin/dashboard' ?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gears"></i>
            <span>Administrasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() . 'admin/kategoridokumen' ?>"><i class="fa fa-circle-o"></i>Hak Akses</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">



            <li><a href="<?php echo base_url() . 'admin/regional' ?>"><i class="fa fa-circle-o"></i>Master Regional</a></li>
            <!-- <li><a href="<?php echo base_url() . 'admin/site' ?>"><i class="fa fa-circle-o"></i>Master Site</a></li> -->
            <li><a href="<?php echo base_url() . 'admin/principal' ?>"><i class="fa fa-circle-o"></i>Master Customer</a></li>
            <li><a href="<?php echo base_url() . 'admin/project' ?>"><i class="fa fa-circle-o"></i>Master Project</a></li>
            <li><a href="<?php echo base_url() . 'admin/subcon' ?>"><i class="fa fa-circle-o"></i>Master Subcon</a></li>
            <li><a href="<?php echo base_url() . 'admin/jenispekerjaan' ?>"><i class="fa fa-circle-o"></i>Master Jenis Pekerjaan</a></li>
            <li><a href="<?php echo base_url() . 'admin/msow' ?>"><i class="fa fa-circle-o"></i>Master SOW</a></li>
            <!--<li><a href="<?php echo base_url() . 'admin/pohwawei' ?>"><i class="fa fa-circle-o"></i>PO Customer Hwawei</a></li> -->
            <li><a href="<?php echo base_url() . 'admin/kategoridokumen' ?>"><i class="fa fa-circle-o"></i>Matriks PDP</a></li>
            <li><a href="<?php echo base_url() . 'admin/unit' ?>"><i class="fa fa-circle-o"></i>Master Unit</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa  fa-area-chart"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() . 'admin/customer' ?>"><i class="fa fa-circle-o"></i> PO Ke Customer</a></li>
            <li><a href="<?php echo base_url() . 'admin/supplier' ?>"><i class="fa fa-circle-o"></i> PO Ke Subcon</a></li>
            <!--<li><a href="<?php echo base_url() . 'admin/profit' ?>"><i class="fa fa-circle-o"></i> Profit</a></li> -->

            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Invoice Ke Customer</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Invoice Dari Subcon</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Pembayaran Invoice Ke Subcon</a></li>
            <li><a href="<?php echo base_url() . 'admin/kategoridokumen' ?>"><i class="fa fa-circle-o"></i>Update PDP</a></li>


          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-print"></i>
            <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url() . 'admin/customer' ?>"><i class="fa fa-circle-o"></i> Laporan PO Belum Tutup</a></li>
            <li><a href="<?php echo base_url() . 'admin/supplier' ?>"><i class="fa fa-circle-o"></i> Laporan Outstanding Invoice</a></li>
            <li><a href="<?php echo base_url() . 'admin/profit' ?>"><i class="fa fa-circle-o"></i> Laporan Invoice Terbit</a></li>
            <li><a href="<?php echo base_url() . 'admin/sow' ?>"><i class="fa fa-circle-o"></i> Laporan PC</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i> Laporan Penerimaan Invoice</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Laporan VR</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Laporan PO Customer</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Laporan PO Supplier</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Laporan PDP</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Laporan Invoice Customer</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i>Laporan Invoice Supplier</a></li>

            <li><a href="<?php echo base_url() . 'admin/sow' ?>"><i class="fa fa-circle-o"></i> PO SOW</a></li>
            <li><a href="<?php echo base_url() . 'admin/sowdetail' ?>"><i class="fa fa-circle-o"></i> PO SOW Detail</a></li>
          </ul>
        </li>

      <?php endif; ?>

  </section>
  <!-- /.sidebar -->
</aside>