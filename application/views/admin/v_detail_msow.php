<?php
include('database_connection.php');
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Detail SOW
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#"> Detail SOW</a></li>
          <li class="active">Tambah Detail SOW</li>
        </ol>
      </section>



      <!-- Main content -->
      <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">

          <?php $b = $data1->row_array(); ?>
          <form class="form-horizontal" id="insert_form" method="post" enctype="multipart/form-data">

            <section class="invoice" style="padding-top:5px;">
              <!-- title row -->
              <div class="row">
                <div class="col-md-12 col-xs-12" style="background-color:#3C8DBC;">
                  <h4 class="page">
                    <b>SOW :</b><?php echo $b['SOW']; ?>
                    <small class="pull-right" style="color:black;">Cod/Item/Line : <b><?php echo $b['Line']; ?></b></small>
                    <input type="hidden" value="<?php echo $b['Code']; ?>" name="Code">
                    </h2>
                </div>
                <br><br>
              </div>
            </section>
            <!-- /.content -->

            <div class="invoice" style="padding-top:5px;">
              <div class="row">
                <div class="col-md-12">
                  <!-- Custom Tabs (Pulled to the right) -->
                  <div class="nav-tabs-custom">

                    <div class="tab-content">
                      <div class="tab-pane active" id="tab_1-1">
                        <table id="item_table" class="table table-sm table-striped table-hover " style="font-size:12px;">

                          <tr>
                            <th class="bg-primary" style="width: 50%; ">
                              <center>Regional</center>
                            </th>
                            <th class="bg-primary" style="width: 25%;">
                              <center>Price</center>
                            </th>
                            <th class="bg-primary">
                              <center><button type="button" name="add" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></center></button>
                            </th>
                          </tr>
                        </table>
                        <div align="center">
                          <input type="submit" name="submit" class="btn btn-primary" value="Simpan" />
                        </div>
                      </div>

                      <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                  </div>
                  <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->
              </div>



          </form>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

    <script>
      $(document).ready(function() {

        var count = 0;

        $(document).on('click', '.add', function() {
          count++;
          var html = '';
          html += '<tr>';
          html += '<td><select name="item_category[]" class="form-control input-sm item_category" style="margin:-5px;" ><option value="">No Selected</option><?php echo fill_select_box_regional($connect); ?></select></td>';
          html += '<td><input type="text" name="item_sub_category[]" class="form-control input-sm " style="margin:-5px;"></td>';
          html += '<td><center><button type="button" name="remove" class="btn btn-danger btn-xs remove" style="margin:-5px;"><span class="glyphicon glyphicon-minus"></span></button></center></td>';
          $('tbody').append(html);
        });

        $(document).on('click', '.remove', function() {
          $(this).closest('tr').remove();
        });

        $('#insert_form').on('submit', function(event) {
          event.preventDefault();
          var error = '';

          $('.item_category').each(function() {
            var count = 1;

            if ($(this).val() == '') {
              error += '<p>Select Item Category at ' + count + ' row</p>';
              return false;
            }

            count = count + 1;

          });

          $('.item_sub_category').each(function() {

            var count = 1;

            if ($(this).val() == '') {
              error += '<p>Select Item Sub category ' + count + ' Row</p> ';
              return false;
            }

            count = count + 1;

          });

          var form_data = $(this).serialize();

          if (error == '') {
            $.ajax({
              url: "<?php echo site_url('admin/msow/simpan_msow_detail'); ?>",
              method: "POST",
              data: form_data,
              success: function(data) {
                window.location.reload()

                $.toast({
                  heading: 'Success',
                  text: "Detail SOW Berhasil Disimpan .",
                  showHideTransition: 'slide',
                  icon: 'success',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#7EC857'
                });
              }
            });
          } else {
            $('#error').html('<div class="alert alert-danger">' + error + '</div>');
          }

        });

      });
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker2').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>

    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PO Subcon berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Ukuran dokumen terlalu besar ",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>


</body>

</html>