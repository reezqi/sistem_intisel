<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>

  <script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>
  <script>
    $(document).ready(function() {
      // Sembunyikan alert validasi kosong
      $("#kosong").hide();
    });
  </script>

  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          SOW
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
          <li class="active">SOW</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">

              <div class="box">
                <div class="box-header">
                  <a class="btn btn-primary btn-sm" href="<?php echo base_url("excel/format_sow1.xlsx"); ?>"><span class="fa fa-user-plus"></span> Download File</a>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="<?php echo base_url("admin/msow/form"); ?>" enctype="multipart/form-data">
                    <!--
    -- Buat sebuah input type file
    -- class pull-left berfungsi agar file input berada di sebelah kiri
    -->
                    <input type="file" name="file">

                    <!--
    -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
    -->
                    <br>
                    <input class="btn btn-warning btn-sm" type="submit" name="preview" value="Preview Dokumen">
                  </form>

                  <br>
                  <?php
                  if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
                    if (isset($upload_error)) { // Jika proses upload gagal
                      echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
                      die; // stop skrip
                    }


                    // Buat sebuah tag form untuk proses import data ke database
                    echo "<form method='post' action='" . base_url("admin/msow/import") . "'>";

                    // Buat sebuah div untuk alert validasi kosong
                    echo "<div style='color: red;' id='kosong'>
                               Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
                          </div>";

                    echo "
                    <div class='table-responsive'>
                    <table id='example1' class='table table-sm table-striped table-hover' style='font-size:12px;'>
    <tr>
      <th colspan='18'><center><b>Preview Data</b></center></th>
    </tr>
    <tr>
      <th class='bg-primary'><Center> Kode SOW</center></th>
      <th class='bg-primary'><Center> Nama SOW</center></th>
      <th class='bg-primary'><Center> Kode Unit</center></th>
      <th class='bg-primary'><Center> Nama Unit</center></th>
      <th class='bg-primary'><Center> Persentase 1 (%)</center></th>
      <th class='bg-primary'><Center> Persentase 2 (%)</center></th>
      <th class='bg-primary'><Center> Persentase 3 (%)</center></th>
      <th class='bg-primary'><Center> Pembayaran 1 (Rp)</center></th>
      <th class='bg-primary'><Center> Pembayaran 2 (Rp)</center></th>
      <th class='bg-primary'><Center> Pembayaran 3 (Rp)</center></th>
      <th class='bg-primary'><Center> Kode Customer</center></th>
      <th class='bg-primary'><Center> Nama Customer</center></th>
      <th class='bg-primary'><Center> Kode Subcon</center></th>
      <th class='bg-primary'><Center> Nama Subcon</center></th>
      <th class='bg-primary'><Center> Kode Regional</center></th>
      <th class='bg-primary'><Center> Nama Regional</center></th>
      <th class='bg-primary'><Center> Kode Perusahaan</center></th>
      <th class='bg-primary'><Center> Nama Perusahaan</center></th>
      <th class='bg-primary'><Center> Harga</center></th>
    </tr>";

                    $numrow = 1;
                    $kosong = 0;

                    // Lakukan perulangan dari data yang ada di excel
                    // $sheet adalah variabel yang dikirim dari controller
                    foreach ($sheet as $row) {
                      // Ambil data pada excel sesuai Kolom
                      $Code = $row['A']; // Ambil data NIS
                      $SOWDesc = $row['B']; // Ambil data nama
                      $SOWKodeUnit = $row['C'];
                      $SOWNamaUnit = $row['D'];
                      $PemFull = $row['E'];
                      $F7 = $row['F'];
                      $F8 = $row['G'];
                      $Pem1 = $row['H'];
                      $Pem2 = $row['I'];
                      $Pem3 = $row['J'];
                      $SOWKodeCustomer = $row['K'];
                      $SOWNamaCustomer = $row['L'];
                      $SOWKodeSubcon = $row['M'];
                      $SOWNamaSubcon = $row['N'];
                      $SOWKodeRegional = $row['O'];
                      $SOWNamaRegional = $row['P'];
                      $SOWKodePerusahaan = $row['Q'];
                      $SOWNamaPerusahaan = $row['R'];
                      $HrgRegional = $row['S'];

                      // Cek jika semua data tidak diisi
                      if (
                        $Code == ""
                        && $SOWDesc == ""
                        && $SOWKodeUnit == ""
                        && $SOWNamaUnit == ""
                        && $PemFull == ""
                        && $F7 == ""
                        && $F8 == ""
                        && $Pem1 == ""
                        && $Pem2 == ""
                        && $Pem3 == ""
                        && $SOWKodeCustomer == ""
                        && $SOWNamaCustomer == ""
                        && $SOWKodeSubcon = ""
                        && $SOWNamaSubcon = ""
                        && $SOWKodeRegional = ""
                        && $SOWNamaRegional = ""
                        && $SOWKodePerusahaan = ""
                        && $SOWNamaPerusahaan = ""
                        && $HrgRegional = ""
                      )
                        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                      // Cek $numrow apakah lebih dari 1
                      // Artinya karena baris pertama adalah nama-nama kolom
                      // Jadi dilewat saja, tidak usah diimport
                      if ($numrow > 1) {
                        // Validasi apakah semua data telah diisi
                        $a = (!empty($Code)) ? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                        $b = (!empty($SOWDesc)) ? "" : " style='background: #E07171;'";
                        $c = (!empty($SOWKodeUnit)) ? "" : " style='background: #E07171;'";
                        $d = (!empty($SOWNamaUnit)) ? "" : " style='background: #E07171;'";
                        $e = (!empty($PemFull)) ? "" : " style='background: #E07171;'";
                        $f = (!empty($F7)) ? "" : " style='background: #E07171;'";
                        $g = (!empty($F8)) ? "" : " style='background: #E07171;'";
                        $h = (!empty($Pem1)) ? "" : " style='background: #E07171;'";
                        $i = (!empty($Pem2)) ? "" : " style='background: #E07171;'";
                        $j = (!empty($Pem3)) ? "" : " style='background: #E07171;'";
                        $k = (!empty($SOWKodeCustomer)) ? "" : " style='background: #E07171;'";
                        $l = (!empty($SOWNamaCustomer)) ? "" : " style='background: #E07171;'";
                        $m = (!empty($SOWKodeSubcon)) ? "" : " style='background: #E07171;'";
                        $n = (!empty($SOWNamaSubcon)) ? "" : " style='background: #E07171;'";
                        $o = (!empty($SOWKodeRegional)) ? "" : " style='background: #E07171;'";
                        $p = (!empty($SOWNamaRegional)) ? "" : " style='background: #E07171;'";
                        $q = (!empty($SOWKodePerusahaan)) ? "" : " style='background: #E07171;'";
                        $r = (!empty($SOWNamaPerusahaan)) ? "" : " style='background: #E07171;'";
                        $s = (!empty($HrgRegional)) ? "" : " style='background: #E07171;'";


                        // Jika salah satu data ada yang kosong
                        if (
                          $Code == ""
                          or $SOWDesc == ""
                          or $SOWKodeUnit == ""
                          or $SOWNamaUnit == ""
                          or $PemFull == ""
                          or $F7 == ""
                          or $F8 == ""
                          or $Pem1 == ""
                          or $Pem2 == ""
                          or $Pem3 == ""
                          or $SOWoKdeCustomer == ""
                          or $SOWNamaCustomer == ""
                          or $SOWKodeSubcon = ""
                          or $SOWNamaSubcon = ""
                          or $SOWKodeRegional = ""
                          or $SOWNamaRegional = ""
                          or $SOWKodePerusahaan = ""
                          or $SOWNamaPerusahaan = ""
                          or $HrgRegional = ""
                        ) {
                          $kosong++; // Tambah 1 variabel $kosong
                        }

                        echo "<tr>";
                        echo "<td" . $a . ">" . $Code . "</td>";
                        echo "<td" . $b . ">" . $SOWDesc . "</td>";
                        echo "<td" . $c . ">" . $SOWKodeUnit . "</td>";
                        echo "<td" . $d . ">" . $SOWNamaUnit . "</td>";
                        echo "<td" . $e . ">" . $PemFull . "</td>";
                        echo "<td" . $f . ">" . $F7 . "</td>";
                        echo "<td" . $g . ">" . $F8 . "</td>";
                        echo "<td" . $h . ">" . $Pem1 . "</td>";
                        echo "<td" . $i . ">" . $Pem2 . "</td>";
                        echo "<td" . $j . ">" . $Pem3 . "</td>";
                        echo "<td" . $k . ">" . $SOWKodeCustomer . "</td>";
                        echo "<td" . $l . ">" . $SOWNamaCustomer . "</td>";
                        echo "<td" . $m . ">" . $SOWKodeSubcon . "</td>";
                        echo "<td" . $n . ">" . $SOWNamaSubcon . "</td>";
                        echo "<td" . $o . ">" . $SOWKodeRegional . "</td>";
                        echo "<td" . $p . ">" . $SOWNamaRegional . "</td>";
                        echo "<td" . $q . ">" . $SOWKodePerusahaan . "</td>";
                        echo "<td" . $r . ">" . $SOWNamaPerusahaan . "</td>";
                        echo "<td" . $s . ">" . $HrgRegional . "</td>";
                        echo "</tr>";
                      }

                      $numrow++; // Tambah 1 setiap kali looping
                    }

                    echo "</table> </div>";

                    // Cek apakah variabel kosong lebih dari 0
                    // Jika lebih dari 0, berarti ada data yang masih kosong
                    if ($kosong > 0) {
                  ?>
                      <script>
                        $(document).ready(function() {
                          // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                          $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                          $("#kosong").show(); // Munculkan alert validasi kosong
                        });
                      </script>
                  <?php
                    } else { // Jika semua data sudah diisi
                      echo "<hr>";
                      // Buat sebuah tombol untuk mengimport data ke database
                      echo "<button type='submit' class='btn btn-primary btn-sm' name='import'><span class='fa fa-user-plus'></span> Import File</button>";
                      echo "<a href='" . base_url("admin/msow") . "'  style='margin:10px;' class='btn btn-warning btn-sm'><span class='fa  fa-close '></span> Batal</a>";
                    }

                    echo "</form>";
                  }
                  ?>

                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

</body>

</html>