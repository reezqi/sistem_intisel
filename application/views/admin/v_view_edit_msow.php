<?php
include('database_connection.php');
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Tambah SOW
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">SOW</a></li>
                    <li class="active">Tambah SOW</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <!-- /.box-header -->
                    <?php
                    $b = $data1->row_array(0);
                    ?>

                    <div class="box-body">
                        <form class="form-horizontal" id="insert_form" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Cod/Item/Line</label>
                                            <div class="col-sm-5">
                                                <input value="<?php echo $b['Line']; ?>" type="hidden" name="LineLama" class="form-control form-control-lg" id="inputUserName" placeholder="Cod/Item/Line" required>
                                                <input readonly value="<?php echo $b['Line']; ?>" type="text" name="Line" class="form-control form-control-lg" id="inputUserName" placeholder="Cod/Item/Line" required>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="1" name="ListJob">
                                                        <label for="inlineRadio1"> List Job </label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
-->
                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Jenis SOW</label>
                                            <div class="col-sm-7">
                                                <?php if ($b['SOWJenis'] == 'Internal') : ?>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="radio1" value="Internal" name="JenisSOW" checked>
                                                        <label for="inlineRadio1"> Internal </label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="radio2" value="External" name="JenisSOW">
                                                        <label for="inlineRadio2"> External </label>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="radio1" value="Internal" name="JenisSOW" checked>
                                                        <label for="inlineRadio1"> Internal </label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline">
                                                        <input type="radio" id="radio2" value="External" name="JenisSOW">
                                                        <label for="inlineRadio2"> External </label>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Kode SOW</label>
                                            <div class="col-sm-7">
                                                <input type="hidden" id="No" name="No" value="<?php echo $b['No']; ?>">
                                                <input value="<?php echo $b['Code']; ?>" type="text" name="Code" class="form-control form-control-lg" id="Code" placeholder="Kode SOW" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 "> SOW</label>
                                            <div class="col-sm-7">
                                                <input value="<?php echo $b['SOWDesc']; ?>" type="text" name="SOWDesc" class="form-control" id="SOWDesc" placeholder=" SOW" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 "> Unit </label>
                                            <div class="col-sm-5">
                                                <select name="Unit" id="Unit" class="js-example-basic-single form-control" required>
                                                    <option value="">No Selected</option>
                                                    <?php
                                                    foreach ($refunit->result_array() as $i) {
                                                        $KodeUnit = $i['KodeUnit'];
                                                        $NamaUnit = $i['NamaUnit'];
                                                        if ($b['SOWKodeUnit'] == $KodeUnit)
                                                            echo "<option value='$KodeUnit' selected>$KodeUnit - $NamaUnit</option>";
                                                        else
                                                            echo "<option value='$KodeUnit'>$KodeUnit - $NamaUnit</option>";
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Term 1 (%) / (Rp)</label>
                                            <div class="col-sm-2">
                                                <input value="<?php echo $b['PemFull']; ?>" type="text" onclick="disable()" name="PemFull" id="PemFull" class="form-control" id="inputUserName" placeholder="%" required>
                                            </div>
                                            <div class="col-sm-5">
                                                <input value="<?php echo $b['Pem1']; ?>" type="text" onclick="disable2()" name="Pem1" id="Pem1" class="form-control" id="inputUserName" placeholder="Rp" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Term 2 (%) / (Rp)</label>
                                            <div class="col-sm-2">
                                                <input value="<?php echo $b['F7']; ?>" type="text" name="F7" id="F7" class="form-control" id="inputUserName" placeholder="%" required>
                                            </div>
                                            <div class="col-sm-5">
                                                <input value="<?php echo $b['Pem2']; ?>" type="text" name="Pem2" id="Pem2" class="form-control" id="inputUserName" placeholder="Rp" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Term 3 (%) / (Rp)</label>
                                            <div class="col-sm-2">
                                                <input value="<?php echo $b['F8']; ?>" type="text" name="F8" id="F8" class="form-control" id="inputUserName" placeholder="%" required>
                                            </div>
                                            <div class="col-sm-5">
                                                <input value="<?php echo $b['Pem3']; ?>" type="text" name="Pem3" id="Pem3" class="form-control" id="inputUserName" placeholder="Rp" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 "> Customer </label>
                                            <div class="col-sm-7">
                                                <select name="Customer" id="Customer" class="js-example-basic-single form-control" required>
                                                    <option value="">No Selected</option>
                                                    <?php
                                                    foreach ($refprincipal->result_array() as $i) {
                                                        $kodePrincipal = $i['kodePrincipal'];
                                                        $NamaPrincipal = $i['NamaPrincipal'];
                                                        if ($b['SOWKodeCustomer'] == $kodePrincipal)
                                                            echo "<option value='$kodePrincipal' selected>$kodePrincipal - $NamaPrincipal</option>";
                                                        else
                                                            echo "<option value='$kodePrincipal'>$kodePrincipal - $NamaPrincipal</option>";
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 "> Perusahaan </label>
                                            <div class="col-sm-7">
                                                <select name="Perusahaan" id="Perusahaan" class="js-example-basic-single form-control" required>
                                                    <option value="">No Selected</option>
                                                    <?php
                                                    foreach ($refperusahaan->result_array() as $i) {
                                                        $KodePerusahaan = $i['KodePerusahaan'];
                                                        $NamaPerusahaan = $i['NamaPerusahaan'];
                                                        if ($b['SOWKodePerusahaan'] == $KodePerusahaan)
                                                            echo "<option value='$KodePerusahaan' selected>$KodePerusahaan - $NamaPerusahaan</option>";
                                                        else
                                                            echo "<option value='$KodePerusahaan'>$KodePerusahaan - $NamaPerusahaan</option>";
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 "> Subcon </label>
                                            <div class="col-sm-7">
                                                <select name="Subcon" id="Subcon" class="js-example-basic-single form-control" required>
                                                    <option value="">No Selected</option>
                                                    <?php
                                                    foreach ($refsubcon->result_array() as $i) {
                                                        $KodeSuppl = $i['KodeSuppl'];
                                                        $Nama = $i['Nama'];
                                                        if ($b['SOWKodeSubcon'] == $KodeSuppl)
                                                            echo "<option value='$KodeSuppl' selected>$KodeSuppl - $Nama</option>";
                                                        else
                                                            echo "<option value='$KodeSuppl'>$KodeSuppl - $Nama</option>";
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">No Kontrak</label>
                                            <div class="col-sm-5">
                                                <input value="<?php echo $b['NoKontrak']; ?>" type="text" name="NoKontrak" class="form-control" id="NoKontrak" placeholder="No Kontrak" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4">Tahun Kontrak </label>
                                            <div class="col-sm-2">
                                                [1] <input class="form-control" rows="3" value="<?php echo $b['ThnKontrak1']; ?>" name="ThnKontrak1" id="ThnKontrak1" placeholder="Tahun 1 " required>
                                            </div>
                                            <div class="col-sm-2">
                                                [2] <input class="form-control" rows="3" value="<?php echo $b['ThnKontrak2']; ?>" name="ThnKontrak2" id="ThnKontrak2" placeholder="Tahun 2 " required>
                                            </div>
                                            <div class="col-sm-2">
                                                [3] <input class="form-control" rows="3" value="<?php echo $b['ThnKontrak3']; ?>" name="ThnKontrak3" id="ThnKontrak3" placeholder="Tahun 3 " required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Komentar</label>
                                            <div class="col-sm-7">
                                                <input value="<?php echo $b['Komentar']; ?>" type="text" class="form-control" rows="3" name="Keterangan" id="Keterangan" class="form-control" id="inputUserName" placeholder="Komentar" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-4 ">Tanggal</label>
                                            <div class="col-sm-7">
                                                <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="datepicker" name="datepicker" placeholder="Tanggal PO ">
                                            </div>
                                        </div>
                                    

                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-5">
                                    <div class="tab-content" style="margin-top:12px">
                                        <div class="tab-pane active" id="tab_1-1">
                                            <table id="item_table" class="table table-sm table-striped table-hover " style="font-size:12px; ">
                                                <thead>
                                                    <tr>
                                                        <th class="bg-primary" style="width: 50%; ">
                                                            <center>Regional</center>
                                                        </th>
                                                        <th class="bg-primary" style="width: 25%;">
                                                            <center>Price</center>
                                                        </th>
                                                        <th class="bg-primary">
                                                            <center><button type="button" name="add" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></center></button>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($data1->result() as $x) {
                                                        //echo $data1->num_rows();
                                                    ?>
                                                        <tr>
                                                            <td>
                                                                <select name="item_category[]" class="form-control input-sm item_category" style="margin:-5px;">
                                                                    <option value="">No Selected</option>
                                                                    <?php
                                                                    foreach ($refregional as $i) {
                                                                        $KODE = $i->KODE;
                                                                        $NAMA = $i->NAMA;
                                                                        if (trim($x->SOWKodeRegional) == $KODE)
                                                                            echo "<option value='$KODE' selected>$NAMA</option>";
                                                                        else
                                                                            echo "<option value='$KODE'>$NAMA</option>";
                                                                    } ?>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input value="<?php echo $x->HrgRegional ?>" type="text" name="item_sub_category[]" class="form-control input-sm " style="margin:-5px;">
                                                                <input value="<?php echo $x->No ?>" type="hidden" name="item[]" class="form-control input-sm " style="margin:-5px;">
                                                            </td>
                                                            <td>
                                                                <center><button type="button" name="remove" class="btn btn-danger btn-xs remove" style="margin:-5px;"><span class="glyphicon glyphicon-minus"></span></button></center>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" onclick="window.history.back();" style="margin-top:10px;" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" style="margin-top:10px;" class="btn btn-primary btn-flat" name="submit" id="simpan">Simpan</button>
                            </div>
                        </form>

                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

 <!--Modal Edit Regional-->
 <div class="modal fade" id="Modal"  role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#337AB7">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">CARI SOW</h4>
            </div>

              <div class="modal-body">
                <div class="form-group">
                    <label for="exampleInput">Cari SOW</label>
                    <select style="width: 100%;" id="cari" class="js-example-basic-single" name="state">
                    </select>
                </div>
              </div>
              <div class="modal-footer">
               
              </div>
          </div>
        </div>
      </div>
    <!--Modal Edit Unit-->

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <script>
            function disable() {
                document.getElementById("Pem1").disabled = true;
                document.getElementById("Pem2").disabled = true;
                document.getElementById("Pem3").disabled = true;
            }

            function disable2() {
                document.getElementById("PemFull").disabled = true;
                document.getElementById("F7").disabled = true;
                document.getElementById("F8").disabled = true;
            }

            function enable() {
                document.getElementById("name").disabled = false;
            }

            
        </script>

<script>
            $(document).ready(function() {
                for (var i=0; i<1; i++)
                {
                    if ($('#PemFull').val() == null || $('#F8').val() == null || $('#F7').val() == null) {
                        disable2()
                    } else {
                        disable()
                    }
                }

                $('#Customer').select2();
                $('#Perusahaan').select2();
                $('#Unit').select2();
                $('#Subcon').select2();

                function baris_baru()
                {
                    var Baris = "<tr>";
                    Baris += '<td><select name="item_category[]" class="form-control input-sm item_category" style="margin:-5px;" ><option value="">No Selected</option><?php echo fill_select_box_regional($connect); ?></select></td>';
                    
                    Baris += '<td>';
                    Baris += '<input type="text" name="item_sub_category[]" class="form-control input-sm " style="margin:-5px;">';
                    Baris += '</td>';
                    
                    Baris += '<td><center><button type="button" name="remove" class="btn btn-danger btn-xs remove" style="margin:-5px;"><span class="glyphicon glyphicon-minus"></span></button></center></td>';
                    Baris += "</tr>";

                    $('#item_table tbody').append(Baris);
                }

                $('#cari').select2()
                $('#radio2').on('click', function() {
                    if($('input:radio[name=JenisSOW]:checked').val() == "External"){
                        $('#Modal').modal('show');
                        // 
                        $('#cari').select2({
                            minimumInputLength: 1,
                            ajax: {
                                url: "<?php echo site_url('admin/msow/ajax_sow')?>",
                                dataType: 'json',
                                type: "POST",
                                delay: 250,
                                data: function (params) {
                                    return {
                                        term: params.term
                                    };
                                },
                                processResults: function (data) {
                                    return {
                                        results : data
                                    }
                                }
                            }
                        })
                        //

                        $('#cari').on('select2:select', function (e) {
                            var data = e.params.data;
                            $.ajax({
                                url: "<?php echo site_url('admin/msow/ajax_sow_select')?>",
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    line: data.id
                                },
                                success: function(response){
                                    $('#Code').val(response[0].Code);
                                    $('#SOWDesc').val(response[0].SOWDesc);
                                    $('#PemFull').val(response[0].PemFull);
                                    $('#Pem1').val(response[0].Pem1);
                                    $('#F7').val(response[0].F7);
                                    $('#Pem2').val(response[0].Pem2);
                                    $('#F8').val(response[0].F8);
                                    $('#Pem3').val(response[0].Pem3);
                                    $("#Customer").val(response[0].SOWKodeCustomer).trigger('change');
                                    $('#Perusahaan').val(response[0].SOWKodePerusahaan).trigger('change');
                                    $('#Unit').val(response[0].SOWKodeUnit).trigger('change');
                                    $('#Subcon').val(response[0].SOWKodeSubcon).trigger('change');
                                    $('#NoKontrak').val(response[0].NoKontrak);
                                    $('#ThnKontrak1').val(response[0].ThnKontrak1);
                                    $('#ThnKontrak2').val(response[0].ThnKontrak2);
                                    $('#ThnKontrak3').val(response[0].ThnKontrak3);
                                    $('#Keterangan').val(response[0].Keterangan);

                                    $('#item_table tbody').empty();
                                    for (var q=0; q < response.length; q++)
                                    {
                                        baris_baru()
                                        $('#item_table tbody tr:eq(' + q + ') td:nth-child(1) select').val(response[q].SOWKodeRegional);
                                        $('#item_table tbody tr:eq(' + q + ') td:nth-child(2) input').val(response[q].HrgRegional);
                                    }

                                    $('#Modal').modal('hide');

                                    if ($('#PemFull').val() == null || $('#F8').val() == null || $('#F7').val() == null) {
                                        disable2()
                                    } else {
                                        disable()
                                    }

                                }
                            });
                        });
                        
                    }
                })
            })
        </script>

        <script>
            $(document).ready(function() {

                var count = 0;

                $(document).on('click', '.add', function() {
                    count++;
                    var html = '';
                    html += '<tr>';
                    html += '<td><select name="item_category[]" class="form-control input-sm item_category" style="margin:-5px;" ><option value="">No Selected</option><?php echo fill_select_box_regional($connect); ?></select></td>';
                    html += '<td><input type="text" name="item_sub_category[]" class="form-control input-sm " style="margin:-5px;"></td>';
                    html += '<td><center><button type="button" name="remove" class="btn btn-danger btn-xs remove" style="margin:-5px;"><span class="glyphicon glyphicon-minus"></span></button></center></td>';
                    $('tbody').append(html);
                });

                $(document).on('click', '.remove', function() {
                    $(this).closest('tr').remove();
                });

                $('#insert_form').on('submit', function(event) {
                    event.preventDefault();
                    var error = '';

                    $('.item_category').each(function() {
                        var count = 1;

                        if ($(this).val() == '') {
                            error += '<p>Select Item Category at ' + count + ' row</p>';
                            return false;
                        }

                        count = count + 1;

                    });

                    $('.item_sub_category').each(function() {

                        var count = 1;

                        if ($(this).val() == '') {
                            error += '<p>Select Item Sub category ' + count + ' Row</p> ';
                            return false;
                        }

                        count = count + 1;

                    });

                    var form_data = $(this).serialize();

                    if (error == '') {
                        $.ajax({
                            url: "<?php echo site_url('admin/msow/submit_edit_msow'); ?>",
                            method: "POST",
                            data: form_data,
                            success: function(data) {
                                window.location.href = "<?php echo site_url('admin/msow'); ?>"


                                $.toast({
                                    heading: 'Success',
                                    text: " SOW Berhasil Disimpan .",
                                    showHideTransition: 'slide',
                                    icon: 'success',
                                    hideAfter: false,
                                    position: 'bottom-right',
                                    bgColor: '#7EC857'
                                });
                            }
                        });
                    } else {
                        $('#error').html('<div class="alert alert-danger">' + error + '</div>');
                    }

                });

            });
        </script>

<script>
      $(function() {
       

        $('#datepicker').datepicker({
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          changeYear: true
        });

      });
    </script>

        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Customer Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Customer berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Customer Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>


</body>

</html>