<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
</head>


<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Tambah Site
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Site</a></li>
                    <li class="active">Tambah Site</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="modal-dialog" role="document">
                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/site/simpan_site' ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Ref</label>
                                <div class="col-sm-5">
                                    <input type="text" name="noRef" class="form-control" id="inputUserName" value="<?php echo $noRef; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site Utama (Base on PO Customer)</label>
                                <div class="col-sm-7">
                                    <input type="text" name="codeSite" class="form-control" id="inputUserName" placeholder="Kode Site Utama  " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site Utama (Base on PO Customer)</label>
                                <div class="col-sm-7">
                                    <input type="text" name="siteName" class="form-control" id="inputUserName" placeholder="Nama Site Utama" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site (Projek)</label>
                                <div class="col-sm-7">
                                    <input type="text" name="codeSite1" class="form-control" id="inputUserName" placeholder="Kode Site (Projek)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site (Projek)</label>
                                <div class="col-sm-7">
                                    <input type="text" name="siteName1" class="form-control" id="inputUserName" placeholder="Nama Site (Projek)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site (Regional)</label>
                                <div class="col-sm-7">
                                    <input type="text" name="codeSite2" class="form-control" id="inputUserName" placeholder="Kode Site (Projek)" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site (Regional)</label>
                                <div class="col-sm-7">
                                    <input type="text" name="siteName2" class="form-control" id="inputUserName" placeholder="Nama Site (Projek)" required>
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Site 3</label>
                                <div class="col-sm-7">
                                    <input type="text" name="codeSite3" class="form-control" id="inputUserName" placeholder="Kode Site 3" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Site 3</label>
                                <div class="col-sm-7">
                                    <input type="text" name="siteName3" class="form-control" id="inputUserName" placeholder="Nama Site 3" required>
                                </div>
                            </div>-->

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">DUID/ No Shipment</label>
                                <div class="col-sm-7">
                                    <input type="text" name="shipmentNo" class="form-control" id="inputUserName" placeholder="No Shipment" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">PO Line</label>
                                <div class="col-sm-7">
                                    <input type="text" name="POLine" class="form-control" id="inputUserName" placeholder="PO Line" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> SAM</label>
                                <div class="col-sm-5">
                                    <select class="form-control js-example-basic-single" name="sam" style="width: 100%;" required>
                                        <option value="">No Selected</option>
                                        <?php foreach ($refsam as $i) { // Lakukan looping pada variabel siswa dari controller
                                            echo "<option value='" . $i->KodeSAM . "'>" . $i->KodeSAM . " - " . $i->NamaSAM . "</option>";
                                        } ?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> VPRM</label>
                                <div class="col-sm-5">
                                    <select class="js-example-basic-single form-control" name="owner" style="width: 100%;" required>
                                        <option value="">No Selected</option>
                                        <?php foreach ($refowner as $i) { // Lakukan looping pada variabel siswa dari controller
                                            echo "<option value='" . $i->KodeOwner . "'>" . $i->KodeOwner . " - " . $i->NamaOwner . "</option>";
                                        } ?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Regional </label>
                                <div class="col-sm-7">
                                    <select class="form-control js-example-basic-single" name="Regional" id="Regional" style="width: 100%;" required>
                                         <?php if ($this->session->userdata('is_admin') === TRUE) {
                                        ?>
                                            <option value="">No Selected</option>
                                        <?php } ?>
                                        <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                                            echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                                        } ?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Customer </label>
                                <div class="col-sm-7">
                                    <select class="form-control js-example-basic-single" name="Customer" id="Customer" style="width: 100%;" required>
                                        <option value="">No Selected</option>
                                        <?php foreach ($refprincipal as $i) { // Lakukan looping pada variabel siswa dari controller
                                            echo "<option value='" . $i->kodePrincipal . "'>" . $i->kodePrincipal . " - " . $i->NamaPrincipal . "</option>";
                                        } ?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Kode SOW </label>
                                <div class="col-sm-7">
                                    <select class="form-control js-example-basic-single" name="KodeSOW" id="KodeSOW" style="width: 100%;" required>
                                       
                                    </select>
                                    <!-- <input type="text" name="KodeSOW" class="form-control" id="KodeSOW" placeholder="Kode SOW"> -->
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Nama SOW </label>
                                <div class="col-sm-7">
                                    <textarea name="NamaSOW" id="NamaSOW" class="form-control" placeholder="Nama SOW" readonly></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Kontrak</label>
                                <div class="col-sm-5">
                                    <input type="text" name="noContract" class="form-control" id="inputUserName" placeholder="No Kontrak " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Tahun Kontrak </label>
                                <div class="col-sm-2">
                                    [1] <input type="text" name="thnContract_1" class="form-control" id="inputUserName" placeholder="Tahun 1" required>
                                </div>
                                <div class="col-sm-2">
                                    [2] <input type="text" name="thnContract_2" class="form-control" id="inputUserName" placeholder="Tahun 2" required>
                                </div>
                                <div class="col-sm-2">
                                    [3]<input type="text" name="thnContract_3" class="form-control" id="inputUserName" placeholder="Tahun 3" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Koordinat</label>
                                <div class="col-sm-5">
                                    <input type="text" name="Koordinat" class="form-control" id="inputUserName" placeholder="Koordinat" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Komentar Masa Kontrak</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="keterangan" class="form-control" id="inputUserName" placeholder="Komentar Masa Kontrak" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Tanggal</label>
                                <div class="col-sm-7">
                                    <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="datepicker" name="datepicker" placeholder="Tanggal PO ">
                                </div>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                    </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

            <!-- jQuery 2.2.3 -->
            <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <!-- page script -->

        <script type="text/javascript">
            $(document).ready(function() {
                $('.js-example-basic-single').select2();

                // $('#KodeSOW').autocomplete({
                //     source: "<?php echo site_url('admin/msow/get_autocomplete'); ?>",

                //     select: function(event, ui) {
                //         $('[name="KodeSOW"]').val(ui.item.label);
                //         $('[name="NamaSOW"]').val(ui.item.description);
                //     }
                // });

                $('#KodeSOW').select2({
                    minimumInputLength: 1,
                    placeholder: "PILIH KODE SOW",
                    ajax: {
                        url: "<?php echo site_url('admin/msow/ajax_sow_region_customer')?>",
                        dataType: 'json',
                        type: "POST",
                        delay: 250,
                        data: function (params) {
                            return {
                                term: params.term,
                                region: $('#Regional').val(),
                                customer: $('#Customer').val()
                            };
                        },
                        processResults: function (data) {
                            return {
                                results : data
                            }
                        }
                    }
                })

                $('#KodeSOW').on('select2:select', function (e) {
                    var data = e.params.data;
                    $('#NamaSOW').text(data.slug);
                })
                

            });
        </script>

        <script>
            $(function() {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });

                $('#datepicker').datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });

            });
        </script>
        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Customer Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Customer berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Customer Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>


</body>

</html>