<?php
include('database_connection.php');
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Tambah Matrik PDP
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Matrik PDP</a></li>
                    <li class="active">Tambah Matrik PDP</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form class="form-horizontal" id="insert_form" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="modal-body">
                                        <input type="hidden" name="line" id='line' value="<?php echo $line; ?>">

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-3 ">Regional</label>
                                            <div class="col-sm-7">
                                                <select class="form-control js-example-basic-single" name="KodeRegional" id="KodeRegional" style="width: 100%;" required>
                                                    <?php if ($this->session->userdata('is_admin') === TRUE) {
                                                    ?>
                                                        <option value="">No Selected</option>
                                                    <?php } ?>
                                                    <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                                                        echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-3 ">Customer</label>
                                            <div class="col-sm-7">
                                                <select class="form-control js-example-basic-single" name="KodeCustomer" id="KodeCustomer" style="width: 100%;" required>
                                                    <option value="">No Selected</option>
                                                    <?php foreach ($refprincipal as $i) { // Lakukan looping pada variabel siswa dari controller
                                                        echo "<option value='" . $i->kodePrincipal . "'>" . $i->kodePrincipal . " - " . $i->NamaPrincipal . "</option>";
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-3 ">Kode SOW</label>
                                            <div class="col-sm-7">
                                            <select class="form-control js-example-basic-single" name="KodeSOW" id="KodeSOW" style="width: 100%;" required>
                                       
                                            </select>
                                                <!-- <input type="text" name="KodeSOW" class="form-control" id="KodeSOW" placeholder="Kode SOW"> -->
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-3 ">Nama SOW</label>
                                            <div class="col-sm-9">
                                                <input name="NamaSOW" id="NamaSOW" class="form-control" placeholder="Nama SOW" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-3 ">Kode Site</label>
                                            <div class="col-sm-7">
                                                <select class="form-control js-example-basic-single" name="KodeSite" id="KodeSite" style="width: 100%;" required>
                                                
                                                </select>
                                                <!-- <input type="text" name="KodeSite" class="form-control" id="KodeSite" placeholder="Kode Site"> -->
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-3 ">Nama Site</label>
                                            <div class="col-sm-9">
                                                <input readonly name="NamaSite" id="NamaSite" class="form-control" placeholder="Nama Site">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputUserName" class="col-sm-3 ">Status</label>
                                            <div class="col-sm-5">
                                                <select class="form-control" name="active" required>
                                                    <option value="Y">Aktif</option>
                                                    <option value="N">Tidak Aktif </option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="tab-content" style="margin-top:12px">
                                        <div class="tab-pane active" id="tab_1-1">
                                            <table id="item_table" class="table table-sm table-striped table-hover " style="font-size:12px;">
                                                <thead>
                                                    <tr>
                                                        <th class="bg-primary" style="width: 40%;">
                                                            Progres/ Tahapan
                                                        </th>
                                                        <th class="bg-primary">
                                                          Dokumen
                                                        </th>
                                                        <th class="bg-primary">
                                                           Persen (%)
                                                        </th>
                                                        <th class="bg-primary">
                                                            <button type="button" name="add" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></button>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.tab-pane -->
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" style="margin-top:10px;" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" style="margin-top:10px;" class="btn btn-primary btn-flat" name="submit" id="simpan">Simpan</button>
                            </div>
                        </form>

                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

            <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        

        <script type="text/javascript">
            $(document).ready(function() {
                $('.js-example-basic-single').select2();

                // $('#KodeSOW').autocomplete({
                //     source: "<?php echo site_url('admin/msow/get_autocomplete'); ?>",

                //     select: function(event, ui) {
                //         $('[name="KodeSOW"]').val(ui.item.label);
                //         $('[name="NamaSOW"]').val(ui.item.description);
                //     }
                // });
                $('#KodeSOW').select2({
                    minimumInputLength: 1,
                    placeholder: "PILIH KODE SOW",
                    ajax: {
                        url: "<?php echo site_url('admin/msow/ajax_sow_region_customer')?>",
                        dataType: 'json',
                        type: "POST",
                        delay: 250,
                        data: function (params) {
                            return {
                                term: params.term,
                                region: $('#KodeRegional').val(),
                                customer: $('#KodeCustomer').val()
                            };
                        },
                        processResults: function (data) {
                            return {
                                results : data
                            }
                        }
                    }
                })

                $('#KodeSite').select2({
                    minimumInputLength: 1,
                    placeholder: "PILIH KODE SOW",
                    ajax: {
                        url: "<?php echo site_url('admin/site/ajax_site_region_customer')?>",
                        dataType: 'json',
                        type: "POST",
                        delay: 250,
                        data: function (params) {
                            return {
                                term: params.term,
                                region: $('#KodeRegional').val(),
                                customer: $('#KodeCustomer').val()
                            };
                        },
                        processResults: function (data) {
                            return {
                                results : data
                            }
                        }
                    }
                })

                $('#KodeSOW').on('select2:select', function (e) {
                    var data = e.params.data;
                    $('#NamaSOW').val(data.slug);
                })

                $('#KodeSite').on('select2:select', function (e) {
                    var data = e.params.data;
                    $('#NamaSite').val(data.slug);
                })

                // $('#KodeSite').autocomplete({
                //     source: "<?php echo site_url('admin/site/get_autocomplete'); ?>",

                //     select: function(event, ui) {
                //         $('[name="KodeSite"]').val(ui.item.label);
                //         $('[name="NamaSite"]').val(ui.item.description);
                //     }
                // });

            });
        </script>

        <script>
            $(document).ready(function() {
                
                function initializeSelect2(selectElementObj) {
                    selectElementObj.select2({
                        minimumInputLength: 1,
                        placeholder: "PILIH PEKERJAAN",
                        ajax: {
                            url: "<?php echo site_url('admin/jenispekerjaan/ajax_pekerjaan_customer')?>",
                            dataType: 'json',
                            type: "POST",
                            delay: 250,
                            data: function (params) {
                                return {
                                    term: params.term,
                                    customer: $('#KodeCustomer').val()
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results : data
                                }
                            }
                        }
                    });
                };

                var count = 0;

                $(document).on('click', '.add', function() {
                   baris_baru()
                });

                function baris_baru()
                {
                    var index = $('#item_table tbody tr').length;
                    var Baris = "<tr>";
                    Baris += '<td><select data-leng="'+ index +'" name="item_category[]" class="jenis form-control input-sm item_category js-example-basic-single" style="margin:-5px;" ><option value="">No Selected</option></select></td>';
                    
                    Baris += '<td>';
                    Baris += '<input type="text" name="item_sub_category[]" class="form-control input-sm " style="margin:-5px;">';
                    Baris += '</td>';

                    Baris += '<td>';
                    Baris += '<input type="text" name="item_persen[]" class="form-control input-sm " style="margin:-5px;"></td>';
                    Baris += '</td>';
                    
                    Baris += '<td><center><button type="button" name="remove" class="btn btn-danger btn-xs remove" style="margin:-5px;"><span class="glyphicon glyphicon-minus"></span></button></center></td>';
                    Baris += "</tr>";

                    $('#item_table tbody').append(Baris);
                    var newSelect=$("#item_table").find(".js-example-basic-single").last();
                    initializeSelect2(newSelect);
                }
                
                $('#item_table').on('select2:select', '.jenis', function(e) {
                    var data = e.params.data;
                    var index = $(this).attr('data-leng');

                    $('#item_table tbody tr:eq(' + index + ') td:nth-child(2) input').val(data.slug);

                });

                $(document).on('click', '.remove', function() {
                    $(this).closest('tr').remove();
                });

                $('#insert_form').on('submit', function(event) {
                    event.preventDefault();
                    var error = '';

                    $('.item_category').each(function() {
                        var count = 1;

                        if ($(this).val() == '') {
                            error += '<p>Select Item Category at ' + count + ' row</p>';
                            return false;
                        }

                        count = count + 1;

                    });

                    $('.item_sub_category').each(function() {

                        var count = 1;

                        if ($(this).val() == '') {
                            error += '<p>Select Item Sub category ' + count + ' Row</p> ';
                            return false;
                        }

                        count = count + 1;

                    });

                    var form_data = $(this).serialize();

                    if (error == '') {
                        $.ajax({
                            url: "<?php echo site_url('admin/kategoridokumen/simpan_kategoridokumen'); ?>",
                            method: "POST",
                            data: form_data,
                            success: function(data) {
                               if (data > 0) {
                                   window.location.reload()

                                    $.toast({
                                        heading: 'Success',
                                        text: " SOW Berhasil Disimpan .",
                                        showHideTransition: 'slide',
                                        icon: 'success',
                                        hideAfter: false,
                                        position: 'bottom-right',
                                        bgColor: '#7EC857'
                                    });
                               }
                            }
                        });
                    } else {
                        $('#error').html('<div class="alert alert-danger">' + error + '</div>');
                    }

                });

            });
        </script>
         <script>
            $(function() {
                // $("#item_table").DataTable({
                //     "lengthChange": false,
                //     "searching": false,
                //     "ordering": false,
                //     "info": false,
                //     drawCallback: function() {
                //         $('.dt-select2').select2();
                //     }
                // });
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });

                $('#datepicker').datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true,
                    changeYear: true
                });

            });
        </script>

        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Customer Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Customer berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Customer Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>


</body>

</html>