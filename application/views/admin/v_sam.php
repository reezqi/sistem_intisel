<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
</head>


<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          SAM
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li class="active"> SAM</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">

            <div class="box">
              <div class="box-header">
                <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Tambah SAM</a>
                <a class="btn btn-primary btn-sm" href="<?php echo base_url("admin/sam/form"); ?>"><span class="fa fa-user-plus"></span> Import SAM</a>
                <a class="btn btn-info btn-sm" href="<?php echo base_url("admin/sam/export"); ?>"><span class="fa fa-file-excel-o"> </span> Export Data</a>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                  <thead>
                    <tr>
                      <th class="bg-primary">Kode SAM</th>
                      <th class="bg-primary">Nama SAM</th>
                      <th class="bg-primary" style="text-align:right;">Operasi</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    foreach ($mastersam as $i) {
                    ?>

                      <tr>
                        <td> <?php echo $i->KodeSAM ?></td>
                        <td> <?php echo $i->NamaSAM ?></td>

                        <td style="text-align:right;">
                        <a style="padding: 6px 6px;" id="btn_edit" data-id="<?php echo $i->KodeSAM; ?>"><span class="fa fa-pencil"></span></a>
                          <a style="padding: 6px 6px;" data-toggle="modal" data-target="#ModalHapus<?php echo $i->KodeSAM; ?>"><span class="fa fa-trash"></span></a>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>


    <!--Modal Add sam-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#337AB7">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel" style="color:white">Tambah SAM</h4>
          </div>
          <form class="form-horizontal" action="<?php echo base_url() . 'admin/sam/simpan_sam' ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Kode SAM</label>
                <div class="col-sm-3">
                  <input type="text" name="KodeSAM" class="form-control" id="inputUserName" value="<?php echo $KodeSAM; ?>" readonly>
                </div>
              </div>

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Nama SAM</label>
                <div class="col-sm-7">
                  <input type="text" name="NamaSAM" class="form-control" id="inputUserName" placeholder="Nama SAM" required>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-4 control-label">Tanggal</label>
                <div class="col-md-7">
                  <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="datepicker" name="datepicker" placeholder="Tanggal">
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Add Regional-->

    <!--Modal Edit Regional-->
    <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#337AB7">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel" style="color:white">Edit SAM</h4>
          </div>

          <form class="form-horizontal" action="<?php echo base_url() . 'admin/sam/update_sam' ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="form-group">
                <label for="inputUserName" class="col-sm-4 control-label">Nama SAM</label>
                <div class="col-sm-7">
                  <input type="hidden" name="KodeSAM" id="KodeSAM" value="">
                  <input type="text" name="NamaSAM" id="NamaSAM" class="form-control" value="" id="inputUserName" placeholder="Nama SAM" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Tanggal</label>
                <div class="col-md-7">
                  <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="datepicker" name="datepicker" placeholder="Tanggal">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Perbarui</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Edit Unit-->

    <?php foreach ($mastersam as $i) { ?>
      <!--Modal Hapus sam-->
      <div class="modal fade" id="ModalHapus<?php echo $i->KodeSAM; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#337AB7">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Hapus SAM</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/sam/hapus_sam' ?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="KODE" value="<?php echo $i->KodeSAM; ?>" />
                <p>Apakah Anda yakin menghapus SAM <b><?php echo $i->NamaSAM; ?></b> ?</p>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    <?php } ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>
    <script>
      $(document).ready(function() {

        $('#example1').on('click', '#btn_edit', function(e) {
          var KodeSAM = $(this).attr('data-id');
          $.ajax({
            url: "<?php echo site_url('admin/sam/ajax_get_sam') ?>",
            type: "POST",
            data: {
              KodeSAM: KodeSAM,
            },
            cache: false,
            dataType: 'json',
            success: function(json) {
              $('#KodeSAM').val(json.KodeSAM);
              $('#NamaSAM').val(json.NamaSAM);
              $('#datepicker').val(json.created_at);
              $('#ModalEdit').modal('show');
            }
          })

        });

      });

      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          changeYear: true
        });
        $('#datepicker2').datepicker({
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          changeYear: true
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "SAM Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "SAM berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "SAM Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>
</body>

</html>