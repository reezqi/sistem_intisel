<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit Karyawan
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Edit Karyawan</a></li>
                    <li class="active">Edit Karyawan</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="modal-dialog" role="document">
                        <!--Modal Edit Regional-->
                        <?php
                        $b = $data1->row_array();
                        ?>

                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/karyawan/simpan_update_karyawan' ?>" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Karyawan</label>
                                <div class="col-sm-7">
                                    <input type="hidden" name="KodeAkun" value="<?php echo $b['KodeAkun']; ?>">
                                    <input type="text" name="NamaKaryawan" class="form-control" value="<?php echo $b['Nama']; ?>" id="inputUserName" placeholder="Nama Karyawan" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Alamat </label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="Alamat" placeholder="Alamat " required><?php echo $b['Alamat']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Regional</label>
                                <div class="col-sm-7">
                                    <select name="Regional" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refregional->result_array() as $i) {
                                            $KODE = $i['KODE'];
                                            $NAMA = $i['NAMA'];
                                            if ($b['KodeReg'] == $KODE)
                                                echo "<option value='$KODE' selected>$KODE - $NAMA</option>";
                                            else
                                                echo "<option value='$KODE'>$KODE - $NAMA</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Bank</label>
                                <div class="col-sm-7">
                                    <select name="Bank" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refbank->result_array() as $i) {
                                            $KodeBank = $i['KodeBank'];
                                            $NamaBank = $i['NamaBank'];
                                            if ($b['KodeBank'] == $KodeBank)
                                                echo "<option value='$KodeBank' selected>$KodeBank - $NamaBank</option>";
                                            else
                                                echo "<option value='$KodeBank'>$KodeBank - $NamaBank</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Rekening</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoRek" class="form-control" value="<?php echo $b['NoRek']; ?>" id="inputUserName" placeholder="No Rekening" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Status</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="StsKaryawan" required>
                                        <option value="1">Aktif</option>
                                        <option value="2">Tidak Aktif </option>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" onclick="window.history.back();" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                        </form>
                    </div>
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

        <!-- FastClick -->
        <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
        <!-- DataTables -->



</body>

</html>