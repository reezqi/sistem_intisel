<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit SOW
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Edit SOW</a></li>
                    <li class="active">Edit SOW</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="modal-dialog" role="document">
                        <!--Modal Edit Regional-->
                        <?php
                        $b = $data1->row_array();
                        ?>

                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/msow/simpan_update_msow' ?>" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Cod/Item/Line </label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['Line']; ?>" name="Line" placeholder="Cod/Item/Line " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Jenis SOW</label>
                                <div class="col-sm-7">
                                    <?php if ($b['SOWJenis'] == 'Internal') : ?>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="Internal" name="JenisSOW" checked>
                                            <label for="inlineRadio1"> Internal </label>
                                        </div>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="External" name="JenisSOW">
                                            <label for="inlineRadio2"> External </label>
                                        </div>
                                    <?php else : ?>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="Internal" name="JenisSOW">
                                            <label for="inlineRadio1"> Internal </label>
                                        </div>
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadio1" value="External" name="JenisSOW" checked>
                                            <label for="inlineRadio2"> External </label>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode SOW</label>
                                <div class="col-sm-7">
                                    <input type="hidden" name="No" value="<?php echo $b['No']; ?>">
                                    <input class="form-control" value="<?php echo $b['Code']; ?>" rows="3" name="Code" placeholder="Kode " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">SOW </label>
                                <div class="col-sm-7">
                                    <input class="form-control" rows="3" value="<?php echo $b['SOWDesc']; ?>" name="SOWDesc" placeholder="SOW " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Unit</label>
                                <div class="col-sm-5">
                                    <select name="Unit" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refunit->result_array() as $i) {
                                            $KodeUnit = $i['KodeUnit'];
                                            $NamaUnit = $i['NamaUnit'];
                                            if ($b['SOWKodeUnit'] == $KodeUnit)
                                                echo "<option value='$KodeUnit' selected>$KodeUnit - $NamaUnit</option>";
                                            else
                                                echo "<option value='$KodeUnit'>$KodeUnit - $NamaUnit</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Term 1 (%) / (Rp) </label>
                                <div class="col-sm-2">
                                    <input class="form-control" onclick="disable()" id="name4" rows="3" value="<?php echo $b['PemFull']; ?>" name="PemFull" placeholder="% " required>
                                </div>
                                <div class="col-sm-5">
                                    <input class="form-control" onclick="disable2()" id="name1" rows="3" value="<?php echo $b['Pem1']; ?>" name="Pem1" placeholder="Rp " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Term 2 (%) / (Rp)</label>
                                <div class="col-sm-2">
                                    <input class="form-control" rows="3" id="name5" value="<?php echo $b['F7']; ?>" name="F7" placeholder="% " required>
                                </div>
                                <div class="col-sm-5">
                                    <input class="form-control" rows="3" id="name2" value="<?php echo $b['Pem2']; ?>" name="Pem2" placeholder="Rp " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Term 3 (%) / (Rp)</label>
                                <div class="col-sm-2">
                                    <input class="form-control" rows="3" id="name6" value="<?php echo $b['F8']; ?>" name="F8" placeholder="% " required>
                                </div>
                                <div class="col-sm-5">
                                    <input class="form-control" rows="3" id="name3" value="<?php echo $b['Pem3']; ?>" name="Pem3" placeholder="Rp " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Customer</label>
                                <div class="col-sm-7">
                                    <select name="Customer" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refprincipal->result_array() as $i) {
                                            $kodePrincipal = $i['kodePrincipal'];
                                            $NamaPrincipal = $i['NamaPrincipal'];
                                            if ($b['SOWKodeCustomer'] == $kodePrincipal)
                                                echo "<option value='$kodePrincipal' selected>$kodePrincipal - $NamaPrincipal</option>";
                                            else
                                                echo "<option value='$kodePrincipal'>$kodePrincipal - $NamaPrincipal</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Perusahaan</label>
                                <div class="col-sm-7">
                                    <select name="Perusahaan" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refperusahaan->result_array() as $i) {
                                            $KodePerusahaan = $i['KodePerusahaan'];
                                            $NamaPerusahaan = $i['NamaPerusahaan'];
                                            if ($b['SOWKodePerusahaan'] == $KodePerusahaan)
                                                echo "<option value='$KodePerusahaan' selected>$KodePerusahaan - $NamaPerusahaan</option>";
                                            else
                                                echo "<option value='$KodePerusahaan'>$KodePerusahaan - $NamaPerusahaan</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label"> Subcon</label>
                                <div class="col-sm-7">
                                    <select name="Subcon" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refsubcon->result_array() as $i) {
                                            $KodeSuppl = $i['KodeSuppl'];
                                            $Nama = $i['Nama'];
                                            if ($b['SOWKodeSubcon'] == $KodeSuppl)
                                                echo "<option value='$KodeSuppl' selected>$KodeSuppl - $Nama</option>";
                                            else
                                                echo "<option value='$KodeSuppl'>$KodeSuppl - $Nama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Kontrak </label>
                                <div class="col-sm-5">
                                    <input class="form-control" rows="3" value="<?php echo $b['NoKontrak']; ?>" name="NoKontrak" placeholder="No Kontrak  " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Tahun Kontrak </label>
                                <div class="col-sm-2">
                                    [1] <input class="form-control" rows="3" value="<?php echo $b['ThnKontrak1']; ?>" name="ThnKontrak1" placeholder="Tahun 1 " required>
                                </div>
                                <div class="col-sm-2">
                                    [2] <input class="form-control" rows="3" value="<?php echo $b['ThnKontrak2']; ?>" name="ThnKontrak2" placeholder="Tahun 2 " required>
                                </div>
                                <div class="col-sm-2">
                                    [3] <input class="form-control" rows="3" value="<?php echo $b['ThnKontrak3']; ?>" name="ThnKontrak3" placeholder="Tahun 3 " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Komentar</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="Keterangan" class="form-control" id="inputUserName" placeholder="Komentar" required> <?php echo $b['Komentar']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Regional</label>
                                <div class="col-sm-5">
                                    <select name="Regional" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($refregional->result_array() as $i) {
                                            $KODE = $i['KODE'];
                                            $NAMA = $i['NAMA'];
                                            if ($b['SOWKodeRegional'] == $KODE)
                                                echo "<option value='$KODE' selected>$KODE - $NAMA</option>";
                                            else
                                                echo "<option value='$KODE'>$KODE - $NAMA</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Harga </label>
                                <div class="col-sm-5">
                                    <input class="form-control" rows="3" value="<?php echo $b['HrgRegional']; ?>" name="HrgRegional" placeholder="Harga  " required>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" onclick="window.history.back();" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                        </form>
                    </div>
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
        <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>
        <!-- page script -->

        <script>
            function disable() {
                document.getElementById("name1").disabled = true;
                document.getElementById("name2").disabled = true;
                document.getElementById("name3").disabled = true;
            }

            function disable2() {
                document.getElementById("name4").disabled = true;
                document.getElementById("name5").disabled = true;
                document.getElementById("name6").disabled = true;
            }

            function enable() {
                document.getElementById("name").disabled = false;
            }
        </script>

        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Agenda berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>



</body>

</html>