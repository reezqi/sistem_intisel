<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo-intisel2.jpg'?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/AdminLTE.min.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.css'?>">
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.css'?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datepicker/datepicker3.css'?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/skins/_all-skins.min.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
  ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Site
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"> Site</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

          <div class="box">
            <div class="box-header">
              <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-user-plus"></span> Tambah Site</a>
              <a class="btn btn-primary btn-flat" href="<?php echo base_url("admin/regional/form"); ?>" ><span class="fa fa-user-plus"></span> Tambah Site Excel</a>
              <a class="btn btn-info btn-flat" href="<?php echo base_url("admin/regional/export"); ?>"><span class="fa fa-file-excel-o"> </span>  Export Site</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
                    <th>ID Site</th>
                    <th>No Ref</th>
                    <th>Reg Kode</th>
                    <th>Akun Site</th>
                    <th>Kode Site</th>
                    <th>Site Name </th>
                    <th>Kode Site 1</th>
                    <th>Site Name 1</th>
                    <th>Kode Site 2</th>
                    <th>Site Name 2</th>
                    <th>Kode Site 3</th>
                    <th>Site Name 3</th>
                    <th>Kode Site 4</th>
                    <th>Site Name 4</th>
                    <th>Kode Site 5</th>
                    <th>Site Name 5</th>
                    <th>Owner ID</th>
                    <th>Owner Nama</th>
                    <th>Shipment No</th>
                    <th>PO Line</th>
                    <th>WPID</th>
                    <th>COA</th>
                    <th>ID Shop List</th>
                    <th>ID Principal</th>
                    <th>ID Regional</th>
                    <th>No Contract</th>
                    <th>Tahun Contract 1</th>
                    <th>Tahun Contract 2</th>
                    <th>Tahun Contract 3</th>
                    <th>Tahun Contract 4</th>
                    <th>Tahun Contract 5</th>
                    <th>Comment</th>
                    <th>Keterangan</th>
                    <th style="text-align:center;">Operasi</th>
                </tr>
                </thead>
                <tbody>

                <?php

      if(count($RegCode)>0){
			  foreach ($RegCode as $data) {
          ?>

                      <tr>
                       <td> <?php echo $data->idSite ?></td>
                       <td><?php echo $data->noRef?></td>
                       <td><?php echo $data->RegCode?></td>
                       <td><?php echo $data->akunsite?></td>
                       <td><?php echo $data->codeSite?></td>
                       <td><?php echo $data->siteName?></td>
                       <td><?php echo $data->codeSite1?></td>
                       <td><?php echo $data->siteName1?></td>
                       <td><?php echo $data->codeSite2?></td>
                       <td><?php echo $data->siteName2?></td>
                       <td><?php echo $data->codeSite3?></td>
                       <td><?php echo $data->siteName3?></td>
                       <td><?php echo $data->codeSite4?></td>
                       <td><?php echo $data->siteName4?></td>
                       <td><?php echo $data->codeSite5?></td>
                       <td><?php echo $data->siteName5?></td>
                       <td><?php echo $data->ownerId?></td>
                       <td><?php echo $data->ownerName?></td>
                       <td><?php echo $data->shipmentNo?></td>
                       <td><?php echo $data->POLine?></td>
                       <td><?php echo $data->WPID?></td>
                       <td><?php echo $data->coa?></td>
                       <td><?php echo $data->idShopList?></td>
                       <td><?php echo $data->idPrincipal?></td>
                       <td><?php echo $data->idRegional?></td>
                       <td><?php echo $data->noContract?></td>
                       <td><?php echo $data->thnContract_1?></td>
                       <td><?php echo $data->thnContract_2?></td>
                       <td><?php echo $data->thnContract_3?></td>
                       <td><?php echo $data->thnContract_4?></td>
                       <td><?php echo $data->thnContract_5?></td>
                       <td><?php echo $data->comment?></td>
                       <td><?php echo $data->keterangan?></td>
                       <td style="text-align:right;">
                  <a style="padding: 6px 6px;" href="<?php echo base_url(). 'admin/project/update_project/'.$data->idSite;?>">
				            <span class="fa fa-pencil"></span>  
			            </a>
                  <a style="padding: 6px 6px;" href="<?php echo base_url(). 'admin/project/hapus_project/'.$data->idSite;?>">
				            <span class="fa fa-trash"></span>  
			            </a>
                  </td>
        </tr>
      <?php }} ?>
                </tbody>
              </table>
            </div></div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
  ?>
  

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datepicker/bootstrap-datepicker.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/timepicker/bootstrap-timepicker.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/daterangepicker/daterangepicker.js'?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });

    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker3').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('.datepicker4').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $(".timepicker").timepicker({
      showInputs: true
    });

  });
</script>
<?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Agenda berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
</body>
</html>
