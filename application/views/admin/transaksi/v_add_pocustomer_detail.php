<?php
include('database_connection.php');
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">


</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Tambah Detail PO Customer
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#"> PO Customer</a></li>
          <li class="active">Tambah Detail PO Customer</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="box box-default">
          <?php $b = $data1->row_array(); ?>
          <section class="invoice" style="padding-top:5px;">
            <!-- title row -->
            <div class="row">
              <div class="col-md-12 col-xs-12 bg-primary" style="margin: 0px; padding: 5px; border-radius: 5px;">
                <h4 class="page">
                  <div class="col-sm-3 invoice-col">
                    <b>No Dokumen :</b>
                    <p style="font-size:12px; margin-top:5px;"><?php echo $b['NoDokumen']; ?></p>
                  </div>
                  <div class="col-sm-3 invoice-col">
                    <b>No PO Customer :</b>
                    <p style="font-size:12px; margin-top:5px;"><?php echo $b['NoPo']; ?></p>
                  </div>
                  <div class="col-sm-3 invoice-col">
                    <b>Customer :</b><br>
                    <p style="font-size:12px; margin-top:5px;"><?php echo $b['KodeCustomer']; ?> - <?php echo $b['NamaCustomer']; ?></p>
                  </div>
                  <div class="col-sm-3 invoice-col">
                    <b>Tanggal Issued PO :</b><br>
                    <p style="font-size:12px; margin-top:5px;"><?php echo $b['TglPo']; ?></p>
                  </div>

                  <input type="hidden" id='NoDokumen' value="<?php echo $b['NoDokumen']; ?>" name="NoDokumen">
                  <input type="hidden" id='NoPo' value="<?php echo $b['NoPo']; ?>" name="NoPo">
                  <input type="hidden" id='TglPo' value="<?php echo $b['TglPo']; ?>" name="TglPo">
                  <input type="hidden" id='KodeCustomer' value="<?php echo $b['KodeCustomer']; ?>" name="KodeCustomer">
                  <input type="hidden" id='NamaCustomer' value="<?php echo $b['NamaCustomer']; ?>" name="NamaCustomer">
                  </h2>
              </div>
              <!-- /.col -->
              <br><br><br><br>
              <div class="col-md-12">
                <!-- Custom Tabs (Pulled to the right) -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs pull-right">
                    <li class="active"><a href="#tab_1-1" data-toggle="tab"><b>Site</b></a></li>
                    <li><a href="#tab_2-2" data-toggle="tab"><b>SOW</b></a></li>
                    <li><a href="#tab_3-2" data-toggle="tab"><b>Site & SOW</b></a></li>

                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_1-1">
                      <table id='TabelTransaksi' class="table table-sm table-striped table-hover">
                        <div class='row'>
                          <div class="form-group">
                            <div class="col-xs-3">
                              <label for="exampleInputEmail1">Regional</label>
                              <select class="form-control select2 input-sm" id='KodeRegional' name="Regional" style="width: 100%;" required>
                                <option value="">No Selected</option>
                                <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                                  echo "<option value='" . $i->KODE . "'>" . $i->NAMA . "</option>";
                                } ?>
                              </select>
                            </div>
                            <div class="col-xs-4">
                              <label for="exampleInputEmail1">Kode Site</label>
                              <input type="text" name="KodeSite" id='KodeSite' class="form-control input-sm" id="KodeSite" placeholder="Kode Site">
                            </div>
                            <div class="col-xs-5">
                              <label for="exampleInputEmail1">Nama Site</label>
                              <input name="NamaSite" id='NamaSite' class="form-control input-sm" placeholder="Nama Site" readonly>
                            </div>
                          </div>
                        </div>
                    </div>
                    <br>

                    <thead>
                      <tr>
                        <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary" style='width:35px;'>#</th>
                        <th class="bg-primary" style='width:210px;'>Kode SOW</th>
                        <th class="bg-primary">Nama SOW</th>
                        <th class="bg-primary" style='width:120px;'>Harga</th>
                        <th class="bg-primary" style='width:75px;'>Qty</th>
                        <th class="bg-primary" style='width:125px;'>Sub Total</th>
                        <th class="bg-primary" style='width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'><button id='BarisBaru' style='margin-right:10px;' class='btn btn-default btn-xs'><i class='fa fa-plus' style='color:green;'></i></button></th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                    </table>

                    <div class='alert alert-info TotalBayar'>
                      <h3 style="text-align:right; margin:0px;">Total : <span id='TotalBayar'>Rp. 0</span></h3>
                      <input type="hidden" id='TotalBayarHidden'>
                    </div>

                    <div align="center">
                      <input type="button" id='Simpann' name="submit" class="btn btn-primary pull-right btn-sm" value="Simpan" />
                      <input type="button" onclick="goBack()" name="submit" class="btn btn-default pull-right btn-sm" value="Kembali" style="margin-right:10px" />
                    </div>

                    <br><br>
                  </div>

                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2-2">
                    <div class="row">

                      <div class="col-md-6">
                        <!-- general form elements -->
                        <div class="box box-primary">
                          <!-- form start -->
                          <form role="form">
                            <div class="box-body">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Regional</label>
                                <select class="form-control select2 input-sm" id='KodeRegional' name="Regional" style="width: 100%;" required>
                                  <option value="">No Selected</option>
                                  <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                                    echo "<option value='" . $i->KODE . "'>" . $i->NAMA . "</option>";
                                  } ?>
                                </select>
                              </div>

                              <div class="form-group">
                                <label for="exampleInputPassword1">Kode SOW</label>
                                <input type="text" name="KodeSite" id='KodeSOW' class="form-control input-sm" id="KodeSOW" placeholder="Kode SOW">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputFile">Nama SOW</label>
                                <input name="NamaSOW" id='NamaSOW' class="form-control input-sm" placeholder="Nama SOW" readonly>
                              </div>
                              <div class="form-group">
                                <div class="col-sm-5">
                                  <label for="exampleInputFile">Harga</label>
                                  <input name="HrgRegional" id='Qty' class="form-control input-sm" placeholder="Harga" readonly>
                                </div>
                                <div class="col-sm-2">
                                  <label for="exampleInputFile">Qty</label>
                                  <input name="Qty" id='Qty' class="form-control input-sm" placeholder="Qty" required>
                                </div>
                                <div class="col-sm-5">
                                  <label for="exampleInputFile">Total</label>
                                  <input name="Total" id='Total' class="form-control input-sm" placeholder="Total" readonly>
                                </div>
                              </div>
                            </div>
                            <!-- /.box-body -->
                          </form>
                        </div>
                        <!-- /.box -->
                      </div>

                      <!-- right column -->
                      <div class="col-md-6">
                        <!-- Horizontal Form -->
                        <div class="box box-info">
                          <!-- form start -->
                          <form class="form-horizontal">
                            <div class="box-body">
                              <table id='TabelTransaksi2' class="table table-sm table-striped table-hover">
                                <thead>
                                  <tr>
                                    <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary" style='width:35px;'>#</th>
                                    <th class="bg-primary" style='width:210px;'>Kode Site</th>
                                    <th class="bg-primary">Nama Site</th>
                                    <th class="bg-primary" style='width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'><button id='BarisBaru2' style='margin-right:10px;' class='btn btn-default btn-xs'><i class='fa fa-plus' style='color:green;'></i></button></th>
                                  </tr>
                                </thead>
                                <tbody></tbody>
                              </table>
                            </div>
                          </form>
                        </div>
                        <!-- /.box -->

                      </div>
                      <!--/.col (right) -->
                    </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3-2">
                    <table id='#' class="table table-sm table-striped table-hover">

                      <div class="box-body">
                        <div class="form-group">
                          <label for="inputUserName" class="col-sm-2 control-label"> Regional</label>
                          <div class="col-sm-3">
                            <select class="form-control select2 input-sm" id='KodeRegional' name="Regional" style="width: 100%;" required>
                              <option value="">No Selected</option>
                              <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                                echo "<option value='" . $i->KODE . "'>" . $i->NAMA . "</option>";
                              } ?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <thead>
                        <tr>
                          <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary" style='width:35px;'>#</th>
                          <th class="bg-primary">Kode Site</th>
                          <th class="bg-primary">Nama Site</th>
                          <th class="bg-primary">Kode SOW</th>
                          <th class="bg-primary">Nama SOW</th>
                          <th class="bg-primary">Harga</th>
                          <th class="bg-primary" style='width:10px;'>Qty</th>
                          <th class="bg-primary">Sub Total</th>
                          <th class="bg-primary" style='width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'><button id='BarisBaru' style='margin-right:10px;' class='btn btn-default btn-xs'><i class='fa fa-plus' style='color:green;'></i></button></th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>

                    <div class='alert alert-info TotalBayar'>
                      <h3 style="text-align:right; margin:0px;">Total : <span id='TotalBayar'>Rp. 0</span></h3>
                      <input type="hidden" id='TotalBayarHidden'>
                    </div>

                    <div align="center">
                      <input type="button" id='Simpann' name="submit" class="btn btn-primary pull-right btn-sm" value="Simpan" />
                      <input type="button" onclick="goBack()" name="submit" class="btn btn-default pull-right btn-sm" value="Kembali" style="margin-right:10px" />
                    </div>
                  </div>
                  <br><br>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- nav-tabs-custom -->
            </div>
          </section>
        </div>
      </section>
      <!-- /.content -->

      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!--Modal Add -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content" style=" border-radius: 5px;">
          <form class="form-horizontal" id="SimpanTransaksi" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background-color:#337AB7; border-top-left-radius: 5px; border-top-right-radius: 5px;">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Konfirmasi</h4>
            </div>
            <div class="modal-body">
              <p> Apakah anda yakin ingin menyimpan transaksi ini ?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default " data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary ">Ya, saya yakin</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Add -->

    <!--Modal Add -->
    <div class="modal fade" id="myModalKosong" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content" style=" border-radius: 5px;">
          <form class="form-horizontal" id="a" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background-color:#337AB7; border-top-left-radius: 5px; border-top-right-radius: 5px;">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Konfirmasi</h4>
            </div>
            <div class="modal-body">
              <p> Harus Diisi</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default " data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary ">Ya, saya yakin</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Add -->

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

    <script type="text/javascript">
      $(document).ready(function() {

        $('#KodeSite').autocomplete({
          source: "<?php echo site_url('admin/site/get_autocomplete'); ?>",

          select: function(event, ui) {
            $('[name="KodeSite"]').val(ui.item.label);
            $('[name="NamaSite"]').val(ui.item.label);
          }
        });
      });
    </script>

    <script>
      function goBack() {
        window.history.back();
      }

      $(document).ready(function() {

        for (B = 1; B <= 1; B++) {
          BarisBaru();
        }

        $('#BarisBaru').click(function() {
          BarisBaru();
        });

        $("#TabelTransaksi tbody").find('input[type=text],textarea,select').filter(':visible:first').focus();
      });

      function BarisBaru() {
        var Nomor = $('#TabelTransaksi tbody tr').length + 1;
        var Baris = "<tr>";
        Baris += "<td style='margin:-5px;'>" + Nomor + "</td>";

        Baris += "<td>";
        Baris += "<input type='text' class='form-control input-sm' style='margin:-5px;' name='kode_barang[]' id='pencarian_kode' placeholder='Ketik Kode / Nama SOW'>";
        Baris += "<div id='hasil_pencarian'></div>";
        Baris += "</td>";

        Baris += "<td></td>";
        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='harga_satuan[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td><input type='text' style='margin:-5px;' class='form-control input-sm' id='jumlah_beli' name='jumlah_beli[]' onkeypress='return check_int(event)' disabled></td>";

        Baris += "<td>";
        Baris += "<input type='hidden' style='margin:-5px;' name='sub_total[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td><button style='margin-left:0px;' class='btn btn-default btn-xs' id='HapusBaris'><i class='fa fa-times' style='color:red;'></i></button></td>";
        Baris += "</tr>";

        $('#TabelTransaksi tbody').append(Baris);

        $('#TabelTransaksi tbody tr').each(function() {
          $(this).find('td:nth-child(2) input').focus();
        });

        HitungTotalBayar();
      }

      $(document).on('click', '#HapusBaris', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();

        var Nomor = 1;
        $('#TabelTransaksi tbody tr').each(function() {
          $(this).find('td:nth-child(1)').html(Nomor);
          Nomor++;
        });

        HitungTotalBayar();
      });


      function AutoCompleteGue(Lebar, KataKunci, Indexnya) {
        $('div#hasil_pencarian').hide();
        var Lebar = Lebar + 25;

        var Registered = '';
        $('#TabelTransaksi tbody tr').each(function() {
          if (Indexnya !== $(this).index()) {
            if ($(this).find('td:nth-child(2) input').val() !== '') {
              Registered += $(this).find('td:nth-child(2) input').val() + ',';
            }
          }
        });

        if (Registered !== '') {
          Registered = Registered.replace(/,\s*$/, "");
        }

        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/ajax_kode'); ?>",
          type: "POST",
          cache: false,
          data: 'keyword=' + KataKunci + '&registered=' + Registered,
          dataType: 'json',
          success: function(json) {
            if (json.status == 1) {
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').css({
                'width': Lebar + 'px'
              });
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').show('fast');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').html(json.datanya);
            }
            if (json.status == 0) {
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3)').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').prop('disabled', true).val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) input').val(0);
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) span').html('');
            }
          }
        });

        HitungTotalBayar();
      }

      $(document).on('keyup', '#pencarian_kode', function(e) {
        if ($(this).val() !== '') {
          var charCode = e.which || e.keyCode;
          if (charCode == 40) {
            if ($('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
              var Selanjutnya = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').next();
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

              Selanjutnya.addClass('autocomplete_active');
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
            }
          } else if (charCode == 38) {
            if ($('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
              var Sebelumnya = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').prev();
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

              Sebelumnya.addClass('autocomplete_active');
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
            }
          } else if (charCode == 13) {
            var Field = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)');
            var Kodenya = Field.find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
            var Barangnya = Field.find('div#hasil_pencarian li.autocomplete_active span#barangnya').html();
            var Harganya = Field.find('div#hasil_pencarian li.autocomplete_active span#harganya').html();

            Field.find('div#hasil_pencarian').hide();
            Field.find('input').val(Kodenya);

            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(3)').html(Barangnya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(4) input').val(Harganya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(4) span').html(to_rupiah(Harganya));
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(5) input').removeAttr('disabled').val(1);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(6) input').val(Harganya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(6) span').html(to_rupiah(Harganya));

            var IndexIni = $(this).parent().parent().index() + 1;
            var TotalIndex = $('#TabelTransaksi tbody tr').length;
            if (IndexIni == TotalIndex) {
              BarisBaru();

              $('html, body').animate({
                scrollTop: $(document).height()
              }, 0);
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(5) input').focus();
            }
          } else {
            AutoCompleteGue($(this).width(), $(this).val(), $(this).parent().parent().index());
          }
        } else {
          $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
        }

        HitungTotalBayar();
      });

      $(document).on('click', '#daftar-autocomplete li', function() {
        $(this).parent().parent().parent().find('input').val($(this).find('span#kodenya').html());

        var Indexnya = $(this).parent().parent().parent().parent().index();
        var NamaBarang = $(this).find('span#barangnya').html();
        var Harganya = $(this).find('span#harganya').html();

        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) ').html(NamaBarang);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val(Harganya);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) span').html(to_rupiah(Harganya));
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').removeAttr('disabled').val(1);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) input').val(Harganya);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) span').html(to_rupiah(Harganya));

        var IndexIni = Indexnya + 1;
        var TotalIndex = $('#TabelTransaksi tbody tr').length;
        if (IndexIni == TotalIndex) {
          BarisBaru();
          $('html, body').animate({
            scrollTop: $(document).height()
          }, 0);
        } else {
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').focus();
        }

        HitungTotalBayar();
      });

      $(document).on('keyup', '#jumlah_beli', function() {
        var Indexnya = $(this).parent().parent().index();
        var Harga = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val();
        var JumlahBeli = $(this).val();
        var KodeBarang = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2) input').val();


        var SubTotal = parseInt(Harga) * parseInt(JumlahBeli);
        if (SubTotal > 0) {
          var SubTotalVal = SubTotal;
          SubTotal = to_rupiah(SubTotal);
        } else {
          SubTotal = '';
          var SubTotalVal = 0;
        }

        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) input').val(SubTotalVal);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) span').html(SubTotal);
        HitungTotalBayar();

      });


      $(document).on('keydown', '#jumlah_beli', function(e) {
        var charCode = e.which || e.keyCode;
        if (charCode == 9) {
          var Indexnya = $(this).parent().parent().index() + 1;
          var TotalIndex = $('#TabelTransaksi tbody tr').length;
          if (Indexnya == TotalIndex) {
            BarisBaru();
            return false;
          }
        }

        HitungTotalBayar();
      });

      $(document).on('keyup', '#UangCash', function() {
        HitungTotalKembalian();
      });

      function HitungTotalBayar() {
        var Total = 0;
        $('#TabelTransaksi tbody tr').each(function() {
          if ($(this).find('td:nth-child(6) input').val() > 0) {
            var SubTotal = $(this).find('td:nth-child(6) input').val();
            Total = parseInt(Total) + parseInt(SubTotal);
          }
        });

        $('#TotalBayar').html(to_rupiah(Total));
        $('#TotalBayarHidden').val(Total);

        $('#UangCash').val('');
        $('#UangKembali').val('');
      }


      function to_rupiah(angka) {
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
          rev2 += rev[i];
          if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
            rev2 += '.';
          }
        }
        return 'Rp. ' + rev2.split('').reverse().join('');
      }

      function check_int(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        return (charCode >= 48 && charCode <= 57 || charCode == 8);
      }

      $(document).on('keydown', 'body', function(e) {
        var charCode = (e.which) ? e.which : event.keyCode;

      });

      $(document).on('click', '#Simpann', function() {
        $('#myModal').modal('show');
      });

      $('#SimpanTransaksi').on('submit', function(event) {
        var NoDokumen = $("#NoDokumen").val();

        var FormData = "NoDokumen=" + encodeURI($('#NoDokumen').val());
        FormData += "&NoPo=" + encodeURI($('#NoPo').val());
        FormData += "&TglPo=" + encodeURI($('#TglPo').val());
        FormData += "&KodeCustomer=" + encodeURI($('#KodeCustomer').val());
        FormData += "&NamaCustomer=" + encodeURI($('#NamaCustomer').val());
        FormData += "&KodeRegional=" + encodeURI($('#KodeRegional').val());
        FormData += "&KodeSite=" + encodeURI($('#KodeSite').val());
        FormData += "&NamaSite=" + encodeURI($('#NamaSite').val());
        FormData += "&" + $('#TabelTransaksi tbody input').serialize();
        FormData += "&grand_total=" + $('#TotalBayarHidden').val();

        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/simpan_pocustomer_detail'); ?>",
          type: "POST",
          cache: false,
          data: FormData,
          dataType: 'json',
          success: function(data) {
            if (data.status == 1) {
              window.location.href = "<?php echo site_url('admin/pocustomer/view_pocustomer_detail/'); ?>" + data.nodokumen;
              $.toast({
                heading: 'Success',
                text: "PO Customer Detail Berhasil disimpan ke database.",
                showHideTransition: 'slide',
                icon: 'success',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#7EC857'
              });
            } else {
              $.toast({
                heading: 'Error',
                text: "Ukuran dokumen terlalu besar ",
                showHideTransition: 'slide',
                icon: 'error',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#00C9E6'
              });

            }
          }
        });
      });
    </script>

    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PO Subcon berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Ukuran dokumen terlalu besar ",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>
</body>

</html>