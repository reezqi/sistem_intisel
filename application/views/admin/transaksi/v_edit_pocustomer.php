<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Entri Purchase Order Customer
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#">Purchase Order Customer</a></li>
          <li class="active"> Entri Purchase Order Customer</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <!-- /.box-header -->
          <div class="box-body">
            <table id='TabelTransaksi' class="table table-sm table-striped table-hover">
              <div class="row">

                <div class="col-md-3">
                  <div class="form-group">
                    <label>No PO Internal</label>
                    <input type="hidden" name="NoUrutPo" class="form-control input-sm" value="<?php echo $NoUrutPo; ?>" id="NoUrutPo">
                    <input type="text" name="NoPoIn" class="form-control input-sm" id="NoPoIn" placeholder="No PO Internal" readonly>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <p class="form-group">
                    <label> No PR </label>
                    <select class="form-control select2 input-sm" name="NoPr" style="width: 100%;" onchange="changeValue(this.value)" required>
                      <option value="">No Selected</option>

                      <?php
                      $host = "202.78.195.194";
                      $user = "usersakti";
                      $pass = "u5ers4kti";
                      $db = "dbinvoice";
                      $con = mysqli_connect($host, $user, $pass, $db);

                      //$dataKoDE = "<p id='NoKodeCustomer'></p>";
                      $dataKoDE = "<script language=javascript>document.write(NoKodeCustomer);</script>";

                      $sql = mysqli_query($con, "SELECT * FROM trxprregionald GROUP BY NoPr");
                      $jsArray = "var prdName = new Array();\n";
                      while ($data = mysqli_fetch_array($sql)) {

                        echo '<option value="' . $data['NoPr'] . '">' . $data['NoPr'] . '</option> ';
                        $jsArray .= "prdName['" . $data['NoPr'] . "'] = {KodeSite:'" . addslashes($data['KodeSite']) . "',NamaSite:'" . addslashes($data['NamaSite']) . "',KodeRegional:'" . addslashes($data['KodeRegional']) . "',KodeSOW:'" . addslashes($data['KodeSOW']) . "',NamaSOW:'" . addslashes($data['NamaSOW']) . "',HargaSOW:'" . addslashes($data['HargaSOW']) . "'};\n";
                      }
                      ?>
                    </select>
                </div>
                <!-- /.form-group -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label> Customer </label>
                    <select class="form-control select2 input-sm" name="KodeCustomer" id="KodeCustomer" style="width: 100%;" required>
                      <option value="">No Selected</option>
                      <?php foreach ($refprincipal as $i) { // Lakukan looping pada variabel siswa dari controller
                        echo "<option value='" . $i->kodePrincipal . "'>" . $i->kodePrincipal . " - " . $i->NamaPrincipal . "</option>";
                      } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>No PO Customer</label>
                    <input type="text" name="NoPo" class="form-control input-sm" id="NoPo" placeholder="No PO Customer" required>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>No Dokumen</label>
                    <input type="text" name="NoDokumen" class="form-control input-sm" id="NoDokumen" value="<?php echo $NoDokumen; ?>" readonly>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Tanggal PO</label>
                    <input class="form-control input-sm" value="<?php echo date('Y-m-d'); ?>" type="text" id="datepicker3" name="datepicker3" placeholder="Tanggal PO ">
                  </div>
                  <!-- /.form-group -->

                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Regional</label>
                    <select onchange="NoPoInternal()" class="form-control input-sm" name="KodeRegional" id='KodeRegional' style="width: 100%;">
                      <option value="">No Selected</option>
                      <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                        echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                      } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Kode Site </label>
                    <input type="text" name="KodeSite" class="form-control input-sm" id="KodeSite" placeholder="Kode Site">
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Nama Site</label>
                    <input name="NamaSite" class="form-control input-sm" id='NamaSite' placeholder="Nama Site" readonly>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
          </div>
          <!-- /.row -->

          <div class="row">
            <!-- /.row -->
            <br>
            <div class="col-xs-12">
              <div class="row">
                <thead>
                  <tr>
                    <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary" style='width:20px;'>No</th>
                    <th class="bg-primary" style='width:180px;'>Kode SOW</th>
                    <th class="bg-primary">Nama SOW</th>
                    <th class="bg-primary" style='width:100px;'>Harga</th>
                    <th class="bg-primary" style='width:50px;'>Qty</th>
                    <th class="bg-primary" style='width:20px;'>PPN</th>
                    <th class="bg-primary" style='width:20px;'>PPH</th>
                    <th class="bg-primary" style='width:100px;'>Sub Total</th>
                    <th class="bg-primary" style='width:100px;'>Nilai PPN</th>
                    <th class="bg-primary" style='width:100px;'>Nilai PPH</th>
                    <th class="bg-primary" style='width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'><button id='BarisBaru' style='margin-right:10px;' class='btn btn-default btn-xs'><i class='fa fa-plus' style='color:green;'></i></button></th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>

                <div class='alert alert-info TotalBayar' style="padding:5px;">
                  <h3 style="text-align:right; margin:0px;">Total : <span id='TotalBayar'>Rp. 0</span></h3>
                  <input type="hidden" id='TotalBayarHidden'>
                </div>

              </div>

              <div class="modal-footer">
                <input type="button" id='Simpann' name="submit" class="btn btn-primary pull-right btn-sm" value="Simpan" />
                <input type="button" onclick="goBack()" name="submit" class="btn btn-default pull-right btn-sm" value="Kembali" style="margin-right:10px" />
              </div>
            </div>

      </section>
      <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!--Modal Add -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content" style=" border-radius: 5px;">
          <form class="form-horizontal" id="SimpanTransaksi" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background-color:#337AB7; border-top-left-radius: 5px; border-top-right-radius: 5px;">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Konfirmasi</h4>
            </div>
            <div class="modal-body">
              <p> Apakah anda yakin ingin menyimpan transaksi ini?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default " data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary ">Ya, saya yakin</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Add -->


    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

    <script type="text/javascript">
      $(document).ready(function() {

        $('#KodeSite').autocomplete({
          source: "<?php echo site_url('admin/site/get_autocomplete'); ?>",

          select: function(event, ui) {
            $('[name="KodeSite"]').val(ui.item.label);
            $('[name="NamaSite"]').val(ui.item.description);
          }
        });
      });
    </script>

    <script type="text/javascript">
      <?php echo $jsArray; ?>

      function changeValue(x) {
        document.getElementById('KodeSite').value = prdName[x].KodeSite;
        document.getElementById('NamaSite').value = prdName[x].NamaSite;
        document.getElementById('KodeRegional').value = prdName[x].KodeRegional;
        document.getElementById('pencarian_kode').value = prdName[x].KodeSOW;
        document.getElementById('nama_barang').value = prdName[x].KodeSOW;
        document.getElementById('harga_satuan').value = prdName[x].KodeSOW;
      };
    </script>

    <script>
      function NoPoInternal() {
        var StrNoUrutPo = document.getElementById("NoUrutPo").value;
        var StrKodeCustomer = document.getElementById("KodeCustomer").value;
        var Strdatepicker3 = document.getElementById("datepicker3").value;
        var StrKodeRegional = document.getElementById("KodeRegional").value;
        var StrNoPoIn = "PO/" + StrKodeCustomer + "/INTISEL/" + StrKodeRegional + "/" + Strdatepicker3 + "/" + StrNoUrutPo;

        document.getElementById("NoPoIn").value = StrNoPoIn;
      }

      function goBack() {
        window.history.back();
      }

      $(document).ready(function() {
        get_transaksi()

        for (B = 1; B <= 1; B++) {
          BarisBaru();
        }

        $('#BarisBaru').click(function() {
          BarisBaru();
        });

        $("#TabelTransaksi tbody").find('input[type=text],textarea,select').filter(':visible:first').focus();
      });

      function BarisBaru() {
        var Nomor = $('#TabelTransaksi tbody tr').length + 1;
        var Baris = "<tr>";
        Baris += "<td style='margin:-5px; width:35px;'>" + Nomor + "</td>";

        Baris += "<td>";
        Baris += "<input type='text' class='form-control input-sm' style='margin:-5px;' name='kode_barang[]' id='pencarian_kode' placeholder='Ketik Kode / Nama SOW'>";
        Baris += "<div id='hasil_pencarian'></div>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='nama_barang[]' id='nama_barang'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='harga_satuan[]' id='harga_satuan'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td><input type='text' style='margin:-5px;' class='form-control input-sm' id='jumlah_beli' name='jumlah_beli[]' onkeypress='return check_int(event)' disabled></td>";
        Baris += "<td><input type='checkbox' style='margin:-5px; margin-left:5px;' id='jumlah_beli1' name='jumlah_beli1[]' onkeypress='return check_int(event)' disabled></input></td>";
        Baris += "<td><input type='checkbox' style='margin:-5px; margin-left:5px;' id='jumlah_beli2' name='jumlah_beli2[]' onkeypress='return check_int(event)' disabled></input></td>";

        Baris += "<td>";
        Baris += "<input type='hidden' style='margin:-5px;' name='sub_total[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input type='hidden' style='margin:-5px;' name='nilai_ppn[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input type='hidden' style='margin:-5px;' name='nilai_pph[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td><button style='margin-left:0px;' class='btn btn-default btn-xs' id='HapusBaris'><i class='fa fa-times' style='color:red;'></i></button></td>";
        Baris += "</tr>";

        $('#TabelTransaksi tbody').append(Baris);

        $('#TabelTransaksi tbody tr').each(function() {
          $(this).find('td:nth-child(2) input').focus();
        });

        HitungTotalBayar();
      }

      $(document).on('click', '#HapusBaris', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();

        var Nomor = 1;
        $('#TabelTransaksi tbody tr').each(function() {
          $(this).find('td:nth-child(1)').html(Nomor);
          Nomor++;
        });

        HitungTotalBayar();
      });


      function AutoCompleteGue(Lebar, KataKunci, Indexnya) {
        $('div#hasil_pencarian').hide();
        var Lebar = Lebar + 25;

        var Registered = '';
        $('#TabelTransaksi tbody tr').each(function() {
          if (Indexnya !== $(this).index()) {
            if ($(this).find('td:nth-child(2) input').val() !== '') {
              Registered += $(this).find('td:nth-child(2) input').val() + ',';
            }
          }
        });

        if (Registered !== '') {
          Registered = Registered.replace(/,\s*$/, "");
        }

        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/ajax_kode'); ?>",
          type: "POST",
          cache: false,
          data: 'keyword=' + KataKunci + '&registered=' + Registered,
          dataType: 'json',
          success: function(json) {
            if (json.status == 1) {
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').css({
                'width': Lebar + 'px'
              });
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').show('fast');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').html(json.datanya);
            }
            if (json.status == 0) {
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) input').val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').prop('disabled', true).val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) input').prop('disabled', true).val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(7) input').prop('disabled', true).val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val(0);
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(0);
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(0);
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html('');
            }
          }
        });

        HitungTotalBayar();
      }

      $(document).on('keyup', '#pencarian_kode', function(e) {
        if ($(this).val() !== '') {
          var charCode = e.which || e.keyCode;
          if (charCode == 40) {
            if ($('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
              var Selanjutnya = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').next();
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

              Selanjutnya.addClass('autocomplete_active');
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
            }
          } else if (charCode == 38) {
            if ($('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
              var Sebelumnya = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').prev();
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

              Sebelumnya.addClass('autocomplete_active');
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
            }
          } else if (charCode == 13) {
            var Field = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)');
            var Kodenya = Field.find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
            var Barangnya = Field.find('div#hasil_pencarian li.autocomplete_active span#barangnya').html();
            var Harganya = Field.find('div#hasil_pencarian li.autocomplete_active span#harganya').html();

            Field.find('div#hasil_pencarian').hide();
            Field.find('input').val(Kodenya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(3) input').val(Barangnya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(3) span').html(Barangnya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(4) input').val(Harganya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(4) span').html(to_rupiah(Harganya));
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(5) input').removeAttr('disabled').val(1);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(6) input').removeAttr('disabled').val(1);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(7) input').removeAttr('disabled').val(1);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(8) input').val(Harganya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(8) span').html(to_rupiah(Harganya));
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(9) input').val(0);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(9) span').html(to_rupiah(0));
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(10) input').val(0);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(10) span').html(to_rupiah(0));


            var IndexIni = $(this).parent().parent().index() + 1;
            var TotalIndex = $('#TabelTransaksi tbody tr').length;
            if (IndexIni == TotalIndex) {
              BarisBaru();

              $('html, body').animate({
                scrollTop: $(document).height()
              }, 0);
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(5) input').focus();
            }
          } else {
            AutoCompleteGue($(this).width(), $(this).val(), $(this).parent().parent().index());
          }
        } else {
          $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
        }

        HitungTotalBayar();
      });

      $(document).on('click', '#daftar-autocomplete li', function() {
        $(this).parent().parent().parent().find('input').val($(this).find('span#kodenya').html());

        var Indexnya = $(this).parent().parent().parent().parent().index();
        var NamaBarang = $(this).find('span#barangnya').html();
        var Harganya = $(this).find('span#harganya').html();

        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) input').val(NamaBarang);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) span').html(NamaBarang);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val(Harganya);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) span').html(to_rupiah(Harganya));
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').removeAttr('disabled').val(1);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) input').removeAttr('disabled').val(1);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(7) input').removeAttr('disabled').val(1);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val(Harganya);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) span').html(to_rupiah(Harganya));
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(0);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html(to_rupiah(0));
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(0);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html(to_rupiah(0));

        var IndexIni = Indexnya + 1;
        var TotalIndex = $('#TabelTransaksi tbody tr').length;
        if (IndexIni == TotalIndex) {
          BarisBaru();
          $('html, body').animate({
            scrollTop: $(document).height()
          }, 0);
        } else {
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').focus();
        }

        HitungTotalBayar();
      });


      $(document).on('keyup', '#jumlah_beli', function() {
        var Indexnya = $(this).parent().parent().index();
        var Harga = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val();
        var JumlahBeli = $(this).val();
        var KodeBarang = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2) input').val();


        var SubTotal = parseInt(Harga) * parseInt(JumlahBeli);

        if (SubTotal > 0) {
          var SubTotalVal = SubTotal;
          SubTotal = to_rupiah(SubTotal);
        } else {
          SubTotal = '';
          var SubTotalVal = 0;
        }

        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val(SubTotalVal);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) span').html(SubTotal);
        HitungTotalBayar();

      });


      $(document).on('click', '#jumlah_beli1', function() {
        if ($(this).prop('checked') == true) {
          var Indexnya = $(this).parent().parent().index();
          var Harga = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val();
          var JumlahBeli = $(this).val();
          var KodeBarang = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val();

          var SubTotal = (parseInt(KodeBarang) / 100) * 10;

          if (SubTotal > 0) {
            var SubTotalVal = SubTotal;
            SubTotal = to_rupiah(SubTotal);
          } else {
            SubTotal = '';
            var SubTotalVal = 0;
          }

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(SubTotalVal);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html(SubTotal);
          HitungTotalBayar();
        } else if ($(this).prop('checked') == false) {
          var Indexnya = $(this).parent().parent().index();

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(0);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html(to_rupiah(0));
          HitungTotalBayar();
        }
      });

      $(document).on('click', '#jumlah_beli2', function() {
        if ($(this).prop('checked') == true) {
          var Indexnya = $(this).parent().parent().index();
          var Harga = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val();
          var JumlahBeli = $(this).val();
          var KodeBarang = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val();
          var KodeBarang1 = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val();

          var SubTotal = ((parseInt(KodeBarang) + parseInt(KodeBarang1)) / 100) * 2;

          if (SubTotal > 0) {
            var SubTotalVal = SubTotal;
            SubTotal = to_rupiah(SubTotal);
          } else {
            SubTotal = '';
            var SubTotalVal = 0;
          }

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(SubTotalVal);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html(SubTotal);
          HitungTotalBayar();
        } else if ($(this).prop('checked') == false) {
          var Indexnya = $(this).parent().parent().index();

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(0);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html(to_rupiah(0));
          HitungTotalBayar();
        }
      });


      $(document).on('keydown', '#jumlah_beli', function(e) {
        var charCode = e.which || e.keyCode;
        if (charCode == 9) {
          var Indexnya = $(this).parent().parent().index() + 1;
          var TotalIndex = $('#TabelTransaksi tbody tr').length;
          if (Indexnya == TotalIndex) {
            BarisBaru();
            return false;
          }
        }

        HitungTotalBayar();
      });

      $(document).on('keyup', '#UangCash', function() {
        HitungTotalKembalian();
      });

      function HitungTotalBayar() {
        var Total = 0;
        $('#TabelTransaksi tbody tr').each(function() {
          if ($(this).find('td:nth-child(8) input').val() > 0) {
            var SubTotal = $(this).find('td:nth-child(8) input').val();
            var PPN = $(this).find('td:nth-child(9) input').val();
            var PPH = $(this).find('td:nth-child(10) input').val();

            Total = parseInt(Total) + parseInt(SubTotal) + parseInt(PPN) - parseInt(PPH);
          }
        });

        $('#TotalBayar').html(to_rupiah(Total));
        $('#TotalBayarHidden').val(Total);
      }

      function to_rupiah(angka) {
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
          rev2 += rev[i];
          if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
            rev2 += '.';
          }
        }
        return 'Rp. ' + rev2.split('').reverse().join('');
      }

      function check_int(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        return (charCode >= 48 && charCode <= 57 || charCode == 8);
      }

      $(document).on('keydown', 'body', function(e) {
        var charCode = (e.which) ? e.which : event.keyCode;

      });

      $(document).on('click', '#Simpann', function() {
        $('#myModal').modal('show');
      });

      $('#SimpanTransaksi').on('submit', function(event) {
        var NoDokumen = $("#NoDokumen").val();

        var FormData = "NoDokumen=" + encodeURI($('#NoDokumen').val());
        FormData += "&NoPoIn=" + encodeURI($('#NoPoIn').val());
        FormData += "&NoPo=" + encodeURI($('#NoPo').val());
        FormData += "&datepicker3=" + encodeURI($('#datepicker3').val());
        FormData += "&KodeCustomer=" + encodeURI($('#KodeCustomer').val());
        FormData += "&KodeRegional=" + encodeURI($('#KodeRegional').val());
        FormData += "&KodeSite=" + encodeURI($('#KodeSite').val());
        FormData += "&NamaSite=" + encodeURI($('#NamaSite').val());
        FormData += "&" + $('#TabelTransaksi tbody input').serialize();
        FormData += "&grand_total=" + $('#TotalBayarHidden').val();

        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/save_pocustomer'); ?>",
          type: "POST",
          cache: false,
          data: FormData,
          dataType: 'json',
          success: function(data) {
            if (data.status == 1) {
              window.location.href = "<?php echo site_url('admin/pocustomer'); ?>"
              $.toast({
                heading: 'Success',
                text: "PO Customer Detail Berhasil disimpan ke database.",
                showHideTransition: 'slide',
                icon: 'success',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#7EC857'
              });
            } else {
              $.toast({
                heading: 'Error',
                text: "Ukuran Dokumen Terlalu Besar ",
                showHideTransition: 'slide',
                icon: 'error',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#00C9E6'
              });

            }
          }
        });
      });

      function get_transaksi() {
        //get master transaksi dan detail transaksi
        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/ajax_detail_transaksi'); ?>",
          type: "POST",
          data: {
            NoDokumen: $('#NoDokumen').val(),
          },
          cache: false,
          dataType: 'json',
          success: function(json) {
            console.log(json)
            $('#NoDokumen').val(json.master[0]['NoDokumen'])




            //tampilkan detail pembelian
            for (var i = 0; i < json.detail.length; i++) {
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(2)').find('div#hasil_pencarian').hide()
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(2) input').val(json.detail[i]['kode_barang'])
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(3)').html(json.detail[i]['nama_barang'])


              BarisBaru()
            }

            HitungTotalBayar()

            $('#UangCash').val(json.master[0]['bayar'])
            HitungTotalKembalian()
          }
        });
      }
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker3').datepicker({
          dateFormat: 'yy-mm-dd',
          changeMonth: true,
          changeYear: true
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Customer Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PO Customer berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Customer Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>

</body>

</html>