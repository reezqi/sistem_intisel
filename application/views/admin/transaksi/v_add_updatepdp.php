<?php
include('database_connection.php');
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Update PDP
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#">Update PDP</a></li>
          <li class="active">Update PDP</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">

          <?php $b = $data1->row_array(); ?>
          <form method="post" id="insert_form" class="form-horizontal" enctype="multipart/form-data">
            <!-- Main content -->
            <section class="invoice">
              <!-- title row -->
              <div class="row">
                <div class="col-md-12 col-xs-12" style="background-color:#3C8DBC;">
                  <h4 class="page">
                    <b>No PO: </b><?php echo $b['NoPo']; ?>
                    <small class="pull-right" style="color:black;">Tanggal PO: <b><?php echo $b['TglPo']; ?></b></small>
                    <input type="hidden" value="<?php echo $b['NoPo']; ?>" name="NoPo">
                    <input type="hidden" value="<?php echo $b['TglPo']; ?>" name="TglPo">
                    <input type="hidden" value="<?php echo $b['KodeRegional']; ?>" name="KodeRegional">
                    <input type="hidden" value="<?php echo $b['NamaRegional']; ?>" name="NamaRegional">
                    <input type="hidden" value="<?php echo $b['KodeSite']; ?>" name="SiteID">
                    <input type="hidden" value="<?php echo $b['NamaSite']; ?>" name="SiteName">
                    <input type="hidden" value="<?php echo $b['KodeSOW']; ?>" name="KodeSOW">
                    <input type="hidden" value="<?php echo $b['NamaSOW']; ?>" name="SOW">
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-3 invoice-col">
                  <b>Regional</b>
                  <address style="font-size:12px;">
                    <?php echo $b['KodeRegional']; ?> - <?php echo $b['NamaRegional']; ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-3 invoice-col">
                  <b>Site</b>
                  <address style="font-size:12px;">
                    <?php echo $b['KodeSite']; ?> - <?php echo $b['NamaSite']; ?>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-3 invoice-col">
                  <b>SOW</b>
                  <address style="font-size:12px;">
                    <?php echo $b['KodeSOW']; ?>
                  </address>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <center><a class="btn btn-primary btn-sm" href="<?php echo base_url() . 'admin/updatepdp/data_updatepdp/' . base64_encode($b['NoPo']) . '/' . $b['KodeRegional'];
                                                              '' ?>">Data Progres</a></center>

            </section>
            <!-- /.content -->
            <!-- ----------------------------- -->
            <div class="invoice">
              <table id="item_table" class="table table-sm table-striped table-hover " style="font-size:12px;">
                <tr>
                  <th class="bg-primary">
                    <center>Pekerjaan</center>
                  </th>
                  <th class="bg-primary">
                    <center>Dokumen</center>
                  </th>
                  <th class="bg-primary">
                    <center> Persentase (%)</center>
                  </th>
                  <th class="bg-primary"><button type="button" name="add" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></button></th>
                </tr>
              </table>
            </div>
            <!-- ----------------------------- -->

            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
              <button type="submit" class="btn btn-primary btn-flat" name="submit">Submit</button>
            </div>
          </form>
        </div>
      </section>
    </div>

    <!-- /.content-wrapper -->
    <?php $this->load->view('admin/v_footer'); ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>

    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->

    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

    <script>
      $(document).ready(function() {

        var count = 0;

        $(document).on('click', '.add', function() {
          count++;
          var html = '';
          html += '<tr>';
          html += '<td><select name="item_category[]" class="form-control item_category" data-sub_category_id="' + count + '"><option value="">No Selected</option><?php echo fill_select_box($connect, "0"); ?></select></td>';
          html += '<td><select name="item_sub_category[]" class="form-control item_sub_category" id="item_sub_category' + count + '"></select></td>';
          html += '<td><select name="item_sub_category[]" class="form-control item_sub_category" id="item_sub_category1' + count + '"></select></td>';
          html += '<td><button type="button" name="remove" class="btn btn-danger btn-xs remove"><span class="glyphicon glyphicon-minus"></span></button></td>';
          $('tbody').append(html);
        });

        $(document).on('click', '.remove', function() {
          $(this).closest('tr').remove();
        });

        //belum menampilkan sub category

        $(document).on('change', '.item_category', function() {
          var category_id = $(this).val();
          var sub_category_id = $(this).data('sub_category_id');
          $.ajax({
            url: "<?php echo site_url('admin/updatepdp/fill_sub_category'); ?>",
            method: "POST",
            data: {
              category_id: category_id
            },
            success: function(data) {
              var html = '';
              html += data;
              $('#item_sub_category' + sub_category_id).html(html);
            }
          })
        });

        $(document).on('change', '.item_category', function() {
          var category_id = $(this).val();
          var sub_category_id = $(this).data('sub_category_id');
          $.ajax({
            url: "<?php echo site_url('admin/updatepdp/fill_sub_category1'); ?>",
            method: "POST",
            data: {
              category_id: category_id
            },
            success: function(data) {
              var html = '';
              html += data;
              $('#item_sub_category1' + sub_category_id).html(html);
            }
          })
        });



        $('#insert_form').on('submit', function(event) {
          event.preventDefault();
          var error = '';
          $('.item_name').each(function() {
            var count = 1;
            if ($(this).val() == '') {
              error += '<p>Enter Item name at ' + count + ' Row</p>';
              return false;
            }
            count = count + 1;
          });

          $('.item_category').each(function() {
            var count = 1;

            if ($(this).val() == '') {
              error += '<p>Select Item Category at ' + count + ' row</p>';
              return false;
            }

            count = count + 1;

          });

          $('.item_sub_category').each(function() {

            var count = 1;

            if ($(this).val() == '') {
              error += '<p>Select Item Sub category ' + count + ' Row</p> ';
              return false;
            }

            count = count + 1;

          });

          var form_data = $(this).serialize();

          if (error == '') {
            $.ajax({
              url: "insert.php",
              method: "POST",
              data: form_data,
              success: function(data) {
                if (data == 'ok') {
                  $('#item_table').find('tr:gt(0)').remove();
                  $('#error').html('<div class="alert alert-success">Item Details Saved</div>');
                }
              }
            });
          } else {
            $('#error').html('<div class="alert alert-danger">' + error + '</div>');
          }

        });

      });
    </script>

</body>

</html>