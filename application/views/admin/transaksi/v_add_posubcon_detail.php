<?php
include('database_connection.php');
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Tambah Detail PO Subcon
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#"> PO Subcon</a></li>
          <li class="active">Tambah Detail PO Subcon</li>
        </ol>
      </section>



      <!-- Main content -->
      <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <?php $b = $data1->row_array(); ?>
          <form class="form-horizontal" id="insert_form" method="post" enctype="multipart/form-data">

            <section class="invoice" style="padding-top:5px;">
              <!-- title row -->
              <div class="row">
                <div class="col-md-12 col-xs-12 bg-primary" style="margin: 0px; padding: 3px; padding-left: 15px; padding-right: 15px; border-radius: 5px;">
                  <h4 class="page">
                    <b>No PO: </b><?php echo $b['NoPo']; ?>
                    <small class="pull-right" style="color:black;">Tanggal PO: <b><?php echo $b['TglPo']; ?></b></small>
                    <input type="hidden" value="<?php echo $b['NoPo']; ?>" name="NoPo">
                    <input type="hidden" value="<?php echo $b['TglPo']; ?>" name="TglPo">
                    <input type="hidden" value="<?php echo $b['KodeProyek']; ?>" name="KodeProyek">
                    <input type="hidden" value="<?php echo $b['NamaProyek']; ?>" name="NamaProyek">
                    <input type="hidden" value="<?php echo $b['KodeRegional']; ?>" name="KodeRegional">
                    <input type="hidden" value="<?php echo $b['NamaRegional']; ?>" name="NamaRegional">
                    </h2>
                </div>
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-3 invoice-col">
                  <b>Subcon :</b>
                  <p style="font-size:12px"><?php echo $b['KodeSubcon']; ?> - <?php echo $b['NamaSubcon']; ?></p>
                </div>
                <!-- /.col -->
                <div class="col-sm-3 invoice-col">
                  <b>Customer :</b>
                  <p style="font-size:12px"><?php echo $b['KodeCustomer']; ?> - <?php echo $b['NamaCustomer']; ?></p>
                </div>
                <!-- /.col -->
                <div class="col-sm-3 invoice-col">
                  <b>Project :</b>
                  <p style="font-size:12px"><?php echo $b['KodeProyek']; ?> - <?php echo $b['NamaProyek']; ?></p>
                </div>
                <!-- /.col -->
                <div class="col-sm-3 invoice-col">
                  <b>Regional :</b>
                  <p style="font-size:12px"><?php echo $b['KodeRegional']; ?> - <?php echo $b['NamaRegional']; ?></p>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </section>
            <!-- /.content -->

            <div class="invoice" style="padding-top:5px;">
              <div class="row">
                <div class="col-md-12">
                  <div class="tab-content">
                    <table id="item_table" class="table table-sm table-striped table-hover " style="font-size:12px;">
                      <div class="form-group">
                        <label for="inputUserName" class="col-sm-1 ">Duid</label>
                        <div class="col-sm-4">
                          <input type="text" name="DUID" id="duid" class="form-control input-sm" placeholder="Duid" required>
                        </div>

                        <label for="inputUserName" class="col-sm-1 "> Site </label>
                        <div class="col-sm-6">
                          <select class="form-control select2 input-sm" name="SiteID" id="site" style="width: 100%;" required>
                            <option value="">No Selected</option>
                            <?php foreach ($refsite as $i) { // Lakukan looping pada variabel siswa dari controller
                              echo "<option value='" . $i->codeSite . "'>" . $i->codeSite . " - " . $i->siteName . "</option>";
                            } ?>
                          </select>
                        </div>
                      </div>
                      <tr>
                        <th class="bg-primary" style="width: 50%; ">
                          <center>SOW</center>
                        </th>
                        <th class="bg-primary" style="width: 25%;">
                          <center>Harga</center>
                        </th>
                        <th class="bg-primary" style="width: 15%;">
                          <center> Qty</center>
                        </th>
                        <th class="bg-primary">
                          <center><button type="button" name="add" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></center></button>
                        </th>
                      </tr>
                    </table>
                    <div align="center">
                      <input type="submit" name="submit" class="btn btn-primary" value="Simpan" />
                    </div>
                  </div>
                  <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->
              </div>



          </form>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

    <script>
      $(document).ready(function() {

        var count = 0;

        $(document).on('click', '.add', function() {
          count++;
          var html = '';
          html += '<tr>';
          html += '<td><select name="item_category[]" class="form-control input-sm item_category" style="margin:-5px;" data-sub_category_id="' + count + '"><option value="">No Selected</option><?php echo fill_select_box_sow($connect, "0"); ?></select></td>';
          html += '<td><select name="item_sub_category[]" class="form-control input-sm item_sub_category" style="margin:-5px;" id="item_sub_category' + count + '"></select></td>';
          html += '<td><input type="text" name="item_name[]" class="form-control input-sm item_name" style="margin:-5px;"></td>';
          html += '<td><center><button type="button" name="remove" class="btn btn-danger btn-xs remove" style="margin:-5px;"><span class="glyphicon glyphicon-minus"></span></button></center></td>';
          $('tbody').append(html);
        });

        $(document).on('click', '.remove', function() {
          $(this).closest('tr').remove();
        });

        //belum menampilkan sub category

        $(document).on('change', '.item_category', function() {
          var category_id = $(this).val();
          var sub_category_id = $(this).data('sub_category_id');
          $.ajax({
            url: "<?php echo site_url('admin/posubcon/fill_sub_category'); ?>",
            method: "POST",
            data: {
              category_id: category_id
            },
            success: function(data) {
              var html = '';
              html += data;
              $('#item_sub_category' + sub_category_id).html(html);
            }
          })
        });

        $('#insert_form').on('submit', function(event) {
          event.preventDefault();
          var error = '';
          $('.item_name').each(function() {
            var count = 1;
            if ($(this).val() == '') {
              error += '<p>Enter Item name at ' + count + ' Row</p>';
              return false;
            }
            count = count + 1;
          });

          $('.item_category').each(function() {
            var count = 1;

            if ($(this).val() == '') {
              error += '<p>Select Item Category at ' + count + ' row</p>';
              return false;
            }

            count = count + 1;

          });

          $('.item_sub_category').each(function() {

            var count = 1;

            if ($(this).val() == '') {
              error += '<p>Select Item Sub category ' + count + ' Row</p> ';
              return false;
            }

            count = count + 1;

          });

          var form_data = $(this).serialize();

          if (error == '') {
            $.ajax({
              url: "<?php echo site_url('admin/posubcon/simpan_posubcon_detail1'); ?>",
              method: "POST",
              data: form_data,
              success: function(data) {
                window.location.reload()

                $.toast({
                  heading: 'Success',
                  text: "Detail PO Subcon Berhasil Disimpan .",
                  showHideTransition: 'slide',
                  icon: 'success',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#7EC857'
                });
              }
            });
          } else {
            $('#error').html('<div class="alert alert-danger">' + error + '</div>');
          }

        });

      });
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker2').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>

    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PO Subcon berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Ukuran dokumen terlalu besar ",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>


</body>

</html>