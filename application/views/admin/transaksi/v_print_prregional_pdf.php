<?php
$q_instansi	= $this->db->query("SELECT * FROM trxprregionalh LIMIT 1")->row();
?>

<html>

<head>
	<title>Administrator - Intisel Invoice</title>
	<!-- Tell the browser to be responsive to screen width -->
	<link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
</head>

<!--<body >-->
<!--onload="window.print()"-->

<body onload="window.print()">
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<!------ Include the above in your HEAD tag ---------->

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="invoice-title">
					<h2>Purchase Requisition Regional </h2>
					<h5> No PR: <?php echo $q_instansi->NoPo; ?></h5>
				</div>
				<hr>
				<div class="row">
					<div class="col-xs-6">
						<address>
							<strong>Customer: </strong><?php echo $q_instansi->NamaCustomer; ?><br>
							<strong>Pelaksana: </strong><?php echo $q_instansi->NamaSubcon; ?><br>
							<strong>Regional: </strong><?php echo $q_instansi->NamaRegional; ?><br>
						</address>
					</div>
					<div class="col-xs-6 text-right">
						<address>
							<strong>Tanggal PO: </strong><?php echo $q_instansi->TglPo; ?><br>
							<strong>Tanggal Permintaan: </strong><?php echo $q_instansi->TglPermin; ?><br>
							<strong>Atas Permintaan: </strong><?php echo $q_instansi->AtasPermin; ?><br>
							<strong>Keterangan: </strong><?php echo $q_instansi->Keterangan; ?><br>
						</address>
					</div>
				</div>
			</div>
		</div>

		<div class="row" style="margin-left:-40px; ">
			<div class="col-md-12">
				<div class="panel-body">
					<table class="table table-sm table-striped table-bordered" style="font-size:12px;">
						<thead>
							<tr>
								<td><strong>No</strong></td>
								<td><strong>Kode Site</strong></td>
								<td class="text-center"><strong>Nama Site</strong></td>
								<td class="text-center"><strong>SOW</strong></td>
							</tr>
						</thead>
						<tbody>
							<!-- foreach ($order->lineItems as $line) or some such thing here -->
							<?php
							if (!empty($reftxtprregionald)) {
								$no = 0;
								$harga = 0;
								$qty = 0;
								$total = 0;
								foreach ($reftxtprregionald as $i) {
									$total = $total +  $i->TotalBayar;
									$harga = $harga + $i->UnitPrice;
									$qty = $qty + $i->Qty;
									$no++; ?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $i->KodeSite ?></td>
										<td><?php echo $i->NamaSite ?></td>
										<td><?php echo $i->KodeSOW ?></td>

									</tr>
							<?php }
							} ?>

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>

</body>

</html>