<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Data Detail PO Subcon
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#"> PO Subcon</a></li>
          <li class="active">Data Detail PO Subcon</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-body">
            <?php $b = $data1->row_array(); ?>
            <form action="<?php echo base_url() . 'admin/posubcon/save_edit_posubcon' ?>" method="post" enctype="multipart/form-data">

              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>No PO Customer</label>
                    <input class="form-control input-sm" value="<?php echo $b['NoPo']; ?>" rows="3" name="NoPO" placeholder="No PO Customer" readonly>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label> Customer </label>
                    <select name="KodeCustomer" class="form-control input-sm" required>
                      <option value="">No Selected</option>
                      <?php
                      foreach ($refprincipal->result_array() as $i) {
                        $kodePrincipal = $i['kodePrincipal'];
                        $NamaPrincipal = $i['NamaPrincipal'];
                        if ($b['KodeCustomer'] == $kodePrincipal)
                          echo "<option value='$kodePrincipal' selected>$kodePrincipal - $NamaPrincipal</option>";
                        else
                          echo "<option value='$kodePrincipal'>$kodePrincipal - $NamaPrincipal</option>";
                      } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Pelaksana</label>
                    <select name="KodeSubcon" class="form-control input-sm" required>
                      <option value="">No Selected</option>
                      <?php
                      foreach ($refsubcon->result_array() as $i) {
                        $KodeSuppl = $i['KodeSuppl'];
                        $Nama = $i['Nama'];
                        if ($b['KodeSubcon'] == $KodeSuppl)
                          echo "<option value='$KodeSuppl' selected>$KodeSuppl - $Nama</option>";
                        else
                          echo "<option value='$KodeSuppl'>$KodeSuppl - $Nama</option>";
                      } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Tanggal PO</label>
                    <input type="text" name="TglPo" value="<?php echo $b['TglPo']; ?>" class="form-control pull-right input-sm" id="datepicker">
                  </div>
                  <!-- /.form-group -->

                </div>
                <!-- /.col -->
              </div>

              <!-- /.row -->

              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Regional</label>
                    <select name="KodeRegional" class="form-control input-sm" required>
                      <option value="">No Selected</option>
                      <?php
                      foreach ($refregional->result_array() as $i) {
                        $KODE = $i['KODE'];
                        $NAMA = $i['NAMA'];
                        if ($b['KodeRegional'] == $KODE)
                          echo "<option value='$KODE' selected>$KODE - $NAMA</option>";
                        else
                          echo "<option value='$KODE'>$KODE - $NAMA</option>";
                      } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Term Of Payment (TOP) </label>
                    <select name="KodeTop" class="form-control input-sm" required>
                      <option value="">No Selected</option>
                      <?php
                      foreach ($reftop->result_array() as $i) {
                        $KodeTop = $i['KodeTop'];
                        $NamaTop = $i['NamaTop'];
                        if ($b['TotalTermin'] == $KodeTop)
                          echo "<option value='$KodeTop' selected> $NamaTop</option>";
                        else
                          echo "<option value='$KodeTop'> $NamaTop</option>";
                      } ?>
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Kode Site </label>
                    <input class="form-control input-sm" id='KodeSite' value="<?php echo $b['KodeSite']; ?>" rows="3" name="KodeSite" placeholder="Kode Site" required>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Nama Site</label>
                    <input name="NamaSite" id='NamaSite' value="<?php echo $b['NamaSite']; ?>" class="form-control input-sm" placeholder="Nama Site" readonly>
                  </div>
                  <!-- /.form-group -->
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <button type="submit" style="margin-top:25px;" class="btn btn-primary btn-sm" id="simpan">Update</button>
                  </div>
                  <!-- /.form-group -->
                </div>

                <!-- /.col -->
              </div>
              <!-- /.row -->
            </form>

            <br>
            <form method="post" class="form-horizontal" action="<?php echo base_url() . 'admin/posubcon/delete_posubcon_detail' ?>" id="form-delete" enctype="multipart/form-data">
              <div class="col-xs-12">
                <div class="row">
                  <div class="table-responsive">
                    <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                      <thead>
                        <tr>
                          <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary"><input type="checkbox" id="check-all"></th>
                          <th class="bg-primary">No</th>
                          <th class="bg-primary">Kode SOW</th>
                          <th class="bg-primary">Nama SOW</th>
                          <th class="bg-primary">Harga</th>
                          <th class="bg-primary">Qty</th>
                          <th style='border-top-right-radius: 5px; border-bottom-right-radius: 5px;' class="bg-primary">Sub Total </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if (!empty($reftxtposed)) {
                          $no = 0;
                          $harga = 0;
                          $qty = 0;
                          $total = 0;
                          foreach ($reftxtposed as $i) {
                            $total = $total +  $i->TotalBayar;
                            $harga = $harga + $i->UnitPrice;
                            $qty = $qty + $i->Qty;
                            $no++; ?>
                            <tr>
                              <td><input type="checkbox" class="check-item" name="Lineno[]" value="<?php echo $i->Lineno ?>"></td>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $i->KodeSOW ?></td>
                              <td><?php echo $i->NamaSOW ?></td>
                              <td><?php echo $i->UnitPrice ?></td>
                              <td><?php echo $i->Qty ?></td>
                              <td><?php echo $i->TotalBayar ?></td>
                            </tr>

                          <?php } ?>
                          <tr class='alert alert-info' style="font-size:15px;">
                            <td colspan='4' style='text-align:right; '><b>Total</b></td>
                            <td><b><?php echo str_replace(",", ".", number_format($harga)); ?></b></td>
                            <td><b><?php echo str_replace(",", ".", number_format($qty)); ?></b></td>
                            <td><b><?php echo str_replace(",", ".", number_format($total)); ?></b></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                  <button class="btn btn-danger btn-xs" style="margin-top: 8px;" id="btn-delete"><span class="fa fa-trash"></span>
                    Hapus Detail</button>
                  <button class="btn btn-success btn-xs" style="margin-top: 8px;" id="btn-delete"><span class="fa fa-exchange"></span>
                    Entri JO</button>
                  <a class="btn btn-primary btn-xs" style="margin-top: 8px;" href="<?php echo site_url() . 'admin/posubcon/cetakpopdf/' . base64_encode($b['NoPo']); ?>"><span class="fa fa-file-pdf-o"></span>
                    Cetak P0 PDF</a>
                  <button class="btn btn-info btn-xs" style="margin-top: 8px;" id="btn-delete"><span class="fa fa-file-excel-o"></span>
                    Cetak P0 Excel</button>
                  <input type="button" onclick="goBack()" style="margin-top: 8px;" class=" btn btn-default btn-xs" value="Kembali">
                </div>
                <!-- /.row -->
              </div>
              <!-- /.content -->
      </section>
      </form>
    </div>
    <!-- /.content -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

    <script>
      function goBack() {
        window.history.back();
      }
    </script>

    <script type="text/javascript">
      $(document).ready(function() {

        $('#KodeSite').autocomplete({
          source: "<?php echo site_url('admin/site/get_autocomplete'); ?>",

          select: function(event, ui) {
            $('[name="KodeSite"]').val(ui.item.label);
            $('[name="NamaSite"]').val(ui.item.description);
          }
        });
      });
    </script>

    <script>
      $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        $("#check-all").click(function() { // Ketika user men-cek checkbox all
          if ($(this).is(":checked")) // Jika checkbox all diceklis
            $(".check-item").prop("checked", true); // ceklis semua checkbox siswa dengan class "check-item"
          else // Jika checkbox all tidak diceklis
            $(".check-item").prop("checked", false); // un-ceklis semua checkbox siswa dengan class "check-item"
        });

        $("#btn-delete").click(function() { // Ketika user mengklik tombol delete
          var confirm = window.confirm("Apakah Anda yakin ingin menghapus data-data ini?"); // Buat sebuah alert konfirmasi

          if (confirm) // Jika user mengklik tombol "Ok"
            $("#form-delete").submit(); // Submit form
        });
      });

      function enable_text(status) {
        status = !status;
        document.f1.other_text.disabled = status;
      }
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker2').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>

    <script>
      $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {
          "placeholder": "dd/mm/yyyy"
        });
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {
          "placeholder": "mm/dd/yyyy"
        });
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          format: 'MM/DD/YYYY h:mm A'
        });
        //Date range as a button
        $('#daterange-btn').daterangepicker({
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
          },
          function(start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          }
        );

        //Date picker
        $('#datepicker').datepicker({
          autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>

    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Detail PO Customer Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "Detail PO Customer berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Ukuran dokumen terlalu besar ",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Detail PO Customer Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>


</body>

</html>