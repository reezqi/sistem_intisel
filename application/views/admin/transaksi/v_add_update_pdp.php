<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
</head>


<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Update Progres Dokumen
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#">Update Progres Dokumen</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="box box-default">
          <?php $b = $data1->row_array(); ?>
          <div class="box-body">
            <div class="alert alert-success alert-dismissible">
              <h4><b>No PO: </b><?php echo $b['NoPo']; ?>
                <small class="pull-right" style="color:white;">Tanggal PO: <b><?php echo $b['TglPo']; ?></b></small></h4>
            </div>
            <input type="hidden" value="<?php echo $this->uri->segment(4) ?>" id="uri">
            <h4 class="page">
              <input type="hidden" value="<?php echo $b['NoPo']; ?>" name="NoPo" id="NoPo">
              <input type="hidden" value="<?php echo $b['NoPoIn']; ?>" name="NoPoIn" id="NoPoIn">
              <input type="hidden" value="<?php echo $b['TglPo']; ?>" name="TglPo" id="TglPo">
              <input type="hidden" value="<?php echo $b['KodeRegional']; ?>" name="KodeRegional" id="KodeRegional">
              <input type="hidden" value="<?php echo $b['NamaRegional']; ?>" name="NamaRegional" id="NamaRegional">
              <input type="hidden" value="<?php echo $b['KodeCustomer']; ?>" name="KodeCustomer" id="KodeCustomer">
              <input type="hidden" value="<?php echo $b['NamaCustomer']; ?>" name="NamaCustomer" id="NamaCustomer">
              <input type="hidden" value="<?php echo $b['KodeSite']; ?>" name="SiteID" id="SiteID">
              <input type="hidden" value="<?php echo $b['NamaSite']; ?>" name="SiteName" id="SiteName">
              <input type="hidden" value="<?php echo $b['KodeSOW']; ?>" name="KodeSOW" id="KodeSOW">
              <input type="hidden" value="<?php echo $b['NamaSOW']; ?>" name="SOW" id="SOW">
            </h4>

            <div class="row invoice-info">
              <div class="col-sm-3 invoice-col">
                <label>Customer</label>
                <address class="form-control input-sm" style="font-size:12px;">
                  <?php echo $b['KodeCustomer']; ?> - <?php echo $b['NamaCustomer']; ?>
                </address>
              </div>
              <div class="col-sm-3 invoice-col">
                <label>Pelaksana</label>
                <address class="form-control input-sm " style="font-size:12px;">
                  <?php echo $b['KodeSubcon']; ?> - <?php echo $b['NamaSubcon']; ?>
                </address>
              </div>
              <div class="col-sm-3 invoice-col">
                <label>Regional</label>
                <address class="form-control input-sm" style="font-size:12px;">
                  <?php echo $b['KodeRegional']; ?> - <?php echo $b['NamaRegional']; ?>
                </address>
              </div>
              <div class="col-sm-3 invoice-col">
                <label>Informasi</label>
                <div class="form-group">
                  <span>Pilih Site & SOW untuk melihat progres</span>
                </div>
              </div>
            </div>

            <div class="row invoice-info">
              <div class="col-sm-3 invoice-col">
                <label>Kode Site</label>
                <div class="form-group">
                  <select name="kode_site_detail" id="kode_site_detail" class="form-control input-sm js-example-basic-single">
                    <?php echo $detail_site; ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-3 invoice-col">
                <label>Kode SOW</label>
                <div class="form-group">
                  <select name="kode_sow_detail" id="kode_sow_detail" class="form-control input-sm js-example-basic-single">

                  </select>
                  <input type="hidden" id="SOWK">
                </div>
              </div>

            </div>


            <!--  -->
            <input type="hidden" value="" id="termin">
            <table id='TabelDetail' class="table table-sm table-striped table-hover" style="margin-top:10px">
              <div class="row">
                <thead>
                  <tr>
                    <th class="bg-primary" style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" style='width:20px;'>No</th>
                    <th class="bg-primary" style='width:10%;'></th>
                    <th class="bg-primary" style='width:30%;'>Pekerjaan</th>
                    <th class="bg-primary">Dokumen</th>
                    <th class="bg-primary" style='width:100px;'>Presentase</th>
                    <th class="bg-primary" style='width:50px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </div>
            </table>
            <!--  -->


            <!--  -->
            <table id='TabelTransaksi' class="table table-sm table-striped table-hover" style="font-size:12px;">
              <div class="row">
                <thead>
                  <tr>
                    <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary" style='width:20px;'>No</th>
                    <th class="bg-primary" style='width:30%;'>Kode Site</th>
                    <th class="bg-primary" style='width:30%;'>Kode SOW</th>
                    <th class="bg-primary" style='width:180px;'>Pekerjaan</th>
                    <th class="bg-primary">Dokumen</th>
                    <th class="bg-primary" style='width:100px;'>Presentase</th>
                    <th class="bg-primary" style='width:50px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'><button id='BarisBaru' style='margin-right:10px;' class='btn btn-default btn-xs'><i class='fa fa-plus' style='color:green;'></i></button></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </div>

              <input type="button" id='simpan_invoice' name="submit" class="btn btn-success pull-left btn-sm" value="Buat Invoice" />
              <br><br>
            </table>
            <br>

            <div class="modal-footer">
              <input type="button" id='Simpann' name="submit" class="btn btn-primary pull-right btn-sm" value="Update" />
              <input type="button" onclick="goBack()" name="submit" class="btn btn-default pull-right btn-sm" value="Kembali" style="margin-right:10px" />
            </div>
          </div>
        </div>
      </section>
    </div>

    <!-- /.content-wrapper -->
    <?php $this->load->view('admin/v_footer'); ?>



    <!--Modal Add -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content" style=" border-radius: 5px;">
          <form class="form-horizontal" id="SimpanTransaksi" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background-color:#337AB7; border-top-left-radius: 5px; border-top-right-radius: 5px;">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Konfirmasi</h4>
            </div>
            <div class="modal-body">
              <p> Apakah anda yakin ingin menyimpan transaksi ini?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default " data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary ">Ya, saya yakin</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Add -->

    <!--Modal Hapus SOW-->
    <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Hapus</h4>
          </div>
          <form class="form-horizontal" id="hapus_matrik" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <input type="hidden" name="lineno" id="lineno" value="" />
              <p>Apakah Anda yakin menghapus data tersebut <b></b> ?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!--Modal Buat Invoice-->
    <div class="modal fade" id="ModalInvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
            <h4 class="modal-title" id="myModalLabel">Buat Invoice</h4>
          </div>
          <form class="form-horizontal" id="invoice_matrik" method="post" enctype="multipart/form-data">
            <div class="modal-body">
           
              <input type="hidden" name="NoInvoice" id="NoInvoice" value="<?php echo $in + 1?>">
              <input type="hidden" name="lineno" id="lineno" value="" />
              <p>Apakah Anda yakin ingin membuat invoice data ini <b><?php echo $in + 1?></b> ?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
              <button type="submit" class="btn btn-primary btn-flat" id="simpan">Buat Invoice</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <!-- page script -->

    <script>
      $(document).ready(function() {
        BarisBaru();
        
        $('.js-example-basic-single').select2();

        $('#BarisBaru').click(function() {
          BarisBaru();
        });

        function BarisBaru() {
          var index = $('#TabelTransaksi tbody tr').length;
          var Nomor = $('#TabelTransaksi tbody tr').length + 1;
          var Baris = "<tr>";
          Baris += "<td style='margin:-5px; width:35px;'>" + Nomor + "</td>";

          Baris += "<td>";
          Baris += "<select class='form-control input-sm js-example-basic-single' style='margin:-5px;' name='kode_site[]' id='kode_site" + index + "'></select>";
          Baris += "<input style='margin:-5px;' type='hidden' name='nama_site[]' id='nama_site'>";
          Baris += "</td>";

          Baris += "<td>";
          Baris += "<select class='form-control input-sm js-example-basic-single' style='margin:-5px;' name='kode_barang[]' id='kode_barang" + index + "'></select>";
          Baris += "<input style='margin:-5px;' type='hidden' name='nama_barang[]' id='nama_barang'>";
          Baris += "</td>";

          Baris += "<td>";
          Baris += "<select class='form-control input-sm js-example-basic-single' style='margin:-5px;' name='kode_pekerjaan[]' id='kode_pekerjaan" + index + "'></select>";
          Baris += "<input style='margin:-5px;' type='hidden' name='nama_pekerjaan[]' id='nama_pekerjaan'>";
          Baris += "</td>";

          Baris += "<td>";
          Baris += "<input style='margin:-5px;' type='hidden' name='dokumen[]' id='dokumen'>";
          Baris += "<span></span>";
          Baris += "</td>";

          Baris += "<td>";
          Baris += "<input style='margin:-5px;' type='hidden' name='persentase[]' id='persentase'>";
          Baris += "<span></span>";
          Baris += "</td>";


          Baris += "<td><button style='margin-left:0px;' class='btn btn-default btn-xs' id='HapusBaris'><i class='fa fa-times' style='color:red;'></i></button></td>";
          Baris += "</tr>";

          $('#TabelTransaksi tbody').append(Baris);

          $('#TabelTransaksi tbody tr').each(function() {
            $(this).find('td:nth-child(2) input').focus();
          });

          //get ajax tampil pilih site
          $.ajax({
            url: "<?php echo site_url('admin/updatepdp/ajax_pilih_site'); ?>",
            method: "POST",
            dataType: "json",
            data: {
              NoPo: $('#NoPo').val(),
            },
            success: function(data) {
              $('#kode_site' + index).html(data)
            }
          });

          $.ajax({
              url: "<?php echo site_url('admin/updatepdp/ajax_pilih_sow_no_site'); ?>",
              method: "POST",
              dataType: "json",
              data: {
                regKode: $('#KodeRegional').val(),
                NoPo: $('#NoPo').val(),
              },
              success: function(data) {
                $('#kode_barang' + index).html(data)
              }
            });

          $('#kode_site' + index).on('change', function() {

            if ($(this).val() !== null) {
              $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(2) input').val($('#kode_site' + index + ' option:selected').html());
            } else {
              $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(2) input').val('');
            }

            //get ajax pilih sow
            $.ajax({
              url: "<?php echo site_url('admin/updatepdp/ajax_pilih_sow'); ?>",
              method: "POST",
              dataType: "json",
              data: {
                regKode: $('#KodeRegional').val(),
                Site: $(this).val(),
                NoPo: $('#NoPo').val(),
              },
              success: function(data) {
                $('#kode_barang' + index).html(data)
              }
            });
          })


          $('#kode_barang' + index).on('change', function() {
            if ($(this).val() !== null) {
              $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(2) input').val($('#kode_barang' + index + ' option:selected').html());
            } else {
              $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(2) input').val('');
            }

            //get ajax pekerjaan
            $.ajax({
              url: "<?php echo site_url('admin/updatepdp/ajax_matrik_pdp'); ?>",
              method: "POST",
              dataType: "json",
              data: {
                regKode: $('#KodeRegional').val(),
                //cara memanggil kode site yg telah dipilih
                Site: $('#kode_site' + index).val(),
                SOW: $(this).val(),
                NoPo: $('#NoPo').val(),
              },
              success: function(data) {
                $('#kode_pekerjaan' + index).html(data)
              }
            });

            $('#kode_pekerjaan' + index).on('change', function() {
              $.ajax({
                url: "<?php echo site_url('admin/updatepdp/ajax_category_matrik_pdp'); ?>",
                method: "POST",
                dataType: "json",
                data: {
                  idDocCat: $(this).val(),
                },
                success: function(data) {
                  if (data.length > 0) {
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(4) input').val($('#kode_pekerjaan' + index + ' option:selected').html());
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(5) input').val(data[0]['descDocCategory']);
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(5) span').html(data[0]['descDocCategory']);
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(6) input').val(data[0]['bobot']);
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(6) span').html(data[0]['bobot'] + '%');
                  } else {
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(4) input').val('');
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(5) input').val('');
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(5) span').html('');
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(6) input').val('');
                    $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(6) span').html('');
                  }
                }
              });
            })
          })
        }

        //Menghapus Baris
        $(document).on('click', '#HapusBaris', function(e) {
          e.preventDefault();
          $(this).parent().parent().remove();
          var Nomor = 1;
          $('#TabelTransaksi tbody tr').each(function() {
            $(this).find('td:nth-child(1)').html(Nomor);
            Nomor++;
          });
        });

        //Menampilkan Modal Simpan
        $(document).on('click', '#Simpann', function() {
          $('#myModal').modal('show');
        });

         //Menampilkan Modal Simpan
         $(document).on('click', '#simpan_invoice', function() {
          $('#ModalInvoice').modal('show');
        });

        //Simpan Transaksi Update PDP
        $('#SimpanTransaksi').on('submit', function(event) {
          event.preventDefault();

          var FormData = "&NoPo=" + encodeURI($('#NoPo').val());
          FormData += "&NoPoIn=" + encodeURI($('#NoPoIn').val());
          FormData += "&date=" + encodeURI($('#TglPo').val());
          FormData += "&KodeRegional=" + encodeURI($('#KodeRegional').val());
          FormData += "&NamaRegional=" + encodeURI($('#NamaRegional').val());
          FormData += "&KodeSite=" + encodeURI($('#SiteID').val());
          FormData += "&NamaSite=" + encodeURI($('#SiteName').val());
          FormData += "&KodeCustomer=" + encodeURI($('#KodeCustomer').val());
          FormData += "&NamaCustomer=" + encodeURI($('#NamaCustomer').val());
          FormData += "&" + $('#TabelTransaksi tbody input').serialize();
          FormData += "&" + $('#TabelTransaksi tbody select').serialize();

          $.ajax({
            url: "<?php echo site_url('admin/updatepdp/submit_matrik_pdp'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: 'json',
            success: function(data) {
              if (data.status == 1) {
                window.location.href = "<?php echo site_url('admin/updatepdp/get_add/'); ?>" + $('#uri').val()
                $.toast({
                  heading: 'Success',
                  text: "PO Customer Detail Berhasil disimpan ke database.",
                  showHideTransition: 'slide',
                  icon: 'success',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#7EC857'
                });
              } else {
                $.toast({
                  heading: 'Error',
                  text: "Ukuran Dokumen Terlalu Besar ",
                  showHideTransition: 'slide',
                  icon: 'error',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#00C9E6'
                });

              }
            }
          });
        });

        //Baris Baru Melihat Hasil Update
        function BarisBaruDetail() {
          var index = $('#TabelDetail tbody tr').length;
          var Nomor = $('#TabelDetail tbody tr').length + 1;

          var Baris = "<tr>";

          Baris += "<td style='margin:-5px; width:35px;' >";
          Baris += "<span>"+ Nomor +"</span>";
          Baris += "";
          Baris += "</td>";

          
          Baris += "<td style='margin:-5px; width:35px;' >";
          Baris += "<input id='check' type='checkbox' name='check[]' class='form-check-input'>";
          Baris += "<input id='status' type='text' name='status[]' class='form-check-input'>";
          Baris += "<span> </span></td>";

          Baris += "<td>";
          Baris += "<span></span>";
          Baris += "</td>";

          Baris += "<td>";
          Baris += "<span></span>";
          Baris += "</td>";

          Baris += "<td>";
          Baris += "<input type='text' name='persentase[]'>";
          Baris += "<span></span>";
          Baris += "</td>";

          Baris += "<td>";
          Baris += '<span> </span>';
          Baris += "</td>";

          Baris += "</tr>";

          $('#TabelDetail tbody').append(Baris);
        }
        //

        //default SOW
        $.ajax({
            url: "<?php echo site_url('admin/updatepdp/ajax_pilih_sow_detail_no_site'); ?>",
            method: "POST",
            dataType: "json",
            data: {
              regKode: $('#KodeRegional').val(),
              NoPo: $('#NoPo').val(),
            },
            success: function(data) {
              $('#kode_sow_detail').html(data);
            }
          });

        $('#kode_site_detail').on('change', function() {
          $.ajax({
            url: "<?php echo site_url('admin/updatepdp/ajax_pilih_sow_detail'); ?>",
            method: "POST",
            dataType: "json",
            data: {
              regKode: $('#KodeRegional').val(),
              Site: $(this).val(),
              NoPo: $('#NoPo').val(),
            },
            success: function(data) {
              $('#kode_sow_detail').html(data);
              $("#TabelDetail tbody").empty();
              $('#KodeSite').val(json.data[0]['KodeSite']);
            }
          });
        })

        $('#TabelDetail').on('click', '#check', function(e) {
          var index = $(this).attr('data-counter');
          // console.log(index);
          //cek checked
          var status = $('#status' + index).val();
          if (status == 'true') {
            $('#status' + index).val(false);
          } else if (status == 'false') {
            $('#status' + index).val(true);
          }

        })

        // $('#TabelDetail tr').click(function(event) {
        //   console.log("s");
        //   // if (event.target.type !== 'checkbox') {
        //   //   $("input[type='checkbox']").change(function (e) {
        //   //       if ($(this).is(":checked")) { //If the checkbox is checked
        //   //           console.log('s');
        //   //           // $(this).closest('tr').addClass("highlight_row"); 
        //   //           //Add class on checkbox checked
        //   //       } else {
        //   //         console.log('f');
        //   //           //$(this).closest('tr').removeClass("highlight_row");
        //   //           //Remove class on checkbox uncheck
        //   //       }
        //   //   });
        //   // }
        // });

        $('#kode_sow_detail').on('change', function() {
          $('#SOWK').val($(this).val())

          $.ajax({
            url: "<?php echo site_url('admin/updatepdp/ajax_detail_matrik_pdp'); ?>",
            method: "POST",
            dataType: "json",
            data: {
              NoPo: $('#NoPo').val(),
              Site: $('#KodeSite').val(),
              KodeSOW: $(this).val(),
            },
            success: function(data) {
              var termin;
              if (data.invoice == null)
              {
                termin = 0;
              }
              else
              {
                termin = data.invoice.Terminke
              }
           

              // var Nomor = $('#TabelDetail tbody tr').length + 1;
              if (data.matrik_detail.length > 0) {
                for (var i = 0; i < data.matrik_detail.length; i++) {

                  var index = $('#TabelDetail tbody tr').length;
                  var Nomor = $('#TabelDetail tbody tr').length + 1;

                  var Baris = "<tr>";

                  Baris += "<td style='margin:-5px; width:35px;' >";
                  Baris += "<span>"+ Nomor +"</span>";
                  Baris += "";
                  Baris += "</td>";

                  if (termin == 1) {
                    if (Math.round(parseInt(data.matrik_detail[i]['Progress'])) >= Math.round(parseInt(data.sow[0].F7)))
                    {
                      Baris += "<td style='margin:-5px; width:35px;' >";
                      Baris += "<input id='check' data-counter='" + i + "' type='checkbox' name='check[]' class='form-check-input' checked>";
                      Baris += "<input id='status"+i+"' type='hidden' name='status[]' class='form-check-input' value='true'>";
                      Baris += "<span> term1</span></td>";
                     
                      $('#simpan_invoice').prop('disabled', false);
                    }
                    else
                    {
                      Baris += "<td style='margin:-5px; width:35px;' >";
                      Baris += "<input id='check' data-counter='" + i + "' type='hidden' name='check[]' class='form-check-input'>";
                      Baris += "<input id='status"+i+"' type='hidden' name='status[]' class='form-check-input' value='false'>";
                      Baris += "<span> term1</span></td>";
                    
                      $('#simpan_invoice').prop('disabled', true);
                    }
                  }
                  else if (termin == 2)
                  {

                    if (Math.round(parseInt(data.matrik_detail[i]['Progress'])) >= Math.round(parseInt(data.sow[0].F8)))
                    {
                      Baris += "<td style='margin:-5px; width:35px;' >";
                      Baris += "<input id='check' data-counter='" + i + "' type='checkbox' name='check[]' class='form-check-input' checked>";
                      Baris += "<input id='status"+i+"' type='hidden' name='status[]' class='form-check-input' value='true'>";
                      Baris += "<span> term2</span></td>";
                    
                    }
                    else
                    {
                      Baris += "<td style='margin:-5px; width:35px;' >";
                      Baris += "<input id='check' data-counter='" + i + "' type='hidden' name='check[]' class='form-check-input'>";
                      Baris += "<input id='status"+i+"' type='hidden' name='status[]' class='form-check-input' value='false'>";
                      Baris += "<span> term2</span></td>";

                    }
                  } else if (termin == 3)
                  {
                      Baris += "<td style='margin:-5px; width:35px;' >";
                      Baris += "<span> Selesai</span></td>";
                  }
                  else {
                    if (Math.round(parseInt(data.matrik_detail[i]['Progress'])) >= Math.round(parseInt(data.sow[0].PemFull)))
                    {
                      Baris += "<td style='margin:-5px; width:35px;' >";
                      Baris += "<input id='check' data-counter='" + i + "' type='checkbox' name='check[]' class='form-check-input' checked>";
                      Baris += "<input id='status"+i+"' type='hidden' name='status[]' class='form-check-input' value='true'>";
                      Baris += "<span> -</span></td>";

                    }
                    else
                    {
                      Baris += "<td style='margin:-5px; width:35px;' >";
                      Baris += "<input id='check' data-counter='" + i + "' type='hidden' name='check[]' class='form-check-input'>";
                      Baris += "<input id='status"+i+"' type='hidden' name='status[]' class='form-check-input' value='false'>";
                      Baris += "<span> -</span></td>";
                    }
                  }

                

                  Baris += "<td>";
                  Baris += "<span>" + data.matrik_detail[i]['JobCode'] + "</span>";
                  Baris += "</td>";

                  Baris += "<td>";
                  Baris += "<span>" + data.matrik_detail[i]['DocumentName'] + "</span>";
                  Baris += "</td>";

                  Baris += "<td>";
                  Baris += "<input type='hidden' name='persentase[]' value='" + data.matrik_detail[i]['Progress'] + "'>";
                  Baris += "<span>" + data.matrik_detail[i]['Progress'] + "</span>";
                  Baris += "</td>";

                  Baris += "<td>";
                  Baris += '<span> <a class="badge" id="hap" data-toggle="modal" data-lineno="' + data.matrik_detail[i]['Lineno'] + '" data-target="#ModalHapus" style="background-color:red;" title="Hapus">Hapus</a>  </span>';
                  Baris += "</td>";

                  Baris += "</tr>";

                  $('#TabelDetail tbody').append(Baris);
                }

              } else {
                $('#TabelDetail tbody').empty()
              }
              $('#TabelDetail tbody').on('click', '#hap', function() {
                var lineno = $(this).attr('data-lineno');
                $('#lineno').val(lineno);
              })

            }
          });
        })

        $("#hapus_matrik").submit(function(event) {
          event.preventDefault();

          $.ajax({
            url: "<?php echo base_url() . 'admin/updatepdp/delete_matrik_pdp_by_leneno' ?>",
            method: "POST",
            dataType: "json",
            data: {
              lineno: $('#lineno').val(),
            },
            success: function(data) {
              if (data.status == 1) {
                window.location.href = "<?php echo site_url('admin/updatepdp/get_add/'); ?>" + $('#uri').val()
                $.toast({
                  heading: 'Success',
                  text: "PO Customer Detail Berhasil disimpan ke database.",
                  showHideTransition: 'slide',
                  icon: 'success',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#7EC857'
                });
              } else {
                $.toast({
                  heading: 'Error',
                  text: "Ukuran Dokumen Terlalu Besar ",
                  showHideTransition: 'slide',
                  icon: 'error',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#00C9E6'
                });

              }
            }
          });

        });


        $("#invoice_matrik").submit(function(event) {
          event.preventDefault();

          var FormData = "&NoPo=" + encodeURI($('#NoPo').val());
          FormData += "&NoPoIn=" + encodeURI($('#NoPoIn').val());
          FormData += "&date=" + encodeURI($('#TglPo').val());
          FormData += "&KodeCustomer=" + encodeURI($('#KodeCustomer').val());
          FormData += "&KodeRegional=" + encodeURI($('#KodeRegional').val());
          FormData += "&NoInvoice=" + $('#NoInvoice').val();
          FormData += "&SOW=" + $('#SOWK').val();
          FormData += "&termin=" + $('#termin').val();
          FormData += "&" + $('#TabelDetail tbody input').serialize();


          $.ajax({
            url: "<?php echo base_url() . 'admin/updatepdp/get_add_invoice' ?>",
            method: "POST",
            dataType: "json",
            data: FormData,
            success: function(data) {
              if (data.status == 1) {
                window.location.href = "<?php echo site_url('admin/updatepdp/get_add/'); ?>" + $('#uri').val()
                $.toast({
                  heading: 'Success',
                  text: "PO Customer Detail Berhasil disimpan ke database.",
                  showHideTransition: 'slide',
                  icon: 'success',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#7EC857'
                });
              } else {
                $.toast({
                  heading: 'Error',
                  text: "Ukuran Dokumen Terlalu Besar ",
                  showHideTransition: 'slide',
                  icon: 'error',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#00C9E6'
                });

              }
            }
          });

        });



      });
    </script>

</body>

</html>