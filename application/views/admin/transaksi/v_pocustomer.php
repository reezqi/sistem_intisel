<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          PO Customer
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li class="active"> PO Customer</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-info">

              <form class="form-horizontal" id="formcari" method="post">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-2 control-label"> Pilih Tanggal Awal</label>
                    <div class="col-sm-3">
                      <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="date_satu" name="date_satu">
                    </div>

                    <label for="inputUserName" class="col-sm-2 control-label"> Pilih Tanggal Akhir </label>
                    <div class="col-sm-3">
                      <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="date_dua" name="date_dua">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-2 control-label"> Regional</label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeRegional" id="KodeRegional" style="width: 100%;">
                        <?php if ($this->session->userdata('is_admin') === TRUE) {
                        ?>
                          <option value="">No Selected</option>
                        <?php } ?>
                        <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                        } ?>
                      </select>
                    </div>

                    <label for="inputUserName" class="col-sm-2 control-label"> Customer </label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" id="KodeCustomer" name="KodeCustomer" style="width: 100%;">
                        <option value="">No Selected</option>
                        <?php foreach ($refprincipal as $i) {
                          echo "<option value='" . $i->kodePrincipal . "'>" . $i->kodePrincipal . " - " . $i->NamaPrincipal . "</option>";
                        } ?>
                      </select>
                    </div>
                    <button name="submit" type="submit" class="btn btn-primary btn-sm" id="simpan"><span class="fa  fa-search"></span> Cari Data</button>
                  </div>
                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-2 control-label"> Site</label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeSite" id="KodeSite" style="width: 100%;">
                        <option value="">No Selected</option>
                        <?php foreach ($refsite as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->codeSite . "'>" . $i->codeSite . " - " . $i->siteName . "</option>";
                        } ?>
                      </select>
                    </div>

                    <label for="inputUserName" class="col-sm-2 control-label">No PR</label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" id="NoPr" name="NoPr" style="width: 100%;">
                        <option value="">No Selected</option>
                        <?php foreach ($refpr as $i) {
                          echo "<option value='" . $i->NoPr . "'>" . $i->NoPr . "</option>";
                        } ?>
                      </select>
                    </div>
                    <a class="btn  btn-success btn-sm" href="<?php echo base_url() . 'admin/pocustomer/add_pocustomer' ?>"><span class="fa fa-user-plus"></span>
                      Entri PO Customer</a>
                  </div>
                </div>
              </form>
            </div>


            <div class="box">
              <!-- /.box-header -->
              <div class="box-body">
              <div class="table-responsive">
                  <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                    <thead>
                      <tr>
                        <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary">No </th>
                        <th class="bg-primary">No Dokumen</th>
                        <th class="bg-primary">Tanggal Mulai PO</th>
                        <th class="bg-primary">No PO Internal</th>
                        <th class="bg-primary">No PO Customer</th>
                        <th class="bg-primary">Customer</th>
                        <th class="bg-primary">Regional</th>
                        <th class="bg-primary">Kode Site</th>
                        <th class="bg-primary">Total PO</th>
                        <th style='border-top-right-radius: 5px; border-bottom-right-radius: 5px;' class="bg-primary" style="text-align:center;">Operasi</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                  
                  </table>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- /.col -->
          </div>
          <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>


      <!--Modal Hapus SOW-->
      <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Hapus PO Customer</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/pocustomer/delete_pocustomer' ?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="NoDokumen" id="datahapus" value="" />
                <p>Apakah Anda yakin menghapus PO Customer <b><span id="datapo"></span></b> ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>



    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

    <script>
      var table;
      $(document).ready(function() {
          //datatables
          
          table = $('#example1').DataTable({
                "bDestroy": true,
                "processing": true, 
                "serverSide": true, 
                "order": [], 
                
                "ajax": {
                    "url": "<?php echo site_url('admin/pocustomer/ajax_table_pocustomer')?>",
                    "type": "POST",
                    "dataType": "JSON",
                    "data": {
                      "date_satu" : $('#date_satu').val(),
                      "date_dua"  : $('#date_dua').val(),
                      "KodeRegional"  : $('#KodeRegional').val(),
                      "KodeCustomer"  : $('#KodeCustomer').val(),
                      "KodeSite"  : $('#KodeSite').val(),
                      "NoPr"  : $('#NoPr').val(),
                    }
                },

                
                "columnDefs": [
                    { 
                        "targets": [ 0 ], 
                        "orderable": false,
                        "searchable": false,
                    },
                    { 
                        "targets": [ 3 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 4 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 5 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 6 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 7 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 8 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 9 ], 
                        "orderable": false, 
                    },
                ],

                // initComplete: function () {
                //     this.api().columns(6).every( function () {
                //         var column = this;
                //         var select = $('<select><option value=""></option></select>')
                //             .appendTo( $(column.footer()).empty() )
                //             .on( 'change', function () {
                //                 var val = $.fn.dataTable.util.escapeRegex(
                //                     $(this).val()
                //                 );
        
                //                 column
                //                     .search( val ? val : '', true, false )
                //                     .draw();
                //             } );

                //         $.ajax({
                //           url:"<?php echo base_url('admin/regional/ajax_regional') ?>",
                //           type:"GET",
                //           dataType: 'JSON',
                //           success:function(res) {
                //             for (let i = 0; i < res.length; i++) {
                //               select.append( '<option value="'+res[i]['KODE']+'">'+res[i]['NAMA']+'</option>' );
                //             }
                //           }
                //         })
                //         // column.data().unique().sort().each( function ( d, j ) {
                //         //     select.append( '<option value="'+d+'">'+d+'</option>' );
                //         // } );
                        
                //     } );
                // }

            });

            

            $('#date_satu').datepicker({
              dateFormat: 'dd-mm-yy',
              changeMonth: true,
              changeYear: true
            });

            $('#date_dua').datepicker({
              dateFormat: 'dd-mm-yy',
              changeMonth: true,
              changeYear: true
            });

        $('#example1').on('click', '#hapus', function(e) {
          var dokumen = $(this).attr('data-hapus')
          var nopoin  = $(this).attr('data-po')
          
          $('#datahapus').val(dokumen)
          $('#datapo').html(nopoin)
          $('#ModalHapus').modal('show');
          
        })

        $("#formcari").submit(function(e){
            //
            e.preventDefault()
            table = $('#example1').DataTable({ 
                "bDestroy": true,
                "processing": true, 
                "serverSide": true, 
                "order": [], 
                
                "ajax": {
                    "url": "<?php echo site_url('admin/pocustomer/ajax_table_pocustomer')?>",
                    "type": "POST",
                    "dataType": "JSON",
                    "data": {
                      "date_satu" : $('#date_satu').val(),
                      "date_dua"  : $('#date_dua').val(),
                      "KodeRegional"  : $('#KodeRegional').val(),
                      "KodeCustomer"  : $('#KodeCustomer').val(),
                      "KodeSite"  : $('#KodeSite').val(),
                      "NoPr"  : $('#NoPr').val(),
                    }
                },

                
                "columnDefs": [
                    { 
                        "targets": [ 0 ], 
                        "orderable": false,
                        "searchable": false,
                    },
                    { 
                        "targets": [ 3 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 4 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 5 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 6 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 7 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 8 ], 
                        "orderable": false, 
                    },
                    { 
                        "targets": [ 9 ], 
                        "orderable": false, 
                    },
                ],

                // initComplete: function () {
                //     this.api().columns(6).every( function () {
                //         var column = this;
                //         var select = $('<select><option value=""></option></select>')
                //             .appendTo( $(column.footer()).empty() )
                //             .on( 'change', function () {
                //                 var val = $.fn.dataTable.util.escapeRegex(
                //                     $(this).val()
                //                 );
        
                //                 column
                //                     .search( val ? val : '', true, false )
                //                     .draw();
                //             } );

                //         $.ajax({
                //           url:"<?php echo base_url('admin/regional/ajax_regional') ?>",
                //           type:"GET",
                //           dataType: 'JSON',
                //           success:function(res) {
                //             for (let i = 0; i < res.length; i++) {
                //               select.append( '<option value="'+res[i]['KODE']+'">'+res[i]['NAMA']+'</option>' );
                //             }
                //           }
                //         })
                //         // column.data().unique().sort().each( function ( d, j ) {
                //         //     select.append( '<option value="'+d+'">'+d+'</option>' );
                //         // } );
                        
                //     } );
                // }

            });
            // 
        });

    
    });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Customer Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PO Customer berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Customer Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>

</body>

</html>