<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Detail PO Subcon
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#"> PO Subcon</a></li>
          <li class="active">Detail PO Subcon</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">

          <?php $b = $data1->row_array(); ?>
          <!-- Main content -->
          <section class="invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12" style="background-color:#3C8DBC;">
                <h2 class="page-header">
                  No PO: <i><?php echo $b['NoPo']; ?></i>
                  <small class="pull-right" style="color:black;">Tanggal PO: <b><?php echo $b['TglPo']; ?></b></small>
                  <input type="hidden" value="<?php echo $b['NoPo']; ?>" name="NoPo">
                  <input type="hidden" value="<?php echo $b['TglPo']; ?>" name="TglPo">
                  <input type="hidden" value="<?php echo $b['KodeProyek']; ?>" name="KodeProyek">
                  <input type="hidden" value="<?php echo $b['NamaProyek']; ?>" name="NamaProyek">
                  <input type="hidden" value="<?php echo $b['KodeRegional']; ?>" name="KodeRegional">
                  <input type="hidden" value="<?php echo $b['NamaRegional']; ?>" name="NamaRegional">
                </h2>
              </div>
              <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-3 invoice-col">
                Subcon
                <address>
                  <strong> <?php echo $b['KodeSubcon']; ?> - <?php echo $b['NamaSubcon']; ?></strong><br>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-3 invoice-col">
                Customer
                <address>
                  <strong><?php echo $b['KodeCustomer']; ?> - <?php echo $b['NamaCustomer']; ?></strong><br>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-3 invoice-col">
                Project
                <address>
                  <strong><?php echo $b['KodeProyek']; ?> - <?php echo $b['NamaProyek']; ?></strong><br>
                </address>
              </div>
              <!-- /.col -->
              <div class="col-sm-3 invoice-col">
                Regional
                <address>
                  <strong><?php echo $b['KodeRegional']; ?> - <?php echo $b['NamaRegional']; ?></strong><br>
                </address>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->

          <section class="invoice">
            <div class="table-responsive">
              <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                <thead>
                  <tr>
                    <th class="bg-primary">DUID</th>
                    <th class="bg-primary">Kode Site</th>
                    <th class="bg-primary">Nama Site</th>
                    <!-- <th class="bg-primary">Kode SOW</th> -->
                    <th class="bg-primary">SOW</th>
                    <th class="bg-primary">Qty</th>
                    <th class="bg-primary">Harga</th>
                    <th class="bg-primary">Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($reftxtposed as $i) { ?>
                    <tr>
                      <td><?php echo $i->DUID ?></td>
                      <td><?php echo $i->SiteID ?></td>
                      <td><?php echo $i->SiteName ?></td>
                      <!-- <td><?php echo $i->KodeSOW ?></td> -->
                      <td><?php echo $i->SOW ?></td>
                      <td><?php echo $i->Qty ?></td>
                      <td><?php echo $i->UnitPrice ?></td>
                      <td style="text-align:center;">
                        <a class="badge" data-toggle="modal" data-target="#ModalEdit<?php echo $i->Lineno; ?>" style="background-color:#3C8DBC;" title="Edit"><span class="fa fa-edit"></span></a>
                        <a class="badge" data-toggle="modal" data-target="#ModalHapus<?php echo $i->Lineno; ?>" style="background-color:red;" title="Hapus"><span class="fa fa-trash"></span></a>
                      </td>

                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
        </div>
        <!-- /.box-body -->


      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!--Modal Edit Regional-->
    <?php foreach ($reftxtposed as $i) { ?>
      <div class="modal fade" id="ModalEdit<?php echo $i->Lineno; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:#337AB7">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Edit Detail PO Subcon</h4>
            </div>

            <form class="form-horizontal" action="<?php echo base_url() . 'admin/posubcon/update_posubcon_detail' ?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">DUID</label>
                  <div class="col-sm-7">
                    <input type="hidden" name="Lineno" value="<?php echo $i->Lineno; ?>">
                    <input type="text" name="DUID" class="form-control" value="<?php echo $i->DUID; ?>" id="inputUserName" placeholder="DUID" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Site</label>
                  <div class="col-sm-7">
                    <select name="Site" class="form-control" required>
                      <option value="">No Selected</option>
                      <?php
                      foreach ($refsite as $a) {
                        if ($i->SiteID == $a->codeSite)
                          echo "<option value='" . $a->codeSite . "' selected >" . $a->codeSite . " - " . $a->siteName . "</option>";
                        else
                          echo "<option value='" . $a->codeSite . "'>" . $a->codeSite . " - " . $a->siteName . "</option>";
                      } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">SOW</label>
                  <div class="col-sm-7">
                    <select name="SOW" class="form-control" onchange='changeValue(this.value)' required>
                      <option value="">No Selected</option>
                      <?php
                      $jsArray = "var prdName = new Array();\n";
                      foreach ($refsow as $a) {
                        if ($i->SOW == $a->SOW) {
                          echo "<option value='" . $a->Code . "' selected >" . $a->Code . " - " . $a->SOW . "</option>";
                          $jsArray .= "prdName['" . $a->Code . "'] = {HrgRegional:'" . addslashes($a->HrgRegional) .  "'};\n";
                        } else {
                          echo "<option value='" . $a->Code . "'>" . $a->Code . " - " . $a->SOW . "</option>";
                          $jsArray .= "prdName['" . $a->Code . "'] = {HrgRegional:'" . addslashes($a->HrgRegional) .  "'};\n";
                        }
                      } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Harga</label>
                  <div class="col-sm-5">
                    <input type="text" name="TotalPo" class="form-control" id="HrgRegional" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputUserName" class="col-sm-4 control-label">Qty</label>
                  <div class="col-sm-2">
                    <input type="text" name="Qty" class="form-control" value="<?php echo $i->Qty; ?>" id="inputUserName" placeholder="Qty" required>
                  </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Perbarui</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    <?php } ?>
    <!--Modal Edit Unit-->

    <?php foreach ($reftxtposed as $i) { ?>
      <!--Modal Hapus Pengguna-->
      <div class="modal fade" id="ModalHapus<?php echo $i->Lineno; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color:red">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Hapus Detail PO</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/posubcon/hapus_posubcon_detail' ?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="Lineno" value="<?php echo $i->Lineno; ?>" />
                <p>Apakah Anda yakin menghapus Site <b><?php echo $i->SiteName; ?></b> dengan SOW <b><?php echo $i->SOW; ?></b> ?</p>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-danger btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    <?php } ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

    <script type="text/javascript">
      <?php echo $jsArray; ?>

      function changeValue(id) {
        document.getElementById('HrgRegional').value = prdName[id].HrgRegional;
      };
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker2').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>

    <script>
      $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {
          "placeholder": "dd/mm/yyyy"
        });
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {
          "placeholder": "mm/dd/yyyy"
        });
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          format: 'MM/DD/YYYY h:mm A'
        });
        //Date range as a button
        $('#daterange-btn').daterangepicker({
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
          },
          function(start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          }
        );

        //Date picker
        $('#datepicker').datepicker({
          autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>

    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PO Subcon berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Ukuran dokumen terlalu besar ",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Subcon Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>


</body>

</html>