<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Edit Data PO Customer
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li><a href="#"> PO Customer</a></li>
          <li class="active">Data Detail PO Customer</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
          <div class="box-body">
            <div class="alert alert-warning alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-warning"></i> Pilih Terlebih Dahulu!</h4>

              <div class="custom-control custom-radio">
                <input type="radio" id="_nopr" name="customRadio" class="custom-control-input">
                <span Style="color:black" class="custom-control-label" for="customRadio2">Input Biasa</span>

                <input type="radio" id="pr" name="customRadio" class="custom-control-input">
                <span Style="color:black" class="custom-control-label" for="customRadio1">Input Dengan PR</span>
              </div>
            </div>
          </div>

          <div class="box-body">
            <?php $b = $data1->row_array(); ?>

            <input type="hidden" id="_pr" value="<?php echo $b['NoPr']; ?>">
            <input type="hidden" id="uri" value="<?php echo $this->uri->segment(4); ?>">
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>No PO Internal</label>
                  <!--  -->
                  <input type="hidden" name="NoUrutPo" class="form-control input-sm" value="<?php echo substr($b['NoPoIn'], 25, 30); ?>" id="NoUrutPo">
                  <input value="<?php echo $b['NoPoIn']; ?>" type="text" name="NoPoIn" class="form-control input-sm" id="NoPoIn" placeholder="No PO Internal" readonly>

                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-3">
                <p class="form-group">
                  <label> No PR </label>
                  <select id="NoPr" class="form-control select2 input-sm" name="NoPr" style="width: 100%;" required>

                    <option value="">No Selected </option>
                    <?php if ($refpr !== NULL) {
                      foreach ($refpr as $field) {
                        if ($b['NoPr'] === $field->NoPr) {
                          echo "<option selected='selected' value='$field->NoPr'>$field->NoPr</option>";
                        } else {
                          echo "<option value='$field->NoPr'>$field->NoPr</option>";
                        }
                    ?>
                    <?php }
                    } ?>
                  </select>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label> Customer </label>
                  <select onchange="NoPoInternal()" name="KodeCustomer" id="KodeCustomer" class="form-control input-sm" required>
                    <option value="">No Selected</option>
                    <?php
                    foreach ($refprincipal->result_array() as $i) {
                      $kodePrincipal = $i['kodePrincipal'];
                      $NamaPrincipal = $i['NamaPrincipal'];
                      if ($b['KodeCustomer'] == $kodePrincipal)
                        echo "<option value='$kodePrincipal' selected>$kodePrincipal - $NamaPrincipal</option>";
                      else
                        echo "<option value='$kodePrincipal'>$kodePrincipal - $NamaPrincipal</option>";
                    } ?>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-3">
                <div class="form-group">
                  <label>No PO Customer</label>
                  <input value="<?php echo $b['NoPo']; ?>" type="text" name="NoPo" class="form-control input-sm" id="NoPo" placeholder="No PO Customer" required>
                </div>
                <!-- /.form-group -->
              </div>


              <div class="col-md-3">
                <div class="form-group">
                  <label>No Dokumen</label>
                  <input readonly class="form-control input-sm" value="<?php echo $b['NoDokumen']; ?>" rows="3" name="NoDokumen" id="NoDokumen" placeholder="No Dokumen" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-3">
                <div class="form-group">
                  <label>Tanggal PO</label>
                  <input onchange="NoPoInternal()" type="text" id="datepicker3" name="datepicker3" value="<?php echo $b['TglPo']; ?>" class="form-control pull-right input-sm">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-3">
                <div class="form-group">
                  <label>Tanggal Akhir PO</label>
                  <input onchange="NoPoInternal()" class="form-control input-sm" value="<?php echo $b['TglAkhirPo']; ?>" type="text" id="datepicker2" name="datepicker2" placeholder="Tanggal Akhir PO ">
                </div>
                <!-- /.form-group -->

              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label>Regional </label>
                  <select onchange="NoPoInternal()" name="KodeRegional" id="KodeRegional" class="form-control input-sm" required>

                    <?php
                    foreach ($refregional as $i) {
                      $KODE = $i->KODE;
                      $NAMA = $i->NAMA;
                      if ($b['KodeRegional'] == $KODE)
                        echo "<option value='$KODE' selected>$KODE - $NAMA</option>";
                      else
                        echo "<option value='$KODE'>$KODE - $NAMA</option>";
                    } ?>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-3">
                <div class="form-group">
                  <label>Kode Site </label>
                  <input class="form-control input-sm" id='KodeSite' value="<?php echo $b['KodeSite']; ?>" rows="3" name="KodeSite" placeholder="Kode Site" required>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->

              <div class="col-md-3">
                <div class="form-group">
                  <label>Nama Site</label>
                  <input name="NamaSite" id='NamaSite' value="<?php echo $b['NamaSite']; ?>" class="form-control input-sm" placeholder="Nama Site" readonly>
                </div>
                <!-- /.form-group -->
              </div>
            </div>

            <!-- /.row -->


            <br>
            <div class="col-xs-12">
              <div class="row">
                <div class="table-responsive">
                  <table id='TabelTransaksi' class="table table-sm table-striped table-hover " style="font-size:12px;">
                    <thead>
                      <tr>
                        <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary" style='width:20px;'>No</th>
                        <th class="bg-primary" style='width:180px;'>Kode SOW</th>
                        <th class="bg-primary">Nama SOW</th>
                        <th class="bg-primary" style='width:100px;'>Harga</th>
                        <th class="bg-primary" style='width:50px;'>Qty</th>
                        <th class="bg-primary" style='width:20px;'>PPN</th>
                        <th class="bg-primary" style='width:20px;'>PPH</th>
                        <th class="bg-primary" style='width:100px;'>Sub Total</th>
                        <th class="bg-primary" style='width:100px;'>Nilai PPN</th>
                        <th class="bg-primary" style='width:100px;'>Nilai PPH</th>
                        <th class="bg-primary" style='width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'><button id='BarisBaru' style='margin-right:10px;' class='btn btn-default btn-xs'><i class='fa fa-plus' style='color:green;'></i></button></th>
                      </tr>
                    </thead>
                    <tbody></tbody>

                  </table>
                  <div class='alert alert-info TotalBayar' style="padding:5px;">
                    <h3 style="text-align:right; margin:0px;">Total : <span id='TotalBayar'>Rp. 0</span></h3>
                    <input type="hidden" id='TotalBayarHidden'>
                  </div>

                  <div class="modal-footer">
                    <input type="button" id='update' class="btn btn-primary pull-right btn-sm" value="Update" />
                    <input type="button" onclick="goBack()" class="btn btn-default pull-right btn-sm" value="Kembali" style="margin-right:10px" />
                    <a href="<?php echo site_url() . 'admin/pocustomer/cetakpopdf/' . base64_encode($b['NoPoIn']); ?>">
                      <input type="button" class="btn btn-info pull-right btn-sm" value="Cetak PO" /> </a>
                  </div>
                </div>


                <!-- /.row -->
              </div>
              <!-- /.content -->
      </section>
    </div>
    <!-- /.content -->

    <!--Modal Add -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content" style=" border-radius: 5px;">
          <form class="form-horizontal" id="SimpanTransaksi" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background-color:#337AB7; border-top-left-radius: 5px; border-top-right-radius: 5px;">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Konfirmasi</h4>
            </div>
            <div class="modal-body">
              <p> Apakah anda yakin ingin menyimpan transaksi ini?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default " data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary ">Ya, saya yakin</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--Modal Add -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>

    <script>
      function goBack() {
        window.history.back();
      }
    </script>



    <script type="text/javascript">
      function disbale_all_form() {
        $("#NoPoIn").prop("disabled", true);
        $("#NoPr").prop("disabled", true);
        $("#KodeCustomer").prop("disabled", true);
        $("#NoPo").prop("disabled", true);
        $("#NoDokumen").prop("disabled", true);
        $("#datepicker3").prop("disabled", true);
        $("#datepicker2").prop("disabled", true);
        $("#KodeRegional").prop("disabled", true);
        $("#KodeSite").prop("disabled", true);
        $("#NamaSite").prop("disabled", true);
      }

      function pr_selected() {
        $("#NoPoIn").prop("disabled", true);
        $("#NoPr").prop("disabled", false);
        $("#KodeCustomer").prop("disabled", true);
        $("#NoPo").prop("disabled", false);
        $("#NoDokumen").prop("disabled", true);
        $("#datepicker3").prop("disabled", false);
        $("#datepicker2").prop("disabled", false);
        $("#KodeRegional").prop("disabled", true);
        $("#KodeSite").prop("disabled", true);
        $("#NamaSite").prop("disabled", true);
      }

      function po_selected() {
        $("#NoPoIn").prop("disabled", true);
        $("#NoPr").prop("disabled", true);
        $("#KodeCustomer").prop("disabled", false);
        $("#NoPo").prop("disabled", false);
        $("#NoDokumen").prop("disabled", true);
        $("#datepicker3").prop("disabled", false);
        $("#datepicker2").prop("disabled", false);
        $("#KodeRegional").prop("disabled", false);
        $("#KodeSite").prop("disabled", false);
        $("#NamaSite").prop("disabled", true);
      }
      $(document).ready(function() {

        $("#KodeSite").autocomplete({
          source: function(request, response) {
            $.ajax({
              url: "<?php echo site_url('admin/site/get_autocomplete'); ?>",
              method: "POST",
              dataType: "json",
              data: {
                regKode: $('#KodeRegional').val(),
                term: request.term,
              },
              success: function(data) {
                response(data);
              }
            });
          },
          min_length: 3,
          select: function(event, ui) {
            $('[name="KodeSite"]').val(ui.item.label);
            $('#NamaSite').val(ui.item.description);
          }
        });

        $('#NoPr').on('change', function(e) {
          $.ajax({
            url: "<?php echo site_url('admin/prregional/ajax_prregional_edit_no_encode'); ?>",
            type: "POST",
            data: {
              no: $(this).val(),
            },
            cache: false,
            dataType: 'json',
            success: function(json) {
              if (json.data.length >= 1) {
                var StrNoUrutPo = $('#NoUrutPo').val()
                var StrKodeCustomer = json.data[0]['KodeCustomer'];
                var Strdatepicker3 = json.data[0]['TglPo'];
                var StrKodeRegional = json.data[0]['KodeRegional'];

                var StrNoPoIn = "PO/" + StrKodeCustomer + "/INTISEL/" + StrKodeRegional + "/" + Strdatepicker3.substring(6, 10) + "/" + StrNoUrutPo;
                document.getElementById("NoPoIn").value = StrNoPoIn;

                $("#TabelTransaksi tbody").empty();
                $('#KodeCustomer').val(json.data[0]['KodeCustomer']);
                $('#datepicker3').val(json.data[0]['TglPo']);
                $('#KodeRegional').val(json.data[0]['KodeRegional']);
                $('#KodeSite').val(json.data[0]['KodeSite']);
                $('#NamaSite').val(json.data[0]['NamaSite']);
                var stok_total = 0;
                for (var i = 0; i < json.reftxtposed.length; i++) {
                  BarisBaru();
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(2) input').val(json.reftxtposed[i]['KodeSOW']);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(3) input').val(json.reftxtposed[i]['NamaSOW']);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(3) span').html(json.reftxtposed[i]['NamaSOW']);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(4) input').val(json.reftxtposed[i]['UnitPrice']);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(4) span').html(to_rupiah(json.reftxtposed[i]['UnitPrice']));
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(5) input').removeAttr('disabled').val(json.reftxtposed[i]['Qty']);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(6) input').prop('disabled', true).val('');
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(7) input').prop('disabled', true).val('');
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(8) input').val(json.reftxtposed[i]['TotalBayar']);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(8) span').html(to_rupiah(json.reftxtposed[i]['TotalBayar']));
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(9) input').val(0);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(9) span').html(to_rupiah(0));
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(10) input').val(0);
                  $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(10) span').html(to_rupiah(0));

                }
                HitungTotalBayar()
              } else {
                $('#KodeCustomer').val('');
                $('#datepicker3').val('');
                $('#KodeRegional').val('');
                $('#KodeSite').val('');
                $('#NamaSite').val('');
                $("#TabelTransaksi tbody").empty();
                $('#TotalBayar').html(to_rupiah(''));
                $('#TotalBayarHidden').val('');
              }
            }
          });
          HitungTotalBayar()

        })

      });
    </script>

    <!-- edit -->

    <script>
      function NoPoInternal() {
        var StrNoUrutPo = $('#NoUrutPo').val()
        var StrKodeCustomer = $('#KodeCustomer').find(":selected").val();
        var Strdatepicker3 = $('#datepicker3').val()
        var StrKodeRegional = $('#KodeRegional').find(":selected").val();

        var StrNoPoIn = "PO/" + StrKodeCustomer + "/INTISEL/" + StrKodeRegional + "/" + Strdatepicker3.substring(6, 10) + "/" + StrNoUrutPo;
        document.getElementById("NoPoIn").value = StrNoPoIn;
      }

      function goBack() {
        window.history.back();
      }

      $(document).ready(function() {

        if ($('#_pr').val() != '') {
          $('#pr').prop('checked', true);
          pr_selected()
        } else {
          $('#nopr').prop('checked', true);
          po_selected()
        }

        $('#pr').click(function() {
          if ($('#pr').is(':checked')) {
            pr_selected()
            $("#NoPr").val($('#_pr').val());
          }
        });

        $('#_nopr').click(function() {
          if ($('#_nopr').is(':checked')) {
            po_selected()
            $("#NoPr").val('');
          }
        });

        for (B = 1; B <= 1; B++) {
          get_transaksi();
        }

        $('#BarisBaru').click(function() {
          BarisBaru();
        });

        $("#TabelTransaksi tbody").find('input[type=text],textarea,select').filter(':visible:first').focus();
      });

      function BarisBaru() {
        var index = $('#TabelTransaksi tbody tr').length;
        var Nomor = $('#TabelTransaksi tbody tr').length + 1;
        var Baris = "<tr>";
        Baris += "<td style='margin:-5px; width:35px;'>" + Nomor + "</td>";

        Baris += "<td>";
        Baris += "<input type='text' class='form-control input-sm' style='margin:-5px;' name='kode_barang[]' id='pencarian_kode' placeholder='Ketik Kode / Nama SOW'>";
        Baris += "<div id='hasil_pencarian'></div>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='nama_barang[]' id='nama_barang'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='harga_satuan[]' id='harga_satuan'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td><input type='text' style='margin:-5px;' class='form-control input-sm' id='jumlah_beli' name='jumlah_beli[]' onkeypress='return check_int(event)' disabled></td>";
        Baris += "<td><input type='checkbox' style='margin:-5px; margin-left:5px;' id='jumlah_beli1' name='jumlah_beli_ppn" + index + "[]' onkeypress='return check_int(event)' disabled></input></td>";
        Baris += "<td><input type='checkbox' style='margin:-5px; margin-left:5px;' id='jumlah_beli2' name='jumlah_beli_pph" + index + "[]' onkeypress='return check_int(event)' disabled></input></td>";

        Baris += "<td>";
        Baris += "<input type='hidden' style='margin:-5px;' name='sub_total[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input type='hidden' style='margin:-5px;' name='nilai_ppn[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td>";
        Baris += "<input type='hidden' style='margin:-5px;' name='nilai_pph[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += "<td><button style='margin-left:0px;' class='btn btn-default btn-xs' id='HapusBaris'><i class='fa fa-times' style='color:red;'></i></button></td>";
        Baris += "</tr>";

        $('#TabelTransaksi tbody').append(Baris);

        $('#TabelTransaksi tbody tr').each(function() {
          $(this).find('td:nth-child(2) input').focus();
        });

        HitungTotalBayar();
      }

      $(document).on('click', '#HapusBaris', function(e) {
        e.preventDefault();
        $(this).parent().parent().remove();

        var Nomor = 1;
        $('#TabelTransaksi tbody tr').each(function() {
          $(this).find('td:nth-child(1)').html(Nomor);
          Nomor++;
        });

        HitungTotalBayar();
      });


      function AutoCompleteGue(Lebar, KataKunci, Indexnya) {
        $('div#hasil_pencarian').hide();
        var Lebar = Lebar + 25;

        var Registered = '';
        $('#TabelTransaksi tbody tr').each(function() {
          if (Indexnya !== $(this).index()) {
            if ($(this).find('td:nth-child(2) input').val() !== '') {
              Registered += $(this).find('td:nth-child(2) input').val() + ',';
            }
          }
        });

        if (Registered !== '') {
          Registered = Registered.replace(/,\s*$/, "");
        }

        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/ajax_kode'); ?>",
          type: "POST",
          cache: false,
          data: 'keyword=' + KataKunci + '&registered=' + Registered + '&regional=' + $('#KodeRegional').val() + '&customer=' + $('#KodeCustomer').val(),
          dataType: 'json',
          success: function(json) {
            if (json.status == 1) {
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').css({
                'width': Lebar + 'px'
              });
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').show('fast');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').html(json.datanya);
            }
            if (json.status == 0) {
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) input').val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').prop('disabled', true).val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) input').prop('disabled', true).val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(7) input').prop('disabled', true).val('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val(0);
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(0);
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html('');
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(0);
              $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html('');
            }
          }
        });

        HitungTotalBayar();
      }

      $(document).on('keyup', '#pencarian_kode', function(e) {
        if ($(this).val() !== '') {
          var charCode = e.which || e.keyCode;
          if (charCode == 40) {
            if ($('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
              var Selanjutnya = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').next();
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

              Selanjutnya.addClass('autocomplete_active');
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
            }
          } else if (charCode == 38) {
            if ($('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').length > 0) {
              var Sebelumnya = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').prev();
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

              Sebelumnya.addClass('autocomplete_active');
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian li:first').addClass('autocomplete_active');
            }
          } else if (charCode == 13) {
            var Field = $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)');
            var Kodenya = Field.find('div#hasil_pencarian li.autocomplete_active span#kodenya').html();
            var Barangnya = Field.find('div#hasil_pencarian li.autocomplete_active span#barangnya').html();
            var Harganya = Field.find('div#hasil_pencarian li.autocomplete_active span#harganya').html();

            Field.find('div#hasil_pencarian').hide();
            Field.find('input').val(Kodenya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(3) input').val(Barangnya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(3) span').html(Barangnya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(4) input').val(Harganya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(4) span').html(to_rupiah(Harganya));
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(5) input').removeAttr('disabled').val(1);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(6) input').removeAttr('disabled').val(1);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(7) input').removeAttr('disabled').val(1);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(8) input').val(Harganya);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(8) span').html(to_rupiah(Harganya));
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(9) input').val(0);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(9) span').html(to_rupiah(0));
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(10) input').val(0);
            $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(10) span').html(to_rupiah(0));


            var IndexIni = $(this).parent().parent().index() + 1;
            var TotalIndex = $('#TabelTransaksi tbody tr').length;
            if (IndexIni == TotalIndex) {
              BarisBaru();

              $('html, body').animate({
                scrollTop: $(document).height()
              }, 0);
            } else {
              $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(5) input').focus();
            }
          } else {
            AutoCompleteGue($(this).width(), $(this).val(), $(this).parent().parent().index());
          }
        } else {
          $('#TabelTransaksi tbody tr:eq(' + $(this).parent().parent().index() + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
        }

        HitungTotalBayar();
      });

      $(document).on('click', '#daftar-autocomplete li', function() {
        $(this).parent().parent().parent().find('input').val($(this).find('span#kodenya').html());

        var Indexnya = $(this).parent().parent().parent().parent().index();
        var NamaBarang = $(this).find('span#barangnya').html();
        var Harganya = $(this).find('span#harganya').html();

        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) input').val(NamaBarang);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(3) span').html(NamaBarang);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val(Harganya);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) span').html(to_rupiah(Harganya));
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').removeAttr('disabled').val(1);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(6) input').removeAttr('disabled').val(1);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(7) input').removeAttr('disabled').val(1);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val(Harganya);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) span').html(to_rupiah(Harganya));
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(0);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html(to_rupiah(0));
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(0);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html(to_rupiah(0));

        var IndexIni = Indexnya + 1;
        var TotalIndex = $('#TabelTransaksi tbody tr').length;
        if (IndexIni == TotalIndex) {
          BarisBaru();
          $('html, body').animate({
            scrollTop: $(document).height()
          }, 0);
        } else {
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(5) input').focus();
        }

        HitungTotalBayar();
      });


      $(document).on('keyup', '#jumlah_beli', function() {
        var Indexnya = $(this).parent().parent().index();
        var Harga = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val();
        var JumlahBeli = $(this).val();
        var KodeBarang = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(2) input').val();


        var SubTotal = parseInt(Harga) * parseInt(JumlahBeli);

        if (SubTotal > 0) {
          var SubTotalVal = SubTotal;
          SubTotal = to_rupiah(SubTotal);
        } else {
          SubTotal = '';
          var SubTotalVal = 0;
        }

        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val(SubTotalVal);
        $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) span').html(SubTotal);
        HitungTotalBayar();

      });


      $(document).on('click', '#jumlah_beli1', function() {
        if ($(this).prop('checked') == true) {
          var Indexnya = $(this).parent().parent().index();
          var Harga = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val();
          var JumlahBeli = $(this).val();
          var KodeBarang = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val();

          var SubTotal = (parseInt(KodeBarang) / 100) * 10;

          if (SubTotal > 0) {
            var SubTotalVal = SubTotal;
            SubTotal = to_rupiah(SubTotal);
          } else {
            SubTotal = '';
            var SubTotalVal = 0;
          }

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(SubTotalVal);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html(SubTotal);
          HitungTotalBayar();
        } else if ($(this).prop('checked') == false) {
          var Indexnya = $(this).parent().parent().index();

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val(0);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) span').html(to_rupiah(0));
          HitungTotalBayar();
        }
      });

      $(document).on('click', '#jumlah_beli2', function() {
        if ($(this).prop('checked') == true) {
          var Indexnya = $(this).parent().parent().index();
          var Harga = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(4) input').val();
          var JumlahBeli = $(this).val();
          var KodeBarang = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(8) input').val();
          var KodeBarang1 = $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(9) input').val();

          var SubTotal = ((parseInt(KodeBarang) + parseInt(KodeBarang1)) / 100) * 2;

          if (SubTotal > 0) {
            var SubTotalVal = SubTotal;
            SubTotal = to_rupiah(SubTotal);
          } else {
            SubTotal = '';
            var SubTotalVal = 0;
          }

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(SubTotalVal);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html(SubTotal);
          HitungTotalBayar();
        } else if ($(this).prop('checked') == false) {
          var Indexnya = $(this).parent().parent().index();

          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) input').val(0);
          $('#TabelTransaksi tbody tr:eq(' + Indexnya + ') td:nth-child(10) span').html(to_rupiah(0));
          HitungTotalBayar();
        }
      });


      $(document).on('keydown', '#jumlah_beli', function(e) {
        var charCode = e.which || e.keyCode;
        if (charCode == 9) {
          var Indexnya = $(this).parent().parent().index() + 1;
          var TotalIndex = $('#TabelTransaksi tbody tr').length;
          if (Indexnya == TotalIndex) {
            BarisBaru();
            return false;
          }
        }

        HitungTotalBayar();
      });

      $(document).on('keyup', '#UangCash', function() {
        HitungTotalKembalian();
      });

      function HitungTotalBayar() {
        var Total = 0;
        $('#TabelTransaksi tbody tr').each(function() {
          if ($(this).find('td:nth-child(8) input').val() > 0) {
            var SubTotal = $(this).find('td:nth-child(8) input').val();
            var PPN = $(this).find('td:nth-child(9) input').val();
            var PPH = $(this).find('td:nth-child(10) input').val();

            Total = parseInt(Total) + parseInt(SubTotal) + parseInt(PPN) - parseInt(PPH);
          }
        });

        $('#TotalBayar').html(to_rupiah(Total));
        $('#TotalBayarHidden').val(Total);
      }

      function to_rupiah(angka) {
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
          rev2 += rev[i];
          if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
            rev2 += '.';
          }
        }
        return 'Rp. ' + rev2.split('').reverse().join('');
      }

      function check_int(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        return (charCode >= 48 && charCode <= 57 || charCode == 8);
      }

      $(document).on('keydown', 'body', function(e) {
        var charCode = (e.which) ? e.which : event.keyCode;

      });

      $(document).on('click', '#update', function() {
        $('#myModal').modal('show');
      });

      $('#SimpanTransaksi').on('submit', function(event) {
        event.preventDefault();
        var NoDokumen = $("#NoDokumen").val();

        var FormData = "NoDokumen=" + $('#NoDokumen').val();
        FormData += "&NoPoIn=" + encodeURI($('#NoPoIn').val());
        FormData += "&NoPo=" + encodeURI($('#NoPo').val());
        FormData += "&NoPr=" + encodeURI($('#NoPr').val());
        FormData += "&datepicker3=" + encodeURI($('#datepicker3').val());
        FormData += "&datepicker2=" + encodeURI($('#datepicker2').val());
        FormData += "&KodeCustomer=" + encodeURI($('#KodeCustomer').val());
        FormData += "&KodeRegional=" + encodeURI($('#KodeRegional').val());
        FormData += "&KodeSite=" + encodeURI($('#KodeSite').val());
        FormData += "&NamaSite=" + encodeURI($('#NamaSite').val());
        FormData += "&" + $('#TabelTransaksi tbody input').serialize();
        FormData += "&grand_total=" + $('#TotalBayarHidden').val();
        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/submit_pocustomer_edit'); ?>",
          type: "POST",
          cache: false,
          data: FormData,
          dataType: 'json',
          success: function(data) {
            if (data.status == 1) {
              window.location.href = "<?php echo site_url('admin/pocustomer'); ?>"
              $.toast({
                heading: 'Success',
                text: "Ubah data berhasi",
                showHideTransition: 'slide',
                icon: 'success',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#7EC857'
              });
            } else {
              $.toast({
                heading: 'Error',
                text: "Ukuran Dokumen Terlalu Besar ",
                showHideTransition: 'slide',
                icon: 'error',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#00C9E6'
              });

            }
          }
        });
      });

      function get_transaksi() {
        //get master transaksi dan detail transaksi
        $.ajax({
          url: "<?php echo site_url('admin/pocustomer/ajax_pocustomer_edit'); ?>",
          type: "POST",
          data: {
            no: $('#uri').val(),
          },
          cache: false,
          dataType: 'json',
          success: function(json) {
            console.log(json.reftxtpocusd)

            for (var i = 0; i < json.reftxtpocusd.length; i++) {
              BarisBaru();
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(2)').find('div#hasil_pencarian').hide();
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(2) input').val(json.reftxtpocusd[i]['KodeSOW']);
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(3) input').val(json.reftxtpocusd[i]['NamaSOW']);
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(3) span').html(json.reftxtpocusd[i]['NamaSOW']);
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(4) input').val(json.reftxtpocusd[i]['HrgRegional']);
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(4) span').html(to_rupiah(json.reftxtpocusd[i]['HrgRegional']));
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(5) input').removeAttr('disabled').val(json.reftxtpocusd[i]['Qty']);
              if (json.reftxtpocusd[i]['ppn'] == 1) {
                var ppn_total = (parseInt(json.reftxtpocusd[i]['HrgRegional'] * json.reftxtpocusd[i]['Qty']) / 100) * 10;
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(6) input').removeAttr('disabled').val(1).prop('checked', true);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(9) input').val(ppn_total);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(9) span').html(to_rupiah(ppn_total));
              } else {
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(6) input').removeAttr('disabled').val(1);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(9) input').val(0);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(9) span').html(to_rupiah(0));
              }
              if (json.reftxtpocusd[i]['pph'] == 1) {
                var pph_total = (((parseInt(json.reftxtpocusd[i]['HrgRegional'] * json.reftxtpocusd[i]['Qty'])) + parseInt(ppn_total)) / 100) * 2;
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(7) input').removeAttr('disabled').val(1).prop('checked', true);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(10) input').val(pph_total);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(10) span').html(to_rupiah(pph_total));
              } else {
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(7) input').removeAttr('disabled').val(1);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(10) input').val(0);
                $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(10) span').html(to_rupiah(0));
              }

              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(8) input').val(json.reftxtpocusd[i]['HrgRegional'] * json.reftxtpocusd[i]['Qty']);
              $('#TabelTransaksi tbody tr:eq(' + i + ') td:nth-child(8) span').html(to_rupiah(json.reftxtpocusd[i]['HrgRegional'] * json.reftxtpocusd[i]['Qty']));

            }
            BarisBaru();

          }
        });
        HitungTotalBayar()
      }
    </script>
    <!-- end edit -->

    <script>
      $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        $("#check-all").click(function() { // Ketika user men-cek checkbox all
          if ($(this).is(":checked")) // Jika checkbox all diceklis
            $(".check-item").prop("checked", true); // ceklis semua checkbox siswa dengan class "check-item"
          else // Jika checkbox all tidak diceklis
            $(".check-item").prop("checked", false); // un-ceklis semua checkbox siswa dengan class "check-item"
        });

        $("#btn-delete").click(function() { // Ketika user mengklik tombol delete
          var confirm = window.confirm("Apakah Anda yakin ingin menghapus data-data ini?"); // Buat sebuah alert konfirmasi

          if (confirm) // Jika user mengklik tombol "Ok"
            $("#form-delete").submit(); // Submit form
        });
      });

      function enable_text(status) {
        status = !status;
        document.f1.other_text.disabled = status;
      }
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker3').datepicker({
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          changeYear: true
        });

        $('#datepicker2').datepicker({
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          changeYear: true
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>

    <script>
      $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {
          "placeholder": "dd/mm/yyyy"
        });
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {
          "placeholder": "mm/dd/yyyy"
        });
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({
          timePicker: true,
          timePickerIncrement: 30,
          format: 'MM/DD/YYYY h:mm A'
        });
        //Date range as a button
        $('#daterange-btn').daterangepicker({
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate: moment()
          },
          function(start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          }
        );

        //Date picker
        $('#datepicker').datepicker({
          autoclose: true
        });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>

    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Detail PO Customer Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "Detail PO Customer berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'warning') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Ukuran dokumen terlalu besar ",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Detail PO Customer Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>


</body>

</html>