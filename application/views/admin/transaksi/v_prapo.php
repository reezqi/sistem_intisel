<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Permohonan PO
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li class="active"> Permohonan PO</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
              <!-- /.box-header -->
              <!-- form start -->
              <form class="form-horizontal" action="<?php echo base_url('admin/prapo') ?>" action="GET">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-3 control-label"> Customer </label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeCustomer" style="width: 100%;">
                        <option value=""> No Selected </option>
                        <?php foreach ($refprincipal as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->kodePrincipal . "'>" . $i->kodePrincipal . " - " . $i->NamaPrincipal . "</option>";
                        } ?>
                      </select>
                    </div>

                    <label for="inputUserName" class="col-sm-1 control-label"> Regional </label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeRegional" style="width: 100%;">
                        <option value=""> No Selected </option>
                        <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                        } ?>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm"><span class="fa  fa-search"></span> Cari Data</button>

                  </div>

                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-3 control-label"> Subcon </label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeSubcon" style="width: 100%;">
                        <option value=""> No Selected </option>
                        <?php foreach ($refsubcon as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->KodeSuppl . "'>" . $i->KodeSuppl . " - " . $i->Nama . "</option>";
                        } ?>
                      </select>
                    </div>
                  </div>
                </div>

              </form>
            </div>

            <div class="box">
              <form method="post" action="<?php echo base_url() . 'admin/prapo/save_prapo' ?>" id="form-input" enctype="multipart/form-data">
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                      <thead>
                        <tr>
                          <th class="bg-primary" style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;">No </th>
                          <th class="bg-primary">Status</th>
                          <th class="bg-primary">No PR</th>
                          <th class="bg-primary">Tanggal PR</th>
                          <th class="bg-primary">Kode Subcon</th>
                          <th class="bg-primary">Nama Subcon</th>
                          <th class="bg-primary">Kode Customer</th>
                          <th class="bg-primary">Nama Customer</th>
                          <th class="bg-primary">Kode Regional</th>
                          <th class="bg-primary">Nama Regional</th>
                          <th class="bg-primary">Kode Site</th>
                          <th class="bg-primary">Nama Site</th>
                          <th class="bg-primary">Kode SOW</th>
                          <!--<th class="bg-primary">Nama SOW</th>-->
                          <th class="bg-primary">Harga (Rp)</th>
                          <th class="bg-primary">Qty</th>
                          <th class="bg-primary">PPN/PPH</th>
                          <th class="bg-primary">Total (Rp)</th>
                          <th class="bg-primary">No PO Customer</th>
                          <th class="bg-primary">No Dokumen</th>
                          <th class="bg-primary" style="text-align:center; width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;"><input type="checkbox" id="check-all"></th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $no = 0;
                        if (count($prapo) > 0) {
                          foreach ($prapo as $i) {
                            $no++; ?>
                            <tr>

                              <td><?php echo $no; ?></td>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $i->nopr ?></td>
                              <td><input style="width:80px; margin:-5px;" type="text" name="TglPr[]" value="<?php echo $i->tglpr ?>" class="form-control input-sm"></td>
                              <td><input style="width:60px; margin:-5px;" type="text" name="KodeSubcon[]" value="<?php echo $i->KodeSubcon ?>" class="form-control input-sm"></td>
                              <td><input style="width:200px; margin:-5px;" type="text" name="NamaSubcon[]" value="<?php echo $i->NamaSubcon ?>" class="form-control input-sm"></td>
                              <td><input style="width:60px; margin:-5px;" type="text" name="KodeCustomer[]" value="<?php echo $i->KodeCustomer ?>" class="form-control input-sm"></td>
                              <td><input style="width:150px; margin:-5px;" type="text" name="NamaCustomer[]" value="<?php echo $i->NamaCustomer ?>" class="form-control input-sm"></td>
                              <td><input style="width:60px; margin:-5px;" type="text" name="KodeRegional[]" value="<?php echo $i->KodeRegional ?>" class="form-control input-sm"></td>
                              <td><input style="width:150px; margin:-5px;" type="text" name="NamaRegional[]" value="<?php echo $i->NamaRegional ?>" class="form-control input-sm"></td>
                              <td><input style="width:150px; margin:-5px;" type="text" name="KodeSite[]" value="<?php echo $i->KodeSite ?>" class="form-control input-sm"></td>
                              <td><input style="width:200px; margin:-5px;" type="text" name="NamaSite[]" value="<?php echo $i->NamaSite ?>" class="form-control input-sm"></td>
                              <td><input style="width:250px; margin:-5px;" type="text" name="KodeSOW[]" value="<?php echo $i->kodesow ?>" class="form-control input-sm"></td>
                              <!--<td><?php echo $i->namasow ?></td>-->
                              <td><?php echo $i->hrgregional ?></td>
                              <td><?php echo $i->qty ?></td>
                              <td><input type="checkbox" class="check-item" id='ppn' name='ppn[]' onkeypress='return check_int(event)'></input> / <input type='checkbox' class="check-item" id='pph' name='pph[]' onkeypress='return check_int(event)'></input></td>
                              <td><?php echo $i->totalpo ?></td>
                              <td><input style="width:150px; margin:-5px;" type="text" name="NoPo[]" class="form-control input-sm"></td>
                              <td><input style="width:60px; margin:-5px;" type="text" name="NoDokumen[]" class="form-control input-sm"></td>
                              <td><input type="checkbox" class="check-item" name="nopr[]" value="<?php echo $i->nopr ?>"></td>
                            </tr>
                        <?php }
                        } ?>
                      </tbody>
                    </table>
                  </div>
                  <br>
                  <button class="btn btn-success btn-xs" style="margin-top: 8px;" id="btn-input"><span class="fa fa-user-plus"></span>
                    Entri PO Customer</button>
                </div>
                <!-- /.box-body -->

            </div>
          </div>
          <!-- /.row -->
      </section>
      <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>

    <script>
      $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        $("#check-all").click(function() { // Ketika user men-cek checkbox all
          if ($(this).is(":checked")) // Jika checkbox all diceklis
            $(".check-item").prop("checked", true); // ceklis semua checkbox siswa dengan class "check-item"
          else // Jika checkbox all tidak diceklis
            $(".check-item").prop("checked", false); // un-ceklis semua checkbox siswa dengan class "check-item"
        });

        $("#btn-input").click(function() { // Ketika user mengklik tombol delete
          var confirm = window.confirm("Apakah Anda yakin ingin membuat PO Customer dengan data ini?"); // Buat sebuah alert konfirmasi

          if (confirm) // Jika user mengklik tombol "Ok"
            $("#form-input").submit(); // Submit form
        });
      });

      function enable_text(status) {
        status = !status;
        document.f1.other_text.disabled = status;
      }
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Permohonan PO Berhasil disimpan ke PO Customer.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PR Regional berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'peringatan') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'error',
          text: "Data harus dipilih terlebih dahulu.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>

</body>

</html>