<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Edit Job SAM
          <small></small>
        </h1>

      </section>

      <!-- Main content -->
      <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="box box-info">
          <!-- /.box-header -->


          <div class="box-body">
            <table id='TabelTransaksi' class="table table-sm table-striped table-hover">
              <div class="row">
                <input type="hidden" id="uri" value="<?php echo $this->uri->segment('4') ?>">
              </div>
          </div>
          <!-- /.row -->

          <div class="row">
            <!-- /.row -->
            <br>
            <div class="col-xs-12">
              <div class="row">
                <thead>
                  <tr>
                    <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary" style='width:20px;'>No</th>
                    <th class="bg-primary" style='width:100px;'>Kode Regional</th>
                    <th class="bg-primary" style='width:50px'>Nama Regional</th>
                    <th class="bg-primary" style='width:100px;'>Kode Customer</th>
                    <th class="bg-primary" style='width:50px;'>Nama Customer</th>
                    <th class="bg-primary" style='width:100px;'>Kode Site</th>
                    <th class="bg-primary" style='width:50px;'>Nama Site</th>
                    <th class="bg-primary" style='width:100px;'>Kode SAM</th>
                    <th class="bg-primary" style='width:50px;'>Nama SAM</th>
                    <th class="bg-primary" style='width:100px;'>Kode PVRM</th>
                    <th class="bg-primary" style='width:50px;'>Nama PVRM</th>
                    <th class="bg-primary" style='width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;'><button id='BarisBaru' style='margin-right:10px;' class='btn btn-default btn-xs'><i class='fa fa-plus' style='color:green;'></i></button></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($sam as $key => $field) {
                  ?>
                    <tr>
                      <td style='margin:-5px; width:35px;'><?php echo $key + 1 ?>
                        <input style="margin:-5px;" type="hidden" name="idjob[]" value="<?php echo $field->IdJob ?>">
                      </td>

                      <td>
                        <select data-leng="<?php echo $key ?>" class="regional form-control input-sm item_category js-example-basic-single" style="margin:-5px;">
                          <option selected value="<?php echo $field->KodeRegional ?>"><?php echo $field->NamaRegional ?></option>
                        </select>
                        <input value="<?php echo $field->KodeRegional ?>" style="margin:-5px;" type="hidden" name="KodeRegional[]">
                      </td>
                      <td>
                        <input value="<?php echo $field->NamaRegional ?>" style='margin:-5px;' type='hidden' name='NamaRegional[]'>
                        <span><?php echo $field->NamaRegional ?></span>
                      </td>

                      <td>
                        <select data-leng="<?php echo $key ?>" class="customer form-control input-sm item_category js-example-basic-single" style="margin:-5px;">
                          <option selected value="<?php echo $field->KodeCustomer ?>"><?php echo $field->NamaCustomer ?></option>
                        </select>
                        <input value="<?php echo $field->KodeCustomer ?>" style="margin:-5px;" type="hidden" name="KodeCustomer[]">
                      </td>
                      <td>
                        <input value="<?php echo $field->NamaCustomer ?>" style='margin:-5px;' type='hidden' name='NamaCustomer[]'>
                        <span><?php echo $field->NamaCustomer ?></span>
                      </td>

                      <td>
                        <select data-leng="<?php echo $key ?>" class="site form-control input-sm item_category js-example-basic-single" style="margin:-5px;">
                          <option selected value="<?php echo $field->KodeSite ?>"><?php echo $field->NamaSite ?></option>
                        </select>
                        <input value="<?php echo $field->KodeSite ?>" style="margin:-5px;" type="hidden" name="KodeSite[]">
                      </td>
                      <td>
                        <input value="<?php echo $field->NamaSite ?>" style='margin:-5px;' type='hidden' name='NamaSite[]'>
                        <span><?php echo $field->NamaSite ?></span>
                      </td>

                      <td>
                        <select data-leng="<?php echo $key ?>" class="sam form-control input-sm item_category js-example-basic-single" style="margin:-5px;">
                          <option selected value="<?php echo $field->KodeSAM ?>"><?php echo $field->NamaSAM ?></option>
                        </select>
                        <input value="<?php echo $field->KodeSAM ?>" style="margin:-5px;" type="hidden" name="KodeSAM[]">
                      </td>
                      <td>
                        <input value="<?php echo $field->NamaSAM ?>" style='margin:-5px;' type='hidden' name='NamaSAM[]'>
                        <span><?php echo $field->NamaSAM ?></span>
                      </td>

                      <td>
                        <select data-leng="<?php echo $key ?>" class="vprm form-control input-sm item_category js-example-basic-single" style="margin:-5px;">
                          <option selected value="<?php echo $field->KodeVPRM ?>"><?php echo $field->NamaVPRM ?></option>
                        </select>
                        <input value="<?php echo $field->KodeVPRM ?>" style="margin:-5px;" type="hidden" name="KodeVPRM[]">
                      </td>
                      <td>
                        <input value="<?php echo $field->NamaVPRM ?>" style='margin:-5px;' type='hidden' name='NamaVPRM[]'>
                        <span><?php echo $field->NamaVPRM ?></span>
                      </td>


                      <td><button data-id='<?php echo $field->IdJob ?>' style='margin-left:0px;' class='btn btn-default btn-xs' id='HapusBaris'><i class='fa fa-times' style='color:red;'></i></button></td>
                    </tr>
                  <?php
                  }
                  ?>
                  <tr>

                  </tr>
                </tbody>
                </table>


              </div>

              <div class="modal-footer">
                <input type="button" id='Simpann' name="submit" class="btn btn-primary pull-right btn-sm" value="Simpan" />
                <input type="button" onclick="goBack()" name="submit" class="btn btn-default pull-right btn-sm" value="Kembali" style="margin-right:10px" />
              </div>
            </div>

      </section>
      <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>


    <!--Modal Add -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width:25%; border-radius: 50px;">
        <div class="modal-content" style=" border-radius: 5px;">
          <form class="form-horizontal" id="SimpanTransaksi" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background-color:#337AB7; border-top-left-radius: 5px; border-top-right-radius: 5px;">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel" style="color:white">Konfirmasi</h4>
            </div>
            <div class="modal-body">
              <p> Apakah anda yakin ingin menyimpan transaksi ini?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default " data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary ">Ya, saya yakin</button>
            </div>
          </form>
        </div>
      </div>
    </div>


    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>




    <script>
      function goBack() {
        window.history.back();
      }

      $(document).ready(function() {
        $('.regional').select2({
          minimumInputLength: 0,
          placeholder: "PILIH REGIONAL",
          ajax: {
            url: "<?php echo site_url('admin/regional/select_regional') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });

        $('.customer').select2({
          minimumInputLength: 0,
          placeholder: "PILIH CUSTOMER",
          ajax: {
            url: "<?php echo site_url('admin/principal/select_principal') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });

        $('.site').select2({
          minimumInputLength: 0,
          placeholder: "PILIH SITE",
          ajax: {
            url: "<?php echo site_url('admin/site/select_site') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });

        $('.sam').select2({
          minimumInputLength: 0,
          placeholder: "PILIH SAM",
          ajax: {
            url: "<?php echo site_url('admin/sam/select_sam') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });

        $('.vprm').select2({
          minimumInputLength: 0,
          placeholder: "PILIH VPRM",
          ajax: {
            url: "<?php echo site_url('admin/vprm/select_vprm') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });

        for (B = 1; B <= 1; B++) {}

        $('#BarisBaru').click(function() {
          BarisBaru();
        });

        $("#TabelTransaksi tbody").find('input[type=text],textarea,select').filter(':visible:first').focus();

      });

      function select_regional(selectElementObj) {
        selectElementObj.select2({
          minimumInputLength: 0,
          placeholder: "PILIH REGIONAL",
          ajax: {
            url: "<?php echo site_url('admin/regional/select_regional') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });
      };

      function select_customer(selectElementObj) {
        selectElementObj.select2({
          minimumInputLength: 0,
          placeholder: "PILIH CUSTOMER",
          ajax: {
            url: "<?php echo site_url('admin/principal/select_principal') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });
      };

      function select_site(selectElementObj) {
        selectElementObj.select2({
          minimumInputLength: 0,
          placeholder: "PILIH SITE",
          ajax: {
            url: "<?php echo site_url('admin/site/select_site') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });
      };

      function select_sam(selectElementObj) {
        selectElementObj.select2({
          minimumInputLength: 0,
          placeholder: "PILIH SAM",
          ajax: {
            url: "<?php echo site_url('admin/sam/select_sam') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });
      };

      function select_vprm(selectElementObj) {
        selectElementObj.select2({
          minimumInputLength: 0,
          placeholder: "PILIH SAM",
          ajax: {
            url: "<?php echo site_url('admin/vprm/select_vprm') ?>",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function(params) {
              return {
                term: params.term,
              };
            },
            processResults: function(data) {
              return {
                results: data
              }
            }
          }
        });
      };

      function BarisBaru() {
        var index = $('#TabelTransaksi tbody tr').length;
        var Nomor = $('#TabelTransaksi tbody tr').length + 1;
        var Baris = "<tr>";
        Baris += "<td style='margin:-5px; width:35px;'>" + Nomor;
        Baris += '<input style="margin:-5px;" type="hidden" name="idjob[]">';
        Baris += "</td>";

        Baris += '<td>';
        Baris += '<select id="region' + index + '" data-leng="' + index + '"   class="regional form-control input-sm item_category js-example-basic-single" style="margin:-5px;" ></select>';
        Baris += '<input style="margin:-5px;" type="hidden" name="KodeRegional[]">';
        Baris += '</td>';

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='NamaRegional[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += '<td>';
        Baris += '<select data-leng="' + index + '"  class="customer form-control input-sm item_category js-example-basic-single" style="margin:-5px;" ></select>';
        Baris += '<input style="margin:-5px;" type="hidden" name="KodeCustomer[]">';
        Baris += '</td>';

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='NamaCustomer[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += '<td>';
        Baris += '<select data-leng="' + index + '"  class="site form-control input-sm item_category js-example-basic-single" style="margin:-5px;" ></select>';
        Baris += '<input style="margin:-5px;" type="hidden" name="KodeSite[]">';
        Baris += '</td>';

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='NamaSite[]'>";
        Baris += "<span></span>";
        Baris += "</td>";


        Baris += '<td>';
        Baris += '<select data-leng="' + index + '"  class="sam form-control input-sm item_category js-example-basic-single" style="margin:-5px;" ></select>';
        Baris += '<input style="margin:-5px;" type="hidden" name="KodeSAM[]">';
        Baris += '</td>';

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='NamaSAM[]'>";
        Baris += "<span></span>";
        Baris += "</td>";

        Baris += '<td>';
        Baris += '<select data-leng="' + index + '"  class="vprm form-control input-sm item_category js-example-basic-single" style="margin:-5px;" ></select>';
        Baris += '<input style="margin:-5px;" type="hidden" name="KodeVPRM[]">';
        Baris += '</td>';

        Baris += "<td>";
        Baris += "<input style='margin:-5px;' type='hidden' name='NamaVPRM[]'>";
        Baris += "<span></span>";
        Baris += "</td>";


        Baris += "<td><button data-id='' style='margin-left:0px;' class='btn btn-default btn-xs' id='HapusBaris'><i class='fa fa-times' style='color:red;'></i></button></td>";
        Baris += "</tr>";

        $('#TabelTransaksi tbody').append(Baris);

        var regional = $("#TabelTransaksi").find(".regional").last();
        select_regional(regional);

        var customer = $("#TabelTransaksi").find(".customer").last();
        select_customer(customer);

        var site = $("#TabelTransaksi").find(".site").last();
        select_site(site);

        var sam = $("#TabelTransaksi").find(".sam").last();
        select_sam(sam);

        var vprm = $("#TabelTransaksi").find(".vprm").last();
        select_vprm(vprm);
      }

      $('#TabelTransaksi').on('select2:select', '.regional', function(e) {
        var data = e.params.data;
        var index = $(this).attr('data-leng');

        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(2) input').val(data.id);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(3) input').val(data.slug);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(3) span').html(data.slug);

      });

      $('#TabelTransaksi').on('select2:select', '.customer', function(e) {
        var data = e.params.data;
        var index = $(this).attr('data-leng');

        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(4) input').val(data.id);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(5) input').val(data.slug);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(5) span').html(data.slug);

      });


      $('#TabelTransaksi').on('select2:select', '.site', function(e) {
        var data = e.params.data;
        var index = $(this).attr('data-leng');

        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(6) input').val(data.id);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(7) input').val(data.slug);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(7) span').html(data.slug);

      });


      $('#TabelTransaksi').on('select2:select', '.sam', function(e) {
        var data = e.params.data;
        var index = $(this).attr('data-leng');

        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(8) input').val(data.id);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(9) input').val(data.slug);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(9) span').html(data.slug);

      });

      $('#TabelTransaksi').on('select2:select', '.vprm', function(e) {
        var data = e.params.data;
        var index = $(this).attr('data-leng');

        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(10) input').val(data.id);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(11) input').val(data.slug);
        $('#TabelTransaksi tbody tr:eq(' + index + ') td:nth-child(11) span').html(data.slug);

      });

      $(document).on('click', '#HapusBaris', function(e) {
        e.preventDefault();
        var idjob = $(this).attr('data-id')

        if (idjob != '') {
          $.ajax({
            url: "<?php echo site_url('admin/jobsam/ajax_delete_sam'); ?>",
            type: "POST",
            cache: false,
            data: {
              id: idjob,
            },
            dataType: 'json',
            success: function(data) {
              if (data.status == 1) {
                window.location.href = "<?php echo site_url('admin/jobsam/view_jobsam_edit/'); ?>" + idjob

                $.toast({
                  heading: 'Success',
                  text: "Berhasil dihapus ",
                  showHideTransition: 'slide',
                  icon: 'success',
                  hideAfter: false,
                  position: 'bottom-right',
                  bgColor: '#7EC857'
                });

                $(this).parent().parent().remove();
                var Nomor = 1;
                $('#TabelTransaksi tbody tr').each(function() {
                  $(this).find('td:nth-child(1)').html(Nomor);
                  Nomor++;
                });
              }
            }
          });
        } else {
          $(this).parent().parent().remove();
          var Nomor = 1;
          $('#TabelTransaksi tbody tr').each(function() {
            $(this).find('td:nth-child(1)').html(Nomor);
            Nomor++;
          });
        }

      });



      $(document).on('click', '#Simpann', function() {
        $('#myModal').modal('show');
      });

      $('#SimpanTransaksi').on('submit', function(event) {
        event.preventDefault();

        var NoDokumen = $("#NoDokumen").val();
        var FormData = $('#TabelTransaksi tbody input').serialize();


        $.ajax({
          url: "<?php echo site_url('admin/jobsam/update_jobsam'); ?>",
          type: "POST",
          cache: false,
          data: FormData,
          dataType: 'json',
          success: function(data) {
            if (data.status == 1) {
              window.location.href = "<?php echo site_url('admin/jobsam'); ?>"
              $.toast({
                heading: 'Success',
                text: "Berhasil disimpan ",
                showHideTransition: 'slide',
                icon: 'success',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#7EC857'
              });
            } else {
              $.toast({
                heading: 'Error',
                text: "Ukuran Dokumen Terlalu Besar ",
                showHideTransition: 'slide',
                icon: 'error',
                hideAfter: false,
                position: 'bottom-right',
                bgColor: '#00C9E6'
              });

            }
          }
        });
      });
    </script>

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker3').datepicker({
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          changeYear: true
        });

        $('#datepicker2').datepicker({
          dateFormat: 'dd-mm-yy',
          changeMonth: true,
          changeYear: true
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Customer Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PO Customer berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PO Customer Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>

</body>

</html>