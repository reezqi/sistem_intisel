<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/jquery-ui.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/style-gue.css' ?>">
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          PR Regional
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
          <li class="active"> PR Regional</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
              <!-- /.box-header -->
              <!-- form start -->
              <form class="form-horizontal" id="formcari" method="POST">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-2 control-label"> Pilih Tanggal Awal</label>
                    <div class="col-sm-3">
                      <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="date_satu" name="date_satu">
                    </div>

                    <label for="inputUserName" class="col-sm-2 control-label"> Pilih Tanggal Akhir </label>
                    <div class="col-sm-3">
                       <input class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" type="text" id="date_dua" name="date_dua">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-2 control-label"> Customer </label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeCustomer" id="KodeCustomer" style="width: 100%;">
                        <option value=""> No Selected </option>
                        <?php foreach ($refprincipal as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->kodePrincipal . "'>" . $i->kodePrincipal . " - " . $i->NamaPrincipal . "</option>";
                        } ?>
                      </select>
                    </div>

                    <label for="inputUserName" class="col-sm-2 control-label"> Regional </label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeRegional" id="KodeRegional" style="width: 100%;">
                      <?php if ($this->session->userdata('is_admin') === TRUE) {
                      ?>
                        <option value="">No Selected</option>
                      <?php } ?>
                        <?php foreach ($refregional as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->KODE . "'>" . $i->KODE . " - " . $i->NAMA . "</option>";
                        } ?>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm"><span class="fa  fa-search"></span> Cari Data</button>

                  </div>

                  <div class="form-group">
                    <label for="inputUserName" class="col-sm-2 control-label"> Subcon </label>
                    <div class="col-sm-3">
                      <select class="form-control input-sm" name="KodeSubcon" id="KodeSubcon" style="width: 100%;">
                        <option value=""> No Selected </option>
                        <?php foreach ($refsubcon as $i) { // Lakukan looping pada variabel siswa dari controller
                          echo "<option value='" . $i->KodeSuppl . "'>" . $i->KodeSuppl . " - " . $i->Nama . "</option>";
                        } ?>
                      </select>
                    </div>
                    <a class="btn  btn-success btn-sm" href="<?php echo base_url() . 'admin/prregional/add_prregional' ?>"><span class="fa fa-user-plus"></span>
                      Entri PR Regional</a>

                    <label for="inputUserName" class="col-sm-2 control-label"> </label>
                    <div class="col-sm-3">
                    </div>

                  </div>

              </form>
            </div>

            <div class="box">

              <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                  <table id="example1" class="table table-sm table-striped table-hover " style="font-size:12px;">
                    <thead>
                      <tr>
                        <th style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" class="bg-primary">No </th>
                        <th class="bg-primary">No PR</th>
                        <th class="bg-primary">Tanggal PR</th>
                        <th class="bg-primary">Subcon</th>
                        <th class="bg-primary">Customer</th>
                        <th class="bg-primary">Regional</th>
                        <th class="bg-primary" style="text-align:center; width:20px; border-top-right-radius: 5px; border-bottom-right-radius: 5px;">Operasi</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- /.col -->
          </div>
          <!-- /.row -->
      </section>
      <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

    <?php $this->load->view('admin/v_footer'); ?>

      <!--Modal Hapus SOW-->
      <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
              <h4 class="modal-title" id="myModalLabel">Hapus PR Regional</h4>
            </div>
            <form class="form-horizontal" action="<?php echo base_url() . 'admin/prregional/delete_prregional' ?>" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                <input type="hidden" name="NoPr" id="NoPr" value="" />
                <p>Apakah Anda yakin menghapus PR Regional <b><span id="customer"></span></b> ?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>


  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/js/jquery-ui.js' ?>"></script>
    <script>
     var table;
        $(document).ready(function() {

          //datatables
           table = $('#example1').DataTable({ 
                "processing": true, 
                "serverSide": true,

                "order": [], 
                
                "ajax": {
                    "url": "<?php echo site_url('admin/prregional/ajax_table_prregional')?>",
                    "type": "POST",
                    "dataType": "JSON",
                    "data": {
                      "KodeRegional"    : $('#KodeRegional').val(),
                      "KodeCustomer"    : $('#KodeCustomer').val(),
                      "KodeSOW"         : $('#KodeSOW').val(),
                      "KodeSubcon"      : $('#KodeSubcon').val(),
                      "date_satu" : $('#date_satu').val(),
                      "date_dua"  : $('#date_dua').val(),
                    }
                },

                
                "columnDefs": [
                    { 
                        "targets": [ 0 ], 
                        "orderable": false, 
                    },
                ],

            });

            $('#example1').on('click', '#hapus', function(e) {
                var nopr = $(this).attr('data-hapus')
                var customer = $(this).attr('data-customer')
                $('#NoPr').val(nopr)
                $('#customer').html(customer)
                $('#ModalHapus').modal('show');
            })

            $("#formcari").submit(function(e){
            //
            e.preventDefault()
            table = $('#example1').DataTable({ 
                "bDestroy": true,
                "processing": true, 
                "serverSide": true,
                "orderable": false,
                "order": [], 
                
                "ajax": {
                    "url": "<?php echo site_url('admin/prregional/ajax_table_prregional')?>",
                    "type": "POST",
                    "dataType": "JSON",
                    "data": {
                      "KodeRegional"    : $('#KodeRegional').val(),
                      "KodeCustomer"    : $('#KodeCustomer').val(),
                      "KodeSOW"         : $('#KodeSOW').val(),
                      "KodeSubcon"         : $('#KodeSubcon').val(),
                      "date_satu" : $('#date_satu').val(),
                      "date_dua"  : $('#date_dua').val(),
                    }
                },

                
                "columnDefs": [
                    { 
                        "targets": [ 0 ], 
                        "orderable": false, 
                    },
                ],
            });
            // 
        });
       

        $('#date_satu').datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true
          });

          $('#date_dua').datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true
          });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PR Regional Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "PR Regional berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "PR Regional Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          autohide: true,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>

</body>

</html>