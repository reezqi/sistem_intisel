<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrator - Intisel Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.css' ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php
        $this->load->view('admin/v_header');
        $this->load->view('admin/v_menu');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Edit Custommer
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
                    <li><a href="#">Edit Custommer</a></li>
                    <li class="active">Edit Custommer</li>
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->
                <div class="box box-default">
                    <div class="modal-dialog" role="document">
                        <!--Modal Edit Custommer-->
                        <?php
                        $b = $data1->row_array();
                        ?>
                        <form class="form-horizontal" action="<?php echo base_url() . 'admin/principal/simpan_update_principal' ?>" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Nama Customer</label>
                                <div class="col-sm-7">
                                    <input type="hidden" name="kodePrincipal" value="<?php echo $b['kodePrincipal']; ?>">
                                    <input type="text" name="NamaPrincipal" class="form-control" value="<?php echo $b['NamaPrincipal']; ?>" id="inputUserName" placeholder="Nama Unit" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Alamat </label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="3" name="Alamat" placeholder="Alamat " required><?php echo $b['Alamat']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Provinsi</label>
                                <div class="col-sm-7">
                                    <select name="provinsi" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($provinsi->result_array() as $i) {
                                            $ProvinsiId = $i['ProvinsiId'];
                                            $ProvinsiNama = $i['ProvinsiNama'];
                                            if ($b['PrincipalProvinsiId'] == $ProvinsiId)
                                                echo "<option value='$ProvinsiId' selected>$ProvinsiId - $ProvinsiNama</option>";
                                            else
                                                echo "<option value='$ProvinsiId'>$ProvinsiId - $ProvinsiNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kota</label>
                                <div class="col-sm-7">
                                    <select name="kabupaten" class="form-control" required>
                                        <option value="">No Selected</option>
                                        <?php
                                        foreach ($kabupaten->result_array() as $i) {
                                            $KabupatenId = $i['KabupatenId'];
                                            $KabupatenNama = $i['KabupatenNama'];
                                            if ($b['PrincipalKabupatenId'] == $KabupatenId)
                                                echo "<option value='$KabupatenId' selected>$KabupatenId - $KabupatenNama</option>";
                                            else
                                                echo "<option value='$KabupatenId'>$KabupatenId - $KabupatenNama</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Kode Pos</label>
                                <div class="col-sm-4">
                                    <input type="text" name="KodePos" class="form-control" value="<?php echo $b['KodePos']; ?>" id="inputUserName" placeholder="Kode Pos" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No Telepon</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoTelp" class="form-control" value="<?php echo $b['NoTelp']; ?>" id="inputUserName" placeholder="No Telepon" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">No HP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NoHp" class="form-control" value="<?php echo $b['NoHp']; ?>" id="inputUserName" placeholder="No HP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Email" class="form-control" value="<?php echo $b['Email']; ?>" id="inputUserName" placeholder="Email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">NPWP</label>
                                <div class="col-sm-7">
                                    <input type="text" name="NPWP" class="form-control" value="<?php echo $b['NPWP']; ?>" id="inputUserName" placeholder="NPWP" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Web</label>
                                <div class="col-sm-7">
                                    <input type="text" name="Website" class="form-control" value="<?php echo $b['Website']; ?>" id="inputUserName" placeholder="Web" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputUserName" class="col-sm-4 control-label">Contact Person</label>
                                <div class="col-sm-7">
                                    <input type="text" name="ContactPerson" class="form-control" value="<?php echo $b['ContactPerson']; ?>" id="inputUserName" placeholder="Contact Person" required>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" onclick="window.history.back();" class="btn btn-default btn-flat" data-dismiss="modal">Kembali</button>
                                <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                            </div>
                        </form>
                    </div>
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php
        $this->load->view('admin/v_footer');
        ?>

        <!-- FastClick -->
        <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/jquery-3.3.1.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/bootstrap.js' ?>"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                //call function get data edit
                get_data_edit();
                $('.Bank').change(function() {
                    var id = $(this).val();
                    var KabupatenId = "<?php echo $Kabupaten_Id; ?>";
                    $.ajax({
                        url: "<?php echo site_url('admin/subcon/get_sub_bank'); ?>",
                        method: "POST",
                        data: {
                            id: id
                        },
                        async: true,
                        dataType: 'json',
                        success: function(data) {

                            $('select[name="kabupaten"]').empty();

                            $.each(data, function(key, value) {
                                if (KabupatenId == value.KabupatenId) {
                                    $('select[name="kabupaten"]').append('<option value="' + value.KabupatenId + '" selected>' + value.KabupatenNama + '</option>').trigger('change');
                                } else {
                                    $('select[name="kabupaten"]').append('<option value="' + value.KabupatenId + '">' + value.KabupatenNama + '</option>');
                                }
                            });

                        }
                    });
                    return false;
                });

                //load data for edit
                function get_data_edit() {
                    var KodeRegional = $('[name="KodeRegional"]').val();
                    $.ajax({
                        url: "<?php echo site_url('admin/subcon/get_data_edit'); ?>",
                        method: "POST",
                        data: {
                            KodeRegional: KodeRegional
                        },
                        async: true,
                        dataType: 'json',
                        success: function(data) {
                            $.each(data, function(i, item) {
                                $('[name="Nama"]').val(data[i].Nama);
                                $('[name="Bank"]').val(data[i].KodeBank).trigger('change');

                            });
                        }
                    });
                }

            });
        </script>

        <?php if ($this->session->flashdata('msg') == 'error') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
            </script>

        <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil disimpan ke database.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Agenda berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
            </script>
        <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
            <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Agenda Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
            </script>
        <?php else : ?>

        <?php endif; ?>



</body>

</html>