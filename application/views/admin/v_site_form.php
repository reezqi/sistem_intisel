<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator - Intisel Invoice</title>

  <script src="<?php echo base_url('js/jquery.min.js'); ?>"></script>
  <script>
    $(document).ready(function() {
      // Sembunyikan alert validasi kosong
      $("#kosong").hide();
    });
  </script>

  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shorcut icon" href="<?php echo base_url() . 'theme/images/logo-intisel2.jpg' ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/bootstrap/css/bootstrap.min.css' ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/font-awesome/css/font-awesome.min.css' ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/AdminLTE.min.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.css' ?>">
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.css' ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/plugins/datepicker/datepicker3.css' ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() . 'assets/dist/css/skins/_all-skins.min.css' ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.css' ?>" />

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/v_menu');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Site
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Beranda</a></li>
          <li class="active"> Site</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">

              <div class="box">
                <div class="box-header">
                  <a class="btn btn-primary btn-sm" href="<?php echo base_url("excel/format_site1.xlsx"); ?>"><span class="fa fa-user-plus"></span> Download File</a>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                  <form method="post" action="<?php echo base_url("admin/site/form"); ?>" enctype="multipart/form-data">
                    <!-- 
    -- Buat sebuah input type file
    -- class pull-left berfungsi agar file input berada di sebelah kiri
    -->
                    <input type="file" name="file">

                    <!--
    -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
    -->
                    <br>
                    <input class="btn btn-warning btn-sm" type="submit" name="preview" value="Preview Dokumen">
                  </form>

                  <br>
                  <?php
                  if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
                    if (isset($upload_error)) { // Jika proses upload gagal
                      echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
                      die; // stop skrip
                    }


                    // Buat sebuah tag form untuk proses import data ke database
                    echo "<form method='post' action='" . base_url("admin/site/import") . "'>";

                    // Buat sebuah div untuk alert validasi kosong
                    echo "<div style='color: red;' id='kosong'>
    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
    </div>";

                    echo "<div class='table-responsive'>
                    <table id='example1' class='table table-sm table-striped table-hover' style='font-size:12px;'>
    <tr>
    <th colspan='24'><center><b>Preview Data</b></center></th>
    </tr>
    <tr>
    <th class='bg-primary'>Kode Ref</th>
    <th class='bg-primary'>Kode Site Utama</th>
    <th class='bg-primary'>Nama Site Utama</th>
    <th class='bg-primary'>Kode Site (Projek)</th>
    <th class='bg-primary'>Nama Site (Projek)</th>
    <th class='bg-primary'>Kode Site (Regional)</th>
    <th class='bg-primary'>Nama Site (Regional)</th>
    <th class='bg-primary'>Kode Site 3</th>
    <th class='bg-primary'>Nama Site 3</th>
    <th class='bg-primary'>No Shipment</th>
    <th class='bg-primary'>PO Line</th>
    <th class='bg-primary'>Nama Owner</th>
    <th class='bg-primary'>Kode Regional</th>
    <th class='bg-primary'>Nama Regional</th>
    <th class='bg-primary'>Kode Customer</th>
    <th class='bg-primary'>Nama Customer</th>
    <th class='bg-primary'>Kode SOW</th>
    <th class='bg-primary'>Nama SOW</th>
    <th class='bg-primary'>No Kontrak</th>
    <th class='bg-primary'>Tahun Kontrak 1</th>
    <th class='bg-primary'>Tahun Kontrak 2</th>
    <th class='bg-primary'>Tahun Kontrak 3</th>
    <th class='bg-primary'>Komentar Masa Kontrak</th>
    </tr>";

                    $numrow = 1;
                    $kosong = 0;

                    // Lakukan perulangan dari data yang ada di excel
                    // $sheet adalah variabel yang dikirim dari controller
                    foreach ($sheet as $row) {
                      // Ambil data pada excel sesuai Kolom
                      $noRef = $row['A']; // Ambil data NIS
                      $codeSite = $row['B'];
                      $siteName = $row['C'];
                      $codeSite1 = $row['D'];
                      $siteName1 = $row['E'];
                      $codeSite2 = $row['F'];
                      $siteName2 = $row['G'];
                      $codeSite3 = $row['H'];
                      $siteName3 = $row['I'];
                      $shipmentNo = $row['J'];
                      $POLine = $row['K'];
                      $ownerName = $row['L'];
                      $SiteRegionalKode = $row['M'];
                      $SiteRegionalNama = $row['N'];
                      $SiteCustomerKode = $row['O'];
                      $SiteCustomerNama = $row['P'];

                      $SiteSOWKode = $row['R'];
                      $SiteSOWNama = $row['S'];
                      $noContract = $row['T'];
                      $thnContract_1 = $row['U'];
                      $thnContract_2 = $row['V'];
                      $thnContract_3 = $row['W'];
                      $keterangan = $row['X'];


                      // Cek jika semua data tidak diisi
                      if (
                        $noRef == ""
                        && $codeSite == ""
                        && $siteName == ""
                        && $codeSite1 == ""
                        && $siteName1 == ""
                        && $codeSite2 == ""
                        && $siteName2 == ""
                        && $codeSite3 == ""
                        && $siteName3 == ""
                        && $shipmentNo == ""
                        && $POLine == ""
                        && $ownerName == ""
                        && $SiteRegionalKode == ""
                        && $SiteRegionalNama == ""
                        && $SiteCustomerKode == ""
                        && $SiteCustomerNama == ""

                        && $SiteSOWKode == ""
                        && $SiteSOWNama == ""
                        && $noContract == ""
                        && $thnContract_1 == ""
                        && $thnContract_2 == ""
                        && $thnContract_3 == ""
                        && $keterangan == ""

                      )
                        continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                      // Cek $numrow apakah lebih dari 1
                      // Artinya karena baris pertama adalah nama-nama kolom
                      // Jadi dilewat saja, tidak usah diimport
                      if ($numrow > 1) {
                        // Validasi apakah semua data telah diisi
                        $noRef_td = (!empty($noRef)) ? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                        $codeSite_td = (!empty($codeSite)) ? "" : " style='background: #E07171;'";
                        $siteName_td = (!empty($siteName)) ? "" : " style='background: #E07171;'";
                        $codeSite1_td = (!empty($codeSite1)) ? "" : " style='background: #E07171;'";
                        $siteName1_td = (!empty($siteName1)) ? "" : " style='background: #E07171;'";
                        $codeSite2_td = (!empty($codeSite2)) ? "" : " style='background: #E07171;'";
                        $siteName2_td = (!empty($siteName2)) ? "" : " style='background: #E07171;'";
                        $codeSite3_td = (!empty($codeSite3)) ? "" : " style='background: #E07171;'";
                        $siteName3_td = (!empty($siteName3)) ? "" : " style='background: #E07171;'";
                        $shipmentNo_td = (!empty($shipmentNo)) ? "" : " style='background: #E07171;'";
                        $POLine_td = (!empty($POLine)) ? "" : " style='background: #E07171;'";
                        $ownerName_td = (!empty($ownerName)) ? "" : " style='background: #E07171;'";
                        $SiteRegionalKode_td = (!empty($SiteRegionalKode)) ? "" : " style='background: #E07171;'";
                        $SiteRegionalNama_td = (!empty($SiteRegionalNama)) ? "" : " style='background: #E07171;'";
                        $SiteCustomerKode_td = (!empty($SiteCustomerKode)) ? "" : " style='background: #E07171;'";
                        $SiteCustomerNama_td = (!empty($SiteCustomerNama)) ? "" : " style='background: #E07171;'";

                        $SiteSOWKode_td = (!empty($SiteSOWKode)) ? "" : " style='background: #E07171;'";
                        $SiteSOWNama_td = (!empty($SiteSOWNama)) ? "" : " style='background: #E07171;'";
                        $noContract_td = (!empty($noContract)) ? "" : " style='background: #E07171;'";
                        $thnContract_1_td = (!empty($thnContract_1)) ? "" : " style='background: #E07171;'";
                        $thnContract_2_td = (!empty($thnContract_2)) ? "" : " style='background: #E07171;'";
                        $thnContract_3_td = (!empty($thnContract_3)) ? "" : " style='background: #E07171;'";
                        $keterangan_td = (!empty($keterangan)) ? "" : " style='background: #E07171;'";


                        // Jika salah satu data ada yang kosong
                        if (
                          $noRef == ""
                          or $codeSite == ""
                          or $siteName == ""
                          or $codeSite1 == ""
                          or $siteName1 == ""
                          or $codeSite2 == ""
                          or $siteName2 == ""
                          or $codeSite3 == ""
                          or $siteName3 == ""
                          or $shipmentNo == ""
                          or $POLine == ""
                          or $ownerName == ""
                          or $SiteRegionalKode == ""
                          or $SiteRegionalNama == ""
                          or $SiteCustomerKode == ""
                          or $SiteCustomerNama == ""

                          or $SiteSOWKode == ""
                          or $SiteSOWNama == ""
                          or $noContract == ""
                          or $thnContract_1 == ""
                          or $thnContract_2 == ""
                          or $thnContract_3 == ""
                          or $keterangan == ""
                        ) {

                          $kosong++; // Tambah 1 variabel $kosong
                        }

                        echo "<tr>";
                        echo "<td" . $noRef_td . ">" . $noRef . "</td>";
                        echo "<td" . $codeSite_td . ">" . $codeSite . "</td>";
                        echo "<td" . $siteName_td . ">" . $siteName . "</td>";
                        echo "<td" . $codeSite1_td . ">" . $codeSite1 . "</td>";
                        echo "<td" . $siteName1_td . ">" . $siteName1 . "</td>";
                        echo "<td" . $codeSite2_td . ">" . $codeSite2 . "</td>";
                        echo "<td" . $siteName2_td . ">" . $siteName2 . "</td>";
                        echo "<td" . $codeSite3_td . ">" . $codeSite3 . "</td>";
                        echo "<td" . $siteName3_td . ">" . $siteName3 . "</td>";
                        echo "<td" . $shipmentNo_td . ">" . $shipmentNo . "</td>";
                        echo "<td" . $POLine_td . ">" . $POLine . "</td>";
                        echo "<td" . $ownerName_td . ">" . $ownerName . "</td>";
                        echo "<td" . $SiteRegionalKode_td . ">" . $SiteRegionalKode . "</td>";
                        echo "<td" . $SiteRegionalNama_td . ">" . $SiteRegionalNama . "</td>";
                        echo "<td" . $SiteCustomerKode_td . ">" . $SiteCustomerKode . "</td>";
                        echo "<td" . $SiteCustomerNama_td . ">" . $SiteCustomerNama . "</td>";

                        echo "<td" . $SiteSOWKode_td . ">" . $SiteSOWKode . "</td>";
                        echo "<td" . $SiteSOWNama_td . ">" . $SiteSOWNama . "</td>";
                        echo "<td" . $noContract_td . ">" . $noContract . "</td>";
                        echo "<td" . $thnContract_1_td . ">" . $thnContract_1 . "</td>";
                        echo "<td" . $thnContract_2_td . ">" . $thnContract_2 . "</td>";
                        echo "<td" . $thnContract_3_td . ">" . $thnContract_3 . "</td>";
                        echo "<td" . $keterangan_td . ">" . $keterangan . "</td>";
                        echo "</tr>";
                      }

                      $numrow++; // Tambah 1 setiap kali looping
                    }

                    echo "</table> </div>";

                    // Cek apakah variabel kosong lebih dari 0
                    // Jika lebih dari 0, berarti ada data yang masih kosong
                    if ($kosong > 0) {
                  ?>
                      <script>
                        $(document).ready(function() {
                          // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                          $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                          $("#kosong").show(); // Munculkan alert validasi kosong
                        });
                      </script>
                  <?php
                    } else { // Jika semua data sudah diisi
                      echo "<hr>";

                      // Buat sebuah tombol untuk mengimport data ke database
                      echo "<button type='submit' class='btn btn-primary btn-sm' name='import'><span class='fa fa-user-plus'></span> Import File</button>";
                      echo "<a href='" . base_url("admin/site") . "'style='margin:10px;' class='btn btn-warning btn-sm'><span class='fa  fa-close '></span> Batal</a>";
                    }

                    echo "</form>";
                  }
                  ?>



                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    $this->load->view('admin/v_footer');
    ?>



    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url() . 'assets/plugins/jQuery/jquery-2.2.3.min.js' ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() . 'assets/bootstrap/js/bootstrap.min.js' ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() . 'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datatables/dataTables.bootstrap.min.js' ?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url() . 'assets/plugins/slimScroll/jquery.slimscroll.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/datepicker/bootstrap-datepicker.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/timepicker/bootstrap-timepicker.min.js' ?>"></script>
    <script src="<?php echo base_url() . 'assets/plugins/daterangepicker/daterangepicker.js' ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url() . 'assets/plugins/fastclick/fastclick.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() . 'assets/dist/js/app.min.js' ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url() . 'assets/dist/js/demo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'assets/plugins/toast/jquery.toast.min.js' ?>"></script>
    <!-- page script -->

    <script>
      $(function() {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });

        $('#datepicker').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('#datepicker2').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker3').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $('.datepicker4').datepicker({
          autoclose: true,
          format: 'yyyy-mm-dd'
        });
        $(".timepicker").timepicker({
          showInputs: true
        });

      });
    </script>
    <?php if ($this->session->flashdata('msg') == 'error') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Error',
          text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
          showHideTransition: 'slide',
          icon: 'error',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#FF4859'
        });
      </script>

    <?php elseif ($this->session->flashdata('msg') == 'success') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Principal Berhasil disimpan ke database.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'info') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Info',
          text: "Principal berhasil di update",
          showHideTransition: 'slide',
          icon: 'info',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#00C9E6'
        });
      </script>
    <?php elseif ($this->session->flashdata('msg') == 'success-hapus') : ?>
      <script type="text/javascript">
        $.toast({
          heading: 'Success',
          text: "Principal Berhasil dihapus.",
          showHideTransition: 'slide',
          icon: 'success',
          hideAfter: false,
          position: 'bottom-right',
          bgColor: '#7EC857'
        });
      </script>
    <?php else : ?>

    <?php endif; ?>
</body>

</html>