$(document).ready(function () {
    var count = 0;

    $("#add_btn").click(function () {
        count += 1;
        $('#container').append(
            '<tr class="records">' +
            '<td><div id="' + count + '">' + count + '</div></td>' +
            '<td><input id="nim_' + count + '" name="nim_' + count + '"  class="form-control input-sm" type="text"></td>' +
            '<td><input id="nama_depan_' + count + '" name="nama_depan_' + count + '" class="form-control input-sm" type="text"></td>' +
            '<td><input id="nama_belakang_' + count + '" name="nama_belakang_' + count + '" class="form-control input-sm" type="text"></td>' +
            '<td><a class="remove_item" href="#" style="color:red;"><center>Delete</center></a>' +
            '<input id="rows_' + count + '" name="rows[]" value="' + count + '" type="hidden"></td></tr>'
        );
    });

    $(".remove_item").on('click', function (ev) {
        if (ev.type == 'click') {
            $(this).parents(".records").fadeOut();
            $(this).parents(".records").remove();
        }
    });
});